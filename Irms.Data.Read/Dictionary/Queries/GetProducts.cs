﻿using Irms.Data.Read.Dictionary.ReadModels;
using MediatR;
using System.Collections.Generic;

namespace Irms.Data.Read.Dictionary.Queries
{
    public class GetProducts : IRequest<IEnumerable<DictionaryItem>>
    {
    }
}
