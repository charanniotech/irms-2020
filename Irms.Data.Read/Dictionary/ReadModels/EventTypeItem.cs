﻿using System;

namespace Irms.Data.Read.Dictionary.ReadModels
{
    public class EventTypeItem
    {
        public EventTypeItem(Int16 id, string value)
        {
            Id = id;
            Value = value;
        }

        public Int16 Id { get; }
        public string Value { get; }
    }
}
