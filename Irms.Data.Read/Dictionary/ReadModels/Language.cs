﻿using System;

namespace Irms.Data.Read.Dictionary.ReadModels
{
    public class Language
    {
        public Language(Guid id, string name, string culture)
        {
            Id = id;
            Name = name;
            Culture = culture;
        }

        public Guid Id { get; }
        public string Name { get; }
        public string Culture { get; }
    }
}
