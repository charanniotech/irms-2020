﻿using System;
using System.Collections.Generic;

namespace Irms.Data.Read.Dictionary.ReadModels
{
    public class TranslatableDictionaryItem
    {
        public TranslatableDictionaryItem(Guid id, string value, IEnumerable<DictionaryTranslation> translations)
        {
            Id = id;
            Value = value;
            Translations = translations;
        }

        public Guid Id { get; }
        public string Value { get; }
        public IEnumerable<DictionaryTranslation> Translations { get; }
    }
}
