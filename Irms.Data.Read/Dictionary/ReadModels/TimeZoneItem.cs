﻿namespace Irms.Data.Read.Dictionary.ReadModels
{
    public class TimeZoneItem
    {
        public TimeZoneItem(string id, string value)
        {
            Id = id;
            Value = value;
        }

        public string Id { get; }
        public string Value { get; }
    }
}
