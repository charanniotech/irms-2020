﻿using System;

namespace Irms.Data.Read.Dictionary.ReadModels
{
    public class DictionaryTranslation
    {
        internal DictionaryTranslation(Guid id, Guid languageId, string value, string languageName, string culture)
        {
            Id = id;
            Value = value;
            LanguageName = languageName;
            Culture = culture;
            LanguageId = languageId;
        }

        public Guid Id { get; }
        public Guid LanguageId { get; }
        public string Culture { get; }
        public string LanguageName { get; }
        public string Value { get; set; }
    }
}