﻿using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract;
using Irms.Data.Abstract;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Dictionary.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;

namespace Irms.Data.Read.Dictionary.QueryHandlers
{
    public class GetProductsHandler : IRequestHandler<GetProducts, IEnumerable<DictionaryItem>>
    {
        private const string Query = @"
                    SELECT Id,
                           Title AS Value
                    FROM Product
                    WHERE IsDeleted = 0";

        private readonly IConnectionString _connectionString;

        public GetProductsHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// get product list for drop down
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<DictionaryItem>> Handle(GetProducts request, CancellationToken cancellationToken)
        {
            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<DictionaryItem>(Query);
                return list;
            }
        }
    }
}
