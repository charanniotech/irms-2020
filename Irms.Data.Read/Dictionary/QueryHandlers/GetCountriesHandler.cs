﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract;
using Irms.Data.Abstract;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Dictionary.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Irms.Data.Read.Dictionary.QueryHandlers
{
    public class GetCountriesHandler : IRequestHandler<GetCountries, IEnumerable<CountryItem>>
    {
        private const string Query = @"
        SELECT
            Id,
            ISNULL(t.Translation, n.Name) AS Value,
            ShortIso
        FROM Country n
          LEFT JOIN
          (SELECT
             nt.CountryId,
              nt.Translation
           FROM CountryTranslation nt
             INNER JOIN Language l ON nt.LanguageId = l.Id 
           WHERE l.Culture = @culture
          ) AS t ON t.CountryId = n.Id
            ORDER BY Value";

        private readonly IConnectionString _connectionString;
        private readonly ICurrentLanguage _currentLanguage;

        public GetCountriesHandler(IConnectionString connectionString, ICurrentLanguage currentLanguage)
        {
            _connectionString = connectionString;
            _currentLanguage = currentLanguage;
        }

        /// <summary>
        /// get countries list for drop down
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CountryItem>> Handle(GetCountries request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                culture = _currentLanguage.GetRequestCulture()?.Name ?? "en-US"
            };

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<CountryItem>(Query, parameters);
                return list;
            }
        }
    }
}
