﻿using Irms.Data.Abstract;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Dictionary.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Dictionary.QueryHandlers
{
    public class GetEventTypesHandler : IRequestHandler<GetEventTypes, IEnumerable<EventTypeItem>>
    {
        private const string Query = @"
        SELECT [Id]
              ,[Title] as Value
        FROM [dbo].[EventType]
        ORDER BY ID";

        private readonly IConnectionString _connectionString;

        public GetEventTypesHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }
        public async Task<IEnumerable<EventTypeItem>> Handle(GetEventTypes request, CancellationToken cancellationToken)
        {
            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<EventTypeItem>(Query);
                return list;
            }
        }
    }
}
