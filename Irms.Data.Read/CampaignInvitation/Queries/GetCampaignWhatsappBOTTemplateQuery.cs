﻿using Irms.Data.Read.CampaignInvitation.ReadModels;
using MediatR;
using System;

namespace Irms.Data.Read.CampaignInvitation.Queries
{
    public class GetCampaignWhatsappBOTTemplateQuery : IRequest<CampaignWhatsappBOTTemplate>
    {
        public GetCampaignWhatsappBOTTemplateQuery(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
