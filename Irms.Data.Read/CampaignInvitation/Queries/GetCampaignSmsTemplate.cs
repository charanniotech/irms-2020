﻿using Irms.Data.Read.CampaignInvitation.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CampaignInvitation.Queries
{
    public class GetCampaignSmsTemplate : IRequest<CampaignSmsTemplate>
    {
        public GetCampaignSmsTemplate(Guid campaignInvitationId)
        {
            CampaignInvitationId = campaignInvitationId;
        }

        public Guid CampaignInvitationId { get; }
    }
}
