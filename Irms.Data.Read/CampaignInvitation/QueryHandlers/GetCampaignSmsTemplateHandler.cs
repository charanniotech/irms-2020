﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data.Read.CampaignInvitation.Queries;
using Irms.Data.Read.CampaignInvitation.ReadModels;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CampaignInvitation.QueryHandlers
{
    public class GetCampaignSmsTemplateHandler : IRequestHandler<GetCampaignSmsTemplate, ReadModels.CampaignSmsTemplate>
    {
        private readonly IrmsDataContext _context;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly IFileUploadService _fileUploadService;

        public GetCampaignSmsTemplateHandler(IrmsDataContext context,
            IConfiguration config,
            IMapper mapper,
            IFileUploadService fileUploadService)
        {
            _context = context;
            _config = config;
            _mapper = mapper;
            _fileUploadService = fileUploadService;
        }

        /// <summary>
        /// get campaign sms template
        /// </summary>
        /// <param name="request">mostly invitation id</param>
        /// <param name="cancellationToken">cancellation token</param>
        /// <returns>sms template</returns>
        public async Task<ReadModels.CampaignSmsTemplate> Handle(GetCampaignSmsTemplate request, CancellationToken token)
        {
            var smsTemplate = await _context.CampaignSmstemplate
                .FirstOrDefaultAsync(x => x.CampaignInvitationId == request.CampaignInvitationId, token);

            if (smsTemplate == null)
            {
                return null;
            }

            var template = _mapper.Map<ReadModels.CampaignSmsTemplate>(smsTemplate);
            template.CopyTemplate = await _context.CampaignWhatsappTemplate
                .AnyAsync(x => x.CampaignInvitationId == request.CampaignInvitationId
                && x.IsBot != true, token);

            template.ListName = await _context.CampaignInvitation
                .Where(x => x.Id == request.CampaignInvitationId)
                .Select(x => x.EventCampaign.GuestList.Name)
                .FirstOrDefaultAsync(token);

            if (!string.IsNullOrEmpty(template.BackgroundImagePath))
            {
                //var bytes = await _fileUploadService.LoadFile(template.BackgroundImagePath);
                //template.BackgroundImagePath = Encoding.ASCII.GetString(bytes);
                template.BackgroundImagePath = $"{_config["AzureStorage:BaseUrl"]}{template.BackgroundImagePath}";
            }

            return template;
        }
    }
}
