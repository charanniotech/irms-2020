﻿using Irms.Domain.Entities;
using System;

namespace Irms.Data.Read.CampaignInvitation.ReadModels
{
    public class CampaignInvitation
    {
        public Guid Id { get; set; }
        public bool IsInstant { get; set; }
        public int? Interval { get; set; }
        public string Title { get; set; }
        public IntervalType? IntervalType { get; set; }
        public string EmailSubject { get; set; }
        public string EmailSender { get; set; }
        public string SmsSender { get; set; }
        public string WhatsappSender { get; set; }
        public bool FormCreated { get; set; }
        public string FormBackgroundPath { get; set; }
        public string SmsImage { get; set; }
        public string EmailImage { get; set; }
        public string WhatsappImage { get; set; }
        public DateTime StartDate { get; set; }
        public ActiveMedia ActiveMedia { get; set; }
        public WebhookStatistics Statistics { get; set; }
    }

    public class ActiveMedia
    {
        public bool Email { get; set; }
        public bool Sms { get; set; }
        public bool Whatsapp { get; set; }
    }

    public class WebhookStatistics
    {
        public EmailStatistics Email { get; set; }
        public SmsStatistics Sms { get; set; }
        public WhatsappStatistics Whatsapp { get; set; }

    }

    public class EmailStatistics
    {
        public long Triggered { get; set; }
        public long Processed { get; set; }
        public long Delivered { get; set; }
        public long Opens { get; set; }
        public long UniqueOpens { get; set; }
        public long Bounces { get; set; }
        public long SpamReports { get; set; }
        public long Clicks { get; set; }
        public long UniqueClicks { get; set; }
        public long Accepted { get; set; }
        public long Rejected { get; set; }
        public long Blocked { get; set; }
    }

    public class SmsStatistics
    {
        public long Triggered { get; set; }
        public long Sent { get; set; }
        public long Delivered { get; set; }
        public long UnDelivered { get; set; }
        public long Accepted { get; set; }
        public long Rejected { get; set; }
        public long Failed { get; set; }
    }

    public class WhatsappStatistics
    {
        public long Initiated { get; set; }
        public long Delivered { get; set; }
        public long Sent { get; set; }
        public long Read { get; set; }
        public long Failed { get; set; }
        public long Accepted { get; set; }
        public long Rejected { get; set; }
    }
}