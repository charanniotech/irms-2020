﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CampaignInvitation.ReadModels
{
    public class CampaignInvitationBasicInfo
    {
        public Guid Id { get; set; }

        public InvitationType InvitationType { get; set; }
    }
}
