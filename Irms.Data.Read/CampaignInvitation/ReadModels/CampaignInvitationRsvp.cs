﻿using System;

namespace Irms.Data.Read.CampaignInvitation.ReadModels
{
    public class CampaignInvitationRsvp
    {
        public Guid Id { get; set; }
        public bool IsInstant { get; set; }
        public string EmailSubject { get; set; }
        public string EmailSender { get; set; }
        public string SmsSender { get; set; }
        public DateTime StartDate { get; set; }
        public string SmsImage { get; set; }
        public string EmailImage { get; set; }
        public int SortOrder { get; set; }
    }
}