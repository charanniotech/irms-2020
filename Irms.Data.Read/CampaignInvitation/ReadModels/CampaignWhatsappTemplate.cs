﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CampaignInvitation.ReadModels
{
    public class CampaignWhatsappTemplate
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public bool CopyTemplate { get; set; }
        public string SenderName { get; set; }
        public string Body { get; set; }
        public string WelcomeHtml { get; set; }
        public string ProceedButtonText { get; set; }
        public string Rsvphtml { get; set; }
        public string AcceptButtonText { get; set; }
        public string RejectButtonText { get; set; }
        public string AcceptHtml { get; set; }
        public string RejectHtml { get; set; }
        public string ThemeJson { get; set; }
        public string BackgroundImagePath { get; set; }

        public Guid? TemplateId { get; set; }
        public string ListName { get; set; }
    }
}
