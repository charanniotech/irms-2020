﻿using MediatR;

namespace Irms.Data.Read.Tenant.Queries
{
    public class CheckContactInfoEmailUniqueness: IRequest<bool>
    {
        public string Email { get; set; }
    }
}
