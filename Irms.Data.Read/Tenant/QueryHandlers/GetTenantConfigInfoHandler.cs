﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Data.Read.Tenant.Queries;
using Irms.Data.Read.Tenant.ReadModels;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Irms.Data.Read.Dictionary.ReadModels;
using Microsoft.Extensions.Configuration;
using Irms.Application.Abstract;

namespace Irms.Data.Read.Tenant.QueryHandlers
{
    public class GetTenantConfigInfoHandler : IRequestHandler<GetTenantConfigInfo, TenantConfigInfo>
    {
        private readonly IrmsDataContext _context;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly IDomainParser _domainParser;
        public GetTenantConfigInfoHandler(IrmsDataContext context,
            IConfiguration config,
            IMapper mapper,
            IDomainParser domainParser)
        {
            _context = context;
            _config = config;
            _mapper = mapper;
            _domainParser = domainParser;
        }

        /// <summary>
        /// get tenant basic info
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<TenantConfigInfo> Handle(GetTenantConfigInfo request, CancellationToken token)
        {
            var user = await _context.UserInfo
                .Include(_ => _.TenantNavigation)
                .FirstOrDefaultAsync(x => x.TenantId == request.Id, token);

            var config = _mapper.Map<TenantConfigInfo>(user);
            _mapper.Map(user?.TenantNavigation, config);
            config = config ?? new TenantConfigInfo();
            (string scheme, string host) = _domainParser.GetDomain();
            config.Scheme = scheme;
            config.Host = host;

            if (config.HasOwnDomain == false)
            {
                config.SubDomain = _domainParser.GetSubDomain(config.ClientUrl);
            }

            return config;
        }
    }
}
