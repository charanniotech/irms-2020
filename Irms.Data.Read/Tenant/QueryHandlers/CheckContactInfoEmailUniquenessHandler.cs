﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Data.Read.Tenant.Queries;
using Microsoft.EntityFrameworkCore;

namespace Irms.Data.Read.Tenant.QueryHandlers
{
    public class CheckContactInfoEmailUniquenessHandler : IRequestHandler<CheckContactInfoEmailUniqueness, bool>
    {
        private readonly IrmsDataContext _context;

        public CheckContactInfoEmailUniquenessHandler(IrmsDataContext context)
        {
            _context = context;
        }

        public async Task<bool> Handle(CheckContactInfoEmailUniqueness request, CancellationToken token)
        {
            bool isExist = await _context.TenantContactInfo
                .AnyAsync(x => request.Email.ToLower().Equals(x.Email), token);

            return !isExist;
        }
    }
}
