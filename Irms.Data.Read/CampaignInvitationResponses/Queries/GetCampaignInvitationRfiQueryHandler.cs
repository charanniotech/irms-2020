﻿using AutoMapper;
using Irms.Application;
using Irms.Data.EntityClasses;
using Irms.Data.Read.CampaignInvitationResponses.QueryHandlers;
using Irms.Data.Read.CampaignInvitationResponses.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static Irms.Data.Read.CampaignInvitationResponses.ReadModels.InvitationResponseRfiModel;

namespace Irms.Data.Read.CampaignInvitationResponses.Queries
{
    public class GetCampaignInvitationRfiQueryHandler : IRequestHandler<GetCampaignInvitationRfiQuery, InvitationResponseRfiModel>
    {
        private readonly IrmsDataContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;

        public GetCampaignInvitationRfiQueryHandler(
            IrmsDataContext context,
            IMapper mapper,
            IConfiguration config
        )
        {
            _context = context;
            _mapper = mapper;
            _config = config;
        }

        public async Task<InvitationResponseRfiModel> Handle(GetCampaignInvitationRfiQuery request, CancellationToken token)
        {
            var media = await _context.CampaignInvitationResponseMediaType
                .Include(x => x.CampaignInvitationResponse)
                .ThenInclude(x => x.CampaignInvitation)
                .ThenInclude(x => x.RfiForm)
                .ThenInclude(x => x.RfiFormQuestions)
                .FirstOrDefaultAsync(x => x.Id == request.Id);

            if (media == null)
            {
                return InvitationFail("Link is not found or was removed by tenant.");
            }

            if (media.CampaignInvitationResponse.CampaignInvitation.RfiForm.Count == 0)
            {
                return InvitationFail("There is no RFI form for this campaign invitation");
            }

            if (media.CampaignInvitationResponse == null
                || media.CampaignInvitationResponse.CampaignInvitation == null
                || media.CampaignInvitationResponse.CampaignInvitation.RfiForm.First() == null
                || media.CampaignInvitationResponse.CampaignInvitation.RfiForm.Count == 0
                || media.CampaignInvitationResponse.CampaignInvitation.RfiForm.First().RfiFormQuestions.Count == 0)
            {
                return InvitationFail("Link is broken");
            }

            var rfiResponses = await _context.RfiFormResponse
                .Where(x => x.RfiFormId == media.CampaignInvitationResponse.CampaignInvitation.RfiForm.First().Id
                && x.ContactId == media.CampaignInvitationResponse.ContactId)
                .ToListAsync(token);

            var responseMediaType = media.CampaignInvitationResponse.Answer;
            if (responseMediaType.HasValue && responseMediaType != 0 || rfiResponses.Count() > 0)
            {
                return InvitationFail("You have already answered to this rfi");
            }

            // Store telemetry
            var telemetry = new EntityClasses.ContactInvitationTelemetry
            {
                MediaType = (int)media.MediaType,
                CampaignInvitationId = media.CampaignInvitationResponse.CampaignInvitationId,
                ContactId = media.CampaignInvitationResponse.ContactId,
                OpenedOn = DateTime.UtcNow,
                TenantId = media.TenantId,
                Id = Guid.NewGuid()
            };
            _context.ContactInvitationTelemetry.Add(telemetry);
            await _context.SaveChangesAsync(token);

            var response = new InvitationResponseRfiModel();
            response.RfiType = (Domain.Entities.InvitationType)media.CampaignInvitationResponse.CampaignInvitation.InvitationTypeId;
            _mapper.Map(media.CampaignInvitationResponse.CampaignInvitation.RfiForm.First(), response);
            response.Questions = await MapRfiResponses(media.CampaignInvitationResponse.ContactId, media.CampaignInvitationResponse.CampaignInvitation.RfiForm.First().RfiFormQuestions.OrderBy(x => x.SortOrder).ToList(), token);
            _mapper.Map(media, response);
            response.BackgroundImageUrl = $"{_config["AzureStorage:BaseUrl"]}{media.CampaignInvitationResponse.CampaignInvitation.RfiForm.First().ThemeBackgroundImagePath}";
            response.CampaignInvitationResponseId = media.CampaignInvitationResponseId;
            response.MediaType = media.MediaType;
            return response;
        }

        private async Task<IEnumerable<InvitationResponseRfiQuestionModel>> MapRfiResponses(Guid contactId, IEnumerable<RfiFormQuestion> dbQuestions, CancellationToken token)
        {
            var contact = await _context.Contact
                .FirstOrDefaultAsync(x => x.Id == contactId, token);

            var contactCustomFields = await _context.ContactCustomField
                .Include(x => x.CustomField)
                .Where(x => x.ContactId == contactId)
                .ToListAsync(token);

            var questions = new List<InvitationResponseRfiQuestionModel>();
            foreach (var dbQuestion in dbQuestions)
            {
                var question = new InvitationResponseRfiQuestionModel();
                _mapper.Map(dbQuestion, question);

                var customField = contactCustomFields
                    .FirstOrDefault(x => x.CustomField.FieldName.Equals(dbQuestion.MappedField, StringComparison.OrdinalIgnoreCase));

                if (dbQuestion.Mapped)
                {
                    if (dbQuestion.MappedField.Equals("Title", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = contact.Title;
                    }

                    else if (dbQuestion.MappedField.Equals("FullName", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = contact.FullName;
                    }

                    else if (dbQuestion.MappedField.Equals("PrefferedName", StringComparison.OrdinalIgnoreCase) || dbQuestion.MappedField.Equals("PreferredName", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = contact.PreferredName;
                    }

                    else if (dbQuestion.MappedField.Equals("Gender", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = Convert.ToString(contact.GenderId);
                    }

                    else if (dbQuestion.MappedField.Equals("Email", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = contact.Email;
                    }

                    else if (dbQuestion.MappedField.Equals("AlternateEmail", StringComparison.OrdinalIgnoreCase) || dbQuestion.MappedField.Equals("AlternativeEmail", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = contact.AlternativeEmail;
                    }

                    else if (dbQuestion.MappedField.Equals("MobileNumber", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = contact.MobileNumber;
                    }

                    else if (dbQuestion.MappedField.Equals("WorkNumber", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = contact.WorkNumber;
                    }

                    else if (dbQuestion.MappedField.Equals("SecondaryMobileNumber", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = contact.SecondaryMobileNumber;
                    }

                    //to do
                    else if (dbQuestion.MappedField.Equals("Nationality", StringComparison.OrdinalIgnoreCase) || dbQuestion.MappedField.Equals("NationalityId", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = Convert.ToString(contact.NationalityId);
                    }

                    else if (dbQuestion.MappedField.Equals("DocumentType", StringComparison.OrdinalIgnoreCase) || dbQuestion.MappedField.Equals("DocumentTypeId", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = Convert.ToString(contact.DocumentTypeId);
                    }

                    else if (dbQuestion.MappedField.Equals("DocumentNumber", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = contact.DocumentNumber;
                    }

                    else if (dbQuestion.MappedField.Equals("Organization", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = contact.Organization;
                    }

                    else if (dbQuestion.MappedField.Equals("Position", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = contact.Position;
                    }
                    else if (dbQuestion.MappedField.Equals("IssuingCountryId", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = Convert.ToString(contact.IssuingCountryId);
                    }

                    else if (dbQuestion.MappedField.Equals("ExpirationDate", StringComparison.OrdinalIgnoreCase))
                    {
                        question.Response = Convert.ToString(contact.ExpirationDate);
                    }

                    else if (customField != null && customField.Value.IsNotNullOrEmpty())
                    {
                        question.Response = customField.Value;
                    }
                }
                questions.Add(question);
            }
            return questions.OrderByDescending(e => string.IsNullOrWhiteSpace(e.Response));
        }

        private InvitationResponseRfiModel InvitationFail(string message)
        {
            return new InvitationResponseRfiModel { Error = message };
        }
    }
}
