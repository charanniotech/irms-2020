﻿using Irms.Data.Read.CampaignInvitationResponses.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CampaignInvitationResponses.Queries
{
    public class GetCampaignInvitationResponseQuery : IRequest<InvitationResponseReadModel>
    {
        public GetCampaignInvitationResponseQuery(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }
    }
}
