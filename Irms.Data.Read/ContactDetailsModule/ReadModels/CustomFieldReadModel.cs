﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class CustomFieldReadModel
    {
        public CustomFieldReadModel(IEnumerable<CustomFieldEntryModel> fields)
        {
            Fields = fields;
        }

        public IEnumerable<CustomFieldEntryModel> Fields { get; set; }
    }
    public class CustomFieldEntryModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }
        public int CustomFieldType { get; set; }
        public long? MinValue { get; set; }
        public long? MaxValue { get; set; }
    }
}
