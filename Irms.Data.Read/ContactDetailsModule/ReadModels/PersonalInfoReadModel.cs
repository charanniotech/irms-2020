﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class PersonalInfoReadModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string FullName { get; set; }
        public string PreferredName { get; set; }
        public string Gender { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string Email { get; set; }
        public string AlternativeEmail { get; set; }
        public string MobileNumber { get; set; }
        public string SecondaryMobileNumber { get; set; }
        public string WorkNumber { get; set; }
        public Guid? NationalityId { get; set; }
        public int DocumentTypeId { get; set; }
        public Guid? IssuingCountryId { get; set; }
        
        //global contact update
        public string Organization { get; set; }
        public string Position { get; set; }
    }
}
