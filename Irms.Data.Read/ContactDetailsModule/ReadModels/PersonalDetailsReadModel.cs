﻿using System;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class PersonalDetailsReadModel
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Status { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}
