﻿using Irms.Data.Abstract;
using Irms.Data.Read.DataModule.Queries;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    [HasSqlQuery(AssociatedListQuery)]
    public class GetAssociatedListHandler : IRequestHandler<GetAssociatedListQuery, IEnumerable<AssociatedListReadModel>>
    {
        private const string AssociatedListQuery = @"
            SELECT 
	            cl.Id,
	            cl.Name AS ListName
            FROM contact c
            INNER JOIN ContactListToContacts cltc ON c.id = cltc.ContactId
            INNER JOIN ContactList cl ON cl.Id = cltc.ContactListId
            WHERE c.Id = @id";

        private readonly IConnectionString _connectionString;
        public GetAssociatedListHandler(
            IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<IEnumerable<AssociatedListReadModel>> Handle(GetAssociatedListQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                id = request.Id
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var details = await connection.QueryAndLog<AssociatedListReadModel>(AssociatedListQuery, parameters);
                return details;
            }
        }
    }
}
