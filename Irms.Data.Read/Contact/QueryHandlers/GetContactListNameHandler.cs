﻿using AutoMapper;
using Irms.Application;
using Irms.Data.Read.Contact.Queries;
using Irms.Data.Read.Contact.ReadModels;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    public class GetContactListNameHandler : IRequestHandler<GetContactListName, ContactListName>
    {
        private readonly IrmsDataContext _context;
        private readonly TenantBasicInfo _tenant;
        public GetContactListNameHandler(IrmsDataContext context,
            TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        public async Task<ContactListName> Handle(GetContactListName request, CancellationToken cancellationToken)
        {
            var list = await _context.ContactList
                .FirstOrDefaultAsync(x => x.TenantId == _tenant.Id
                         && x.Id == request.Id,
                cancellationToken);

            var live = await _context.EventCampaign
                .AnyAsync(x => x.TenantId == _tenant.Id
                         && x.GuestListId == request.Id
                         && x.CampaignType == (int)CampaignType.ListAnalysis
                         && x.Active,
                cancellationToken);

            return new ContactListName { ListName = list.Name, IsLive = live };
        }
    }
}
