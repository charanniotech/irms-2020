﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Contact.Queries;
using Irms.Data.Read.Contact.ReadModels;
using Irms.Data.Read.Registration;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    [HasSqlQuery(nameof(CountQuery), nameof(SearchClause))]
    [HasSqlQuery(nameof(PageQuery), nameof(SearchClause))]
    public class GetAllContactsHandler : IPageHandler<GetAllContacts, AllContactItem>
    {
        private const string CountQuery = @"
                SELECT COUNT(c.Id)
                FROM Contact c
                WHERE c.TenantId = @tenant
                AND c.Deleted = 0
                {0}";

        private const string PageQuery = @"
                SELECT c.Id,
                        ISNULL(c.FullName,c.PreferredName) AS FullName,
                        c.Email,
                        c.MobileNumber AS Phone,
						c.CreatedOn,
						c.ModifiedOn,
						(SELECT COUNT(DISTINCT Id) from ContactListToContacts where ContactId = c.id) as ListCount
                FROM Contact c
                WHERE c.TenantId = @tenant
                AND c.Deleted = 0
                {0}
                ORDER BY c.CreatedOn DESC
                OFFSET @skip ROWS
                FETCH NEXT @take ROWS ONLY";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;

        private const string SearchClause = "AND  CONCAT(c.FullName,c.PreferredName) like @searchText";

        public GetAllContactsHandler(IConnectionString connectionString, TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }

        /// <summary>
        /// this method will return list of events with pagination
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IPageResult<AllContactItem>> Handle(GetAllContacts request, CancellationToken cancellationToken)
        {
            var clauses = new List<string>();
            if (request.SearchText.IsNotNullOrEmpty())
            {
                clauses.Add(SearchClause);
            }

            var parameters = new
            {
                tenant = _tenant.Id,
                searchText = FullTextHelpers.MakeLike(request.SearchText),
                skip = request.Skip(),
                take = request.Take()
            };

            var conditionStr = string.Join("\r\n", clauses);
            var countQuery = string.Format(CountQuery, conditionStr);
            var pageQuery = string.Format(PageQuery, conditionStr);

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var total = await connection.ExecuteScalarAndLog<int>(countQuery, parameters);
                var contactList = await connection.QueryAndLog<AllContactItem>(pageQuery, parameters);
                return new PageResult<AllContactItem>(contactList, total);
            }
        }
    }
}
