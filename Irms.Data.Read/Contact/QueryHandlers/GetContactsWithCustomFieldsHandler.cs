﻿using AutoMapper;
using Irms.Application;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Contact.Queries;
using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    public class GetContactsWithCustomFieldsHandler : IPageHandler<GetContactsWithCustomFieldsQuery, ContactWithCustomFields>
    {
        private readonly TenantBasicInfo _tenant;
        private readonly IrmsDataContext _context;
        private readonly IMapper _mapper;
        public GetContactsWithCustomFieldsHandler(
            TenantBasicInfo tenant,
            IrmsDataContext context,
            IMapper mapper
        )
        {
            _tenant = tenant;
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Retrieves contacts with custom fields
        /// </summary>
        /// <param name="request">contact ids</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<IPageResult<ContactWithCustomFields>> Handle(GetContactsWithCustomFieldsQuery request, CancellationToken token)
        {
            var q = _context.Contact
                .Where(x => x.ContactListToContacts.Any(y => y.ContactListId == request.ListId)
                && x.TenantId == _tenant.Id);

            if (request.SearchText.IsNotNullOrEmpty())
            {
                q = q.Where(x => x.FullName.Contains(request.SearchText));
            }

            var count = await q.CountAsync(token);
            var contacts = await q
                .Skip(request.Skip())
                .Take(request.Take())
                .ToListAsync(token);

            var contactCustomFields = await _context.ContactCustomField
                .Include(x => x.CustomField)
                .Where(x => x.Contact.ContactListToContacts.Any(y => y.ContactListId == request.ListId)
                && x.TenantId == _tenant.Id)
                .ToListAsync(token);

            var result = new List<ContactWithCustomFields>();

            foreach (var contact in contacts)
            {
                var mapped = _mapper.Map<ContactWithCustomFields>(contact);

                var fields = contactCustomFields.Where(x => x.ContactId == contact.Id);
                if (fields.Count() > 0)
                {
                    var data = _mapper.Map<IEnumerable<ContactWithCustomFields.ContactCustomField>>(fields);
                    mapped.CustomFields = data;
                }

                result.Add(mapped);
            }

            return new PageResult<ContactWithCustomFields>(result, count);
        }
    }
}
