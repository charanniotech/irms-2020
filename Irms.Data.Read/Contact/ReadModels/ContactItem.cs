﻿using System;

namespace Irms.Data.Read.Contact.ReadModels
{
    public class ContactItem
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Title { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
