﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.Contact.ReadModels;
using System;

namespace Irms.Data.Read.Contact.Queries
{
    public class GetContactsWithCustomFieldsQuery : IPageQuery<ContactWithCustomFields>
    {
        public GetContactsWithCustomFieldsQuery(int pageNo, int pageSize, string searchText, Guid listId)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
            ListId = listId;
        }

        public int PageNo { get; }
        public int PageSize { get; }
        public string SearchText { get; }
        public Guid ListId { get; }
    }
}
