﻿using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using System;

namespace Irms.Data.Read.Contact.Queries
{
    public class GetContactListName : IRequest<ContactListName>
    {
        public GetContactListName(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
