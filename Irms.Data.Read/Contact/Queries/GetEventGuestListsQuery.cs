﻿using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Contact.Queries
{
    /// <summary>
    /// Fetches all event guest lists that are not in use
    /// </summary>
    public class GetEventGuestListsQuery : IRequest<IEnumerable<EventGuestReadModel>>
    {
        public GetEventGuestListsQuery(Guid eventId)
        {
            EventId = eventId;
        }

        public Guid EventId { get; set; }
    }
}
