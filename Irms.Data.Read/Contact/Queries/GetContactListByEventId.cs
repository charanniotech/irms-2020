﻿using System;
using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using System.Collections.Generic;

namespace Irms.Data.Read.Contact.Queries
{
    public class GetContactListByEventId : IRequest<IReadOnlyCollection<ContactListItem>>
    {
        public GetContactListByEventId(Guid eventId)
        {
            EventId = eventId;
        }

        public Guid EventId { get; }
    }
}
