﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.Contact.ReadModels;

namespace Irms.Data.Read.Contact.Queries
{
    public class GetAllContacts : IPageQuery<AllContactItem>
    {
        public GetAllContacts(
         int pageNo,
         int pageSize,
         string searchText)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
        }

        public int PageNo { get; }
        public int PageSize { get; }
        public string SearchText { get; }
    }
}
