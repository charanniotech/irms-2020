﻿using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Registration;
using Irms.Data.Read.Subscription.Queries;
using Irms.Data.Read.Subscription.ReadModels;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace Irms.Data.Read.Subscription.QueryHandlers
{
    [HasSqlQuery(nameof(CountQuery), nameof(SearchClause))]
    [HasSqlQuery(nameof(PageQuery), nameof(SearchClause))]
    public class GetSubscriptionListHandler : IPageTimeHandler<GetSubscriptionListQuery, SubscriptionListItem>
    {
        private const string CountQuery = @"
            SELECT COUNT(s.Id)
            FROM TenantSubscription s
            INNER JOIN Product p ON s.ProductId = p.Id
            INNER JOIN tenant t ON s.TenantId = t.Id
            WHERE t.Id <> @tenant
                AND s.CreatedOn >= @date 
                {0}";

        private const string PageQuery = @"
            SELECT s.Id,
                   p.Title AS Product,
                   t.Name AS Tenant,
                   s.ExpiryDateTime,
                   s.IsActive
            FROM TenantSubscription s
            INNER JOIN Product p ON s.ProductId = p.Id
            INNER JOIN tenant t ON s.TenantId = t.Id
            WHERE t.Id <> @tenant 
              AND s.CreatedOn >= @date 
              {0}
            ORDER BY s.CreatedOn DESC
              OFFSET @skip ROWS
              FETCH NEXT @take ROWS ONLY";

        private const string SearchClause = "AND (t.Name +' '+p.Title) like @searchText";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;

        public GetSubscriptionListHandler(IConnectionString connectionString,
            TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }

        /// <summary>
        /// get subscription list 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IPageResult<SubscriptionListItem>> Handle(GetSubscriptionListQuery request, CancellationToken cancellationToken)
        {
            var clauses = new List<string>();
            if (request.SearchText.IsNotNullOrEmpty())
            {
                clauses.Add(SearchClause);
            }

            var parameters = new
            {
                tenant = _tenant.Id,
                date = request.MinDate(),
                searchText = FullTextHelpers.MakeLike(request.SearchText),
                skip = request.Skip(),
                take = request.Take(),
            };


            var conditionStr = string.Join("\r\n", clauses);
            var pageQuery = string.Format(PageQuery, conditionStr);
            var countQuery = string.Format(CountQuery, conditionStr);

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var total = await db.ExecuteScalarAndLog<int>(countQuery, parameters);
                var page = await db.QueryAndLog<SubscriptionListItem>(pageQuery, parameters);

                return new PageResult<SubscriptionListItem>(page, total);
            }
        }
    }
}
