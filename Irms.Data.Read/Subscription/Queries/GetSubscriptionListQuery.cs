﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.Subscription.ReadModels;
using Newtonsoft.Json;

namespace Irms.Data.Read.Subscription.Queries
{
    public class GetSubscriptionListQuery : IPageTimeQuery<SubscriptionListItem>
    {
        [JsonConstructor]
        public GetSubscriptionListQuery(int pageNo, int pageSize, TimeFilterRange timeFilter, string searchText)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            TimeFilter = timeFilter;
            SearchText = searchText;
        }

        public int PageNo { get; }
        public int PageSize { get; }
        public TimeFilterRange TimeFilter { get; }

        public string SearchText { get; }
    }
}
