﻿using Dapper;
using Irms.Application.Product.Commands;
using Irms.Data.Abstract;
using Irms.Data.Read.Service.Queries;
using Irms.Data.Read.Service.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Service.QueryHandlers
{
    class GetAvailableServicesHandler : IRequestHandler<GetAvailableServicesCmd, IEnumerable<ServicesList>>
    {
        private const string query = @"SELECT 
              [sc].[Id], 
              [sc].[IsActive], 
              [sc].[Title]
              FROM [ServiceCatalog] AS [sc]
              WHERE ([sc].[IsActive] = CAST(1 AS bit)) AND [sc].[IsActive] IS NOT NULL
              SELECT [scf].[Id] as FeatureId, 
              [scf].[IsActive] as IsFeatureEnabled, 
              [scf].[IsCustom], 
              [scf].[ServiceCatalogId] as ServiceId, 
              [scf].[Title] as FeatureTitle
              FROM [ServiceCatalogFeature] AS [scf]";

        private readonly IConnectionString _connectionString;
        public GetAvailableServicesHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }
        /// <summary>
        /// this method returns lias of all avalible <see cref="ServicesList"/> which we can use in creating new <see cref="EntityClasses.Product"/> 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>list of all avalible <see cref="ServicesList"/> and list of <see cref="ServiceFeature"/> for each of them </returns>
        public async Task<IEnumerable<ServicesList>> Handle(GetAvailableServicesCmd request, CancellationToken cancellationToken)
        {
            IEnumerable<ServicesList> serviceList;

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                using (var multi = await connection.QueryMultipleAsync(query))
                {
                    serviceList = (await multi.ReadAsync<ServicesList>()).ToList();
                    if (serviceList != null)
                    {
                        var features = (await multi.ReadAsync<ServiceFeature>()).ToList();
                        foreach (var s in serviceList)
                        {
                            s.ServiceFeatures = features.Where(x => x.ServiceId == s.Id).ToList();
                        }
                    }
                }
            }

            return serviceList;
        }
    }
}
