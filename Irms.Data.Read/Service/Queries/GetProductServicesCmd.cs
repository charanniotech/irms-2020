﻿using Irms.Data.Read.Service.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Service.Queries
{
    public class GetProductServicesCmd : IRequest<IEnumerable<ServicesList>>
    {
        public GetProductServicesCmd(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
