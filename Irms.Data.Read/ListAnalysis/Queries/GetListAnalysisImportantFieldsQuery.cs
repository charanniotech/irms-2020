﻿using Irms.Data.Read.Contact.ReadModels;
using Irms.Data.Read.ListAnalysis.ReadModels;
using MediatR;
using System;

namespace Irms.Data.Read.ListAnalysis.Queries
{
    public class GetListAnalysisImportantFieldsQuery : IRequest<ListAnalysisImportantFields>
    {
        public GetListAnalysisImportantFieldsQuery(Guid listId)
        {
            ListId = listId;
        }
        public Guid ListId { get; }
    }
}
