﻿using Irms.Data.Read.ListAnalysis.ReadModels;
using MediatR;
using System;

namespace Irms.Data.Read.ListAnalysis.Queries
{
    public class GetListAnalysisStateQuery : IRequest<ListAnalysisState>
    {
        public Guid ListId { get; set; }
        public Guid EventId { get; set; }
    }
}
