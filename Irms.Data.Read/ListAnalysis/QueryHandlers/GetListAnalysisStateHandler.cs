﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;
using AutoMapper;
using System;
using Irms.Data.Read.ListAnalysis.Queries;
using Irms.Data.Read.ListAnalysis.ReadModels;
using Irms.Domain.Entities;

namespace Irms.Data.Read.ListAnalysis.QueryHandlers
{
    public class GetListAnalysisStateHandler : IRequestHandler<GetListAnalysisStateQuery, ListAnalysisState>
    {
        private readonly IrmsDataContext _context;

        public GetListAnalysisStateHandler(IrmsDataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// get important fields
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ListAnalysisState> Handle(GetListAnalysisStateQuery request, CancellationToken token)
        {
            var impFields = await _context.ContactList
                .FirstOrDefaultAsync(x => x.Id == request.ListId && x.EventId == request.EventId, token);

            return new ListAnalysisState(
                (ListAnalysisStep?)impFields.ListAnalysisStep,
                impFields.ListAnalysisPageNo,
                impFields.ListAnalysisPageSize);
        }
    }

}