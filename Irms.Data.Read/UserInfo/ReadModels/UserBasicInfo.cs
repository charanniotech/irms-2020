﻿using System;
using Irms.Domain.Entities;

namespace Irms.Data.Read.UserInfo.ReadModels
{
    public class UserBasicInfo
    {
        public UserBasicInfo(Guid id, string name, string phone, string nationalId, RoleType role, bool isActive)
        {
            Id = id;
            Name = name;
            Phone = phone;
            Role = role;
            NationalId = nationalId;
            IsActive = isActive;
        }

        public Guid Id { get; }
        public string Name { get; }
        public string Phone { get; }
        public string NationalId { get; }
        public RoleType Role { get; }
        public bool IsActive { get; }
    }
}
