﻿using System.Collections.Generic;
using MediatR;
using Irms.Data.Read.Dictionary.ReadModels;
using Irms.Domain.Entities;

namespace Irms.Data.Read.UserInfo.Queries
{
    public class GetUserInRole : IRequest<IEnumerable<DictionaryItem>>
    {
        public GetUserInRole(RoleType roleType)
        {
            RoleType = roleType;
        }

        public RoleType RoleType { get; }
    }
}
