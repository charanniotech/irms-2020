﻿using System;
using System.Collections.Generic;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.UserInfo.ReadModels;

namespace Irms.Data.Read.UserInfo.Queries
{
    public class GetUserInfoList : IPageQuery<UserBasicInfo>
    {
        public GetUserInfoList(int pageNo, int pageSize, string searchText)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
        }

        public int PageNo { get; }
        public int PageSize { get; }
        public string SearchText { get; }
    }
}
