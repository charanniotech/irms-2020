﻿using System;
using Irms.Data.Read.Templates.ReadModels;
using MediatR;

namespace Irms.Data.Read.Templates.Queries
{
    /// <summary>
    /// Request object based on which randler gets an extra information about SMS or Email template 
    /// </summary>
    public class GetTemplateSelectionDetails : IRequest<TemplateSelectionDetails>
    {
        public GetTemplateSelectionDetails(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
