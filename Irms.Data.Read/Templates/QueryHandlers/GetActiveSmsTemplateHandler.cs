﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;
using Irms.Application.Templates.Queries;
using Irms.Domain.Entities.Templates;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Irms.Data.Read.Templates.QueryHandlers
{
    /// <summary>
    /// Handler for getting active SMS template 
    /// </summary>
    public class GetActiveSmsTemplateHandler : IRequestHandler<GetActiveSmsTemplate, Template>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly ILogger<GetActiveSmsTemplateHandler> _logger;
        private readonly TenantBasicInfo _tenant;

        public GetActiveSmsTemplateHandler(IrmsTenantDataContext context, ILogger<GetActiveSmsTemplateHandler> logger, TenantBasicInfo tenant)
        {
            _context = context;
            _logger = logger;
            _tenant = tenant;
        }

        public async Task<Template> Handle(GetActiveSmsTemplate request, CancellationToken cancellationToken)
        {
            var templateSel = await _context.TemplateSelection
                .FirstOrDefaultAsync(x => x.TypeId == (int)request.TemplateType && x.TenantId == _tenant.Id, cancellationToken);

            if (templateSel == null)
            {
                _logger.LogError("The template selection for the template type {0} is absent.", request.TemplateType.ToString());
                return null;
            }

            if (!templateSel.IsEnabled)
            {
                _logger.LogInformation("The template for {0} is disabled. The SMS will NOT be sent", request.TemplateType.ToString());
                return null;
            }

            if (!templateSel.SmsTemplateId.HasValue)
            {
                _logger.LogError("The notifications for {0} are enabled, but the SMS template is not specified", request.TemplateType.ToString());
                return null;
            }

            return await _context.Template
                .Where(x => x.Id == templateSel.SmsTemplateId.Value)
                .Include(x => x.TemplateTranslation)
                .Select(x => new Template(
                    x.Id,
                    x.Name,
                    x.EmailCompatible,
                    x.SmsCompatible,
                    x.EmailBody,
                    x.EmailSubject,
                    x.SmsText,
                    (TemplateType)x.TypeId,
                    x.TemplateTranslation
                        .Select(t => new TemplateTranslation(
                            t.LanguageId,
                            t.EmailSubject,
                            t.EmailBody,
                            t.SmsText))))
                .FirstOrDefaultAsync(cancellationToken);
        }
    }
}
