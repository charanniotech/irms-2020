﻿using Irms.Application;
using Irms.Domain.Entities.Templates;
using System;

namespace Irms.Data.Read.Templates.ReadModels
{
    public class TemplateSelectionListItem
    {
        public Guid Id { get; set; }
        public bool IsEnabled { get; set; }
        public bool HasEmailTemplate { get; set; }
        public bool HasSmsTemplate { get; set; }
        public TemplateType TemplateType { get; set; }

        public string Title => TemplateType.ToString().SplitCamelCase();
    }
}
