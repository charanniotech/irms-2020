﻿using MediatR;

namespace Irms.Data.Read.Abstract
{
    public interface IPageHandler<in TQuery, TResult> : IRequestHandler<TQuery, IPageResult<TResult>>
        where TQuery : IPageQuery<TResult>
    {
    }
}
