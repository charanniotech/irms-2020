﻿using MediatR;

namespace Irms.Data.Read.Abstract
{
    public interface IPageQuery<out TItem> : IRequest<IPageResult<TItem>>
    {
        int PageNo { get; }
        int PageSize { get; }
        string SearchText { get; }
    }

    public static class PageQueryExtensions
    {
        public static int Skip<T>(this IPageQuery<T> q) => (q.PageNo - 1) * q.PageSize;
        public static int Take<T>(this IPageQuery<T> q) => q.PageSize;
    }
}
