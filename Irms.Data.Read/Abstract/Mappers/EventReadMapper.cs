﻿using Irms.Data.Read.Event.ReadModels;

namespace Irms.Data.Read.Abstract.Mappers
{
    public class EventReadMapper : AutoMapper.Profile
    {
        public EventReadMapper()
        {
            //for data read layer
            CreateMap<EntityClasses.Event, EventInfo>();
            CreateMap<EntityClasses.EventLocation, EventInfo>();
        }
    }
}
