﻿using Irms.Data.Read.CustomField.ReadModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Abstract.Mappers
{
    public class CustomFieldMapper : AutoMapper.Profile
    {
        public CustomFieldMapper()
        {
            CreateMap<EntityClasses.CustomField, CustomFieldReadModel>()
                .ForMember(x => x.Name, y => y.MapFrom(z => z.FieldName));
        }
    }
}
