﻿using System;

namespace Irms.Data.Read.Abstract
{
    public interface IPageTimeQuery<out T> : IPageQuery<T>
    {
        TimeFilterRange TimeFilter { get; }
    }

    public static class PageTimeQueryExtensions
    {
        public static DateTime MinDate<T>(this IPageTimeQuery<T> q)
        {
            switch (q.TimeFilter)
            {
                case TimeFilterRange.LastDay: return DateTime.UtcNow.AddDays(-1);
                case TimeFilterRange.LastWeek: return DateTime.UtcNow.AddDays(-7);
                case TimeFilterRange.LastMonth: return DateTime.UtcNow.AddMonths(-1);
                case TimeFilterRange.AllTime: return new DateTime(1970, 1, 1);
                default: throw new ArgumentOutOfRangeException();
            }
        }

        public static DateTime MinDate(this TimeFilterRange q)
        {
            switch (q)
            {
                case TimeFilterRange.LastDay: return DateTime.UtcNow.AddDays(-1);
                case TimeFilterRange.LastWeek: return DateTime.UtcNow.AddDays(-7);
                case TimeFilterRange.LastMonth: return DateTime.UtcNow.AddMonths(-1);
                case TimeFilterRange.AllTime: return new DateTime(1970, 1, 1);
                default: throw new ArgumentOutOfRangeException();
            }
        }
    }
}
