﻿using System.Collections.Generic;

namespace Irms.Data.Read.Abstract
{
    public class PageResult<T> : IPageResult<T>
    {
        public PageResult(IEnumerable<T> items, int totalCount, bool isReadonly = false)
        {
            Items = items;
            TotalCount = totalCount;
            IsReadonly = isReadonly;
        }

        public IEnumerable<T> Items { get; }
        public int TotalCount { get; }
        public bool IsReadonly { get; }
    }
}
