﻿using MediatR;

namespace Irms.Data.Read.Abstract
{
    public interface IPageTimeHandler<in TQuery, TResult> : IRequestHandler<TQuery, IPageResult<TResult>>
        where TQuery : IPageTimeQuery<TResult>
    {
    }
}
