﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using Irms.Infrastructure.Services.ExcelExport;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    [HasSqlQuery(QuestionQuery)]
    public class GetRfiFormQuestionHandler : IRequestHandler<GetRfiFormQuestionQuery, IEnumerable<RfiFormQuestionsModel>>
    {
        private const string QuestionQuery = @"
            SELECT 
            rfq.Id,
            JSON_VALUE(rfq.Question,'$.headline') as Question,
			rfq.SortOrder AS SortOrder
            FROM RfiFormQuestion rfq
            WHERE rfq.RfiFormId = @formId AND rfq.TenantId=@tenantId
        ";


        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;
        public GetRfiFormQuestionHandler(
            IConnectionString connectionString,
            TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }

        /// <summary>
        /// Handles questions array 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<RfiFormQuestionsModel>> Handle(GetRfiFormQuestionQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                formId = request.Id,
                tenantId = _tenant.Id
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var data = await connection.QueryAndLog<RfiFormQuestionsModel>(QuestionQuery, parameters);
                return data;
            }
        }
    }
}
