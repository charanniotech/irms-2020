﻿using Autofac.Core;
using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    public class GetRfiFormResponsesDetailPageHandler : IRequestHandler<GetRfiFormResponseDetailPageQuery, GuestResponseReadModel>
    {
		public const string GeneralInfoQuery = @"
			SELECT
			c.FullName AS GuestName,
			MAX(rfr.ResponseDate) AS SubmittedDate
			FROM RfiFormResponse rfr
			INNER JOIN Contact c ON c.Id = rfr.ContactId
			WHERE rfr.contactid=@contactId
			AND rfr.rfiformId=@formId
			GROUP BY c.FullName";

        public const string QuestionsQuery = @"
			SELECT
			rfr.Id,
			rfq.Mapped AS QuestionMappedType,
			rfq.MappedField,
			rfq.Question,
				CASE
					WHEN JSON_VALUE(rfq.Question, '$.type') = 'select' THEN
						(
						SELECT 
							existingChoices.Name
						FROM RfiFormResponse userResponse
						INNER JOIN 
							(
							SELECT 
								JSON_VALUE(AllChoices.Value, '$.label') AS Name,
								JSON_VALUE(AllChoices.Value, '$.value') AS Id
							FROM RfiFormQuestion rfqI
							OUTER APPLY OPENJSON(JSON_QUERY(rfqI.Question, '$.choices')) AllChoices
							WHERE rfqI.Id = rfq.Id
							) existingChoices
							ON userResponse.Answer IN (existingChoices.Id, existingChoices.Name) 
						WHERE userResponse.Id = rfr.Id
						AND userResponse.Answer IN (existingChoices.Id, existingChoices.Name) 
						)

					WHEN JSON_VALUE(rfq.Question, '$.type') = 'selectSearch' THEN

					'[' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
					(
					SELECT
						joinedData.Name
					FROM RfiFormResponse rfrI
					OUTER APPLY OPENJSON(rfr.Answer) UserChoice
					INNER JOIN 
						(
						SELECT 
							JSON_VALUE(AllChoices.Value, '$.label') AS Name,
							JSON_VALUE(AllChoices.Value, '$.value') AS Id
						FROM RfiFormQuestion rfqI
						OUTER APPLY OPENJSON(JSON_QUERY(rfqI.Question, '$.choices')) AllChoices
						WHERE rfqI.Id =rfq.Id
						) AS joinedData 
						ON joinedData.Id=UserChoice.value
					WHERE rfrI.Id = rfr.Id FOR JSON AUTO
					{0}
					WHEN LEN(rfr.Answer)  > 3 THEN
						rfr.Answer
					ELSE
						rfr.Answer
				END AS QuestionResponse,
				rfq.SortOrder 
			FROM RfiFormResponse rfr
			INNER JOIN Contact c ON c.Id = rfr.ContactId
			INNER JOIN RfiFormQuestion rfq ON rfq.Id= rfr.RfiFormQuestionId
			WHERE rfr.contactid=@contactId
			AND rfr.rfiformId=@formId
			AND rfr.TenantId=@tenantId
			GROUP BY c.Id, c.FullName, rfr.Id, rfq.Mapped, rfq.MappedField, rfq.Question, rfr.Answer, rfq.Id, rfq.SortOrder
			ORDER BY rfq.SortOrder";

		private readonly IConnectionString _connectionString;
		private readonly TenantBasicInfo _tenant;
        public GetRfiFormResponsesDetailPageHandler(
			IConnectionString connectionString,
			TenantBasicInfo tenant
			)
        {
			_tenant = tenant;
			_connectionString = connectionString;
        }

		/// <summary>
		/// Response handler for retrieving single user information
		/// </summary>
		/// <param name="request">contact Id and form Id</param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
        public async Task<GuestResponseReadModel> Handle(GetRfiFormResponseDetailPageQuery request, CancellationToken cancellationToken)
        {
			var parameters = new
			{
				contactId = request.ContactId,
				formId = request.FormId,
				tenantId = _tenant.Id
			};

			string questionsQuery = QuestionsQuery.Replace("{0}", "), '[{', ''), '}]',''), '}]', ''), '\"Name\"', ''), ':\"', '\"'), '\"}', ''), ',{', '\",') + ']'");

			using (IDbConnection connection = new SqlConnection(_connectionString.Value))
			{
				var result = await connection.QueryFirstOrDefaultAndLog<GuestResponseReadModel>(GeneralInfoQuery, parameters);
				result.ResponseData = await connection.QueryAndLog<GuestResponseReadModel.GuestResponseData>(questionsQuery, parameters);
				return result;
			}
		}
    }
}
