﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using Irms.Data.Read.Registration;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
	[HasSqlQuery(CountQuery, SearchClause)]
	[HasSqlQuery(PageQuery, SearchClause)]
	public class GetGlobalGuestListHandler : IPageHandler<GetGlobalGuestListQuery, GlobalGuestListReadModel>
    {
        private const string CountQuery = @"
		SELECT COUNT(*) FROM (
			SELECT
			cl.Name AS Name,
			(select count(1) from ContactListToContacts clc INNER JOIN Contact c ON clc.ContactId = c.Id where clc.ContactListId = cl.Id AND c.Deleted = 0 ) AS Count,
			e.Name AS Event,
			ec.Name AS Campaign,
			CASE
				WHEN cl.ModifiedOn IS NOT NULL THEN ui.FullName
				WHEN cl.ModifiedOn IS NULL THEN uiz.FullName
			END AS LastUpdatedBy,
			CASE
				WHEN cl.ModifiedOn IS NOT NULL THEN cl.ModifiedOn
				WHEN cl.ModifiedOn IS NULL THEN cl.CreatedOn
			END As UpdatedDate
			FROM ContactList cl
			INNER JOIN ContactListToContacts cltc ON cltc.ContactListId = cl.Id
			OUTER APPLY ( SELECT TOP 1 * FROM EventCampaign ec WHERE ec.GuestListId = cl.Id) ec
			INNER JOIN Event e on e.Id = ec.EventId
			LEFT JOIN UserInfo ui on ui.Id=cl.ModifiedById
			LEFT JOIN UserInfo uiz on uiz.Id=cl.CreatedById
			WHERE cl.IsGuest=1 
			AND cl.IsGlobal = 1
			AND cl.TenantId=@tenantId
			{0}
			GROUP BY cl.Id, cl.Name, e.Name, ec.name, cl.ModifiedOn, cl.ModifiedById, ui.Id, ui.FullName, cl.TenantId,uiz.FullName,cl.CreatedOn
		) InnerQuery";

        private const string PageQuery = @"
		SELECT
            cl.Id AS Id,
			cl.Name AS Name,
			(select count(1) from ContactListToContacts clc INNER JOIN Contact c ON clc.ContactId = c.Id where clc.ContactListId = cl.Id AND c.Deleted = 0 ) AS Count,
			e.Name AS Event,
			ec.Name AS Campaign,
			CASE
				WHEN cl.ModifiedOn IS NOT NULL THEN ui.FullName
				WHEN cl.ModifiedOn IS NULL THEN uiz.FullName
			END AS LastUpdatedBy,
			CASE
				WHEN cl.ModifiedOn IS NOT NULL THEN cl.ModifiedOn
				WHEN cl.ModifiedOn IS NULL THEN cl.CreatedOn
			END As UpdatedDate
			FROM ContactList cl
			INNER JOIN ContactListToContacts cltc ON cltc.ContactListId = cl.Id
			OUTER APPLY ( SELECT TOP 1 * FROM EventCampaign ec WHERE ec.GuestListId = cl.Id) ec
			INNER JOIN Event e on e.Id = ec.EventId
			LEFT JOIN UserInfo ui on ui.Id=cl.ModifiedById
			LEFT JOIN UserInfo uiz on uiz.Id=cl.CreatedById
			WHERE cl.IsGuest= 1 
			AND cl.IsGlobal = 1
			AND cl.TenantId=@tenantId
			{0}
			GROUP BY cl.Id, cl.Name, e.Name, ec.name, cl.ModifiedOn, cl.ModifiedById, ui.Id, ui.FullName, cl.TenantId, uiz.FullName, cl.CreatedOn
            ORDER BY cl.Name DESC
            OFFSET @skip ROWS
            FETCH NEXT @take ROWS ONLY";

		private const string SearchClause = @"
            AND cl.Name like @searchText";

		private readonly IConnectionString _connectionString;
		private readonly TenantBasicInfo _tenant;
		public GetGlobalGuestListHandler(
			IConnectionString connectionString,
			TenantBasicInfo tenant)
		{
			_tenant = tenant;
			_connectionString = connectionString;
		}

		/// <summary>
		/// Handler to a command to fetch global guest lists
		/// </summary>
		/// <param name="request">Request for data</param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<IPageResult<GlobalGuestListReadModel>> Handle(GetGlobalGuestListQuery request, CancellationToken cancellationToken)
        {
			var clauses = new List<string>();
			if (request.SearchText.IsNotNullOrEmpty())
			{
				clauses.Add(SearchClause);
			}

			var parameters = new
			{
				searchText = FullTextHelpers.MakeLike(request.SearchText),
				tenantId = _tenant.Id,
				skip = request.Skip(),
				take = request.Take()
			};

			var pageQuery = string.Format(PageQuery, string.Join("\r\n", clauses));
			var countQuery = string.Format(CountQuery, string.Join("\r\n", clauses));

			using (IDbConnection connection = new SqlConnection(_connectionString.Value))
			{
				var total = await connection.ExecuteScalarAndLog<int>(countQuery, parameters);
				var data = await connection.QueryAndLog<GlobalGuestListReadModel>(pageQuery, parameters);
				return new PageResult<GlobalGuestListReadModel>(data, total);
			}
		}
    }
}
