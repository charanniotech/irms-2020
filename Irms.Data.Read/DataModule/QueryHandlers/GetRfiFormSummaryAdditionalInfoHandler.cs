﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    public class GetRfiFormSummaryAdditionalInfoHandler : IPageHandler<GetRfiFormSummaryAdditionalInfoQuery, RfiFormSummaryAdditionalInfo>
    {
        private const string CountQuery = @"
            SELECT 
            COUNT(*)
            FROM RfiFormResponse rfr
            WHERE rfr.RfiFormId=@formId AND rfr.RfiFormQuestionId=@questionId
            AND rfr.TenantId=@tenantId";

        private const string PageQuery = @"
            SELECT
            rfr.Id,
            LEFT(RIGHT(rfr.Answer,LEN(rfr.Answer)-1),LEN(rfr.Answer)-2) AS response
            FROM RfiFormResponse rfr
            WHERE rfr.RfiFormId=@formId AND rfr.RfiFormQuestionId=@questionId
            AND rfr.TenantId=@tenantId
            ORDER BY rfr.ResponseDate DESC
            OFFSET @skip ROWS
            FETCH NEXT @take ROWS ONLY";


        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;
        public GetRfiFormSummaryAdditionalInfoHandler(IConnectionString connectionString,
            TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }

        /// <summary>
        /// Handler for additional information for rfi form (when user presses "EXPAND" button)
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IPageResult<RfiFormSummaryAdditionalInfo>> Handle(GetRfiFormSummaryAdditionalInfoQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                formId = request.FormId,
                questionId = request.QuestionId,
                skip = request.Skip(),
                take = request.Take(),
                tenantId = _tenant.Id
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var total = await connection.ExecuteScalarAndLog<int>(CountQuery, parameters);
                var data = await connection.QueryAndLog<RfiFormSummaryAdditionalInfo>(PageQuery, parameters);
                return new PageResult<RfiFormSummaryAdditionalInfo>(data, total);
            }
        }
    }
}
