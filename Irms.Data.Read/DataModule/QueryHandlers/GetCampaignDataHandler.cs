﻿using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    [HasSqlQuery(GetCampaignDataQuery)]
    public class GetCampaignDataHandler : IRequestHandler<GetCampaignDataQuery, IPageResult<CampaignDataReadModel>>
    {
        public const string GetCampaignDataQuery = @"
            SELECT 
            ec.Id,
            ec.Name,
            CASE
	            WHEN e.EndDateTime <= GETUTCDATE() THEN 3
	            WHEN ec.CampaignStatus = 1 THEN 1
	            WHEN ec.CampaignStatus = 2 THEN 2
            END AS Status,
            e.EndDateTime as LastUpdate
            FROM EventCampaign ec
            INNER JOIN Event e on e.Id = ec.EventId
			OUTER APPLY (SELECT TOP 1 ci.Id FROM CampaignInvitation ci 
			INNER JOIN RfiForm rf ON ci.Id = rf.CampaignInvitationId
			WHERE ci.EventCampaignId = ec.Id) ci
            WHERE e.Id = @eventId
            AND ec.CampaignStatus != 0
            AND ec.CampaignType = 0
            AND ci.Id IS NOT NULL";

        private readonly IConnectionString _connectionString;

        public GetCampaignDataHandler(
            IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<IPageResult<CampaignDataReadModel>> Handle(GetCampaignDataQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                eventId = request.EventId
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var details = await connection.QueryAndLog<CampaignDataReadModel>(GetCampaignDataQuery, parameters);
                return new PageResult<CampaignDataReadModel>(details, details.Count());
            }
        }
    }
}
