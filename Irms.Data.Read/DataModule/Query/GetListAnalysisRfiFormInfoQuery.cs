﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.ReadModels;
using System;

namespace Irms.Data.Read.DataModule.Query
{
    public class GetListAnalysisRfiFormInfoQuery : IPageQuery<ListAnalysisRfiFormInfoReadModel>
    {
        public GetListAnalysisRfiFormInfoQuery(Guid id,
            int pageNo,
            int pageSize)
        {
            EventId = id;
            PageNo = pageNo;
            PageSize = pageSize;
        }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public Guid EventId { get; set; }
        public string SearchText { get; set; }
    }
}
