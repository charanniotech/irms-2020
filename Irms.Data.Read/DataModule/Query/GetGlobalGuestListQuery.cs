﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.ReadModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    /// <summary>
    /// Query to fetch list of global guest lists
    /// </summary>
    public class GetGlobalGuestListQuery : IPageQuery<GlobalGuestListReadModel>
    {
        public GetGlobalGuestListQuery(
            int pageNo,
            int pageSize,
            string searchText)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
        }

        public int PageNo { get; set; }

        public int PageSize { get; set; }

        public string SearchText { get; set; }
    }
}
