﻿using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    public class GetRfiFormResponseDetailPageQuery : IRequest<GuestResponseReadModel>
    {
        public GetRfiFormResponseDetailPageQuery(Guid contactId, Guid formId)
        {
            ContactId = contactId;
            FormId = formId;
        }
        public Guid ContactId { get; set; }
        public Guid FormId { get; set; }
    }
}
