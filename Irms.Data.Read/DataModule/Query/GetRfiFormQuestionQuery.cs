﻿using Irms.Data.Read.DataModule.ReadModels;
using Irms.Infrastructure.Services.ExcelExport;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    /// <summary>
    /// Query to retrieve questions from rfi form by id
    /// </summary>
    public class GetRfiFormQuestionQuery : IRequest<IEnumerable<RfiFormQuestionsModel>>
    {
        public GetRfiFormQuestionQuery(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }
    }
}
