﻿using Irms.Infrastructure.Services.ExcelExport;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    /// <summary>
    /// Query to retrieve data from rfi form to export
    /// </summary>
    public class GetRfiFormExportDataQuery : IRequest<IEnumerable<RfiFormAnswersExportModel>>
    {
        public Guid FormId { get; set; }
        public bool HasGuestNameSelected { get; set; }
        public bool HasSubmissionDateSelected { get; set; }
        public IEnumerable<Guid> Questions { get; set; }
        public IEnumerable<Guid> Users { get; set; }
    }
}
