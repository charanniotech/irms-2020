﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.ReadModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    /// <summary>
    /// Query to gather additional information for rfi form(when user presses "Expand" button on frontend
    /// </summary>
    public class GetRfiFormSummaryAdditionalInfoQuery : IPageQuery<RfiFormSummaryAdditionalInfo>
    {
        public GetRfiFormSummaryAdditionalInfoQuery(
            Guid formId,
            Guid questionId,
            int pageNo,
            int pageSize,
            string searchText)
        {
            FormId = formId;
            QuestionId = questionId;
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
        }

        public Guid FormId { get; set; }
        public Guid QuestionId { get; set; }
        public int PageNo { get; }
        public int PageSize { get; }
        public string SearchText { get; }
    }
}
