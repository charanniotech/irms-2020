﻿using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    /// <summary>
    /// Query to receive rfi form name
    /// </summary>
    public class GetRfiFormNameQuery : IRequest<RfiFormNameReadModel>
    {
        public GetRfiFormNameQuery(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }
    }
}
