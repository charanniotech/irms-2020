﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    /// <summary>
    /// Rfi form summary read model (all items)
    /// </summary>
    public class RfiFormSummaryReadModel
    {
        public Guid Id { get; set; }
        public int ResponseCount { get; set; }
        public IEnumerable<RfiFormSummaryQuestionReadModel> Questions { get; set; }

        public class RfiFormSummaryQuestionReadModel
        {
            public Guid Id { get; set; }
            public string MappedField { get; set; }
            public int QuestionMappingType { get; set; }
            public int SortOrder { get; set; }
            public int AnsweredCount { get; set; }
            public string Question { get; set; }
            public string Responses { get; set; }
        }
    }
}
