﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    /// <summary>
    /// Read model for a global guest list item
    /// </summary>
    public class GlobalGuestListReadModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Count { get; set; }
        public string Event { get; set; }
        public string Campaign { get; set; }
        public string LastUpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
