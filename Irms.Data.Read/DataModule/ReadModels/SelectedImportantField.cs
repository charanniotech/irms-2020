﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class SelectedImportantField
    {
        public Guid Id { get; set; }
        public string FieldName { get; set; }
        public Guid? CustomFieldId { get; set; }
    }
}
