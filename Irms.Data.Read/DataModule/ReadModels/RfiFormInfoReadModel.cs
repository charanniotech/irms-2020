﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class RfiFormInfoReadModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Target { get; set; }
        public int Responses { get; set; }
        public int UniqueVisits { get; set; }
        public int TotalVisits { get; set; }
    }
}
