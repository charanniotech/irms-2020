﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class GuestResponseReadModel
    {
        public string GuestName { get; set; }
        public DateTime SubmittedDate { get; set; }
        public IEnumerable<GuestResponseData> ResponseData { get; set; }

        public class GuestResponseData
        {
            public Guid Id { get; set; }
            public string MappedField { get; set; }
            public int QuestionMappedType { get; set; }
            public int SortOrder { get; set; }
            public string Question { get; set; }
            public string QuestionResponse { get; set; }
        }
    }
}
