﻿using System;

namespace Irms.Data.Read.RfiForm.ReadModels
{
    public class RfiFormQuestion
    {
        public Guid Id { get; set; }
        public string Question { get; set; }
        public int QuestionMappingType { get; set; }
        public string MappedField { get; set; }
        public int SortOrder { get; set; }
    }

}
