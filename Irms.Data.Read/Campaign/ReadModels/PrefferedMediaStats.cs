﻿namespace Irms.Data.Read.Campaign.ReadModels
{
    public class PrefferedMediaStats
    {
        public long EmailCount { get; set; }
        public long SmsCount { get; set; }
        public long WhatsAppCount { get; set; }
    }
}
