﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Campaign.ReadModels
{
    public class CampaignSummary
    {
        public CampaignSummaryStats Stats { get; set; }
        public CampaignSummaryInvitation Invitation { get; set; }
        public IEnumerable<CampaignSummaryInvitation> Pending { get; set; }
        public IEnumerable<CampaignSummaryInvitation> Accepted { get; set; }
        public IEnumerable<CampaignSummaryInvitation> Rejected { get; set; }


        public class CampaignSummaryStats
        {
            public int GuestCount { get; set; }
            public int SendTime { get; set; }
            public int EmailCount { get; set; }
            public int SmsCount { get; set; }
            public int WhatsAppCount { get; set; }
        }

        public class CampaignSummaryInvitation
        {
            public Guid Id { get; set; }
            public int Interval { get; set; }
            public int IntervalType { get; set; }
            public DateTime StartDate { get; set; }
            public string Title { get; set; }
            public bool IsEmail { get; set; }
            public bool IsSms { get; set; }
            public bool IsWhatsapp { get; set; }
            public bool RfiForm { get; set; }
            public string RfiFormTitle { get; set; }
            public int InvitationType { get; set; }
            public bool IsInstant { get; set; }
        }
    }
}
