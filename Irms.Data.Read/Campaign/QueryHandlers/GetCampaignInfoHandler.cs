﻿using AutoMapper;
using Irms.Data.Read.Dictionary.ReadModels;
using Irms.Data.Read.Campaign.Queries;
using Irms.Data.Read.Campaign.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Campaign.QueryHandlers
{
    public class GetCampaignInfoHandler : IRequestHandler<GetCampaignInfo, CampaignInfo>
    {
        private readonly IrmsDataContext _context;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        public GetCampaignInfoHandler(IrmsDataContext context,
            IConfiguration config,
            IMapper mapper)
        {
            _context = context;
            _config = config;
            _mapper = mapper;
        }

        /// <summary>
        /// get event basic info
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CampaignInfo> Handle(GetCampaignInfo request, CancellationToken token)
        {
            var c = await _context.EventCampaign
                .FirstOrDefaultAsync(x => x.Id == request.Id, token);

            var campaign = _mapper.Map<CampaignInfo>(c);
            return campaign;
        }
    }
}
