﻿using Irms.Application;
using Irms.Application.Campaigns.Queries;
using Irms.Application.Campaigns.ReadModels;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Event.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Event.QueryHandlers
{
    [HasSqlQuery(nameof(PageQuery))]
    public class GetGuestsInfoHandler : IRequestHandler<GetGuestsByListId, (bool reachabilitySubscribed, IEnumerable<GuestInfoListItem> guests)>
    {
        private const string PageQuery = @"
                SELECT c.Id,
                        c.Email,
                        c.MobileNumber
                FROM contact c
                INNER JOIN ContactListToContacts clc ON c.Id=clc.ContactId
                WHERE clc.ContactListId = @listId";


        private readonly IConnectionString _connectionString;
        private readonly IrmsDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public GetGuestsInfoHandler(IConnectionString connectionString,
            IrmsDataContext context,
            TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _context = context;
            _tenant = tenant;
        }

        /// <summary>
        /// this method will return list of guest with email, phone info
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<(bool reachabilitySubscribed, IEnumerable<GuestInfoListItem> guests)> Handle(GetGuestsByListId request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                listId = request.ListId,
            };

            Guid reachabilityId = Guid.Parse("D4BBA94C-926C-4CE0-A432-B731080E3A3B");

            var reachabilitySubscribed = await _context.Tenant.Where(x =>
            x.Id == _tenant.Id && x.TenantSubscription
            .Any(y => y.Product.ProductServiceCatalogFeature
            .Any(z => z.ServiceCatalogFeatureId == reachabilityId && z.ServiceCatalogFeature.IsActive))).AnyAsync(cancellationToken);

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var guests = await connection.QueryAndLog<GuestInfoListItem>(PageQuery, parameters);
                return (reachabilitySubscribed, guests);
            }
        }
    }
}
