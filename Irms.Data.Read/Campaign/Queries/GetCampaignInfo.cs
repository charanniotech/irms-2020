﻿using Irms.Data.Read.Campaign.ReadModels;
using MediatR;
using System;

namespace Irms.Data.Read.Campaign.Queries
{
    public class GetCampaignInfo : IRequest<CampaignInfo>
    {

        public GetCampaignInfo(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
