﻿using Irms.Data.Read.BackgroundService.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.BackgroundService.QueryHandlers
{
    public class GetAcceptedRejectedDataForContactQuery : IRequest<IEnumerable<AcceptedRejectedContactInvitationListItem>>
    {
        public GetAcceptedRejectedDataForContactQuery(Guid campaignInvitationId, Guid contactId)
        {
            CampaignInvitationId = campaignInvitationId;
            ContactId = contactId;
        }

        public Guid CampaignInvitationId { get; set; }
        public Guid ContactId { get; set; }
    }
}
