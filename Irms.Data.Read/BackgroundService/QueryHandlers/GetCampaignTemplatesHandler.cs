﻿using Irms.Application.BackgroundService.Queries;
using Irms.Application.BackgroundService.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.BackgroundService.QueryHandlers
{
    public class GetCampaignTemplatesHandler : IRequestHandler<GetCampaignTemplates, CampaignTemplates>
    {
        private readonly IrmsDataContext _context;
        public GetCampaignTemplatesHandler(IrmsDataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// get placeholders data
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CampaignTemplates> Handle(GetCampaignTemplates request, CancellationToken token)
        {
            var temp = await _context.CampaignInvitation
                .Where(x => x.Id == request.CampaignInvitationId)
                .Select(x => new CampaignTemplates(x.TenantId,
                x.EventCampaign.Event.Name,
                x.EventCampaign.Event.StartDateTime,
                x.EventCampaign.Event.EventLocation.First().Name,
                x.CampaignEmailTemplate.FirstOrDefault().SenderEmail,
                x.CampaignEmailTemplate.FirstOrDefault().Subject,
                x.CampaignEmailTemplate.FirstOrDefault().Body,
                x.CampaignSmsTemplate.FirstOrDefault().Body)
                )
                .FirstOrDefaultAsync(token);

            return temp;
        }
    }
}

