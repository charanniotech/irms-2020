﻿using Irms.Application.BackgroundService.Queries;
using Irms.Application.BackgroundService.ReadModels;
using Irms.Data.Abstract;
using MediatR;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.BackgroundService.QueryHandlers
{
    public class GetRvspGuestsListHandler : IRequestHandler<GetRsvpGuestsList, IEnumerable<GuestsListItem>>,
        IRequestHandler<GetPendingGuestsList, IEnumerable<GuestsListItem>>,
        IRequestHandler<GetListAnalysisGuestsList, IEnumerable<GuestsListItem>>
    {
        private const string RsvpGuests = @"SELECT c.Id,
                       c.FullName,
                       c.PreferredName,
                       c.Email,
						cpm.Email AS EmailPreferred,
                        c.MobileNumber AS Phone,
                        cpm.Sms AS SmsPreferred,
                        cpm.WhatsApp AS WhatsappPreferred,
                       c.Organization,
                       c.Position,
                       @baseUrl + '/response/'+ CONVERT(nvarchar(MAX), (
							SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 1 AND MediaType = 0
					   )) AS EmailAcceptedLink,
                       @baseUrl + '/response/'+ CONVERT(nvarchar(MAX), (
							SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 2 AND MediaType = 0					   
					   )) AS EmailRejectedLink,
                       @baseUrl + '/response/'+ CONVERT(nvarchar(MAX), (
							SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 1 AND MediaType = 1
					   )) AS SmsAcceptedLink,
                       @baseUrl + '/response/'+ CONVERT(nvarchar(MAX), (
							    SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							    INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							    WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 1 AND MediaType = 2
					   )) AS WhatsappAcceptedLink,
                       @baseUrl + '/response/'+ CONVERT(nvarchar(MAX), (
							SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 2 AND MediaType = 1					   
					   )) AS SmsRejectedLink,
                       t.Name AS SenderName
                FROM Contact c
				OUTER APPLY ( SELECT  TOP 1 cpm1.Email, cpm1.Sms, cpm1.WhatsApp
								FROM CampaignPrefferedMedia cpm1
                                INNER JOIN EventCampaign ec on ec.Id=cpm1.EventCampaignId
								INNER JOIN CampaignInvitation ci on ci.EventCampaignId = ec.Id
								WHERE cpm1.ContactId = c.Id AND ci.id = @invitationId ORDER BY cpm1.CreatedOn DESC) cpm
                INNER JOIN ContactListToContacts clc ON c.Id = clc.ContactId
                INNER JOIN EventCampaign ec ON clc.ContactListId = ec.GuestListId
                INNER JOIN CampaignInvitation ci ON ec.Id = ci.EventCampaignId
                INNER JOIN Tenant t ON c.TenantId = t.Id
                OUTER APPLY ( SELECT TOP 1 * FROM CampaignInvitationMessageLog WHERE CampaignInvitationId = ci.Id AND ContactId = c.Id ORDER BY TimeStamp DESC) ciml
                WHERE ci.Id = @invitationId
                AND (cpm.Email = 1 OR cpm.Sms = 1 OR cpm.Whatsapp = 1)";

        private const string PendingGuests = @"SELECT * FROM (
                    SELECT DISTINCT c.Id,
                           c.FullName,
                           c.PreferredName,
                           c.Email,
					       cpm.Email AS EmailPreferred,
                           c.MobileNumber AS Phone,
                           cpm.Sms AS SmsPreferred,
                           cpm.WhatsApp AS WhatsappPreferred,
                           c.Organization,
                           c.Position,
                           @baseUrl + '/response/'+ CONVERT(nvarchar(MAX), (
							    SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							    INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							    WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 1 AND MediaType = 0
					       )) AS EmailAcceptedLink,
                           @baseUrl + '/response/'+ CONVERT(nvarchar(MAX), (
							    SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							    INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							    WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 2 AND MediaType = 0					   
					       )) AS EmailRejectedLink,
                           @baseUrl + '/response/'+ CONVERT(nvarchar(MAX), (
							    SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							    INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							    WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 1 AND MediaType = 1
					       )) AS SmsAcceptedLink,
                           @baseUrl + '/response/'+ CONVERT(nvarchar(MAX), (
							        SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							        INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							        WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 1 AND MediaType = 2
					       )) AS WhatsappAcceptedLink,
                           @baseUrl + '/response/'+ CONVERT(nvarchar(MAX), (
							    SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							    INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							    WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 2 AND MediaType = 1					   
					       )) AS SmsRejectedLink,
					       (SELECT COUNT(*) FROM campaigninvitation ci 
							    INNER JOIN CampaignInvitationResponse cir ON cir.CampaignInvitationId = ci.Id
							    WHERE ci.EventCampaignId = 
							    (SELECT TOP 1 ec.Id FROM EventCampaign ec
							    INNER JOIN campaigninvitation ci ON ci.EventCampaignId = ec.Id WHERE ci.id=@invitationId)
						    AND cir.ResponseMediaType IS NOT NULL AND cir.ContactId = c.Id) AS AnswersCount,
                           t.Name AS SenderName,
						   ciml.MessageStatus
                    FROM Contact c
				    OUTER APPLY ( SELECT  TOP 1 cpm1.Email, cpm1.Sms, cpm1.WhatsApp
								FROM CampaignPrefferedMedia cpm1
                                INNER JOIN EventCampaign ec on ec.Id=cpm1.EventCampaignId
								INNER JOIN CampaignInvitation ci on ci.EventCampaignId = ec.Id
								WHERE cpm1.ContactId = c.Id AND ci.id = @invitationId ORDER BY cpm1.CreatedOn DESC) cpm
                    INNER JOIN ContactListToContacts clc ON c.Id = clc.ContactId
                    INNER JOIN EventCampaign ec ON clc.ContactListId = ec.GuestListId
                    INNER JOIN CampaignInvitation ci ON ec.Id = ci.EventCampaignId
                    INNER JOIN Tenant t ON c.TenantId = t.Id
                    LEFT JOIN CampaignInvitationResponse cir ON ci.Id = cir.CampaignInvitationId AND c.Id = cir.ContactId
                    OUTER APPLY ( SELECT TOP 1 * FROM CampaignInvitationMessageLog WHERE CampaignInvitationId = ci.Id AND ContactId = c.Id ORDER BY TimeStamp DESC) ciml
                    WHERE ci.Id = @invitationId
                   AND (cpm.Email = 1 OR cpm.Sms = 1 OR cpm.WhatsApp = 1)
				    AND cir.Answer IS NULL
					) AS innerQuery where AnswersCount = 0";

        private const string ListAnalysisGuests = @"SELECT c.Id,
                       c.FullName,
                       c.PreferredName,
                       c.Email,
						cpm.Email AS EmailPreferred,
                        c.MobileNumber AS Phone,
                        cpm.Sms AS SmsPreferred,
                        cpm.WhatsApp AS WhatsappPreferred,
                       c.Organization,
                       c.Position,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 1 AND MediaType = 0
					   )) AS EmailAcceptedLink,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 2 AND MediaType = 0					   
					   )) AS EmailRejectedLink,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 1 AND MediaType = 1
					   )) AS SmsAcceptedLink,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							    SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							    INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							    WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 1 AND MediaType = 2
					   )) AS WhatsappAcceptedLink,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT TOP 1 cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 2 AND MediaType = 1					   
					   )) AS SmsRejectedLink,
                       t.Name AS SenderName
                FROM Contact c
				OUTER APPLY ( SELECT  TOP 1 cpm1.Email, cpm1.Sms, cpm1.WhatsApp
								FROM CampaignPrefferedMedia cpm1
                                INNER JOIN EventCampaign ec on ec.Id=cpm1.EventCampaignId
								INNER JOIN CampaignInvitation ci on ci.EventCampaignId = ec.Id
								WHERE cpm1.ContactId = c.Id AND ci.id = @invitationId ORDER BY cpm1.CreatedOn DESC) cpm
                INNER JOIN ContactListToContacts clc ON c.Id = clc.ContactId
                INNER JOIN EventCampaign ec ON clc.ContactListId = ec.GuestListId
                INNER JOIN CampaignInvitation ci ON ec.Id = ci.EventCampaignId
                INNER JOIN Tenant t ON c.TenantId = t.Id
                OUTER APPLY ( SELECT TOP 1 * FROM CampaignInvitationMessageLog WHERE CampaignInvitationId = ci.Id AND ContactId = c.Id ORDER BY TimeStamp DESC) ciml
                WHERE ci.Id = @invitationId
                AND (cpm.Email = 1 OR cpm.Sms = 1 OR cpm.Whatsapp = 1)";

        private readonly IConnectionString _connectionString;
        private readonly IConfiguration _config;

        public GetRvspGuestsListHandler(IConnectionString connectionString,
            IConfiguration config)
        {
            _connectionString = connectionString;
            _config = config;
        }

        /// <summary>
        /// get list of guests for specific campaign to send email,sms,whatsapp
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<GuestsListItem>> Handle(GetRsvpGuestsList request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                invitationId = request.CampaignInvitationId,
                baseUrl = $"{_config["JwtAuthority:FrontendUrl"]}"
            };

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<GuestsListItem>(RsvpGuests, parameters);
                return list;
            }
        }

        public async Task<IEnumerable<GuestsListItem>> Handle(GetPendingGuestsList request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                invitationId = request.CampaignInvitationId,
                baseUrl = $"{_config["JwtAuthority:FrontendUrl"]}"
            };

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<GuestsListItem>(PendingGuests, parameters);
                return list;
            }
        }

        /// <summary>
        /// list analysis guests
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<GuestsListItem>> Handle(GetListAnalysisGuestsList request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                invitationId = request.CampaignInvitationId,
                baseUrl = $"{_config["JwtAuthority:FrontendUrl"]}"
            };

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<GuestsListItem>(ListAnalysisGuests, parameters);
                return list;
            }
        }

    }
}

