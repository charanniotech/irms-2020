﻿using Irms.Data.Abstract;
using Irms.Data.Read.BackgroundService.Queries;
using Irms.Data.Read.BackgroundService.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.BackgroundService.QueryHandlers
{
    public class GetAcceptedRejectedContactInvitationsHandler : IRequestHandler<GetAcceptedRejectedContactInvitations, IEnumerable<AcceptedRejectedContactInvitationListItem>>
    {
        private const string AcceptedRejectedQuery = @"
                SELECT c.Id,
                       c.Id AS ContactId,
                       c.FullName,
                       c.PreferredName,
                       c.Email,
					   cpm.Email AS EmailPreferred,
                       c.MobileNumber AS Phone,
                       cpm.Sms AS SmsPreferred,
                       c.Organization,
                       c.Position,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 1 AND MediaType = 0
					   )) AS EmailAcceptedLink,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 2 AND MediaType = 0
					   )) AS EmailRejectedLink,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 1 AND MediaType = 1
					   )) AS SmsAcceptedLink,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @invitationId AND AcceptOrReject = 2 AND MediaType = 1
					   )) AS SmsRejectedLink,
					   t.Name as SenderName
                FROM Contact c
				OUTER APPLY ( SELECT  TOP 1 * FROM    CampaignPrefferedMedia WHERE   ContactId = c.Id ORDER BY CreatedOn DESC) cpm
                INNER JOIN CampaignInvitationNextRun cinr ON c.Id = cinr.ContactId AND cinr.CampaignInvitationId = @invitationId
				INNER JOIN Tenant t ON t.Id = c.Tenantid
                WHERE (Status  = 0 /*draft*/ OR Status = 1)
				AND (cpm.Email = 1 OR cpm.Sms = 1)
				AND cinr.StartDate <= GETUTCDATE()";

        private readonly IConnectionString _connectionString;
        private readonly IConfiguration _config;

        public GetAcceptedRejectedContactInvitationsHandler(IConnectionString connectionString, IConfiguration config)
        {
            _connectionString = connectionString;
            _config = config;
        }

        /// <summary>
        /// get product list for drop down
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AcceptedRejectedContactInvitationListItem>> Handle(GetAcceptedRejectedContactInvitations request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                invitationId = request.InvitationId,
                baseUrl = $"{_config["JwtAuthority:FrontendUrl"]}",
            };

            var query = AcceptedRejectedQuery.Replace("{now}", DateTime.UtcNow.ToLongTimeString());

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<AcceptedRejectedContactInvitationListItem>(query, parameters);
                return list;
            }
        }
    }
}