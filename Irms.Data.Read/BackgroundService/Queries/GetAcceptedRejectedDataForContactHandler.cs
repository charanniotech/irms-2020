﻿using Hangfire.Storage;
using Irms.Data.Abstract;
using Irms.Data.Read.BackgroundService.QueryHandlers;
using Irms.Data.Read.BackgroundService.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.BackgroundService.Queries
{
    public class GetAcceptedRejectedDataForContactHandler : IRequestHandler<GetAcceptedRejectedDataForContactQuery, IEnumerable<AcceptedRejectedContactInvitationListItem>>
    {
        private const string AcceptedRejectedDataForContactQuery = @"
                SELECT c.Id,
                       c.Id AS ContactId,
                       c.FullName,
                       c.PreferredName,
                       c.Email,
					   cpm.Email AS EmailPreferred,
                       c.MobileNumber AS Phone,
                       cpm.Sms AS SmsPreferred,
                       cpm.WhatsApp AS WhatsappPreferred,
                       c.Organization,
                       c.Position,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @campaignInvitationId AND AcceptOrReject = 1 AND MediaType = 0
					   )) AS EmailAcceptedLink,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @campaignInvitationId AND AcceptOrReject = 2 AND MediaType = 0
					   )) AS EmailRejectedLink,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @campaignInvitationId AND AcceptOrReject = 1 AND MediaType = 1
					   )) AS SmsAcceptedLink,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @campaignInvitationId AND AcceptOrReject = 1 AND MediaType = 2
					   )) AS WhatsappAcceptedLink,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @campaignInvitationId AND AcceptOrReject = 2 AND MediaType = 2
					   )) AS WhatsappRejectedLink,
                       @baseUrl + '/rfi/'+ CONVERT(nvarchar(MAX), (
							SELECT cirmt1.Id FROM CampaignInvitationResponseMediaType cirmt1
							INNER JOIN campaigninvitationresponse cir1 ON cirmt1.CampaignInvitationResponseId=cir1.Id
							WHERE cir1.ContactId = c.Id AND cir1.CampaignInvitationId = @campaignInvitationId AND AcceptOrReject = 2 AND MediaType = 1
					   )) AS SmsRejectedLink,
					   t.Name as SenderName
                FROM Contact c
				OUTER APPLY ( SELECT  TOP 1 cpm1.Email, cpm1.Sms, cpm1.WhatsApp
								FROM CampaignPrefferedMedia cpm1
                                INNER JOIN EventCampaign ec on ec.Id=cpm1.EventCampaignId
								INNER JOIN CampaignInvitation ci on ci.EventCampaignId = ec.Id
								WHERE cpm1.ContactId = c.Id AND ci.id = @campaignInvitationId ORDER BY cpm1.CreatedOn DESC) cpm
				INNER JOIN Tenant t ON t.Id = c.Tenantid
                WHERE (cpm.Email = 1 OR cpm.Sms = 1 OR cpm.WhatsApp = 1)
				AND c.Id = @contactId";

        private readonly IConnectionString _connectionString;
        private readonly IConfiguration _config;

        public GetAcceptedRejectedDataForContactHandler(
            IConnectionString connectionString, 
            IConfiguration config)
        {
            _connectionString = connectionString;
            _config = config;
        }

        public async Task<IEnumerable<AcceptedRejectedContactInvitationListItem>> Handle(GetAcceptedRejectedDataForContactQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                campaignInvitationId = request.CampaignInvitationId,
                contactId = request.ContactId,
                baseUrl = $"{_config["JwtAuthority:FrontendUrl"]}",
            };

            var query = AcceptedRejectedDataForContactQuery.Replace("{now}", DateTime.UtcNow.ToLongTimeString());

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var list = await db.QueryAndLog<AcceptedRejectedContactInvitationListItem>(query, parameters);
                return list;
            }
        }
    }
}
