﻿using AutoMapper;
using Irms.Application;
using Irms.Data.Read.Dictionary.ReadModels;
using Irms.Data.Read.Event.Queries;
using Irms.Data.Read.Event.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Event.QueryHandlers
{
    public class GetEventInfoHandler : IRequestHandler<GetEventInfo, EventInfo>
    {
        private readonly IrmsDataContext _context;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;

        public GetEventInfoHandler(IrmsDataContext context,
            IConfiguration config,
            IMapper mapper,
            TenantBasicInfo tenant)
        {
            _context = context;
            _config = config;
            _mapper = mapper;
            _tenant = tenant;
        }

        /// <summary>
        /// get event basic info
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<EventInfo> Handle(GetEventInfo request, CancellationToken token)
        {
            var e = await _context.Event
                .Include(_ => _.EventType)
                .Include(_ => _.EventLocation)
                .ThenInclude(y => y.Country)
                .FirstOrDefaultAsync(x => x.Id == request.Id 
                && x.TenantId == _tenant.Id, token);

            var @event = _mapper.Map<EventInfo>(e);
            _mapper.Map(e.EventLocation.First(), @event);
            @event.LocationName = e.EventLocation.First().Name;
            @event.Country = e.EventLocation.First().Country.Name;
            @event.Id = e.Id;
            @event.Name = e.Name;
            @event.EventType = e.EventType == null ? null : new EventTypeItem(e.EventType.Id, e.EventType.Title);
            @event.ImagePath = e.ImagePath == null ? e.ImagePath : $"{_config["AzureStorage:BaseUrl"]}{e.ImagePath}";

            return @event;
        }
    }
}
