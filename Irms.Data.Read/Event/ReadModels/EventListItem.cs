﻿using System;

namespace Irms.Data.Read.Event.ReadModels
{
    public class EventListItem
    {
        public Guid Id { get; set;}
        public string Name { get; set;}
        public DateTime StartDateTime { get; set;}
        public DateTime EndDateTime { get; set;}
        public string ImagePath { get; set;}
        public string EventType { get; set;}
    }
}
