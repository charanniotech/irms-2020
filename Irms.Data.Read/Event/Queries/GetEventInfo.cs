﻿using Irms.Data.Read.Event.ReadModels;
using MediatR;
using System;

namespace Irms.Data.Read.Event.Queries
{
    public class GetEventInfo : IRequest<EventInfo>
    {

        public GetEventInfo(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
