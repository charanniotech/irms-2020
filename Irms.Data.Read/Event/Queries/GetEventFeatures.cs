﻿using Irms.Data.Read.Event.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Data.Read.Event.Queries
{
    public class GetEventFeatures : IRequest<IEnumerable<EventFeatureListItem>>
    {
        public GetEventFeatures(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
