﻿using MediatR;

namespace Irms.Application.Contact.Commands
{
    public class TwilioWebhookSmsCmd : IRequest<Unit>
    {
        //SmsSid: SM2xxxxxx
        //SmsStatus: sent
        //Body: McAvoy or Stewart? These timelines can get so confusing.
        //MessageStatus: sent
        //To: +1512zzzyyyy
        //MessageSid: SM2xxxxxx
        //AccountSid: ACxxxxxxx
        //From: +1512xxxyyyy
        //ApiVersion: 2010-04-01

        public string SmsSid { get; set; }
        public string SmsStatus { get; set; }
        public string Body { get; set; }
        public string MessageStatus { get; set; }
        public string To { get; set; }
        public string MessageSid { get; set; }
        public string AccountSid { get; set; }
        public string From { get; set; }
        public string ApiVersion { get; set; }
    }
}
