﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using Irms.Application.Webhooks.Events;
using Irms.Domain.Entities;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class SendgridWebhookEmailHandler : IRequestHandler<SendgridWebhookEmailCmd, Unit>
    {
        private readonly IBackgroundServiceRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        public SendgridWebhookEmailHandler(IBackgroundServiceRepository<CampaignInvitation, Guid> repo,
            IMediator mediator)
        {
            _repo = repo;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(SendgridWebhookEmailCmd request, CancellationToken token)
        {
            string data = JsonConvert.SerializeObject(request);

            IEnumerable<string> messageIds = request.Responses.Select(x => x.sg_message_id.Split('.').First());
            var logs = await _repo.GetMessagesByEmails(messageIds, token);

            foreach (var resp in request.Responses)
            {
                var selectedLogs = logs.Where(x => x.MessageId == resp.sg_message_id.Split('.').First()).ToList();
                if (selectedLogs != null && selectedLogs.Count > 0)
                {
                    var status = (InvitationMessageStatus)Enum.Parse(typeof(InvitationMessageStatus), CultureInfo.CurrentCulture.TextInfo.ToTitleCase(resp.@event));

                    if (status == InvitationMessageStatus.Bounce && resp.type == "blocked")
                    {
                        status = InvitationMessageStatus.Blocked;
                    }

                    if (!selectedLogs.Select(x => x.MessageStatus).Contains((int)status))
                    {
                        var response = new ProviderResponse
                        {
                            Id = Guid.NewGuid(),
                            TenantId = selectedLogs.First().TenantId,
                            CampaignInvitationMessageLogId = selectedLogs.First().Id,
                            ContactId = selectedLogs.First().ContactId,
                            ProviderType = (int)ProviderType.Sendgrid,
                            MessageStatus = (int)status,
                            ResponseDate = DateTime.UtcNow,
                            ResponseJson = JsonConvert.SerializeObject(resp)
                        };

                        await _repo.LogProviderResponse(response, token);
                        await _mediator.Publish(new SendgridWebhookEmailStatusLogged(response.Id, selectedLogs.First().TenantId), token);
                    }
                }
            }

            return Unit.Value;
        }
    }
}
