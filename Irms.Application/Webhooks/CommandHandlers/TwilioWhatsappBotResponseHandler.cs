﻿using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.CampaignInvitations;
using Irms.Application.Contact.Commands;
using Irms.Application.Events.Commands;
using Irms.Application.Webhooks.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class TwilioWhatsappBotResponseHandler : IRequestHandler<TwilioWhatsappBotResponseCmd, Unit>
    {
        private readonly IBackgroundServiceRepository<CampaignInvitation, Guid> _repo;
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _invitationRepo;
        private readonly IMediator _mediator;
        private readonly ICampaignInvitationNotifier _notifier;
        private readonly IHangfireJobsWorker _hangfireWorker;

        public TwilioWhatsappBotResponseHandler(IBackgroundServiceRepository<CampaignInvitation, Guid> repo,
            ICampaignInvitationRepository<CampaignInvitation, Guid> invitationRepo,
            ICampaignInvitationNotifier notifier,
            IMediator mediator,
            IHangfireJobsWorker hangfireWorker)
        {
            _repo = repo;
            _invitationRepo = invitationRepo;
            _mediator = mediator;
            _notifier = notifier;
            _hangfireWorker = hangfireWorker;
        }

        public async Task<Unit> Handle(TwilioWhatsappBotResponseCmd request, CancellationToken token)
        {
            var instantMsg = await _repo.GetInvitationResponse(request.From, request.Body, token);
            if (instantMsg.HasInvalidCode)
            {
                //send default response
                await SendInstantResponse(instantMsg, token);
                return Unit.Value;
            }

            if (instantMsg.InvitationResponse.Answer.HasValue && instantMsg.InvitationResponse.Answer != UserAnswer.NOT_ANSWERED)
            {
                //already answered
                instantMsg.Body = "You already have answered to this invitation";
                await SendInstantResponse(instantMsg, token);
                return Unit.Value;
            }

            instantMsg.InvitationResponse.Answer = instantMsg.Answer;
            instantMsg.InvitationResponse.ResponseMediaType = MediaType.WhatsApp;
            instantMsg.InvitationResponse.ResponseDate = DateTime.UtcNow;
            await _repo.UpdateInvitationResponse(instantMsg.InvitationResponse, token);

            //send instant response
            await SendInstantResponse(instantMsg, token);

            //schedule accepted/rejected jobs
            var invitations = (await _invitationRepo.LoadAcceptedOrRejectedInvitations(instantMsg.CampaignInvitationId, instantMsg.Answer, token))
                .OrderBy(x => x.SortOrder)
                .ToList();

            var nextDate = DateTime.UtcNow;

            for (var i = 0; i < invitations.Count; ++i)
            {
                var currentInvitation = invitations[i];

                if (!currentInvitation.IsInstant)
                {
                    nextDate = nextDate + ToTimeSpan(currentInvitation.Interval.Value, currentInvitation.IntervalType.Value);
                }

                currentInvitation.StartDate = nextDate;
            }

            for (var i = 0; i < invitations.Count; ++i)
            {
                if (instantMsg.Answer == UserAnswer.ACCEPTED)
                    _hangfireWorker.SetupAcceptedSending(invitations[i].Id, instantMsg.ContactId, invitations[i].StartDate);
                else if (instantMsg.Answer == UserAnswer.REJECTED)
                    _hangfireWorker.SetupRejectedSending(invitations[i].Id, instantMsg.ContactId, invitations[i].StartDate);
                else
                    throw new IncorrectRequestException($"Answer should be accepted or rejected, but it is {instantMsg.Answer}");
            }

            // update invitations
            await _invitationRepo.UpdateRange(invitations, token);
            return Unit.Value;
        }

        private TimeSpan ToTimeSpan(int interval, IntervalType intervalType)
        {
            switch (intervalType)
            {
                case IntervalType.Days:
                    return TimeSpan.FromDays(interval);
                case IntervalType.Hours:
                    return TimeSpan.FromHours(interval);
                case IntervalType.Minutes:
                    return TimeSpan.FromMinutes(interval);
                default:
                    throw new ArgumentException("interval type is invalid");
            }
        }

        private async Task SendInstantResponse(InstantBotResponse instantMsg, CancellationToken token)
        {
            var response = await _notifier.NotifyByWhatsappBOTResponse(instantMsg, token);
            if (response.sent)
            {
                await _mediator.Publish(new TwilioWhatsappBotResponseLogged(instantMsg.ContactId, instantMsg.TenantId), token);
            }
        }
    }
}
