﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Irms.Application.Contact.Events
{
    public class ContactsExported : IEvent
    {
        public ContactsExported(Guid contactListId, IEnumerable<Guid> contactIds)
        {
            ContactListId = contactListId;
            ContactIds = contactIds;
        }
        public Guid ContactListId { get; }
        public IEnumerable<Guid> ContactIds { get; }

        public Guid ObjectId => ContactListId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) 
        {
            string defaultMessage = $"The guest list  {x(ContactListId)} has been exported by {u}";
            if(ContactIds.Count() > 0)
            {
                defaultMessage += $". Ids was exported: { string.Join(",", ContactIds.Select(x => x.ToString())) }.";
            }
            else
            {
                defaultMessage += " fully.";
            }
            return defaultMessage;
        }
    }
}
