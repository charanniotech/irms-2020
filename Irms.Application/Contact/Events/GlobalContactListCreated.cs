﻿using System;
using Irms.Application.Abstract;

namespace Irms.Application.Events.Events
{
    public class GlobalContactListCreated : IEvent
    {
        public GlobalContactListCreated(Guid contactListId)
        {
            ContactListId = contactListId;
        }

        public Guid ContactListId { get; }

        public Guid ObjectId => ContactListId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The contact list  {x(ContactListId)} has been created by {u}";
    }
}
