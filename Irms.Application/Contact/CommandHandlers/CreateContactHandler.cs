﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Events.Events;
using Irms.Application.Contact.Commands;
using Irms.Domain.Entities;
using Irms.Application.Abstract;

namespace Irms.Application.Contact.CommandHandlers
{
    public class CreateContactHandler : IRequestHandler<CreateContactCmd, Guid?>
    {
        private readonly IContactListRepository<ContactList, Guid> _contactListRepository;
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepository;
        private readonly IContactListToContactsRepository<ContactListToContact, Guid> _contactListToContactsRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;
        private readonly ITransactionManager _transaction;

        public CreateContactHandler(
            TenantBasicInfo tenant,
            IContactListRepository<ContactList, Guid> contactListRepository,
            IContactRepository<Domain.Entities.Contact, Guid> contactRepository,
            IContactListToContactsRepository<ContactListToContact, Guid> contactListToContactsRepository,
            IMediator mediator,
            IMapper mapper,
            ITransactionManager transaction
            )
        {
            _contactListRepository = contactListRepository;
            _contactRepository = contactRepository;
            _contactListToContactsRepository = contactListToContactsRepository;
            _mediator = mediator;
            _mapper = mapper;
            _tenant = tenant;
            _transaction = transaction;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid?> Handle(CreateContactCmd message, CancellationToken token)
        {
            Domain.Entities.Contact contact;
            using (var tr = await _transaction.BeginTransactionAsync(token))
            {
                if (!message.DocumentTypeId.HasValue)
                {
                    message.DocumentNumber = null;
                    message.IssuingCountryId = null;
                    message.ExpirationDate = null;
                }

                contact = _mapper.Map<Domain.Entities.Contact>(message);
                contact.Create(_tenant.Id);

                if (message.ContactList.IsNotNullOrEmpty())
                {

                    if (await _contactListRepository.IsNameAlreadyExists(message.ContactList, token))
                    {
                        tr.Rollback();
                        return null;
                    }
                    
                    var contactList = _mapper.Map<ContactList>(message);
                    contactList.Create(_tenant.Id);
                    contactList.SetName(message.ContactList);
                    contactList.SetGlobal();
                    contactList.SetGuest(message.IsGuest);
                    //create list
                    message.ContactListId = await _contactListRepository.Create(contactList, token);
                }

                await _contactRepository.Create(contact, token);

                if (message.ContactListId.HasValue)
                {
                    var cltc = new ContactListToContact();
                    cltc.Create(_tenant.Id);
                    cltc.SetContactId(contact.Id);
                    cltc.SetContactListId(message.ContactListId.Value);

                    await _contactListToContactsRepository.Create(cltc, token);
                }

                tr.Commit();
            }

            var globalContactListCreated = new GlobalContactListCreated(contact.Id);
            await _mediator.Publish(globalContactListCreated, token);

            return contact.Id;
        }
    }
}
