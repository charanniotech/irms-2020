﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class CreateGuestListHandler : IRequestHandler<CreateGuestListCmd, Guid?>
    {
        private readonly IContactListRepository<ContactList, Guid> _contactListRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;
        public CreateGuestListHandler(TenantBasicInfo tenant,
            IContactListRepository<ContactList, Guid> contactListRepository,
            IMediator mediator,
            IMapper mapper)
        {
            _contactListRepository = contactListRepository;
            _mediator = mediator;
            _mapper = mapper;
            _tenant = tenant;
        }
        /// <summary>
        /// create and add guest list to the DB and call mediator's publish method
        /// </summary>
        /// <returns></returns>
        public async Task<Guid?> Handle(CreateGuestListCmd message, CancellationToken token)
        {
            if (await _contactListRepository.IsNameAlreadyExists(message.Name, token))
            {
                return null;
            }

            ContactList contactList = _mapper.Map<ContactList>(message);
            contactList.Create(_tenant.Id);
            contactList.IsGuest = message.IsGuest;
            contactList.IsGlobal = message.IsGlobal ?? false;

            await _contactListRepository.Create(contactList, token);

            var guestListCreated = new GuestListCreated(contactList.Id);
            await _mediator.Publish(guestListCreated, token);

            return contactList.Id;
        }
    }
}
