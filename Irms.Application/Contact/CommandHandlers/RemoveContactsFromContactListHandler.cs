﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.Commands;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class RemoveContactsFromContactListHandler : IRequestHandler<RemoveContactsFromContactListCmd, Unit>
    {
        private readonly IContactListToContactsRepository<ContactListToContact, Guid> _contactListToContactsRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;
        private readonly ITransactionManager _transaction;
        public RemoveContactsFromContactListHandler(
            TenantBasicInfo tenant,
            IContactListToContactsRepository<ContactListToContact, Guid> contactListToContactsRepository,
            IMediator mediator,
            IMapper mapper,
            ITransactionManager transaction
            )
        {
            _contactListToContactsRepository = contactListToContactsRepository;
            _mediator = mediator;
            _transaction = transaction;
            _mapper = mapper;
            _tenant = tenant;
        }

        public async Task<Unit> Handle(RemoveContactsFromContactListCmd request, CancellationToken cancellationToken)
        {
            await _contactListToContactsRepository.DeleteRangeFromContactList(request.ContactIds, request.ContactListId, cancellationToken);
            return Unit.Value;
        }
    }
}
