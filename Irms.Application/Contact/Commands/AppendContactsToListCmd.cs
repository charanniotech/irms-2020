﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Contact.Commands
{
    /// <summary>
    /// Command to add contacts to list (which are not already in it)
    /// </summary>
    public class AppendContactsToListCmd : IRequest<Unit>
    {
        public Guid ListId { get; set; }
        public IEnumerable<Guid> ContactIds { get; set; }
    }
}
