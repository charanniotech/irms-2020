﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Contact.Commands
{
    public class AskRfiQuestionsCmd : IRequest<Unit>
    {
        public Guid ContactListId { get; set; }
    }
}
