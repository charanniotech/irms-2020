﻿using System;
using System.Collections.Generic;
using MediatR;

namespace Irms.Application.Contact.Commands
{
    public class UpdateContactCmd : IRequest<Unit>
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string FullName { get; set; }
        public string PreferredName { get; set; }
        public string Gender { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string AlternativeEmail { get; set; }
        public string SecondaryMobileNumber { get; set; }
        public string WorkNumber { get; set; }
        public Guid? NationalityId { get; set; }
        public int? DocumentTypeId { get; set; }
        public Guid? IssuingCountryId { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }

        public IEnumerable<CustomField> CustomFields { get; set; }
        public class CustomField
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Value { get; set; }
        }
    }
}
