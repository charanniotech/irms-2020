﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CustomFields.ReadModels
{
    public class CustomFieldValueReadModel
    {
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string Value { get; set; }
        public CustomFieldType Type { get; set; }
        public Guid CustomFieldId { get; set; }
        public Guid ContactId { get; set; }
    }
}
