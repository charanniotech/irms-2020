﻿using System;

namespace Irms.Application.Product.Commands
{
    public class ServiceFeature
    {
        public ServiceFeature() { }
        public ServiceFeature(Guid featureId, string featureTitle, bool isFeatureEnabled)
        {
            FeatureId = featureId;
            FeatureTitle = featureTitle;
            IsFeatureEnabled = isFeatureEnabled;
        }

        public Guid FeatureId { get; set; }
        public Guid ServiceId { get; set; }
        public string FeatureTitle { get; set; }
        public bool IsFeatureEnabled { get; set; }
        public bool? IsCustom { get; set; }
    }
}