﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.Product.Commands
{
    public class CreateProductCmd : IRequest<Guid>
    {
        public CreateProductCmd()
        {
            Id = Guid.NewGuid();
        }
        public CreateProductCmd(string productName, decimal price, byte currencyId) : this()
        {
            ProductName = productName;
            Price = price;
            CurrencyId = currencyId;
        }

        public CreateProductCmd(string productName, decimal price, byte currencyId, List<ProductService> services, decimal? disountPercentage) : this(productName, price, currencyId)
        {
            DisountPercentage = disountPercentage;
            Services = services;
        }
        
        public Guid Id { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public decimal? DisountPercentage { get; set; } = 0;
        public byte CurrencyId { get; set; }
        public string LicenseName { get; set; }
        public int LicenseDays { get; set; }
        public bool IsShowOnWeb { get; set; }
        public List<ProductService> Services { get; set; }
    }
}
