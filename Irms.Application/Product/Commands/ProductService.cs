﻿using System;
using System.Collections.Generic;

namespace Irms.Application.Product.Commands
{
    public class ProductService
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }
        public List<ServiceFeature> ServiceFeatures { get; set; }
    }
}