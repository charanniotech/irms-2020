﻿using MediatR;
using System;

namespace Irms.Application.Product.Commands
{
    public class GetProductByIdCmd : IRequest<Domain.Entities.Product>
    {
        public GetProductByIdCmd(Guid productId)
        {
            ProductId = productId;
        }

        public Guid ProductId { get; }
    }
}
