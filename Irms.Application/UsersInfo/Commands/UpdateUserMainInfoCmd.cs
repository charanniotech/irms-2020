﻿using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.UsersInfo.Commands
{
    public class UpdateUserMainInfoCmd : IRequest<bool>
    {
        public UpdateUserMainInfoCmd(
            string firstName,
            string lastName,
            Gender gender,
            DateTime? birthDate,
            string phoneNo,
            string email,
            string passportNo,
            string nationalId,
            string searchText)
        {
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            BirthDate = birthDate;
            PhoneNo = phoneNo;
            Email = email;
            PassportNo = passportNo;
            NationalId = nationalId;
            SearchText = searchText;
        }

        public string FirstName { get; }
        public string LastName { get; }
        public Gender Gender { get; }
        public DateTime? BirthDate { get; }
        public string PhoneNo { get; }
        public string Email { get; }
        public string PassportNo { get; }
        public string NationalId { get; }
        public string SearchText { get; }
    }
}
