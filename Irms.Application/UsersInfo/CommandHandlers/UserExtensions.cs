﻿using System;
using System.Linq;
using Irms.Application.Abstract;
using Irms.Domain.Entities; 

namespace Irms.Application.UsersInfo.CommandHandlers
{
    public static class UserExtensions
    {
        //privilege escalation: left -> right
        private static readonly RoleType[] Roles = { /*RoleType.DepartmentAdmin, RoleType.TenantAdmin,*/ RoleType.SuperAdmin };

        public static bool CanCreateNewUserOfRole(this ICurrentUser currentUser, RoleType newUserRole) =>
            CheckPermission(currentUser, newUserRole);

        public static bool CanAssignRoleToUser(this ICurrentUser currentUser, RoleType newUserRole) =>
            CheckPermission(currentUser, newUserRole);

        private static bool CheckPermission(this ICurrentUser currentUser, RoleType newUserRole)
        {
            var actorRole = Roles.FirstOrDefault(currentUser.IsInRole);

            //default index = -1
            var actorIdx = Array.IndexOf(Roles, actorRole);
            var newUserIdx = Array.IndexOf(Roles, newUserRole);

            return newUserIdx < actorIdx;
        }
    }
}
