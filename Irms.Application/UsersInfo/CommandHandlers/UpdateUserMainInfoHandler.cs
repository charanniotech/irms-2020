﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.UsersInfo.Commands;
using Irms.Application.UsersInfo.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.UsersInfo.CommandHandlers
{
    public class UpdateUserMainInfoHandler : IRequestHandler<UpdateUserMainInfoCmd, bool>
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly IUserInfoRepository<UserInfo, Guid> _repository;
        private readonly ICurrentUser _user;

        public UpdateUserMainInfoHandler(IUserInfoRepository<UserInfo, Guid> repository, IMediator mediator, IMapper mapper, ICurrentUser user)
        {
            _repository = repository;
            _mediator = mediator;
            _mapper = mapper;
            _user = user;
        }
        /// <summary>
        /// this method handle the request for updating the <see cref="EntityClasses.UserInfo"/> 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(UpdateUserMainInfoCmd request, CancellationToken cancellationToken)
        {
            var userInfo = _mapper.Map<UserInfo>(request);
            userInfo.Id = _user.Id;
            userInfo.Gender = request.Gender;

            await _repository.Update(userInfo, cancellationToken);
            var e = new UserInfoUpdated(_user.Id);
            await _mediator.Publish(e, cancellationToken);
            return true;
        }
    }
}
