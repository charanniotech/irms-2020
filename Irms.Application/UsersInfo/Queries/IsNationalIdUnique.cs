﻿using System;
using MediatR;

namespace Irms.Application.UsersInfo.Queries
{
    public class IsNationalIdUnique : IRequest<bool>
    {
        public IsNationalIdUnique(string nationalId)
        {
            NationalId = nationalId;
        }

        public IsNationalIdUnique(string nationalId, Guid userId)
        {
            NationalId = nationalId;
            UserInfoId = userId;
        }

        public string NationalId { get; }
        public Guid? UserInfoId { get; }
    }
}
