﻿using MediatR;

namespace Irms.Application.Registrations.Common.Queries
{
    public class IsPassportUnique : IRequest<bool>
    {
        public IsPassportUnique(string passportNo)
        {
            PassportNo = passportNo;
        }

        public string PassportNo { get; }
    }
}
