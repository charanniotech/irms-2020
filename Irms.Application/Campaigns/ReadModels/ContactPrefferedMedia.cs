﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Campaigns.ReadModels
{
    public class ContactPrefferedMedia
    {
        public Guid Id { get; set; }
        public Guid ContactId { get; set; }
        public bool Email { get; set; }
        public bool Sms { get; set; }
        public bool WhatsApp { get; set; }
    }
}
