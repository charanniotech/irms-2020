﻿using System;
using Irms.Domain.Entities;
using MediatR;

namespace Irms.Application.Campaigns.Commands
{
    public class CreateCampaignCmd : IRequest<Guid>
    {
        public Guid EventId { get; set; }
    }
}
