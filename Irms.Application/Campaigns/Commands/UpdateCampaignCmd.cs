﻿using System;
using MediatR;

namespace Irms.Application.Campaigns.Commands
{
    public class UpdateCampaignCmd : IRequest<Unit>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid GuestListId { get; set; }
        public byte EntryCriteriaTypeId { get; set; }
        public byte ExitCriteriaType { get; set; }
    }
}
