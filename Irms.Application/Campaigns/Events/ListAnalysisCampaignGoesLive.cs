﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.Campaigns.Events
{
    public class ListAnalysisCampaignGoesLive : IEvent
    {
        public ListAnalysisCampaignGoesLive(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }

        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The list analysis campaign {x(Id)} has changed it state to live by {u}";
    }
}
