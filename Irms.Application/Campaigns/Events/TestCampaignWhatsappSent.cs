﻿using System;
using System.Collections.Generic;
using Irms.Application.Abstract;

namespace Irms.Application.Campaigns.Events
{
    public class TestCampaignWhatsappSent : IEvent
    {
        public TestCampaignWhatsappSent(IEnumerable<string> numbers)
        {
            Numbers = numbers;
        }

        public IEnumerable<string> Numbers { get; }
        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"Whatsapp for numbers {string.Join(",", Numbers)} was sent by {u}";
    }
}