﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Domain;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class UpdatePreferredMediaCmdHandler : IRequestHandler<UpdatePreferredMediaCmd, Unit>
    {
        private readonly ICampaignRepository<Campaign, Guid> _repository;
        private readonly IMediator _mediator;

        public UpdatePreferredMediaCmdHandler(
            ICampaignRepository<Campaign, Guid> repository,
            IMediator mediator)
        {
            _repository = repository;
            _mediator = mediator;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(UpdatePreferredMediaCmd request, CancellationToken token)
        {
            foreach (var cpm in request.GuestPreferredMediaList)
            {
                if (!(cpm.Email || cpm.Sms || cpm.WhatsApp))
                {
                    throw new IncorrectRequestException("Each contact should have atleast one media selected.");
                }
            }

            await _repository.UpdatePreferredMedia(request, token);

            var e = new PreferredMediaUpdated(request.CampaignId);
            await _mediator.Publish(e, token);
            return Unit.Value;
        }
    }
}
