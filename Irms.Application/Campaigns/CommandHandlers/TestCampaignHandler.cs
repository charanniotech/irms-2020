﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.CampaignInvitations;
using Irms.Application.CampaignInvitations.ReadModels;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class TestCampaignHandler : IRequestHandler<TestCampaignCmd, TestCampaignSendInfoModel>
    {
        private readonly ICampaignInvitationTestNotifier _notifier;
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;

        public TestCampaignHandler(
            IMediator mediator,
            ICampaignInvitationTestNotifier notifier,
            ICampaignInvitationRepository<CampaignInvitation, Guid> repo)
        {
            _mediator = mediator;
            _notifier = notifier;
            _repo = repo;
        }

        public async Task<TestCampaignSendInfoModel> Handle(TestCampaignCmd request, CancellationToken token)
        {
            var result = new TestCampaignSendInfoModel();
            var invitations = await _repo.LoadInvitationTemplateGroup(request.Id, token);

            if (invitations.Count() > 0)
            {
                string error;
                foreach (var inv in invitations)
                {
                    if (inv.EmailTemplate != null)
                    {
                        (result.EmailsSend, error) = await SendEmails(request, inv, token);
                        result.Error += error;
                    }

                    if (inv.SmsTemplate != null)
                    {
                        (result.SmsSend, error) = await SendSms(request, inv, token);
                        result.Error += error;
                    }

                    if (inv.WhatsappTemplate != null)
                    {
                        (result.WhatsappSend, error) = await SendWhatsappMessage(request, inv, token);
                        result.Error += error;
                    }
                }
            }

            return result;
        }

        //email
        private async Task<(int, string)> SendEmails(TestCampaignCmd request, CampaignInvitation invitation, CancellationToken token)
        {
            List<string> errors = new List<string>();

            if (request.Emails == null || request.Emails.Count() == 0)
            {
                return (0, "");
            }

            try
            {
                string interval = invitation.IsInstant ? "Instant" : $"After {invitation.Interval} {invitation.IntervalType}";
                var msg = new InvitationMessage
                {
                    Sender = invitation.EmailTemplate.SenderEmail,
                    CampaignInvitationId = invitation.Id,
                    Subject = $"[{interval}] {invitation.Title ?? "Rsvp"} - {invitation.EmailTemplate.Subject}",
                    Body = invitation.EmailTemplate.Body.ReplaceDefaultValuePlaceholders(),
                    Recipients = request.Emails.Select(x => new EventGuest
                    {
                        Email = x
                    }).ToList()
                };

                _ = await _notifier.NotifyByEmail(msg, token);
                await _mediator.Publish(new TestEmailSent(request.Id), token);
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message);
            }

            return (request.Emails.Count(), string.Join(",", errors));
        }

        //sms
        private async Task<(int, string)> SendSms(TestCampaignCmd request, CampaignInvitation invitation, CancellationToken token)
        {
            List<string> errors = new List<string>();

            if (request.PhoneNumbers == null || request.PhoneNumbers.Count() == 0)
            {
                return (0, "");
            }

            try
            {
                foreach (var recipientPhone in request.PhoneNumbers)
                {
                    string interval = invitation.IsInstant ? "Instant" : $"After {invitation.Interval} {invitation.IntervalType}";
                    var msg = new InvitationMessage
                    {
                        Sender = invitation.SmsTemplate.SenderName,
                        CampaignInvitationId = invitation.Id,
                        Body = $"[{interval}] {invitation.Title ?? "Rsvp"} - {invitation.SmsTemplate.Body.ReplaceDefaultValuePlaceholders()}",
                        Recipients = (new EventGuest
                        {
                            Phone = recipientPhone
                        })
                        .Enumerate().ToList()
                    };

                    _ = await _notifier.NotifyBySms(msg, token);
                }

                await _mediator.Publish(new TestCampaignSmsSent(request.PhoneNumbers), token);
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message);
            }

            return (request.Emails.Count(), string.Join(",", errors));
        }

        //whatsapp
        private async Task<(int, string)> SendWhatsappMessage(TestCampaignCmd request, CampaignInvitation invitation, CancellationToken token)
        {
            List<string> errors = new List<string>();

            if (request.WhatsappNumbers == null || request.WhatsappNumbers.Count() == 0)
            {
                return (0, "");
            }

            try
            {
                foreach (var recipientPhone in request.WhatsappNumbers)
                {
                    var msg = new InvitationMessage
                    {
                        Sender = invitation.WhatsappTemplate.SenderName,
                        CampaignInvitationId = invitation.Id,
                        Body = invitation.WhatsappTemplate.Body,
                        Recipients = (new EventGuest
                        {
                            Phone = recipientPhone
                        })
                        .Enumerate().ToList()
                    };

                    _ = await _notifier.NotifyByWhatsapp(msg, token);
                }

                await _mediator.Publish(new TestCampaignWhatsappSent(request.WhatsappNumbers), token);
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message);
            }

            return (request.WhatsappNumbers.Count(), string.Join(",", errors));
        }
    }
}
