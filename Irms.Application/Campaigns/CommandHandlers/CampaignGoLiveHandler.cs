﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Application.Campaigns.ReadModels;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class CampaignGoLiveHandler : IRequestHandler<CampaignGoLiveCmd, CampaignGoLiveResponse>
    {
        private readonly ICampaignRepository<Campaign, Guid> _repo;
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepo;
        private readonly ICampaignInvitationRepository<Domain.Entities.CampaignInvitation, Guid> _campaignInvitationRepo;
        private readonly ICampaignInvitationResponseRepository<Domain.Entities.CampaignInvitationResponse, Guid> _campaignInvitationResponseRepo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly ITransactionManager _transaction;
        private readonly IHangfireJobsWorker _hangfireJobsWorker;
        private readonly IEmailSender _emailSender;

        public CampaignGoLiveHandler(
            ICampaignRepository<Campaign, Guid> repo,
            IContactRepository<Domain.Entities.Contact, Guid> contactRepo,
            ICampaignInvitationRepository<Domain.Entities.CampaignInvitation, Guid> campaignInvitationRepo,
            ICampaignInvitationResponseRepository<Domain.Entities.CampaignInvitationResponse, Guid> campaignInvitationResponseRepo,
            IMediator mediator,
            IMapper mapper,
            ITransactionManager transaction,
            IHangfireJobsWorker hangfireJobsWorker,
            IEmailSender emailSender)
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
            _transaction = transaction;
            _contactRepo = contactRepo;
            _campaignInvitationRepo = campaignInvitationRepo;
            _campaignInvitationResponseRepo = campaignInvitationResponseRepo;
            _hangfireJobsWorker = hangfireJobsWorker;
            _emailSender = emailSender;
        }

        /// <summary>
        /// This method sets campaign to live state
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CampaignGoLiveResponse> Handle(CampaignGoLiveCmd request, CancellationToken token)
        {
            var response = new CampaignGoLiveResponse { HasErrors = false };
            var campaign = await _repo.GetById(request.CampaignId, token);

            if (campaign.Active)
            {
                throw new IncorrectRequestException("Campaign is already active!");
            }

            using (var tr = await _transaction.BeginTransactionAsync(token))
            {
                try
                {
                    var existingMedias = await _repo.GetExistingMedias(request.CampaignId, token);

                    if (!existingMedias.Any(x => x))
                    {
                        throw new CampaignGoesLiveException("Users has no active medias");
                    }

                    await _contactRepo.CheckPrefferedMedias(request.CampaignId, token);
                    var invitations = await _campaignInvitationRepo.GetByCampaignId(request.CampaignId, token);
                    var contactPreferences = await _contactRepo.GetContactsByCampaignId(campaign.GuestListId.Value, campaign.Id, token);

                    var responses = new List<CampaignInvitationResponse>();

                    if (invitations != null && invitations.Count() > 0)
                    {
                        foreach (var invitation in invitations)
                        {
                            if (invitation.Email || invitation.Sms || invitation.Whatsapp)
                                foreach (var contact in contactPreferences)
                                {
                                    if (contact.Email || contact.Sms || contact.WhatsApp)
                                        responses.Add(new CampaignInvitationResponse
                                        {
                                            CampaignInvitationId = invitation.Invitation.Id,
                                            ContactId = contact.ContactId,
                                            TenantId = invitation.Invitation.TenantId,
                                            Id = Guid.NewGuid(),
                                            CreatedOn = DateTime.UtcNow,
                                            Answer = UserAnswer.NOT_ANSWERED
                                        });
                                }
                        }

                        await _campaignInvitationResponseRepo.FillRange(responses, token);
                    }

                    //update invitations
                    var invitationsOnly = invitations.Select(x => x.Invitation);
                    invitationsOnly = UpdateStartDateForInvitations(invitationsOnly);

                    var rsvp = invitationsOnly.FirstOrDefault(x => x.InvitationType == InvitationType.Rsvp);
                    _hangfireJobsWorker.SetupRsvpSending(rsvp.Id, rsvp.StartDate);

                    invitationsOnly
                        .Where(x => x.InvitationType == InvitationType.Pending)
                        .ToList()
                        .ForEach(x =>
                        {
                            _hangfireJobsWorker.SetupPendingSending(x.Id, x.StartDate);
                        });


                    await _contactRepo.UpdateContactsToGuests(request.CampaignId, token);
                    await _campaignInvitationRepo.UpdateRange(invitationsOnly, token);
                    campaign.Active = true;
                    campaign.CampaignStatus = CampaignStatus.Live;
                    await _repo.Update(campaign, token);
                    tr.Commit();

                    await SendEmailNotificationToTenantAdmin(campaign.Name, token);
                    await _mediator.Publish(new CampaignGoesLive(request.CampaignId), token);
                }
                catch (CampaignGoesLiveException ex)
                {
                    tr.Rollback();
                    throw new IncorrectRequestException(ex.Message);
                }
            }

            return response;
        }

        private async Task SendEmailNotificationToTenantAdmin(string campaignTitle, CancellationToken token)
        {
            (string name, string email) = await _repo.GetTenantAdmin(token);
            var recipients = new EmailMessage.Recipient(email, $"Campaign launched - [{campaignTitle}]")
                .Enumerate();

            string body = $"Hi {name}. The campaign with title {campaignTitle} has been launched.";
            var emailMsg = new EmailMessage(body, recipients);
            await _emailSender.SendEmail(emailMsg, token);
        }

        private IEnumerable<CampaignInvitation> UpdateStartDateForInvitations(IEnumerable<CampaignInvitation> invitations)
        {
            //check if start date passed.
            foreach (var inv in invitations)
            {
                if (inv.InvitationType == InvitationType.Rsvp && !inv.IsInstant && inv.StartDate <= DateTime.UtcNow)
                {
                    throw new IncorrectRequestException($"Schedule datetime of Rsvp invitation is passed.");
                }
            }

            var startDate = DateTime.UtcNow;
            var listInvitations = invitations.ToList();
            var rsvpInvitation = listInvitations.FirstOrDefault(x => x.InvitationType == InvitationType.Rsvp);
            if (rsvpInvitation.IsInstant)
            {
                rsvpInvitation.StartDate = startDate;
            }

            var nextDate = startDate;
            var pendingsCount = listInvitations
                .Where(x => x.InvitationType == InvitationType.Pending)
                .OrderBy(x => x.SortOrder)
                .Count();

            for (var i = 0; i < pendingsCount; ++i)
            {
                var currentPending = listInvitations.FirstOrDefault(x => x.SortOrder == i + 1 && x.InvitationType == InvitationType.Pending);

                if (!currentPending.IsInstant)
                {
                    nextDate = nextDate + ToTimeSpan(currentPending.Interval.Value, currentPending.IntervalType.Value);
                }

                currentPending.StartDate = nextDate;
            }

            listInvitations.ForEach(x => x.Active = true);

            return listInvitations;
        }

        private TimeSpan ToTimeSpan(int interval, IntervalType intervalType)
        {
            switch (intervalType)
            {
                case IntervalType.Days:
                    return TimeSpan.FromDays(interval);
                case IntervalType.Hours:
                    return TimeSpan.FromHours(interval);
                case IntervalType.Minutes:
                    return TimeSpan.FromMinutes(interval);
                default:
                    throw new ArgumentException("interval type is invalid");
            }
        }

        private CampaignGoLiveResponse CampaignGoesLiveError(string message)
        {
            return new CampaignGoLiveResponse { HasErrors = true, Error = message };
        }

        private class CampaignGoesLiveException : Exception
        {
            public CampaignGoesLiveException(string message) : base(message) { }
        }
    }
}
