﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Campaigns.Commands;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.Events;
using System.Linq;
using Irms.Domain;
using Irms.Application.Abstract;
using Irms.Application.Campaigns.ReadModels;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class CreateListAnalysisCampaignHandler : IRequestHandler<CreateListAnalysisCampaignCmd, ListAnalysisCampaignResponse>
    {
        private readonly ICampaignRepository<Campaign, Guid> _repo;
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _invitationRepo;
        private readonly ITransactionManager _transaction;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public CreateListAnalysisCampaignHandler(
            ICampaignRepository<Campaign, Guid> repo,
            ICampaignInvitationRepository<CampaignInvitation, Guid> invitationRepo,
            ITransactionManager transaction,
            IMediator mediator,
            IMapper mapper
            )
        {
            _repo = repo;
            _invitationRepo = invitationRepo;
            _transaction = transaction;
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<ListAnalysisCampaignResponse> Handle(CreateListAnalysisCampaignCmd message, CancellationToken token)
        {
            var inv = await _invitationRepo.GetByGuestListId(message.GuestListId, message.EventId, token);
            if (inv == null)
            {
                var campaign = _mapper.Map<Campaign>(message);
                campaign.CreateListAnalysisCampaign();
                campaign.GuestListId = message.GuestListId;

                var invitation = new CampaignInvitation();
                invitation.CreateListAnalysisInvitation(campaign.Id);

                using (var tr = await _transaction.BeginTransactionAsync(token))
                {
                    await _repo.Create(campaign, token);
                    await _invitationRepo.Create(invitation, token);

                    tr.Commit();
                }

                await _mediator.Publish(new CampaignListAnalysisCreated(campaign.Id), token);

                return new ListAnalysisCampaignResponse
                {
                    CampaignId = invitation.EventCampaignId,
                    CampaignInvitationId = invitation.Id
                };
            }

            return inv;
        }
    }

}