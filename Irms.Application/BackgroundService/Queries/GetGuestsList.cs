﻿using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.BackgroundService.Queries
{
    public class GetRsvpGuestsList : IRequest<IEnumerable<ReadModels.GuestsListItem>>
    {
        public GetRsvpGuestsList(Guid campaignInvitationId)
        {
            CampaignInvitationId = campaignInvitationId;
        }

        public Guid CampaignInvitationId { get; }
    }

    public class GetListAnalysisGuestsList : IRequest<IEnumerable<ReadModels.GuestsListItem>>
    {
        public GetListAnalysisGuestsList(Guid campaignInvitationId)
        {
            CampaignInvitationId = campaignInvitationId;
        }

        public Guid CampaignInvitationId { get; }
    }
}
