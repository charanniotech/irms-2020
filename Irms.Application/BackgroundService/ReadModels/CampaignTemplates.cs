﻿using System;

namespace Irms.Application.BackgroundService.ReadModels
{
    public class CampaignTemplates
    {
        public CampaignTemplates(Guid tenantId,
            string eventName,
            DateTime eventDateTime,
            string eventLocation,
            string emailSender,
            string emailSubject,
            string emailBody, string smsBody)
        {
            TenantId = tenantId;
            EventName = eventName;
            EventDateTime = eventDateTime;
            EventLocation = eventLocation;
            EmailSender = emailSender;
            EmailSubject = emailSubject;
            EmailBody = emailBody;
            SmsBody = smsBody;
        }

        public Guid TenantId { get; set; }
        public string EventName { get; set; }
        public DateTime EventDateTime { get; set; }
        public string EventLocation { get; set; }
        public string EmailSender { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public string SmsBody { get; set; }
    }
}
