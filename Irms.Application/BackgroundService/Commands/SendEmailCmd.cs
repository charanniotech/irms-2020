﻿using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.CampaignInvitations;
using MediatR;

namespace Irms.Application.BackgroundService.Commands
{
    public class SendEmailCmd : IRequest<(bool sent, EmailMessage msg)>
    {
        public SendEmailCmd(InvitationMessage msg)
        {
            Msg = msg;
        }

        public InvitationMessage Msg { get; set; }
    }
}
