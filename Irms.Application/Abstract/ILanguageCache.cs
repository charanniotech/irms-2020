﻿using Irms.Domain.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract
{
    public interface ILanguageCache
    {
        Task<IEnumerable<Language>> GetSystemLanguages(CancellationToken cancellationToken = default);
    }
}

