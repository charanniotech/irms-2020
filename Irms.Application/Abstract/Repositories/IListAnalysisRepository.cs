﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.ListAnalysis.Commands;
using Irms.Domain.Abstract;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories
{
    public interface IListAnalysisRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        Task AddImportantFieldsToGuestList(CreateListAnalysisImportantFieldsCmd ci, CancellationToken token);
        Task DeleteCampaign(Guid campaignId, CancellationToken token);
        Task SetListAnalysisState(SetListAnalysisStateCmd request, CancellationToken token);
        Task ResetListAnalysisState(Guid guestListId, Guid eventId, CancellationToken token);
    }
}