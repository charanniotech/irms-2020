﻿using System.Collections.Generic;

namespace Irms.Application.Abstract.Repositories.UserManagement
{
    public interface IIdentityResult
    {
        bool Succeeded { get; }
        IEnumerable<(string Code, string Description)> Errors { get; }
    }
}
