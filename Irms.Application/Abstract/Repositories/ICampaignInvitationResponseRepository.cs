﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain.Abstract;
using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories
{
    public interface ICampaignInvitationResponseRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity: IEntity<TKey>
    {
        Task<IEnumerable<RfiFormQuestion>> LoadQuestions(IEnumerable<Guid> ids, CancellationToken token);
        Task CreateRange(IEnumerable<CampaignInvitationResponse> entities, CancellationToken token);
        Task CreateRange(IEnumerable<CampaignInvitationResponse> entities, MediaType type, bool wasCreated, CancellationToken token);
        Task FillRange(IEnumerable<CampaignInvitationResponse> entities, CancellationToken token);
        Task<IEnumerable<ContactListToContact>> GetContactListsByInvitationId(Guid invitationId, CancellationToken token);
        Task SaveRfiAnswers(IEnumerable<Domain.Entities.RfiFormResponse> answers, CancellationToken token);
        Task<Guid> LoadRfiFormIdByResponseId(Guid id, CancellationToken token);
        Task<bool> HasAnswersOnRfi(Guid rfiFormId, Guid contactId, CancellationToken token);
        Task<Domain.Entities.CampaignInvitationResponse> LoadUserStatus(Guid eventId, Guid listId, Guid contactId, CancellationToken token);
    }
}
