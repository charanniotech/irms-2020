﻿using Irms.Domain.Abstract;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories.Base
{
    public interface IReadOnlyBatchRepository<TEntity, in TKey> : IBaseRepository
        where TEntity : IEntity<TKey>
    {
        Task<IEnumerable<TEntity>> GetByIds(IEnumerable<TKey> ids, CancellationToken token);
    }
}
