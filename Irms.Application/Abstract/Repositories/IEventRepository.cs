﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain.Abstract;
using Irms.Domain.Entities;

namespace Irms.Application.Abstract.Repositories
{
    public interface IEventRepository<TEntity, TKey> : IRepository<TEntity, TKey>
    where TEntity : IEntity<TKey>
    {
        Task UpdateFeatures(Event @event, CancellationToken token);
        Task<Event> GetEventWithLocation(Guid eventId, CancellationToken token);
    }
}
