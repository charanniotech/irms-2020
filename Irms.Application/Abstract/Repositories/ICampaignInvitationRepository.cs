﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.CampaignInvitations.CommandHandlers;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.ReadModels;
using Irms.Domain.Abstract;
using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories
{
    public interface ICampaignInvitationRepository<TEntity, TKey> : IRepository<TEntity, TKey>
    where TEntity : IEntity<TKey>
    {
        Task<IEnumerable<(CampaignInvitation Invitation, bool Sms, bool Email, bool Whatsapp)>> GetByCampaignId(Guid id, CancellationToken token);
        Task<CampaignEmailTemplate> GetCampaignEmailTemplateById(TKey id, CancellationToken token);
        Task<CampaignEmailTemplate> GetCampaignEmailTemplateByInvitationId(Guid id, CancellationToken token);
        Task<TKey> CreateCampaignEmailTemplate(CampaignEmailTemplate entity, CancellationToken token);
        Task UpdateCampaignEmailTemplate(CampaignEmailTemplate entity, CancellationToken token);
        Task UpdateCampaignEmailResponseForm(UpdateCampaignEmailResponseFormCmd ci, CancellationToken token);

        Task Delete(Guid id, CancellationToken token);
        Task DeleteEmailTemplate(Guid id, CancellationToken token);
        Task DeleteSmsTemplate(Guid id, CancellationToken token);
        Task UpdateRange(IEnumerable<CampaignInvitation> entities, CancellationToken token);

        Task<CampaignSmsTemplate> GetCampaignSmsTemplateById(Guid id, CancellationToken token);
        Task<CampaignSmsTemplate> GetCampaignSmsTemplateByInvitationId(Guid id, CancellationToken token);
        Task<TKey> CreateCampaignSmsTemplate(CampaignSmsTemplate entity, CancellationToken token);
        Task UpdateCampaignSmsTemplate(CampaignSmsTemplate entity, CancellationToken token);
        Task<ListAnalysisCampaignResponse> GetByGuestListId(Guid guestListId, Guid eventId, CancellationToken token);
        Task<CampaignWhatsappTemplate> GetCampaignWhatsappTemplateById(Guid id, CancellationToken token);
        Task<CampaignWhatsappTemplate> GetCampaignWhatsappTemplateByInvitationId(Guid id, CancellationToken token);

        Task<IEnumerable<CampaignInvitation>> LoadInvitationGroup(Guid eventCampaignId, InvitationType type, CancellationToken token);
        Task<IEnumerable<CampaignInvitation>> LoadInvitationTemplateGroup(Guid eventCampaignId, CancellationToken token);
        Task<IEnumerable<CampaignInvitation>> LoadAcceptedOrRejectedInvitations(Guid invitationId, UserAnswer answer, CancellationToken token);

        Task<IEnumerable<WhatsappApprovedTemplate>> LoadWhatsappApprovedTemplates(InvitationType? invitationType, CancellationToken token);
        Task<Guid> CreateCampaignWhatsappTemplate(CampaignWhatsappTemplate entity, CancellationToken token);
        Task<Guid> UpdateCampaignWhatsappTemplate(CampaignWhatsappTemplate entity, CancellationToken token);
        Task<Guid> UpdateCampaignWhatsappBOTTemplate(CampaignWhatsappTemplate entity, CancellationToken token);
        Task CreateCampaignWhatsappResponseForm(CampaignWhatsappTemplate entity, CancellationToken token);
        Task UpdateCampaignWhatsappResponseForm(CampaignWhatsappTemplate entity, CancellationToken token);
        Task<Guid> CopyCampaignWhatsappTemplate(CampaignWhatsappTemplate existingWhatsapp, CancellationToken token);
        Task DeleteWhatsappTemplate(Guid id, CancellationToken token);
        Task<RfiForm> GetCampaignFormByInvitationId(Guid id, CancellationToken token);
        Task DeleteRfiForm(Guid id, CancellationToken token);
    }
}
