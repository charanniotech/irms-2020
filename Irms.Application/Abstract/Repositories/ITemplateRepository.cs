﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain.Abstract;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories
{
    public interface ITemplateRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        Task<TEntity> GetSmsTemplate(TKey id, CancellationToken token);
        Task<TEntity> GetEmailTemplate(TKey id, CancellationToken token);
    }
}
