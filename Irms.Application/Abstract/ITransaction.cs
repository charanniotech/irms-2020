﻿using System;

namespace Irms.Application.Abstract
{
    public interface ITransaction : IDisposable
    {
        Guid Id { get; }
        void Commit();
        void Rollback();
    }
}
