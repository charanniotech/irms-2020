﻿using System;

namespace Irms.Application.Abstract.Services
{
    public interface ISerializer
    {
        string Serialize(object src);
        T Deserialize<T>(string src);
        object Deserialize(string src, Type type);
    }
}
