﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Irms.Application.Abstract.Services.Notifications
{
    /// <summary>
    /// This class represent minimum requirements for sending the Sms
    /// </summary>
    public class SmsMessage
    {
        public SmsMessage(string messageTemplate, IEnumerable<Recipient> recipients,
                        Guid? campaignInvitationId = null)

        {
            MessageTemplate = messageTemplate;
            Recipients = recipients;
            CampaignInvitationId = campaignInvitationId;
        }

        public string MessageId { get; set; }
        public ProviderType ProviderType { get; set; }
        public string MessageTemplate { get; }
        public IEnumerable<Recipient> Recipients { get; }
        public Guid? CampaignInvitationId { get; set; }

        public class Recipient
        {
            public Recipient(IValidPhoneNumber phone, IEnumerable<TemplateVariable> templateVariables = null)
            {
                Phone = phone.Number;
                TemplateVariables = templateVariables ?? Enumerable.Empty<TemplateVariable>();
            }

            public Recipient(Guid id, string phone, IEnumerable<TemplateVariable> templateVariables = null)
            {
                Id = id;
                Phone = phone;
                TemplateVariables = templateVariables ?? Enumerable.Empty<TemplateVariable>();
            }

            public Guid Id { get; }
            public string Phone { get; }
            public IEnumerable<TemplateVariable> TemplateVariables { get; }
        }

        public class TemplateVariable
        {
            public TemplateVariable(string name, string value)
            {
                Name = name;
                Value = value;
            }

            public string Name { get; }
            public string Value { get; }
        }
    }
}
