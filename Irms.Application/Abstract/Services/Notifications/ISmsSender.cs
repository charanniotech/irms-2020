﻿using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Services.Notifications
{
    /// <summary>
    /// Base interface for resolving SmsSender using MediatR
    /// </summary>
    public interface ISmsSender
    {
        Task<bool> SendSms(SmsMessage message, CancellationToken token);
        Task<bool> SendWebhookSms(SmsMessage sms, CancellationToken token);
    }
}
