﻿using Irms.Domain.Entities.Templates;

namespace Irms.Application.Abstract.Services.Notifications.Notifiers
{
    /// <summary>
    /// Base interface for resolving Notifier using MediatR
    /// </summary>
    public interface INotifierProvider
    {
        INotifier GetNotifier(TemplateType templateType);
    }
}
