﻿using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Services.Notifications.Notifiers
{
    /// <summary>
    /// Interface for sending one time password 
    /// </summary>
    /// <remarks>Used for two factor authentication and for else operations where needed high level of permissions</remarks> 
    public interface IOTPCodeNotifier : INotifier
    {
        Task<bool> NotifyBySms(
            string phoneNo,
            string code,
            CancellationToken cancellationToken);
    }
}
