﻿using Irms.Domain.Entities;
using System;

namespace Irms.Application.Abstract
{
    public interface ICurrentUser
    {
        Guid Id { get; }
        bool IsInRole(RoleType role);
    }
}
