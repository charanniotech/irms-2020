﻿using System;
using MediatR;

namespace Irms.Application.Abstract
{
    public interface ITenantEvent : INotification
    {
        Guid ObjectId { get; }
        Guid TenantId { get; }
        Guid? ObjectId2 { get; }
        Guid? ObjectId3 { get; }
        string Format(Func<Guid, string> x);
    }
}
