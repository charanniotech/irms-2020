﻿using System;
using MediatR;

namespace Irms.Application.Abstract
{
    public interface IEvent : INotification
    {
        Guid ObjectId { get; }
        Guid? ObjectId2 { get; }
        Guid? ObjectId3 { get; }
        string Format(Func<Guid, string> x, string u);
    }
}
