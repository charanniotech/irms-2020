﻿namespace Irms.Application.Abstract
{
    public interface IDomainParser
    {
        string GetSubDomain(string url);
        (string scheme, string host) GetDomain();
        string GetHost(string url);
    }
}
