﻿using System;
using Irms.Application.Abstract;

namespace Irms.Application.Events.Events
{
    public class EventCreated : IEvent
    {
        public EventCreated(Guid eventId)
        {
            EventId = eventId;
        }

        public Guid EventId { get; }

        public Guid ObjectId => EventId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The event {x(EventId)} has been created by {u}";
    }
}
