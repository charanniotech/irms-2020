﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Events.Commands;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Files.Commands;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Events.Events;

namespace Irms.Application.Events.CommandHandlers
{
    public class UpdateEventHandler : IRequestHandler<UpdateEventCmd, Unit>
    {
        private readonly IEventRepository<Event, Guid> _repository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public UpdateEventHandler(
            IEventRepository<Event, Guid> repository,
            IMediator mediator,
            IMapper mapper
            )
        {
            _repository = repository;
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(UpdateEventCmd request, CancellationToken token)
        {
            var @event = await _repository.GetById(request.Id, token);
            _mapper.Map(request, @event);

            @event.Location = _mapper.Map<UpdateEventCmd, EventLocation>(request);

            //upload image to azure storage
            //string fileName = string.Empty;
            if (request.Image != null)
            {
                //delete file from azure
                if (@event.ImagePath.IsNotNullOrEmpty())
                {
                    await _mediator.Send(new DeleteFileCmd(@event.ImagePath), token);
                    @event.SetImagePath(null);
                }

                ////add new logo
                //fileName = $"{FilePath.GetFileDirectory(SystemModule.Events)}{Guid.NewGuid()}_image.png";
                //await _mediator.Send(new UploadFileCmd(fileName, request.Image, FileType.Picture), token);

                string fileName = await UploadExistingImage(request.Image, @event.ImagePath, token);
                @event.SetImagePath(fileName ?? @event.ImagePath);
            }
            else if (@event.ImagePath.IsNotNullOrEmpty())
            {
                await _mediator.Send(new DeleteFileCmd(@event.ImagePath), token);
                @event.SetImagePath(null);
            }

            await _repository.Update(@event, token);

            var e = new EventUpdated(@event.Id);
            await _mediator.Publish(e, token);
            return Unit.Value;
        }

        private async Task<string> UploadExistingImage(string newImagePath, string existingImagePath, CancellationToken token)
        {
            if (!string.IsNullOrEmpty(newImagePath))
            {
                string res = CutExtension(newImagePath);
                if (res.IsNotNullOrEmpty())
                {
                    var path = string.IsNullOrEmpty(existingImagePath) ? $"{FilePath.GetFileDirectory(SystemModule.Events)}{Guid.NewGuid()}_image.{res}" : existingImagePath;
                    await _mediator.Send(new UploadFileCmd(path, newImagePath, FileType.Picture), token);
                    return path;
                }
                return null;
            }

            return null;
        }

        private string CutExtension(string input)
        {
            int length = input.IndexOf(";base64") - "data:image/".Length;
            if (length < 0)
            {
                return string.Empty;
            }
            string extension = input.Substring("data:image/".Length, length);
            return extension;
        }
    }
}
