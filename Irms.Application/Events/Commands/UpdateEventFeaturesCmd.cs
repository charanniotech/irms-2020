﻿using System;
using System.Collections.Generic;
using MediatR;

namespace Irms.Application.Events.Commands
{
    public class UpdateEventFeaturesCmd : IRequest<Unit>
    {
        public Guid Id { get; set; }
        public List<Guid> Features { get; set; }
    }
}
