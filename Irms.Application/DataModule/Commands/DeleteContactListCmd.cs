﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.DataModule.Commands
{
    public class DeleteContactListCmd : IRequest<Unit>
    {
        public DeleteContactListCmd(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }
    }
}
