﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Tenants.Commands;
using Irms.Application.Tenants.Events;
using Irms.Domain.Entities.Tenant;
using Irms.Application.Abstract.Repositories;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.Abstract.Repositories.UserManagement;
using Irms.Application.Abstract.Services;
using Irms.Domain;
using Microsoft.Extensions.Configuration;

namespace Irms.Application.Tenants.CommandHandlers
{
    public class UpdateConfigHandler : IRequestHandler<UpdateConfigCmd, Unit>
    {
        private readonly ITenantRepository<Tenant, Guid> _repository;
        private readonly IUserInfoRepository<UserInfo, Guid> _userRepository;
        private readonly IUserManager _userManager;
        private readonly IUserFactory _factory;
        private readonly IMediator _mediator;
        private readonly ITransactionManager _transactionManager;
        private readonly ICurrentUser _currentUser;
        private readonly IPhoneNumberValidator _phoneValidator;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        private readonly IDNSRecordsManager _dnsRecord;

        public UpdateConfigHandler(
            ITenantRepository<Tenant, Guid> repository,
            IUserInfoRepository<UserInfo, Guid> userRepository,
            IMediator mediator,
            IUserManager userManager,
            ITransactionManager transactionManager,
            IUserFactory factory,
            ICurrentUser currentUser,
            IPhoneNumberValidator phoneValidator,
            IMapper mapper,
            IConfiguration config,
            IDNSRecordsManager dnsRecord)
        {
            _repository = repository;
            _userRepository = userRepository;
            _mediator = mediator;
            _userManager = userManager;
            _transactionManager = transactionManager;
            _factory = factory;
            _currentUser = currentUser;
            _phoneValidator = phoneValidator;
            _mapper = mapper;
            _config = config;
            _dnsRecord = dnsRecord;
        }

        /// <summary>
        /// update tenant basic info handler
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(UpdateConfigCmd request, CancellationToken token)
        {

            var tenant = await _repository.GetById(request.Id, token);
            if (string.IsNullOrEmpty(tenant.ClientUrl))
            {
                await AddTenantAdmin(request, tenant, token);
            }
            else
            {
                await UpdateTenantAdmin(request, tenant, token);
            }

            return Unit.Value;
        }

        private async Task UpdateTenantAdmin(UpdateConfigCmd request, Tenant tenant, CancellationToken token)
        {
            var formattedPhone = _phoneValidator.Validate(request.MobileNo, out var isValid);
            if (!isValid)
            {
                throw new IncorrectRequestException("The mobile number format is invalid");
            }

            var userInfo = await _userRepository.GetTenantAdmin(request.Id, token);
            var formattedDbPhone = _phoneValidator.Validate(userInfo.MobileNo, out var isValidDb);
            if (!isValidDb)
            {
                throw new IncorrectRequestException("The mobile number format is invalid");
            }

            //if phone number changed
            if (!formattedDbPhone.Number.Equals(formattedPhone.Number, StringComparison.Ordinal))
            {
                //check uniqueness of the new phone number
                var user = await _userManager.FindByPhoneNumberAsync(formattedPhone.Number);
                if (user != null)
                {
                    throw new IncorrectRequestException("This mobile number already exists");
                }
            }

            //if email changed
            if (!userInfo.Email.EqualsIgnoreCase(request.Email))
            {
                //check uniqueness of the new email
                var user = await _userManager.FindByEmailAsync(request.Email);
                if (user != null)
                {
                    throw new IncorrectRequestException("This email already exists");
                }
            }

            var aspUser = await _userManager.FindByIdAsync(userInfo.Id);
            if (aspUser == null)
            {
                throw new IncorrectRequestException("The employee doesn't have a user");
            }

            aspUser.PhoneNumber = formattedPhone.Number;
            aspUser.Email = request.Email;
            aspUser.UserName = request.Email;

            userInfo.UpdateProfile(
                request.FirstName,
                request.LastName,
                request.MobileNo,
                request.Email);

            using (var tr = await _transactionManager.BeginTransactionAsync(token))
            {
                //update user
                await _userRepository.Update(userInfo, token);
                await _userManager.UpdateAsync(aspUser);

                if (!request.ClientUrl.Equals(tenant.ClientUrl, StringComparison.OrdinalIgnoreCase))
                {
                    //create subdomain on azure
                    if (!request.HasOwnDomain && tenant.HasOwnDomain == true)
                    {
                        await _dnsRecord.AddSubDomain(request.ClientUrl);
                    }
                    else if (!request.HasOwnDomain)
                    {
                        await _dnsRecord.UpdateSubDomain(tenant.ClientUrl, request.ClientUrl);
                    }
                    else if (tenant.HasOwnDomain == false && request.HasOwnDomain == true)
                    {
                        await _dnsRecord.DeleteSubDomain(tenant.ClientUrl);
                    }
                }
                //update tenant
                _mapper.Map(request, tenant);
                tenant.HasOwnDomain = request.HasOwnDomain;
                await _repository.Update(tenant, token);
                tr.Commit();
            }
        }

        private async Task AddTenantAdmin(UpdateConfigCmd request, Tenant tenant, CancellationToken token)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user != null)
            {
                throw new IncorrectRequestException("This email already exists");
            }

            var formattedPhone = _phoneValidator.Validate(request.MobileNo, out var isValid);
            if (!isValid)
            {
                throw new IncorrectRequestException("The phone number format is invalid");
            }

            user = await _userManager.FindByPhoneNumberAsync(formattedPhone.Number);
            if (user != null)
            {
                throw new IncorrectRequestException("This mobile number already exists");
            }

            using (var tr = await _transactionManager.BeginTransactionAsync(token))
            {
                //add aspnet user
                user = _factory.CreateUserForTenant(
                id: Guid.NewGuid(),
                tenantId: tenant.Id,
                userName: request.Email,
                email: request.Email,
                phoneNumber: formattedPhone.Number,
                isEnabled: true,
                emailConfirmed: true,
                phoneNumberConfirmed: true);

                var creationResult = await _userManager.CreateAsync(user, "Test@123");// request.Password);
                if (!creationResult.Succeeded)
                {
                    throw new IncorrectRequestException(creationResult.GetErrorMessage());
                }

                var addToRoleResult = await _userManager.AddToRole(user, RoleType.TenantAdmin);
                if (!addToRoleResult.Succeeded)
                {
                    throw new IncorrectRequestException(addToRoleResult.GetErrorMessage());
                }

                //add user info
                UserInfo userInfo = _mapper.Map<UserInfo>(request);
                userInfo.Id = user.Id;
                userInfo.Role = RoleType.TenantAdmin;
                userInfo.TenantId = tenant.Id;
                userInfo.UserId = user.Id;

                await _userRepository.Create(userInfo, token);

                //update tenant
                _mapper.Map(request, tenant);

                //setting up default test/email config
                tenant.SetDefaultConfig(_config["Email:ApiKey"],
                    _config["Sms:AccountSid"],
                    _config["Sms:AuthToken"],
                    _config["Sms:NotificationServiceId"]
                    );
                await _repository.Update(tenant, token);

                //create subdomain on azure
                if (!request.HasOwnDomain)
                {
                    await _dnsRecord.AddSubDomain(request.ClientUrl);
                }

                tr.Commit();
            }

            var e = new TenantConfigUpdated(tenant.Id);
            await _mediator.Publish(e, token);
        }
    }
}
