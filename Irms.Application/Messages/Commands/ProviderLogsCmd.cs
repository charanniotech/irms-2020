﻿using System;
using Irms.Domain.Entities;
using MediatR;

namespace Irms.Application.Events.Commands
{
    public class ProviderLogsCmd : IRequest<Unit>
    {
        public Guid? ContactId { get; set; }
        public Guid? UserId { get; set; }
        public ProviderType ProviderType { get; set; }
        public string Request { get; set; }
        public DateTime RequestDate { get; set; }
        public string Response { get; set; }
        public DateTime? ResponseDate { get; set; }
        public Guid TenantId { get; set; }
        public Guid? CampaignInvitationId { get; set; }
    }
}
