﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.ProductServices.Events
{
    public class ServiceCreated : IEvent
    {
        public ServiceCreated(Guid serviceId)
        {
            ServiceId = serviceId;
        }

        public Guid ServiceId { get; }

        public Guid ObjectId => ServiceId;
        public Guid? ObjectId2 => default;
        public Guid? ObjectId3 => default;

        public string Format(Func<Guid, string> x, string u) => $"The service {x(ServiceId)} has been created by {u}";
    }
}
