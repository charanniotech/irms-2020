﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.ProductServices.Commands
{
    public class AddServiceToProductCmd : IRequest<Unit>
    {
        public Guid ProductId { get; set; }
        public Guid ServiceId { get; set; }
    }
}
