﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.ProductServices.Commands
{
    public class UpdateServiceStatusCmd : IRequest<Unit>
    {
        public Guid ServiceId { get; set; }
        public bool IsServiceActive { get; set; }
    }
}
