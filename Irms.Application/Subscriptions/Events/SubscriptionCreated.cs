﻿using System;
using Irms.Application.Abstract;

namespace Irms.Application.Subscriptions.Events
{
    public class SubscriptionCreated : IEvent
    {
        public SubscriptionCreated(Guid subscriptionId)
        {
            SubscriptionId = subscriptionId;
        }

        public Guid SubscriptionId { get; }

        public Guid ObjectId => SubscriptionId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The subscription {x(SubscriptionId)} has been created by {u}";
    }
}
