﻿using Irms.Domain.Entities.Templates;
using MediatR;

namespace Irms.Application.Templates.Queries
{
    /// <summary>
    /// Request object based on which handler return active SMS template 
    /// </summary>
    public class GetActiveSmsTemplate : IRequest<Template>
    {
        public GetActiveSmsTemplate(TemplateType templateType)
        {
            TemplateType = templateType;
        }

        public TemplateType TemplateType { get; }
    }
}
