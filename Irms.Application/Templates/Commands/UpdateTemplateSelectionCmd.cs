﻿using System;
using MediatR;

namespace Irms.Application.Templates.Commands
{
    /// <summary>
    /// Request object for managing status of SMS or Email templates 
    /// </summary>
    public class UpdateTemplateSelectionCmd : IRequest<Unit>
    {
        public UpdateTemplateSelectionCmd(
            Guid id,
            bool isEnabled,
            Guid? emailTemplateId,
            Guid? smsTemplateId)
        {
            Id = id;
            IsEnabled = isEnabled;
            EmailTemplateId = emailTemplateId;
            SmsTemplateId = smsTemplateId;
        }

        public Guid Id { get; }
        public bool IsEnabled { get; }
        public Guid? EmailTemplateId { get; }
        public Guid? SmsTemplateId { get; }
    }
}
