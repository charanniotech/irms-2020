﻿using Irms.Domain.Entities.Templates;
using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.Templates.Commands
{
    /// <summary>
    /// Request object based on which an SMS or Email template will updated
    /// </summary>
    public class UpdateTemplateCmd : IRequest<Unit>
    {
        public UpdateTemplateCmd(
            Guid id,
            string name,
            bool emailCompatible,
            bool smsCompatible,
            string emailSubject,
            string emailBody,
            string smsText,
            IEnumerable<TemplateTranslation> translations)
        {
            Id = id;
            Name = name;
            EmailCompatible = emailCompatible;
            SmsCompatible = smsCompatible;
            EmailSubject = emailSubject;
            EmailBody = emailBody;
            SmsText = smsText;
            Translations = translations;
        }

        public Guid Id { get; }
        public string Name { get; }

        public bool EmailCompatible { get; }
        public bool SmsCompatible { get; }

        public string EmailSubject { get; }
        public string EmailBody { get; }

        public string SmsText { get; }
        public IEnumerable<TemplateTranslation> Translations { get; }
    }
}
