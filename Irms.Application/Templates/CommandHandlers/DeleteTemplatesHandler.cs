﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.Templates.Commands;
using Irms.Application.Templates.Events;
using Irms.Domain.Entities.Templates;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Templates.CommandHandlers
{
    /// <summary>
    /// Handler for removing SMS or Email template from DB based on request <see cref="DeleteTemplatesCmd"/>
    /// </summary>
    public class DeleteTemplatesHandler : IRequestHandler<DeleteTemplatesCmd, Unit>
    {
        private readonly IRepository<Template, Guid> _repository;
        private readonly IMediator _mediator;

        public DeleteTemplatesHandler(IRepository<Template, Guid> repository, IMediator mediator)
        {
            _repository = repository;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(DeleteTemplatesCmd message, CancellationToken cancellationToken)
        {
            foreach (var id in message.Ids)
            {
                await _repository.Delete(id, cancellationToken);
                await _mediator.Publish(new TemplateDeleted(id), cancellationToken);
            }

            return Unit.Value;
        }
    }
}
