﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.Templates.Commands;
using Irms.Application.Templates.Events;
using Irms.Domain.Entities.Templates;
using MediatR;

namespace Irms.Application.Templates.CommandHandlers
{
    /// <summary>
    /// Handler for managing SMS or Email template in DB based on request <see cref="UpdateTemplateSelectionCmd"/>
    /// </summary>
    public class UpdateTemplateSelectionHandler : IRequestHandler<UpdateTemplateSelectionCmd, Unit>
    {
        private readonly IRepository<TemplateSelection, Guid> _repository;
        private readonly IMediator _mediator;

        public UpdateTemplateSelectionHandler(IRepository<TemplateSelection, Guid> repository, IMediator mediator)
        {
            _repository = repository;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(UpdateTemplateSelectionCmd request, CancellationToken cancellationToken)
        {
            var templateSelection = await _repository.GetById(request.Id, cancellationToken);

            templateSelection.Update(
                request.IsEnabled,
                request.SmsTemplateId,
                request.EmailTemplateId);


            await _repository.Update(templateSelection, cancellationToken);

            var e = new TemplateSelectionUpdated(
                templateSelection.Type,
                request.EmailTemplateId,
                request.SmsTemplateId);

            await _mediator.Publish(e, cancellationToken);
            return Unit.Value;
        }
    }
}
