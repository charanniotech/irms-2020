﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.Templates.Events
{
    public class TemplateDeleted : IEvent
    {
        public TemplateDeleted(Guid templateId)
        {
            TemplateId = templateId;
        }

        public Guid TemplateId { get; }

        public Guid ObjectId => TemplateId;
        public Guid? ObjectId2 => default;
        public Guid? ObjectId3 => default;

        public string Format(Func<Guid, string> x, string u) => $"The template {x(TemplateId)} was deleted by {u}";
    }
}
