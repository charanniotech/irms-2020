﻿using System;
using Irms.Application.Abstract;

namespace Irms.Application.Files.Events
{
    public class FileDeleted : IEvent
    {
        public FileDeleted(string fileName)
        {
            FileName = fileName;
        }


        public string FileName { get; }

        public Guid ObjectId => Guid.Empty;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The file  {FileName} has been deleted by {u}";
    }
}
