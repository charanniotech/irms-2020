﻿using System;
using MediatR;

namespace Irms.Application.Files.Commands
{
    public class DeleteFileCmd : IRequest<Unit>
    {
        public DeleteFileCmd(string fileName)
        {
            FileName = fileName;
        }

        public string FileName { get; }
    }
}
