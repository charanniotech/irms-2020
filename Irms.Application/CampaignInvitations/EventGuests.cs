﻿using System;

namespace Irms.Application.CampaignInvitations
{
    public class EventGuest
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string PreferredName { get; set; }
        //public string AcceptedLink { get; set; }
        //public string RejectedLink { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string SenderName { get; set; }
        public string EmailAcceptedLink { get; set; }
        public string EmailRejectedLink { get; set; }
        public string SmsAcceptedLink { get; set; }
        public string WhatsappAcceptedLink { get; set; }
        public string WhatsappRejectedLink { get; set; }
        public string SmsRejectedLink { get; set; }
    }
}