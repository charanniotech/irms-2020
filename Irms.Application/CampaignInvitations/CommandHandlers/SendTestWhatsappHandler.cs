﻿using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Services;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class SendTestWhatsappHandler : IRequestHandler<SendTestWhatsappCmd, IEnumerable<string>>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IWhatsappSender _whatsappSender;
        private readonly IPhoneNumberValidator _phoneValidator;

        public SendTestWhatsappHandler(ICampaignInvitationRepository<CampaignInvitation,
            Guid> repo,
            IMediator mediator,
            IWhatsappSender whatsappSender,
            IPhoneNumberValidator phoneValidator)
        {
            _repo = repo;
            _mediator = mediator;
            _whatsappSender = whatsappSender;
            _phoneValidator = phoneValidator;
        }

        public async Task<IEnumerable<string>> Handle(SendTestWhatsappCmd request, CancellationToken token)
        {
            var result = await _repo.GetCampaignWhatsappTemplateById(request.InvitationId, token);

            if (result == null)
            {
                throw new IncorrectRequestException("Template was not found");
            }

            var whatsappBody = result.Body;

            if (request.WhatsappList.Count() == 0)
            {
                return null;
            }

            foreach (var recipientPhone in request.WhatsappList)
            {
                var formattedPhone = _phoneValidator.Validate(recipientPhone, out var isValid);
                if (!isValid)
                {
                    throw new IncorrectRequestException("Phone number format is incorrect");
                }

                var recipient = new WhatsappMessage.Recipient(formattedPhone).Enumerate();
                var whatsappMessage = new WhatsappMessage(whatsappBody, recipient);
                await _whatsappSender.SendWhatsappMessage(whatsappMessage, token);
            }

            await _mediator.Publish(new CampaignWhatsappSent(request.WhatsappList), token);
            return request.WhatsappList;
        }
    }
}
