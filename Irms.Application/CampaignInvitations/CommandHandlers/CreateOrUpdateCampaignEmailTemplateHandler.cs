﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Campaigns.Commands;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.Events;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using Irms.Application.Files.Commands;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class CreateOrUpdateCampaignEmailTemplateHandler : IRequestHandler<CreateOrUpdateCampaignEmailTemplateCmd, Guid>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;

        public CreateOrUpdateCampaignEmailTemplateHandler(
            ICampaignInvitationRepository<CampaignInvitation, Guid> repo,
            IMediator mediator,
            IMapper mapper,
            IConfiguration config
            )
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
            _config = config;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(CreateOrUpdateCampaignEmailTemplateCmd message, CancellationToken token)
        {
            var ci = _mapper.Map<CampaignEmailTemplate>(message);

            var inv = await _repo.GetCampaignEmailTemplateById(message.Id, token);
            if (inv == null)
            {
                ci.Create();
                ci.BackgroundImagePath = await UploadNewImage(message.BackgroundImagePath, token);
                ci.Body = await UploadBodyImages(ci.Body, token);
                ci.PlainBody = ci.PlainBody ?? string.Empty;
                await _repo.CreateCampaignEmailTemplate(ci, token);
            }
            else
            {
                ci.BackgroundImagePath = await UploadExistingImage(message.BackgroundImagePath, inv.BackgroundImagePath, token);
                ci.Body = await UploadExistingBodyImages(ci.Body, inv.Body, token);
                ci.PlainBody = ci.PlainBody ?? string.Empty;
                await _repo.UpdateCampaignEmailTemplate(ci, token);
            }

            await _mediator.Publish(new CampaignEmailTemplateCreatedOrUpdated(ci.Id), token);
            return ci.Id;
        }

        #region extract image from body and save to azure
        private async Task<string> UploadBodyImages(string body, CancellationToken token)
        {
            var images = GetImagesInHTMLString(body);
            foreach (var img in images)
            {
                string path = $"{FilePath.GetFileDirectory(SystemModule.CampaignEmail)}{Guid.NewGuid()}_image.png";
                await _mediator.Send(new UploadFileCmd(path, img, FileType.Picture), token);
                body = body.Replace(img, $"{_config["AzureStorage:BaseUrl"]}{path}");
            }
            return body;
        }

        private async Task<string> UploadExistingBodyImages(string newBody, string existingBody, CancellationToken token)
        {
            var newImages = GetImagesInHTMLString(newBody);
            var existingImages = GetImagesInHTMLString(existingBody);
            var (toAdd, toDel, toUpd) = existingImages.Compare(newImages, ((n, o) => n == o));

            //add
            foreach (var img in toAdd)
            {
                string path = $"{FilePath.GetFileDirectory(SystemModule.CampaignEmail)}{Guid.NewGuid()}_image.png";
                await _mediator.Send(new UploadFileCmd(path, img, FileType.Picture), token);
                newBody = newBody.Replace(img, $"{_config["AzureStorage:BaseUrl"]}{path}");
            }

            //del
            foreach (var img in toDel)
            {
                string path = img.Replace($"{_config["AzureStorage:BaseUrl"]}", string.Empty);
                await _mediator.Send(new DeleteFileCmd(path), token);
            }

            return newBody;
        }

        private List<string> GetImagesInHTMLString(string htmlString)
        {
            List<string> images = new List<string>();
            string pattern = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(htmlString);

            for (int i = 0, l = matches.Count; i < l; i++)
            {
                images.Add(matches[i].Groups[1].Value);
            }

            return images;
        }

        #endregion

        /// <summary>
        /// Upload new image
        /// </summary>
        /// <param name="backgroundImagePath">image</param>
        /// <returns>image path</returns>
        private async Task<string> UploadNewImage(string backgroundImagePath, CancellationToken token)
        {
            if (!string.IsNullOrEmpty(backgroundImagePath))
            {
                string res = CutExtension(backgroundImagePath);

                var path = $"{FilePath.GetFileDirectory(SystemModule.CampaignEmail)}{Guid.NewGuid()}_image.{res}";
                await _mediator.Send(new UploadFileCmd(path, backgroundImagePath, FileType.Picture), token);

                return path;
            }

            return string.Empty;
        }


        /// <summary>
        /// Override image
        /// </summary>
        /// <param name="backgroundImagePath">image</param>
        /// <returns>image path</returns>
        private async Task<string> UploadExistingImage(string newImagePath, string existingImagePath, CancellationToken token)
        {
            if (!string.IsNullOrEmpty(newImagePath))
            {
                string res = CutExtension(newImagePath);
                if (res.IsNotNullOrEmpty())
                {
                    if (existingImagePath.IsNotNullOrEmpty())
                    {
                        await _mediator.Send(new DeleteFileCmd(existingImagePath), token);
                    }

                    var path = $"{FilePath.GetFileDirectory(SystemModule.CampaignEmail)}{Guid.NewGuid()}_image.{res}";
                    await _mediator.Send(new UploadFileCmd(path, newImagePath, FileType.Picture), token);
                    return path;
                }
                return "";
            }

            return string.Empty;
        }

        private string CutExtension(string input)
        {
            int length = input.IndexOf(";base64") - "data:image/".Length;
            if (length < 0)
            {
                return string.Empty;
            }
            string extension = input.Substring("data:image/".Length, length);
            return extension;
        }

        private string CutImageFromString(string input)
        {
            int index = input.IndexOf(";base64,") + ";base64,".Length;
            if (index < 0)
            {
                return string.Empty;
            }
            return input.Substring(index);
        }
    }
}