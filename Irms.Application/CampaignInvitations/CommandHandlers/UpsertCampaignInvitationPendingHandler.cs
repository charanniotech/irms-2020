﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Campaigns.Commands;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.Events;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class UpsertCampaignInvitationPendingHandler : IRequestHandler<UpsertCampaignInvitationPendingCmd, Guid>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public UpsertCampaignInvitationPendingHandler(
            ICampaignInvitationRepository<CampaignInvitation, Guid> repo,
            IMediator mediator,
            IMapper mapper
            )
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(UpsertCampaignInvitationPendingCmd message, CancellationToken token)
        {
            var ci = _mapper.Map<CampaignInvitation>(message);

            var inv = await _repo.GetById(message.Id, token);
            if (inv == null)
            {
                ci.CreateInvitationPending();
                await _repo.Create(ci, token);
            }
            else
            {
                ci.UpdateInvitationPending(inv);
                await _repo.Update(ci, token);
            }

            await _mediator.Publish(new CampaignInvitationPendingUpserted(ci.Id), token);
            return ci.Id;
        }
    }
}