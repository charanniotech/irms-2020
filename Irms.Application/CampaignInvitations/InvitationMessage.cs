﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Irms.Application.CampaignInvitations
{
    public class InvitationMessage
    {
        public Guid CampaignInvitationId { get; set; }
        public Guid TenantId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public List<string> Placeholders { get; set; }
        public List<EventGuest> Recipients { get; set; }
        public string EventName { get; set; }
        public DateTime EventDateTime { get; set; }
        public string EventLocation { get; set; }
        public string Sender { get; set; }
    }

    public class InstantBotResponse
    {
        public Guid CampaignInvitationId { get; set; }
        public Guid TenantId { get; set; }
        public string Body { get; set; }
        public string To { get; set; }
        public bool HasInvalidCode { get; set; }
        public CampaignInvitationResponse InvitationResponse { get; set; }
        public Guid ContactId { get; set; }
        public UserAnswer Answer { get; set; }
    }
}
