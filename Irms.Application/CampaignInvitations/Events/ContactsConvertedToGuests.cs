﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitations.Events
{
    public class ContactsConvertedToGuests : IEvent
    {
        public ContactsConvertedToGuests(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"Persons was converted to guests for campaign {x(Id)} by {u}";
    }
}
