﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class UpdateCampaignWhatsappResponseFormCmd : IRequest<Unit>
    {
        public Guid CampaignInvitationId { get; set; }
        public string RsvpHtml { get; set; }
        public string WelcomeHtml { get; set; }
        public string AcceptHtml { get; set; }
        public string RejectHtml { get; set; }
        public string ThemeJson { get; set; }
        public string FormSettings { get; set; }
        public string BackgroundImagePath { get; set; }
        public string ProceedButtonText { get; set; }
        public string AcceptButtonText { get; set; }
        public string RejectButtonText { get; set; }
    }
}
