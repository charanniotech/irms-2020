﻿using MediatR;
using System;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class CopyInvitationTemplateCmd : IRequest<Guid>
    {
        public Guid CampaignInvitationId { get; set; }
        public CopyTemplateType CopyTemplateType { get; set; }
    }
}
