﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitations.Commands
{
    public class CreateResponsesForInvitationCmd : IRequest<Guid>
    {
        public CreateResponsesForInvitationCmd(Guid id)
        {
            InvitationId = id;
        }
        public Guid InvitationId { get; set; }
    }
}
