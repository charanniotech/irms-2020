﻿using System;
using MediatR;

namespace Irms.Application.Campaigns.Commands
{
    public class CreateOrUpdateCampaignEmailTemplateCmd : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string Subject { get; set; }
        public string Preheader { get; set; }
        public string SenderEmail { get; set; }
        public string Body { get; set; }
        public string PlainBody { get; set; }
        public string AcceptHtml { get; set; }
        public string RejectHtml { get; set; }
        public string ThemeJson { get; set; }
        public string BackgroundImagePath { get; set; }
    }
}
