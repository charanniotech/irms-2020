﻿using System;
using System.Collections.Generic;
using MediatR;

namespace Irms.Application.Campaigns.Commands
{
    public class SendTestEmailCmd : IRequest<bool>
    {
        public Guid Id { get; set; }
        public List<string> Recipients { get; set; }
    }
}
