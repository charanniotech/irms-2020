﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.DataModule.Commands
{
    public class UpdateEventAdmissionCmd : IRequest<Unit>
    {
        public Guid EventId { get; set; }
        public Guid ListId { get; set; }
        public Guid GuestId { get; set; }
        public string Status { get; set; }
        public string Seat { get; set; }
        public string Parking { get; set; }
        public string BadgeUrl { get; set; }
    }
}
