﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.DataModule.Commands
{
    public class UpdateOrganizationInfoCmd : IRequest<Unit>
    {
        public Guid Id { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
    }
}
