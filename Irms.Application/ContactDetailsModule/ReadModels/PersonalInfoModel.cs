﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.DataModule.ReadModels
{
    public class PersonalInfoModel
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
    }
}
