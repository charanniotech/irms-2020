﻿using MediatR;
using System;

namespace Irms.Application.RfiForms.Commands
{
    public class UpsertRfiFormCmd : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string WelcomeHtml { get; set; }
        public string SubmitHtml { get; set; }
        public string ThanksHtml { get; set; }
        public string FormTheme { get; set; }
        public string FormSettings { get; set; }
        public string ThemeBackgroundImage { get; set; }
        public string QuestionJson { get; set; }
    }
}
