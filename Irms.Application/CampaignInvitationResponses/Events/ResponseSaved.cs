﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitationResponses.Events
{
    public class ResponseSaved : IEvent
    {
        public ResponseSaved(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"Invitation response {x(Id)} has been created/updated by {u}";
    }
}
