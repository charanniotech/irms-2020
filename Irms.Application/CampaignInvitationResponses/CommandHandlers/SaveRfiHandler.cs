﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitationResponses.Commands;
using Irms.Application.CampaignInvitationResponses.Events;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitationResponses.CommandHandlers
{
    public class SaveRfiHandler : IRequestHandler<SaveRfiCmd, Guid>
    {
        private readonly ICampaignInvitationResponseRepository<Domain.Entities.CampaignInvitationResponse, Guid> _repo;
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepo;
        private readonly ICustomFieldRepository<Domain.Entities.CustomField, Guid> _customFieldRepo;
        private readonly IMediator _mediator;
        public SaveRfiHandler(
            IMediator mediator,
            ICampaignInvitationResponseRepository<Domain.Entities.CampaignInvitationResponse, Guid> repo,
            IContactRepository<Domain.Entities.Contact, Guid> contactRepo,
            ICustomFieldRepository<Domain.Entities.CustomField, Guid> customFieldRepo
        )
        {
            _mediator = mediator;
            _repo = repo;
            _contactRepo = contactRepo;
            _customFieldRepo = customFieldRepo;
        }

        public async Task<Guid> Handle(SaveRfiCmd request, CancellationToken token)
        {
            var template = await _repo.GetById(request.Id, token);

            var rfiId = await _repo.LoadRfiFormIdByResponseId(request.Id, token);

            if (await _repo.HasAnswersOnRfi(rfiId, template.ContactId, token))
            {
                throw new ArgumentException($"You have already answered to this invitation! Your answer was {template.Answer}");
            }

            var questionIds = request.Answers
                .Select(x => x.Id)
                .ToList();

            var questions = await _repo.LoadQuestions(questionIds, token);
            var contact = await _contactRepo.GetById(template.ContactId, token);

            var responses = new List<RfiFormResponse>();
            //mediator send
            foreach (var question in questions)
            {
                bool wasContactChanged = false;
                var answer = request.Answers
                    .FirstOrDefault(x => x.Id == question.Id);

                responses.Add(new RfiFormResponse
                {
                    TenantId = template.TenantId,
                    ContactId = template.ContactId,
                    Answer = answer.Answer,
                    QuestionId = question.Id,
                    RfiFormId = rfiId
                });

                if (question.Mapped && answer.Answer.IsNotNullOrEmpty())
                {
                    if (question.MappedField.Equals("Title", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.Title = answer.Answer;
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("FullName", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.FullName = answer.Answer;
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("PrefferedName", StringComparison.OrdinalIgnoreCase) || question.MappedField.Equals("PreferredName", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.PreferredName = answer.Answer;
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("Gender", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.GenderId = answer.Answer == "Male" ? 0 : 1;
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("Email", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.Email = answer.Answer;
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("AlternateEmail", StringComparison.OrdinalIgnoreCase) || question.MappedField.Equals("AlternativeEmail", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.AlternativeEmail = answer.Answer;
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("MobileNumber", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.MobileNumber = answer.Answer;
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("WorkNumber", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.WorkNumber = answer.Answer;
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("SecondaryMobileNumber", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.SecondaryMobileNumber = answer.Answer;
                        wasContactChanged = true;
                    }

                    //to do
                    if (question.MappedField.Equals("Nationality", StringComparison.OrdinalIgnoreCase) || question.MappedField.Equals("NationalityId", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.NationalityId = Guid.Parse(answer.Answer);
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("DocumentType", StringComparison.OrdinalIgnoreCase) || question.MappedField.Equals("DocumentTypeId", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.DocumentTypeId = Convert.ToInt32(answer.Answer);
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("DocumentNumber", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.DocumentNumber = answer.Answer;
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("Organization", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.Organization = answer.Answer;
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("Position", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.Position = answer.Answer;
                        wasContactChanged = true;
                    }
                    if (question.MappedField.Equals("IssuingCountryId", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.IssuingCountryId = Guid.Parse(answer.Answer);
                        wasContactChanged = true;
                    }

                    if (question.MappedField.Equals("ExpirationDate", StringComparison.OrdinalIgnoreCase))
                    {
                        contact.ExpirationDate = Convert.ToDateTime(answer.Answer);
                        wasContactChanged = true;
                    }

                    if (!wasContactChanged && !string.IsNullOrEmpty(question.MappedField))
                    {
                        //custom field
                        await _customFieldRepo.UpdateContactCustomField(contact.Id, question.MappedField, answer.Answer, token);
                    }
                }
                if (wasContactChanged)
                {
                    await _contactRepo.Update(contact, token);
                }
            }

            template.ResponseMediaType = request.MediaType;
            template.Answer = UserAnswer.EXPIRED;
            template.ResponseDate = DateTime.UtcNow;

            await _repo.SaveRfiAnswers(responses, token);
            await _repo.Update(template, token);

            await _mediator.Publish(new RfiSaved(request.Id), token);

            return request.Id;
        }
    }
}
