﻿using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitationResponses.Commands;
using Irms.Application.CampaignInvitationResponses.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitationResponses.CommandHandlers
{
    public class SaveResponseHandler : IRequestHandler<SaveResponseCmd, Guid>
    {
        private readonly ICampaignInvitationResponseRepository<Domain.Entities.CampaignInvitationResponse, Guid> _repo;
        private readonly ICampaignInvitationRepository<Domain.Entities.CampaignInvitation, Guid> _invitationRepo;
        private readonly ICampaignInvitationNextRunRepository<Domain.Entities.CampaignInvitationNextRun, Guid> _nextRunRepo;
        private readonly IMediator _mediator;
        private readonly IHangfireJobsWorker _hangfireWorker;
        public SaveResponseHandler(
            IMediator mediator,
            ICampaignInvitationResponseRepository<Domain.Entities.CampaignInvitationResponse, Guid> repo,
            ICampaignInvitationRepository<Domain.Entities.CampaignInvitation, Guid> invitationRepo,
            ICampaignInvitationNextRunRepository<Domain.Entities.CampaignInvitationNextRun, Guid> nextRunRepo,
            IHangfireJobsWorker hangfireWorker
        )
        {
            _mediator = mediator;
            _repo = repo;
            _invitationRepo = invitationRepo;
            _nextRunRepo = nextRunRepo;
            _hangfireWorker = hangfireWorker;
        }

        public async Task<Guid> Handle(SaveResponseCmd request, CancellationToken token)
        {
            var template = await _repo.GetById(request.Id, token);

            if (template.Answer.HasValue && template.Answer != UserAnswer.NOT_ANSWERED)
            {
                throw new ArgumentException($"You have already answered to this invitation! Your answer was {template.Answer}");
            }

            template.Answer = request.Answer;
            template.ResponseMediaType = request.MediaType;
            template.ResponseDate = DateTime.UtcNow;

            await _repo.Update(template, token);

            var invitations = (await _invitationRepo.LoadAcceptedOrRejectedInvitations(template.CampaignInvitationId, request.Answer, token))
                //.Where(x => x.StartDate < DateTime.UtcNow)
                .OrderBy(x => x.SortOrder)
                .ToList();

            var nextDate = DateTime.UtcNow;

            for (var i = 0; i < invitations.Count; ++i)
            {
                var currentInvitation = invitations[i];

                if (!currentInvitation.IsInstant)
                {
                    nextDate = nextDate + ToTimeSpan(currentInvitation.Interval.Value, currentInvitation.IntervalType.Value);
                }

                currentInvitation.StartDate = nextDate;
            }

            for (var i = 0; i < invitations.Count; ++i)
            {
                if (request.Answer == UserAnswer.ACCEPTED)
                    _hangfireWorker.SetupAcceptedSending(invitations[i].Id, template.ContactId, invitations[i].StartDate);
                else if (request.Answer == UserAnswer.REJECTED)
                    _hangfireWorker.SetupRejectedSending(invitations[i].Id, template.ContactId, invitations[i].StartDate);
                else
                    throw new IncorrectRequestException($"Answer should be accepted or rejected, but it is {request.Answer}");
            }

            // Just prepare and fire jobs
            await _invitationRepo.UpdateRange(invitations, token);

            await _mediator.Publish(new ResponseSaved(request.Id), token);

            return request.Id;
        }

        private TimeSpan ToTimeSpan(int interval, IntervalType intervalType)
        {
            switch (intervalType)
            {
                case IntervalType.Days:
                    return TimeSpan.FromDays(interval);
                case IntervalType.Hours:
                    return TimeSpan.FromHours(interval);
                case IntervalType.Minutes:
                    return TimeSpan.FromMinutes(interval);
                default:
                    throw new ArgumentException("interval type is invalid");
            }
        }
    }
}
