﻿using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories.Base;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application
{
    public class TenantAuditLog : INotificationHandler<ITenantEvent>
    {
        private readonly IEventLogRecorder _recorder;

        public TenantAuditLog(IEventLogRecorder recorder)
        {
            _recorder = recorder;
        }

        public Task Handle(ITenantEvent notification, CancellationToken cancellationToken) => _recorder.AppendEventToTenantLog(notification, cancellationToken);
    }
}
