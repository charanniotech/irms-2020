﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.ListAnalysis.Commands
{
    public class CreateListAnalysisImportantFieldsCmd : IRequest<Unit>
    {
        public CreateListAnalysisImportantFieldsCmd(Guid listId, IEnumerable<Field> reservedFields, IEnumerable<Field> customFields)
        {
            ListId = listId;
            ReservedFields = reservedFields;
            CustomFields = customFields;
        }

        public Guid ListId { get; }
        public IEnumerable<Field> ReservedFields { get; }
        public IEnumerable<Field> CustomFields { get; }
    }

    public class Field
    {
        public string Value { get; set; }
        public bool IsRequired { get; set; }
    }
}