﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Abstract.Repositories;
using Irms.Application.ListAnalysis.Commands;
using Irms.Domain.Entities;
using Irms.Application.Campaigns.ReadModels;
using Irms.Application.ListAnalysis.Events;
using Irms.Application.Abstract;
using Irms.Domain;

namespace Irms.Application.ListAnalysis.CommandHandlers
{
    public class SetListAnalysisStateHandler : IRequestHandler<SetListAnalysisStateCmd, Unit>
    {
        private readonly IListAnalysisRepository<Domain.Entities.ListAnalysis, Guid> _repo;
        private readonly IMediator _mediator;

        public SetListAnalysisStateHandler(
            IListAnalysisRepository<Domain.Entities.ListAnalysis, Guid> repo,
            IMediator mediator)
        {
            _repo = repo;
            _mediator = mediator;
        }

        /// <summary>
        /// deletes invitation (with templates)
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(SetListAnalysisStateCmd request, CancellationToken token)
        {
            await _repo.SetListAnalysisState(request, token);
            await _mediator.Publish(new ListAnalysisStateSet(request.ListId), token);
            return Unit.Value;
        }
    }
}