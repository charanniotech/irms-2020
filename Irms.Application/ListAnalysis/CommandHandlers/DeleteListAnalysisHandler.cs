﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Abstract.Repositories;
using Irms.Application.ListAnalysis.Commands;
using Irms.Domain.Entities;
using Irms.Application.Campaigns.ReadModels;
using Irms.Application.ListAnalysis.Events;
using Irms.Application.Abstract;
using Irms.Domain;

namespace Irms.Application.ListAnalysis.CommandHandlers
{
    public class DeleteListAnalysisHandler : IRequestHandler<DeleteListAnalysisCmd, Guid>
    {
        private readonly IListAnalysisRepository<Domain.Entities.ListAnalysis, Guid> _repo;
        private readonly ICampaignRepository<Campaign, Guid> _campaignRepo;
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _invitationRepo;
        private readonly IMediator _mediator;
        private readonly ITransactionManager _transaction;

        public DeleteListAnalysisHandler(
            IListAnalysisRepository<Domain.Entities.ListAnalysis, Guid> repo,
            ICampaignRepository<Campaign, Guid> campaignRepo,
            ICampaignInvitationRepository<CampaignInvitation, Guid> invitationRepo,
            IMediator mediator,
            ITransactionManager transaction)
        {
            _repo = repo;
            _campaignRepo = campaignRepo;
            _invitationRepo = invitationRepo;
            _mediator = mediator;
            _transaction = transaction;
        }

        /// <summary>
        /// deletes invitation (with templates)
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(DeleteListAnalysisCmd request, CancellationToken token)
        {
            var li = await _invitationRepo.GetByGuestListId(request.GuestListId, request.EventId, token);

            bool isDeleted = true;
            if (li != null)
            {
                using (var tr = await _transaction.BeginTransactionAsync(token))
                {
                    try
                    {
                        await DeleteTemplatesIfExist(li, token);
                        await _repo.DeleteCampaign(li.CampaignId, token);
                        tr.Commit();
                    }
                    catch
                    {
                        tr.Rollback();
                        isDeleted = false;
                    }
                }
            }

            //delete important fields
            if (isDeleted)
            {
                await _repo.Delete(request.GuestListId, token);
                await _repo.ResetListAnalysisState(request.GuestListId, request.EventId, token);

                await _mediator.Publish(new ListAnalysisDeleted(request.GuestListId), token);
            }
            return request.GuestListId;
        }

        /// <summary>
        /// removes sms and email templates if any assigned to reminder
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private async Task DeleteTemplatesIfExist(ListAnalysisCampaignResponse request, CancellationToken token)
        {
            var smsTemplate = await _invitationRepo.GetCampaignSmsTemplateByInvitationId(request.CampaignInvitationId, token);
            if (smsTemplate != null)
            {
                await _invitationRepo.DeleteSmsTemplate(smsTemplate.Id, token);
            }

            var emailTemplate = await _invitationRepo.GetCampaignEmailTemplateByInvitationId(request.CampaignInvitationId, token);
            if (emailTemplate != null)
            {
                await _invitationRepo.DeleteEmailTemplate(emailTemplate.Id, token);
            }

            var whatsapptemplate = await _invitationRepo.GetCampaignWhatsappTemplateByInvitationId(request.CampaignInvitationId, token);
            if (whatsapptemplate != null)
            {
                await _invitationRepo.DeleteWhatsappTemplate(whatsapptemplate.Id, token);
            }

            var rfiForm = await _invitationRepo.GetCampaignFormByInvitationId(request.CampaignInvitationId, token);
            if (rfiForm != null)
            {
                await _invitationRepo.DeleteRfiForm(rfiForm.Id, token);
            }

            await _invitationRepo.Delete(request.CampaignInvitationId, token);
        }
    }
}