using System;
using System.IO;
using Autofac.Extensions.DependencyInjection;
using Irms.Infrastructure;
using Irms.WebApi.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Irms.WebApi
{
    public class Program
    {
        public static int Main(string[] args)
        {
            Console.Title = "Api";

            var cfg = AppConfig.Get(args);
            SerilogConfig.InitGlobalLogger(cfg);

            try
            {
                Log.Information("Starting the web host");

                var host = CreateHostBuilder(args, cfg);
                host.Build().Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }

        }
        private static IHostBuilder CreateHostBuilder(string[] args, IConfiguration cfg) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddSerilog();
                })
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                    .UseContentRoot(Directory.GetCurrentDirectory())
                    .UseIISIntegration()
                    .UseSetting("detailedErrors", "true")
                    .UseStartup<Startup>()
                    .UseConfiguration(cfg)
                    .CaptureStartupErrors(true);
                    //.UseApplicationInsights(); 
                });
    }
}
