﻿using Irms.Application.Templates.Commands;
using Irms.Data.Read.Templates.ReadModels;
using Irms.WebApi.LocalizationTransform.Template.Models;
using System.Threading.Tasks;

namespace Irms.WebApi.LocalizationTransform.Template
{
    public interface ITemplateTranslator : ITranslator
    {
        Task<TemplateDetailsRead> TranslateReadModel(TemplateDetails src);
        Task<CreateTemplateCmd> TranslateCreateModel(TemplateDetailsCreate src);
        Task<UpdateTemplateCmd> TranslateUpdateModel(TemplateDetailsUpdate src);
    }
}
