﻿namespace Irms.WebApi.ViewModels.Account
{
    public class RecoverPasswordByEmailViewModel
    {
        public string Email { get; set; }
    }
}
