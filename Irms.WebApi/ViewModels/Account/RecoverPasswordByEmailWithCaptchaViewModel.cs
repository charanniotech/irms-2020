﻿namespace Irms.WebApi.ViewModels.Account
{
    public class RecoverPasswordByEmailWithCaptchaViewModel
    {
        public string Email { get; set; }
        public string RecaptchaResponse { get; set; }
    }
}
