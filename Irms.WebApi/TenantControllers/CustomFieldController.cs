﻿using Irms.Application.CustomFields.Commands;
using Irms.Data.Read.CustomField.Queries;
using Irms.Data.Read.CustomField.ReadModels;
using Irms.Domain.Entities;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.WebApi.TenantControllers
{
    [AuthorizeRoles(RoleType.TenantAdmin)]
    [Route("api/[controller]")]
    public class CustomFieldController : ControllerBase
    {
        private readonly IMediator _mediator;
        public CustomFieldController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("create")]
        public async Task<Guid> Create([FromBody]AddCustomFieldCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        [HttpPost("update")]
        public async Task<Guid> Update([FromBody]UpdateCustomFieldCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        [HttpPost("get-all")]
        public async Task<IEnumerable<CustomFieldReadModel>> GetAll([FromBody]GetCustomFieldsQuery query, CancellationToken token)
        {
            var result = await _mediator.Send(query, token);
            return result;
        }
    }
}