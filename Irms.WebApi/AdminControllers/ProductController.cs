﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.Product.Commands;
using Irms.Application.ProductServices.Commands;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Product.Queries;
using Irms.Data.Read.Product.ReadModels;
using Irms.Data.Read.Service.Queries;
using Irms.Data.Read.Service.ReadModels;
using Irms.Domain.Entities;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Irms.WebApi.AdminControllers
{
    [AuthorizeRoles(RoleType.SuperAdmin)]
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IMediator _mediator;

        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        //Commands
        /// <summary>
        /// Request for creating <see cref="Product"/>
        /// </summary>
        /// <param name="cmd">Request include list of <see cref="ProductService"/> which need to add to Product </param>
        /// <param name="token"></param>
        /// <returns>Id of created Product</returns>
        [HttpPost]
        public async Task<Guid> Create([FromBody]CreateProductCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }
        /// <summary>
        /// Request for updating the same as fo creating the Product
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<Guid> UpdateProduct([FromBody]UpdateProductCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }
        /// <summary>
        /// Method for getting list of <see cref="Product"/> by Ids with details
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns>List of <see cref="Product"/></returns>
        [HttpPost("GetProductsByIds")]
        public async Task<IEnumerable<Product>> GetProductsByIds([FromBody]GetProductsByIdsCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }
        /// <summary>
        /// Method for getting <see cref="Product"/> by Id with details
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns><see cref="Product"/></returns>
        [HttpGet("{id}")]
        public async Task<Product> GetProductById([FromRoute]Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetProductByIdCmd(id), token);
            return result;
        }
        /// <summary>
        /// Method for getting list of <see cref="ProductListItem"/>. This entity includ general information of Product for showuing on the Product page
        /// </summary>
        /// <param name="cmd"><see cref="GetProductListQuery"/></param>
        /// <param name="token"></param>
        /// <returns><see cref="ProductListItem"/></returns>
        [HttpPost("list")]
        public async Task<IPageResult<ProductListItem>> GetProductList([FromBody]GetProductListQuery cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }
        /// <summary>
        /// Method for deleting the product
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpDelete("DeleteProduct")]
        public async Task<Unit> DeleteProductById([FromBody]DeleteProductByIdCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// Method for deleting the product
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpDelete("DeleteProductList")]
        public async Task<Unit> DeleteProductsByIds([FromBody]DeleteProductsByIdsCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// This method can create base service for adding features
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns>return service Id if success</returns>
        [HttpPost("CreateService")]
        public async Task<Guid> CreateService([FromBody]CreateServiceCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }
        /// <summary>
        /// Method for getting all avalible service <see cref="Data.Read.Service.ReadModels.ServicesList"/> for creating or adding to <see cref="Product"/>
        /// </summary>
        /// <param name="token"></param>
        /// <returns><see cref="Data.Read.Service.ReadModels.ServicesList"/></returns>
        [HttpPost("Services")]
        public async Task<IEnumerable<Data.Read.Service.ReadModels.ServicesList>> GetAvailableServices(GetAvailableServicesCmd request, CancellationToken token)
        {
            var result = await _mediator.Send(request, token);
            return result;
        }

        /// <summary>
        /// add avalible service to product
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut("AddServiceToProduct")]
        public async Task<Unit> AddServiceToProduct([FromBody]AddServiceToProductCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// get list of services witch was added to product
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{id}/Services")]
        public async Task<IEnumerable<ServicesList>> GetAvailableProductServices(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetProductServicesCmd(id), token);
            return result;
        }

        /// <summary>
        /// Create feature for service
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("CreateServiceFeature")]
        public async Task<Guid> CreateServiceFeature([FromBody]CreateServiceFeatureCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }
        //TODO:        
        //UPDATE PRODUCT FEATURES
        //UPDATE SERVICE
        //AND PERHAPS:
        //GET PRODUCT FEATURES
        //GET PRODUCTS FEATURES
    }
}