﻿using System;
using System.Net;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.UserManagement;
using Irms.Application.Abstract.Services;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Domain;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Irms.WebApi.ViewModels.Account;
using Irms.WebApi.Helpers;
using Irms.Application.Registrations.Common.Queries;
using Irms.Application;
using Irms.Application.UsersInfo.Queries;
using System.Diagnostics;

namespace Irms.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        //private readonly string _adminPortal = "admin";
        private readonly IUserManager _userManager;
        private readonly IPhoneNumberValidator _phoneValidator;
        private readonly IForgotPasswordNotifier _notifier;
        private readonly IOTPCodeNotifier _otpNotifier;
        private readonly IReCaptchaValidator _captchaValidator;
        private readonly IMediator _mediator;

        public AccountController(
            IUserManager userManager,
            IPhoneNumberValidator phoneValidator,
            IForgotPasswordNotifier notifier,
            IOTPCodeNotifier otpNotifier,
            IMediator mediator,
            IReCaptchaValidator captchaValidator
            )
        {
            _userManager = userManager;
            _phoneValidator = phoneValidator;
            _notifier = notifier;
            _otpNotifier = otpNotifier;
            _mediator = mediator;
            _captchaValidator = captchaValidator;
        }

        /// <summary>
        /// recover password by email using captcha
        /// </summary>
        /// <param name="model"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        [HttpPost("recoverPassword/emailWithCaptcha")]
        public async Task RecoverPasswordByEmailWithCaptcha(
            [FromBody] RecoverPasswordByEmailWithCaptchaViewModel model,
            CancellationToken t)
        {
            Debug.WriteLine("Captch validation has been ignored");
            var captchaIsValid = true; //await _captchaValidator.Validate(model.RecaptchaResponse);
            if (!captchaIsValid)
            {
                throw new IncorrectRequestException("Captcha validation failed");
            }

            var newModel = new RecoverPasswordByEmailViewModel
            {
                Email = model.Email
            };

            await RecoverPasswordByEmail(newModel, t);
        }

        /// <summary>
        /// recover password by email
        /// </summary>
        /// <param name="model"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        [HttpPost("recoverPassword/email")]
        public async Task RecoverPasswordByEmail([FromBody] RecoverPasswordByEmailViewModel model, CancellationToken t)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return;
            }

            if (!await _userManager.IsEmailConfirmedAsync(user))
            {
                throw new IncorrectRequestException("The specified email is not confirmed");
            }

            var isUnavailable = await GetUserIsUnavaliable(user, t);
            if (isUnavailable)
            {
                throw new IncorrectRequestException(14, "We couldn't find your account registered with this email address");
            }

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);

            Request.Headers.TryGetValue("Origin", out var originValues);
            var callbackUrl = $"{originValues.First()}/account/change-password?userId={user.Id}&token={WebUtility.UrlEncode(token)}";

            Debug.WriteLine($"Callback Url for password recovering: {callbackUrl}");
            await _notifier.NotifyByEmail(user.Email, callbackUrl, t);
        }

        /// <summary>
        /// recover password with sms using captcha
        /// </summary>
        /// <param name="model"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        [HttpPost("recoverPassword/smsWithCaptcha")]
        public async Task<Guid> RecoverPasswordBySmsWithCaptcha(
            [FromBody] RecoverPasswordBySmsWithCaptchaViewModel model,
            CancellationToken t)
        {
            Debug.WriteLine("Captch validation has been ignored");
            var captchaIsValid = true;//await _captchaValidator.Validate(model.RecaptchaResponse);
            if (!captchaIsValid)
            {
                throw new IncorrectRequestException("Captcha validation failed");
            }

            var newModel = new RecoverPasswordBySmsViewModel
            {
                PhoneNumber = model.PhoneNumber
            };

            return await RecoverPasswordBySms(newModel, t);
        }

        /// <summary>
        /// recover password by sms
        /// </summary>
        /// <param name="model"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        [HttpPost("recoverPassword/sms")]
        public async Task<Guid> RecoverPasswordBySms([FromBody] RecoverPasswordBySmsViewModel model, CancellationToken t)
        {
            var formattedPhone = _phoneValidator.Validate(model.PhoneNumber, out var isValid);
            if (!isValid)
            {
                throw new IncorrectRequestException("Phone number format is invalid");
            }

            var user = await _userManager.FindByPhoneNumberAsync(formattedPhone.Number);
            if (user == null)
            {
                throw new IncorrectRequestException(15, "We couldn't find your account registered with this mobile number");
            }

            if (!await _userManager.IsPhoneNumberConfirmedAsync(user))
            {
                throw new IncorrectRequestException("The specified phone number is not confirmed");
            }

            //var isUnavailable = await GetUserIsUnavaliable(user, t);
            //if (isUnavailable)
            //{
            //    throw new IncorrectRequestException(15, "We couldn't find your account registered with this mobile number");
            //}

            var code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, formattedPhone.Number);
            Debug.WriteLine($"Sms code for recover password: {code}");
            await _notifier.NotifyBySms(user.PhoneNumber, code, t);

            return user.Id;
        }

        /// <summary>
        /// get token by sms code
        /// </summary>
        /// <param name="model"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        [HttpPost("getTokenByCode")]
        public async Task<PasswordResetToken> GetPasswordResetTokenByCode([FromBody] GetTokenByCodeViewModel model, CancellationToken t)
        {
            var formattedPhone = _phoneValidator.Validate(model.PhoneNumber, out var isValid);
            if (!isValid)
            {
                throw new IncorrectRequestException("Phone number format is invalid");
            }

            var user = await _userManager.FindByPhoneNumberAsync(formattedPhone.Number);
            if (user == null)
            {
                throw new IncorrectRequestException("Phone number is incorrect");
            }

            var result = await _userManager.ChangePhoneNumberAsync(user, formattedPhone.Number, model.Code);
            if (!result.Succeeded)
            {
                throw new IncorrectRequestException(16, "The code is incorrect");
            }

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            return new PasswordResetToken
            {
                Token = token,
                UserId = user.Id.ToString()
            };
        }

        /// <summary>
        /// setting up password
        /// </summary>
        /// <param name="model"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        [HttpPost("setNewPassword")]
        public async Task SetNewPassword([FromBody] SetNewPasswordViewModel model, CancellationToken t)
        {
            var user = await _userManager.FindByIdAsync(Guid.Parse(model.UserId));
            if (user == null)
            {
                throw new IncorrectRequestException("User Id is incorrect");
            }

            var result = await _userManager.ResetPasswordAsync(user, model.Token, model.NewPassword);
            if (!result.Succeeded)
            {
                throw new IncorrectRequestException("Token is invalid");
            }
        }

        /// <summary>
        /// resend one time password, anonymous method
        /// </summary>
        /// <param name="model">username, token</param>
        /// <param name="t"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("resendOTP")]
        public async Task<ResendOTPViewModel> ResendOTP(ResendOTPViewModel model, CancellationToken t)
        {
            var user = await _userManager.FindByEmailAsync(model.UserName);
            if (user == null)
            {
                throw new IncorrectRequestException("Something went wrong. User can not be found by the claim");
            }

            //verify token
            var tokenValidated = await _userManager.VerifyTwoFactorTokenAsync(user, "Default", model.Token);
            if (!tokenValidated)
            {
                throw new IncorrectRequestException("invalid token with otp code");
            }
            var code = await _userManager.GenerateTwoFactorTokenAsync(user, "Phone");
            Debug.WriteLine($"OTP: {code}");
            bool isSent = await _otpNotifier.NotifyBySms(user.PhoneNumber, code, CancellationToken.None);
            if (!isSent)
            {
                throw new IncorrectRequestException("SMS sending failed");
            }

            model.Token = await _userManager.GenerateTwoFactorTokenAsync(user, "Default");
            return model;
        }

        /// <summary>
        /// check if national id is unique
        /// </summary>
        /// <param name="model"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        [HttpPost("CheckNationalIdUniqueness")]
        public async Task<UniquenessViewModel> CheckNationalIdUniqueness([FromBody]CheckNationalIdUniquenessViewModel model, CancellationToken t)
        {
            var isUnique = await _mediator.Send(new IsNationalIdUnique(model.NationalId), t);
            return new UniquenessViewModel(isUnique, true);
        }

        /// <summary>
        /// check if passport is unique
        /// </summary>
        /// <param name="model"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        [HttpPost("CheckPassportUniqueness")]
        public async Task<UniquenessViewModel> CheckPassportUniqueness([FromBody]CheckPassportUniquenessViewModel model, CancellationToken t)
        {
            var isUnique = await _mediator.Send(new IsPassportUnique(model.PassportNo), t);
            return new UniquenessViewModel(isUnique, true);
        }

        /// <summary>
        /// check if email is unique
        /// </summary>
        /// <param name="model"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        [HttpPost("CheckEmailUniqueness")]
        public async Task<UniquenessViewModel> CheckEmailUniqueness([FromBody]CheckEmailUniquenessViewModel model, CancellationToken t)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            //var isUnavailable = await GetUserIsUnavaliable(user, t);

            return new UniquenessViewModel(user == null, user == null);
        }

        /// <summary>
        /// check if phone is unique
        /// </summary>
        /// <param name="model"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        [HttpPost("CheckPhoneUniqueness")]
        public async Task<PhoneNumberUniquenessValidationResult> CheckPhoneUniqueness([FromBody]CheckPhoneUniquenessViewModel model, CancellationToken t)
        {
            var formattedPhone = _phoneValidator.Validate(model.PhoneNo, out var isValid);
            if (!isValid)
            {
                return new PhoneNumberUniquenessValidationResult(false, false, null, true);
            }

            var user = await _userManager.FindByPhoneNumberAsync(formattedPhone.Number);
            var isUnavailable = await GetUserIsUnavaliable(user, t);

            return new PhoneNumberUniquenessValidationResult(true, user == null, formattedPhone.Number, isUnavailable);
        }

        [HttpPost("CheckEmergencyPhoneUniqueness")]
        public async Task<PhoneNumberUniquenessValidationResult> CheckEmergencyPhoneUniqueness([FromBody]CheckPhoneUniquenessViewModel model, CancellationToken t)
        {
            var formattedPhone = _phoneValidator.Validate(model.PhoneNo, out var isValid);
            if (!isValid)
            {
                return new PhoneNumberUniquenessValidationResult(false, false, null, true);
            }

            var user = await _userManager.FindByPhoneNumberAsync(formattedPhone.Number);
            var isUnavailable = await GetUserIsUnavaliable(user, t);

            return new PhoneNumberUniquenessValidationResult(true, formattedPhone.Number != user.PhoneNumber, formattedPhone.Number, isUnavailable);
        }

        [HttpPost("FormatPhoneNumber")]
        public PhoneNumberValidationResult FormatPhoneNumber([FromBody]CheckPhoneUniquenessViewModel model, CancellationToken t)
        {
            var formattedPhone = _phoneValidator.Validate(model.PhoneNo, out var isValid);
            var result = isValid ? formattedPhone.Number : model.PhoneNo;

            return new PhoneNumberValidationResult(isValid, result);
        }

        [Authorize]
        [HttpPost("changePassword")]
        public async Task<bool> ChangePassword([FromBody]ChangePasswordViewModel model, CancellationToken t)
        {
            var user = await _userManager.FindByIdAsync(User.UserId() ?? Guid.Empty);
            if (user == null)
            {
                throw new IncorrectRequestException("Something went wrong. User can not be found by the claim");
            }

            var result = await _userManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);
            if (!result.Succeeded)
            {
                var msg = result.GetErrorMessage();
                throw new IncorrectRequestException(msg);
            }

            return true;
        }

        /// <summary>
        /// private method to check if user is available from employee table.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        private async Task<bool> GetUserIsUnavaliable(IUser user, CancellationToken t)
        {
            if (user == null)
            {
                return true;
            }

            var referer = Request.Headers?["Referer"].FirstOrDefault() ?? string.Empty;
            var isUnavailable = false;

            //if (referer.Contains(_adminPortal))
            //{
            var admin = await _mediator.Send(new GetUserInfoIdByAspUser(user.Id), t);
            if (admin == null)
            {
                isUnavailable = true;
            }
            //}
            //else
            //{
            //    var guest = await _mediator.Send(new GetAspNetUserGuestId(user.Id), t);
            //    if (guest == null)
            //    {
            //        isUnavailable = true;
            //    }
            //}

            return isUnavailable;
        }
    }
}
