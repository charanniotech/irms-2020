﻿using Autofac;
using FluentValidation;

namespace Irms.WebApi.Configuration
{
    public class FluentValidationConfig : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder
                .RegisterAssemblyTypes(Assemblies.Get())
                .AsClosedTypesOf(typeof(IValidator<>))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope(); // validator creation is expensive. They don't share state and can be reused
        }
    }
}
