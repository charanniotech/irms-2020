﻿using Autofac;
using AutoMapper;
using Irms.Data;


namespace Irms.WebApi.Configuration
{
    public class AutoMapperConfig : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddMaps(Assemblies.Get());
                cfg.IgnoreUnmapped();
            });

            config.AssertConfigurationIsValid();

            var mapper = new Mapper(config);

            builder.Register(c => mapper).As<IMapper>()
                .InstancePerLifetimeScope()
                .PropertiesAutowired()
                .PreserveExistingDefaults();
        }
    }
}
