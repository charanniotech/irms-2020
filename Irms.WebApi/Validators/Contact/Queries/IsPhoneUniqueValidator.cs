﻿using FluentValidation;
using Irms.Data.Read.Contact.Queries;

namespace Irms.WebApi.Validators.Contact.Queries
{
    public class IsPhoneUniqueValidator : AbstractValidator<IsPhoneUnique>
    {
        public IsPhoneUniqueValidator()
        {
            RuleFor(x => x.MobileNumber)
                .NotEmpty();
        }
    }
}
