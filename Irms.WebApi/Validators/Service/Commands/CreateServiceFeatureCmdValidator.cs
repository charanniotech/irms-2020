﻿using FluentValidation;
using Irms.Application.ProductServices.Commands;

namespace Irms.WebApi.Validators.Service.Commands
{
    public class CreateServiceFeatureCmdValidator : AbstractValidator<CreateServiceFeatureCmd>
    {
        public CreateServiceFeatureCmdValidator()
        {
            RuleFor(x => x.ServiceId)
                .NotEmpty();

            RuleFor(x => x.ServiceFeature)
                .NotEmpty();
        }
    }
}
