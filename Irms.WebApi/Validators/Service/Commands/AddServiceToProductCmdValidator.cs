﻿using FluentValidation;
using Irms.Application.ProductServices.Commands;

namespace Irms.WebApi.Validators.Service.Commands
{
    public class AddServiceToProductCmdValidator : AbstractValidator<AddServiceToProductCmd>
    {
        public AddServiceToProductCmdValidator()
        {

            RuleFor(x => x.ProductId)
                .NotEmpty();

            RuleFor(x => x.ServiceId)
                .NotEmpty();
        }
    }
}
