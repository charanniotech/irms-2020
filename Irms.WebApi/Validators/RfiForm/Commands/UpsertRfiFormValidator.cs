﻿using FluentValidation;
using Irms.Application.RfiForms.Commands;
using System;

namespace Irms.WebApi.Validators.CampaignInvitation.Queries
{
    public class UpsertRfiFormValidator : AbstractValidator<UpsertRfiFormCmd>
    {
        public UpsertRfiFormValidator()
        {
            RuleFor(x => x.FormSettings)
                .NotEmpty();

            RuleFor(x => x.FormTheme)
                .NotEmpty();

            RuleFor(x => x.SubmitHtml)
                .NotEmpty();

            RuleFor(x => x.ThanksHtml)
                .NotEmpty();

            RuleFor(x => x.WelcomeHtml)
                .NotEmpty();

            RuleForEach(x => x.QuestionJson)
                .NotEmpty();
        }
    }
}
