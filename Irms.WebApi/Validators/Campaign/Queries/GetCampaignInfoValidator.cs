﻿using FluentValidation;
using Irms.Data.Read.Campaign.Queries;

namespace Irms.WebApi.Validators.Campaign.Queries
{
    public class GetCampaignInfoValidator : AbstractValidator<GetCampaignInfo>
    {
        public GetCampaignInfoValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();
        }
    }
}
