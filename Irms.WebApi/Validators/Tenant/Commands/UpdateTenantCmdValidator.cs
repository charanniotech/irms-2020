﻿using FluentValidation;
using Irms.Application.Tenants.Commands;
using System;

namespace Irms.WebApi.Validators.Tenant.Commands
{
    public class UpdateTenantCmdValidator : AbstractValidator<UpdateTenantCmd>
    {
        public UpdateTenantCmdValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();

            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(x => x.Description)
                .NotEmpty()
                .MaximumLength(500);

            RuleFor(e => e.Address)
             .NotEmpty()
             .MaximumLength(200);

            RuleFor(e => e.CountryId)
                .NotEmpty();

            RuleFor(e => e.City)
                .MaximumLength(200);

            RuleFor(x => x.Logo)
                 .Must(IsBase64Encoded).WithMessage("Logo icon is not a valid Base64");
        }

        private bool IsBase64Encoded(string str)
        {
            try
            {
                if (string.IsNullOrEmpty(str))
                {
                    return true;
                }

                // If no exception is caught, then it is possibly a base64 encoded string
                byte[] data = Convert.FromBase64String(str.Split(',')[1]);
                // The part that checks if the string was properly padded to the
                // correct length was borrowed from d@anish's solution
                return (str.Split(',')[1].Replace(" ", "").Length % 4 == 0);
            }
            catch
            {
                // If exception is caught, then it is not a base64 encoded string
                return false;
            }
        }
    }
}