﻿using FluentValidation;
using Irms.Application.Tenants.Commands;

namespace Irms.WebApi.Validators.Tenant.Commands
{
    public class DeleteTenantCmdValidator : AbstractValidator<DeleteTenantCmd>
    {
        public DeleteTenantCmdValidator()
        {
            RuleForEach(x => x.TenantIds)
                .NotEmpty();

            RuleFor(x => x.TenantIds)
                .NotEmpty();
        }
    }
}
