﻿using FluentValidation;
using Irms.Data.Read.Tenant.Queries;

namespace Irms.WebApi.Validators.Tenant.Queries
{
    public class GetTenantDetailsValidator : AbstractValidator<GetTenantDetails>
    {
        public GetTenantDetailsValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();
        }
    }
}
