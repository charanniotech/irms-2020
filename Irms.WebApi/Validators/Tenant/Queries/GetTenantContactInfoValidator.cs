﻿using FluentValidation;
using Irms.Data.Read.Tenant.Queries;

namespace Irms.WebApi.Validators.Tenant.Queries
{
    public class GetTenantContactInfoValidator : AbstractValidator<GetTenantContactInfo>
    {
        public GetTenantContactInfoValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();
        }
    }
}
