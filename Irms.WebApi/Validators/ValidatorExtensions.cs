﻿using FluentValidation;
using Irms.Application.Abstract.Services;
using Irms.Infrastructure.Services;
using System;

namespace Irms.WebApi.Validators
{
    public static class ValidatorExtensions
    {
        /// <summary>
        /// We can't use DI here :-( Anyway, the google "lib phone number" uses singleton of the validator
        /// </summary>
        private static readonly IPhoneNumberValidator PhoneValidator = new PhoneNumberValidator();

        /// <summary>
        /// A valid HTTP/HTTPS URL
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="ruleBuilder">Builder</param>
        /// <returns>Rule</returns>
        public static IRuleBuilderOptions<T, string> Url<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Must(x => x == null || (Uri.TryCreate(x, UriKind.Absolute, out var u)
                                         && (u.Scheme == "http" || u.Scheme == "https")))
                .WithMessage("The URL is invalid");
        }


        /// <summary>
        /// Contains at least 1 dot surrounded by letters, digits, dashes
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="ruleBuilder">Builder</param>
        /// <returns>Rule</returns>
        public static IRuleBuilderOptions<T, string> DomainName<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Matches("[a-zA-Z0-9\\-]*\\.[a-zA-Z0-9\\-]+$")
                .WithMessage("The domain name is invalid");
        }

        /// <summary>
        /// Length >= 8 and at least 1 lowercase, 1 uppercase, 1 digit, 1 symbol
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="ruleBuilder">Builder</param>
        /// <returns>Rule</returns>
        public static IRuleBuilderOptions<T, string> StrongPassword<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Matches("^(?=.*[\\d])(?=.*[a-z])(?=.*[A-Z])(?!.*\\<)(?!.*\\>)(?!.*\\s)(?=.*[\'/\\\\\\\\!\"#$%&\'()*+,.\\-/:;=?@[\\]^_`{|}~]){8,}")
                .WithMessage("Password must contain at least 1 upper-case letter, 1 lower-case letter, 1 digit, 1 symbol (except < >), and be at least 8 characters long without any space");
        }

        /// <summary>
        /// A valid phone number (international or Saudi local)
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="ruleBuilder">Builder</param>
        /// <returns>Rule</returns>
        public static IRuleBuilderOptions<T, string> PhoneNumber<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Must(phone => phone == null || PhoneValidator.Validate(phone).isValid)
                .WithMessage("The phone number is invalid");
        }
    }
}
