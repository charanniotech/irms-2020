﻿using FluentValidation;
using Irms.Application.Campaigns.Queries;

namespace Irms.WebApi.Validators.CampaignInvitation.Queries
{
    public class GetCampaignPlaceholderListValidator : AbstractValidator<GetCampaignPlaceholderList>
    {
        public GetCampaignPlaceholderListValidator()
        {
            RuleFor(x => x.PageNo)
                .GreaterThan(0);

            RuleFor(x => x.PageSize)
                .InclusiveBetween(1, 1000);
        }
    }
}
