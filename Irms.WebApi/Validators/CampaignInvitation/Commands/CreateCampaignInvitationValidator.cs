﻿using FluentValidation;
using Irms.Application.Campaigns.Commands;

namespace Irms.WebApi.Validators.CampaignInvitation.Queries
{
    public class CreateCampaignInvitationValidator : AbstractValidator<CreateOrUpdateCampaignInvitationCmd>
    {
        public CreateCampaignInvitationValidator()
        {
            RuleFor(x => x.CampaignId)
                .NotEmpty();

            RuleFor(x => x.StartDate)
                .NotEmpty()
                .When(x => !x.IsInstant);
        }
    }
}
