﻿using FluentValidation;
using Irms.Application.Campaigns.Commands;

namespace Irms.WebApi.Validators.CampaignInvitation.Queries
{
    public class CreateOrUpdateCampaignEmailTemplateValidator : AbstractValidator<CreateOrUpdateCampaignEmailTemplateCmd>
    {
        public CreateOrUpdateCampaignEmailTemplateValidator()
        {
            RuleFor(x => x.Body)
                .NotEmpty();

            //RuleFor(x => x.PlainBody)
            //    .NotEmpty();

            RuleFor(x => x.Preheader)
                .MaximumLength(1000);

            RuleFor(x => x.Subject)
                .NotEmpty()
                .MaximumLength(500);
        }
    }

}
