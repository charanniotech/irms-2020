﻿using FluentValidation;
using Irms.Application.Campaigns.Commands;

namespace Irms.WebApi.Validators.CampaignInvitation.Queries
{
    public class SendTestEmailValidator : AbstractValidator<SendTestEmailCmd>
    {
        public SendTestEmailValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();

            RuleFor(x => x.Recipients)
                .ForEach(x => x.NotEmpty());
        }
    }
}
