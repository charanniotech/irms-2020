﻿using FluentValidation;
using Irms.Application.Product.Commands;

namespace Irms.WebApi.Validators.Product.Commands
{
    public class UpdateProductCmdValidator : AbstractValidator<UpdateProductCmd>
    {
        public UpdateProductCmdValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();

            RuleFor(x => x.ProductName)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(x => x.LicenseDays)
                .NotEmpty();

            RuleFor(x => x.CurrencyId)
                .NotEmpty();

            RuleFor(x => x.Price)
                .NotEmpty();

            RuleFor(x => x.Services)
                .NotEmpty();

            RuleFor(x => x.LicenseName)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}
