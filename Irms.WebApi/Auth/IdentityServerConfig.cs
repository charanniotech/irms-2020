﻿using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;

namespace Irms.WebApi.Auth
{
    public class IdentityServerConfig
    {
        // scopes define the resources in your system
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email()
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("api", "Portal API")
                {
                    UserClaims = { "role", "name" }
                }
            };
        }

        // clients want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "client-spa",
                    ClientName = "Angular Portal",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    RequireConsent = false,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "api"
                    },

                    AllowAccessTokensViaBrowser = true,
                    AllowOfflineAccess = true // refresh token
                },
                new Client
                {
                    ClientId = "client-mobile",
                    ClientName = "Mobile Apps",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    RequireConsent = false,

                    ClientSecrets =
                    {
                        new Secret("secret-mobile".Sha256())
                    },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "api"
                    },

                    AllowAccessTokensViaBrowser = true,
                    AllowOfflineAccess = true // refresh token
                }
            };
        }
    }
}
