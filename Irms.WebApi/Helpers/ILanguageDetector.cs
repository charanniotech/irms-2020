﻿using Microsoft.AspNetCore.Http;

namespace Irms.WebApi.Helpers
{
    public interface ILanguageDetector
    {
        string DetectRequestLanguage(HttpContext ctx);
    }
}
