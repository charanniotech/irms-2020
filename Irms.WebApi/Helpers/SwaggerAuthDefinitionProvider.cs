﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Irms.WebApi.Helpers
{
    public class SwaggerAuthDefinitionProvider : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var secure = context.ApiDescription.ActionDescriptor.FilterDescriptors.Any(x => x.Filter is AuthorizeFilter);
            if (!secure)
            {
                return;
            }

            if (operation.Security == null)
            {
                operation.Security = new List<OpenApiSecurityRequirement>();
            }

            var oAuthRequirements = new OpenApiSecurityRequirement
            {
                {                    
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "oauth2"
                        },
                        Scheme = "oauth2",
                        Type = SecuritySchemeType.OAuth2
                    }, 
                    new List<string>()
                }
            };

            //var oAuthRequirements = new Dictionary<string, IEnumerable<string>>
            //{
            //    { "oauth2", Enumerable.Empty<string>() }
            //};

            operation.Security.Add(oAuthRequirements);
        }
    }
}
