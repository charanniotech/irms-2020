﻿using Irms.Application.Abstract.Repositories.UserManagement;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Irms.WebApi.Helpers.UserManagement
{
    public class IdResult : IIdentityResult
    {
        private readonly IdentityResult _result;

        public IdResult(IdentityResult result)
        {
            _result = result;
        }

        public bool Succeeded => _result.Succeeded;
        public IEnumerable<(string Code, string Description)> Errors => _result.Errors.Select(x => (x.Code, x.Description));
    }
}
