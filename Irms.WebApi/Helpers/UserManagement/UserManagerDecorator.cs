﻿using Irms.Application.Abstract.Repositories.UserManagement;
using Irms.Data.IdentityClasses;
using Irms.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Irms.WebApi.Helpers.UserManagement
{
    public class UserManagerDecorator : IUserManager
    {
        private readonly UserManager<ApplicationUser> _manager;

        public UserManagerDecorator(UserManager<ApplicationUser> manager)
        {
            _manager = manager;
        }

        public async Task<IUser> FindByIdAsync(Guid id)
        {
            return await _manager.FindByIdAsync(id.ToString());
        }

        public async Task<IEnumerable<IUser>> FindByIdAsync(IEnumerable<Guid> ids)
        {
            var userIds = ids.ToList();
            return await _manager.Users.Where(x => userIds.Contains(x.Id)).ToListAsync();
        }

        public async Task<IUser> FindByEmailAsync(string email)
        {
            return await _manager.FindByEmailAsync(email);
        }

        public Task<bool> IsEmailConfirmedAsync(IUser user)
        {
            return _manager.IsEmailConfirmedAsync(user as ApplicationUser);
        }

        public Task<string> GeneratePasswordResetTokenAsync(IUser user)
        {
            return _manager.GeneratePasswordResetTokenAsync(user as ApplicationUser);
        }

        public async Task<IUser> FindByPhoneNumberAsync(string phoneNo)
        {
            return await _manager.Users.FirstOrDefaultAsync(u => u.PhoneNumber == phoneNo);
        }

        public Task<bool> IsPhoneNumberConfirmedAsync(IUser user)
        {
            return _manager.IsPhoneNumberConfirmedAsync(user as ApplicationUser);
        }

        public async Task<IIdentityResult> ChangePhoneNumberAsync(IUser user, string phoneNo, string code)
        {
            return (await _manager.ChangePhoneNumberAsync(user as ApplicationUser, phoneNo, code)).ToResult();
        }

        public Task<string> GenerateChangePhoneNumberTokenAsync(IUser user, string phoneNo)
        {
            return _manager.GenerateChangePhoneNumberTokenAsync(user as ApplicationUser, phoneNo);
        }

        public async Task<bool> VerifyTwoFactorTokenAsync(IUser user, string tokenProvider, string token)
        {
            return await _manager.VerifyTwoFactorTokenAsync(user as ApplicationUser, tokenProvider, token);
        }

        public async Task<string> GenerateTwoFactorTokenAsync(IUser user, string tokenProvider)
        {
            return await _manager.GenerateTwoFactorTokenAsync(user as ApplicationUser, tokenProvider);
        }

        public async Task<IIdentityResult> ResetPasswordAsync(IUser user, string token, string newPassword)
        {
            return (await _manager.ResetPasswordAsync(user as ApplicationUser, token, newPassword)).ToResult();
        }

        public async Task<IIdentityResult> ChangePasswordAsync(IUser user, string currentPassword, string newPassword)
        {
            return (await _manager.ChangePasswordAsync(user as ApplicationUser, currentPassword, newPassword)).ToResult();
        }

        public async Task<IIdentityResult> CreateAsync(IUser user, string password)
        {
            return (await _manager.CreateAsync(user as ApplicationUser, password)).ToResult();
        }

        public Task<bool> IsInRole(IUser user, RoleType role)
        {
            return _manager.IsInRoleAsync(user as ApplicationUser, role.ToString());
        }

        public async Task<IIdentityResult> AddToRole(IUser user, RoleType role)
        {
            return (await _manager.AddToRoleAsync(user as ApplicationUser, role.ToString())).ToResult();
        }

        public async Task<IIdentityResult> RemoveFromRole(IUser user, RoleType role)
        {
            return (await _manager.RemoveFromRoleAsync(user as ApplicationUser, role.ToString())).ToResult();
        }

        public async Task<IIdentityResult> UpdateAsync(IUser user)
        {
            return (await _manager.UpdateAsync(user as ApplicationUser)).ToResult();
        }

        public async Task<IIdentityResult> DeleteAsync(IUser user)
        {
            return (await _manager.DeleteAsync(user as ApplicationUser)).ToResult();
        }
    }
}
