﻿using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Irms.Application.Abstract.Services;
using Irms.Domain;
using Irms.WebApi.Resources;

namespace Irms.WebApi.Helpers
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        private readonly ILanguageDetector _detector;
        private readonly IMessageTranslator _translator;

        public ValidateModelAttribute(
            ILanguageDetector detector,
            IMessageTranslator translator)
        {
            _detector = detector;
            _translator = translator;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var ex = ErrorFactory.MissingOrIncorrectInformation;

                var cultString = _detector.DetectRequestLanguage(context.HttpContext);
                var culture = string.IsNullOrEmpty(cultString)
                    ? new CultureInfo("en-US")
                    : new CultureInfo(cultString);

                var msg = _translator.Translate(ex.Message, culture);

                var body = new BadRequestBody(ex.Code, msg, GetErrors(context));
                context.Result = new BadRequestObjectResult(body);
            }
        }

        private object GetErrors(ActionExecutingContext context)
        {
            return context.ModelState.Select(x => new
            {
                Field = x.Key,
                Errors = x.Value.Errors.Select(e => e.ErrorMessage)
            });
        }
    }
}
