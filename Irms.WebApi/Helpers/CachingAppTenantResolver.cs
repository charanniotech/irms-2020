﻿using Irms.Application;
using Irms.Data;
using Irms.Data.Read.Tenant.Queries;
using Irms.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using SaasKit.Multitenancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Threading.Tasks;

namespace Irms.WebApi.Helpers
{
    public class CachingAppTenantResolver : MemoryCacheTenantResolver<TenantBasicInfo>
    {
        private readonly IMediator _mediator;

        public CachingAppTenantResolver(IMemoryCache cache, ILoggerFactory loggerFactory, IMediator mediator)
            : base(cache, loggerFactory)
        {
            _mediator = mediator;
        }

        protected override string GetContextIdentifier(HttpContext context)
        {
            return context.Request.Host.Value.ToLower();
        }

        protected override IEnumerable<string> GetTenantIdentifiers(TenantContext<TenantBasicInfo> context)
        {
            yield return context.Tenant.ClientUrl;
        }

        protected override async Task<TenantContext<TenantBasicInfo>> ResolveAsync(HttpContext context)
        {
            context.Request.Headers.TryGetValue("Origin", out var originValues);
            var clientUrl = originValues.FirstOrDefault();
            TenantContext<TenantBasicInfo> tenantContext = null;

            if (string.IsNullOrEmpty(clientUrl))
            {
                var fakeTenant = new TenantBasicInfo(Guid.Empty, string.Empty, string.Empty, string.Empty, false);
                tenantContext = new TenantContext<TenantBasicInfo>(fakeTenant);
                return await Task.FromResult(tenantContext);
            }
            #region to be removed on prod
#if DEBUG
            if (clientUrl.Contains("3500"))
            {
                clientUrl = "https://admin-qa.takamulstg.com";

            }
            else if (clientUrl.Contains("2500"))
            {
                clientUrl = "https://client-qa.takamulstg.com";
            }
            else if (clientUrl.Contains("5001"))
            {
                clientUrl = "https://client-qa.takamulstg.com";
            }
#endif

            if (clientUrl.Contains("5500"))
            {
                //clientUrl = "https://btoc.takamulstg.com";
                clientUrl = "https://client-dev.takamulstg.com";
            }
            else if (clientUrl.Contains("localhost"))
            {
                clientUrl = "https://client-qa.takamulstg.com";
            }
            #endregion

            var tenant = await _mediator.Send(new GetTenantByUrl(clientUrl));
            if (tenant != null)
            {
                tenantContext = new TenantContext<TenantBasicInfo>(tenant);
            }

            return await Task.FromResult(tenantContext);
        }

        protected override MemoryCacheEntryOptions CreateCacheEntryOptions()
        {
            return base.CreateCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(5));
        }
    }
}
