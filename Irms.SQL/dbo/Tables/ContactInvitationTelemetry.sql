﻿CREATE TABLE [dbo].[ContactInvitationTelemetry] (
    [Id]                   UNIQUEIDENTIFIER NOT NULL,
    [TenantId]             UNIQUEIDENTIFIER NOT NULL,
    [CampaignInvitationId] UNIQUEIDENTIFIER NOT NULL,
    [ContactId]            UNIQUEIDENTIFIER NOT NULL,
    [OpenedOn]             DATETIME         NOT NULL,
    [MediaType]            INT              NOT NULL,
    CONSTRAINT [PK_ContactInvitationTelemetry] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_ContactInvitationTelemetry_CampaignInvitation] FOREIGN KEY ([CampaignInvitationId], [TenantId]) REFERENCES [dbo].[CampaignInvitation] ([Id], [TenantId]),
    CONSTRAINT [FK_ContactInvitationTelemetry_Contact] FOREIGN KEY ([ContactId], [TenantId]) REFERENCES [dbo].[Contact] ([Id], [TenantId]),
    CONSTRAINT [FK_ContactInvitationTelemetry_Tenant] FOREIGN KEY ([ContactId], [TenantId]) REFERENCES [dbo].[Contact] ([Id], [TenantId])
);

