﻿create table CampaignInvitationResponseMediaType
(
	Id uniqueidentifier NOT NULL,
	TenantId uniqueidentifier NOT NULL,
	CampaignInvitationResponseId uniqueidentifier NOT NULL,
	MediaType int NOT NULL,
	AcceptOrReject int NOT NULL,
	CreatedOn datetime NOT NULL,
	CONSTRAINT PK_CampaignInvitationResponseMediaType PRIMARY KEY CLUSTERED (Id ASC, TenantId ASC),
	CONSTRAINT FK_CampaignInvitationResponseMediaType_CampaignInvitationResponse 
		FOREIGN KEY (CampaignInvitationResponseId, TenantId) 
		REFERENCES CampaignInvitationResponse (Id, TenantId)
)