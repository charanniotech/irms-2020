﻿CREATE TABLE [dbo].[EventFeature] (
    [Id]                      UNIQUEIDENTIFIER NOT NULL,
    [TenantId]                UNIQUEIDENTIFIER NOT NULL,
    [EventId]                 UNIQUEIDENTIFIER NOT NULL,
    [ServiceCatalogFeatureId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_EventFeature] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_EventFeature_Event] FOREIGN KEY ([EventId], [TenantId]) REFERENCES [dbo].[Event] ([Id], [TenantId])
);

