﻿CREATE TABLE [dbo].[CampaignPrefferedMedia] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [EventCampaignId] UNIQUEIDENTIFIER NOT NULL,
    [ContactId]       UNIQUEIDENTIFIER NOT NULL,
    [WhatsApp]        BIT              NOT NULL,
    [Sms]             BIT              NOT NULL,
    [Email]           BIT              NOT NULL,
    [CreatedOn]       DATETIME         NOT NULL,
    [CreatedById]     UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]      DATETIME         NULL,
    [ModifiedById]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CampaignPrefferedMedia] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_CampaignPrefferedMedia_Contacts] FOREIGN KEY ([ContactId], [TenantId]) REFERENCES [dbo].[Contact] ([Id], [Tenantid]),
    CONSTRAINT [FK_CampaignPrefferedMedia_EventCampaign] FOREIGN KEY ([EventCampaignId], [TenantId]) REFERENCES [dbo].[EventCampaign] ([Id], [TenantId])
);

