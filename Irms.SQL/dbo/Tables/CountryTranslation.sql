﻿CREATE TABLE [dbo].[CountryTranslation] (
    [ID]          UNIQUEIDENTIFIER NOT NULL,
    [CountryId]   UNIQUEIDENTIFIER NOT NULL,
    [LanguageId]  UNIQUEIDENTIFIER NOT NULL,
    [Translation] NVARCHAR (200)   NOT NULL,
    CONSTRAINT [PK_NationalityTranslation] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_NationalityTranslation_Nationality] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([ID])
);

