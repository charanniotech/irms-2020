﻿CREATE TABLE [dbo].[ContactCustomField] (
    [Id]            UNIQUEIDENTIFIER NOT NULL,
    [TenantId]      UNIQUEIDENTIFIER NOT NULL,
    [CustomFieldId] UNIQUEIDENTIFIER NOT NULL,
    [ContactId]     UNIQUEIDENTIFIER NOT NULL,
    [Value]         NVARCHAR (MAX)   NULL,
    [CreatedOn]     DATETIME         NOT NULL,
    [CreatedById]   UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]    DATETIME         NULL,
    [ModifiedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ContactCustomField] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_ContactCustomField_Contact] FOREIGN KEY ([ContactId], [TenantId]) REFERENCES [dbo].[Contact] ([Id], [TenantId]),
    CONSTRAINT [FK_ContactCustomField_CustomField] FOREIGN KEY ([CustomFieldId], [TenantId]) REFERENCES [dbo].[CustomField] ([Id], [TenantId]),
    CONSTRAINT [FK_ContactCustomField_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([Id])
);

