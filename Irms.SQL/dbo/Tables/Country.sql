﻿CREATE TABLE [dbo].[Country] (
    [ID]          UNIQUEIDENTIFIER NOT NULL,
    [Name]        NVARCHAR (100)   NOT NULL,
    [ShortIso]    NVARCHAR (2)     NULL,
    [LongIso]     NVARCHAR (3)     NULL,
    [Latitude]    NVARCHAR (20)    NULL,
    [Longigude]   NVARCHAR (20)    NULL,
    [Nationality] NVARCHAR (100)   NULL,
    CONSTRAINT [PK_Nationality] PRIMARY KEY CLUSTERED ([ID] ASC)
);



