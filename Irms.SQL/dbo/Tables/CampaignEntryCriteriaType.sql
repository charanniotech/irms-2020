﻿CREATE TABLE [dbo].[CampaignEntryCriteriaType] (
    [Id]    TINYINT        NOT NULL,
    [Title] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_CampaignEntryCriteriaType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

