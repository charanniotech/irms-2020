﻿CREATE TABLE [dbo].[ServiceCatalog] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [Title]        NVARCHAR (100)   NOT NULL,
    [IsActive]     BIT              DEFAULT ((1)) NULL,
    [CreatedById]  UNIQUEIDENTIFIER NULL,
    [CreatedOn]    DATETIME         DEFAULT (getdate()) NULL,
    [ModifiedById] UNIQUEIDENTIFIER NULL,
    [ModifiedOn]   DATETIME         DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_ServiceCatalog] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO

CREATE TRIGGER ServiceCatalogModifiedDate  
ON [dbo].[ServiceCatalog] 
AFTER UPDATE   
AS 
BEGIN

  IF @@rowcount = 0
    RETURN;

  IF UPDATE(ModifiedOn)
    RETURN;

  ELSE
     UPDATE [dbo].[Product] SET ModifiedOn = CURRENT_TIMESTAMP WHERE Id in (SELECT DISTINCT Id FROM Inserted);

END 
GO  

