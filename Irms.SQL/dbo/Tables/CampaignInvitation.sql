﻿CREATE TABLE [dbo].[CampaignInvitation] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TenantId]         UNIQUEIDENTIFIER NOT NULL,
    [EventCampaignId]  UNIQUEIDENTIFIER NOT NULL,
    [Title]            NVARCHAR (200)   NULL,
    [IsInstant]        BIT              NOT NULL,
    [StartDate]        DATETIME         NOT NULL,
    [InvitationTypeId] INT              NOT NULL,
    [Status]           INT              CONSTRAINT [DF_CampaignInvitation_Status] DEFAULT ((0)) NOT NULL,
    [SortOrder]        INT              NOT NULL,
    [Interval]         INT              NULL,
    [IntervalType]     INT              NULL,
    [Active]           BIT              NOT NULL,
    [CreatedOn]        DATETIME         NOT NULL,
    [CreatedById]      UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]       DATETIME         NULL,
    [ModifiedById]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CampaignInvitation] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_CampaignInvitation_EventCampaign] FOREIGN KEY ([EventCampaignId], [TenantId]) REFERENCES [dbo].[EventCampaign] ([Id], [TenantId])
);







