﻿CREATE TABLE [dbo].[AspNetUsers] (
    [Id]                   UNIQUEIDENTIFIER   NOT NULL,
    [AccessFailedCount]    INT                NOT NULL,
    [ConcurrencyStamp]     NVARCHAR (MAX)     NULL,
    [Email]                NVARCHAR (256)     NULL,
    [EmailConfirmed]       BIT                NOT NULL,
    [IsSuperAdmin]         BIT                NOT NULL,
    [LockoutEnabled]       BIT                NOT NULL,
    [LockoutEnd]           DATETIMEOFFSET (7) NULL,
    [NormalizedEmail]      NVARCHAR (256)     NULL,
    [NormalizedUserName]   NVARCHAR (256)     NULL,
    [PasswordHash]         NVARCHAR (MAX)     NULL,
    [PhoneNumber]          NVARCHAR (50)      NULL,
    [PhoneNumberConfirmed] BIT                NOT NULL,
    [SecurityStamp]        NVARCHAR (MAX)     NULL,
    [TwoFactorEnabled]     BIT                NOT NULL,
    [UserName]             NVARCHAR (256)     NULL,
    [TenantId]             UNIQUEIDENTIFIER   NOT NULL,
    [IsEnabled]            BIT                NOT NULL,
    CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AspNetUsers_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([Id])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_EmailIndex]
    ON [dbo].[AspNetUsers]([Email] ASC, [TenantId] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_PhoneIndex]
    ON [dbo].[AspNetUsers]([PhoneNumber] ASC, [TenantId] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_UserNameIndex]
    ON [dbo].[AspNetUsers]([NormalizedUserName] ASC, [TenantId] ASC);

