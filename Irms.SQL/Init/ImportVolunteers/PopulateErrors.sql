﻿/*
CREATE TABLE [dbo].[Errors$](
	[Type] nvarchar(255),
	[Errors] [nvarchar](max) NULL,
	[ID] [float] NULL,
	[Name] [nvarchar](255) NULL,
	[Gender] [nvarchar](255) NULL,
	[Birth date] [datetime] NULL,
	[MaritalStatus] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[Nationality] [nvarchar](255) NULL,
	[Government ID] [float] NULL,
	[Mobile] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Phone] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Status] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
*/

insert into [Errors$]
select 'New', 'Invalid nationality',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Registration$] WHERE Nationality like '????%'
and not exists(select * from [Errors$] where Errors = 'Invalid nationality' and Type='New')

insert into [Errors$]
select 'New', 'Invalid birth date',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Registration$] WHERE [Birth date] is null
and not exists(select * from [Errors$] where Errors = 'Invalid birth date' and Type='New')

insert into [Errors$]
select 'New', 'Invalid email',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Registration$] WHERE NULLIF([Email],'') is null
and not exists(select * from [Errors$] where Errors = 'Invalid email' and Type='New')

insert into [Errors$]
select 'New', 'Empty city',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Registration$] WHERE [City] IS NULL
and not exists(select * from [Errors$] where Errors = 'Empty city' and Type='New')

insert into [Errors$]
select 'New', 'Duplicated emails',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Registration$] WHERE [ID] IN (select MAX([ID]) FROM [Registration$] WHERE [Email] is not null GROUP by [Email] HAVING COUNT(*)>1)
and not exists(select * from [Errors$] where Errors = 'Duplicated emails' and Type='New')

insert into [Errors$]
select 'New', 'Duplicated phones',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Registration$] WHERE [ID] IN (select MAX([ID]) FROM [Registration$] WHERE REPLACE(ISNULL(NULLIF([Mobile],''),NULLIF(Phone,'')),' ','') is not null GROUP by REPLACE(ISNULL(NULLIF([Mobile],''),NULLIF(Phone,'')),' ','') HAVING COUNT(*)>1)
and not exists(select * from [Errors$] where Errors = 'Duplicated phones' and Type='New')

insert into [Errors$]
select 'New', 'Name with 3 spaces',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Registration$] WHERE [Name] LIKE '% % % %'
and not exists(select * from [Errors$] where Errors = 'Name with 3 spaces' and Type='New')

insert into [Errors$]
select 'New', 'Email already in accepted volunteers',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Registration$] WHERE [Email] IS NOT NULL AND [Email] IN (SELECT Email FROM [Volunteers$] WHERE [Email] IS NOT NULL)
and not exists(select * from [Errors$] where Errors = 'Email already in accepted volunteers' and Type='New')

insert into [Errors$]
select 'New', 'Phone already in accepted volunteers',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Registration$] WHERE [Mobile] IS NOT NULL AND [Mobile] IN (SELECT [Mobile] FROM [Volunteers$] WHERE [Mobile] IS NOT NULL)
and not exists(select * from [Errors$] where Errors = 'Phone already in accepted volunteers' and Type='New')

--------------------------------------------------------------------------


insert into [Errors$]
select 'Accepted', 'Invalid nationality',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Volunteers$] WHERE Nationality like '????%'
and not exists(select * from [Errors$] where Errors = 'Invalid nationality' and Type='Accepted')

insert into [Errors$]
select 'Accepted', 'Invalid birth date',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Volunteers$] WHERE [Birth date] is null
and not exists(select * from [Errors$] where Errors = 'Invalid birth date' and Type='Accepted')

insert into [Errors$]
select 'Accepted', 'Invalid email',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Volunteers$] WHERE NULLIF([Email],'') is null
and not exists(select * from [Errors$] where Errors = 'Invalid email' and Type='Accepted')

insert into [Errors$]
select 'Accepted', 'Empty city',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Volunteers$] WHERE [City] IS NULL
and not exists(select * from [Errors$] where Errors = 'Empty city' and Type='Accepted')

insert into [Errors$]
select 'Registration', 'Duplicated emails',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Volunteers$] WHERE [ID] IN (select MAX([ID]) FROM [Volunteers$] WHERE [Email] is not null GROUP by [Email] HAVING COUNT(*)>1)
and not exists(select * from [Errors$] where Errors = 'Duplicated emails' and Type='Accepted')

insert into [Errors$]
select 'Accepted', 'Duplicated phones',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Volunteers$] WHERE [ID] IN (select MAX([ID]) FROM [Volunteers$] WHERE REPLACE(ISNULL(NULLIF([Mobile],''),NULLIF(Phone,'')),' ','') is not null GROUP by REPLACE(ISNULL(NULLIF([Mobile],''),NULLIF(Phone,'')),' ','') HAVING COUNT(*)>1)
and not exists(select * from [Errors$] where Errors = 'Duplicated phones' and Type='Accepted')

insert into [Errors$]
select 'Accepted', 'Name with 3 spaces',[ID],[Name],[Gender],[Birth date],[MaritalStatus],[City],[Nationality],[Government ID],[Mobile],[Email],[Phone],[Address],[Status] from [Volunteers$] WHERE [Name] LIKE '% % % %'
and not exists(select * from [Errors$] where Errors = 'Name with 3 spaces' and Type='Accepted')


-- SELECT * FROM [temp].[dbo].[Errors$] order by id