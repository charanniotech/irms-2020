Alter table [dbo].[Department] Add [ClientURL] VARCHAR (255) NULL
Alter table [dbo].[Department] Add [IsActive] BIT NULL
Alter table [dbo].[Department] Add [IsDefault] BIT NULL
Alter table [dbo].[Department] Add [IsDeleted] BIT NULL
Alter table [dbo].[Department] Add [EmailFrom] VARCHAR (255) NULL


Alter table [dbo].[Department] Add [SendGridApiKey]              VARCHAR (255)    NULL
Alter table [dbo].[Department] Add [TwilioAccountSid]            VARCHAR (255)    NULL
Alter table [dbo].[Department] Add [TwilioAuthToken]             VARCHAR (255)    NULL
Alter table [dbo].[Department] Add [TwilioNotificationServiceId] VARCHAR (255)    NULL
Alter table [dbo].[Department] Add [UpdateDate]                  DATETIME         NULL
Alter table [dbo].[Department] Add [AdminId]                     UNIQUEIDENTIFIER NULL

select * from [Tenant]
GO

update [dbo].[Department] set [ClientURL] = (select [ClientURL] from Tenant t where t.id = [Department].TenantId)
update [dbo].[Department] set [EmailFrom] = (select [EmailFrom] from Tenant t where t.id = [Department].TenantId)
update [dbo].[Department] set [IsActive] = 1
update [dbo].[Department] set [IsDefault] = 1
update [dbo].[Department] set [IsDeleted] = 0
update [dbo].[Department] set [UpdateDate] = GetUtcDate()

GO
DROP INDEX [UX_Tenant_ClientURL] ON [dbo].[Tenant];
ALTER TABLE [dbo].[Tenant] DROP COLUMN [ClientURL], COLUMN [EmailFrom];

Alter table [dbo].[Tenant] Add [IsDefault] BIT NULL
update [dbo].[Tenant] set [IsDefault] = 0
update [dbo].[Tenant] set [IsDefault] = 1 where Name = 'Default'
Alter table [dbo].[Tenant] ALTER COLUMN [IsDefault] BIT NOT NULL

GO

ALTER TABLE [dbo].[Event] ADD [DepartmentId] UNIQUEIDENTIFIER NULL;

update [dbo].[Event] set [DepartmentId] = (select [Id] from [Department] t where t.TenantId = [Event].TenantId)

-- set departments fields not null

Alter table [dbo].[Department] ALTER COLUMN [ClientURL] VARCHAR (255) NOT NULL
Alter table [dbo].[Department] ALTER COLUMN [IsActive] BIT NOT NULL
Alter table [dbo].[Department] ALTER COLUMN [IsDefault] BIT NOT NULL
Alter table [dbo].[Department] ALTER COLUMN [IsDeleted] BIT NOT NULL
Alter table [dbo].[Department] ALTER COLUMN [EmailFrom] VARCHAR (255) NOT NULL
Alter table [dbo].[Department] ALTER COLUMN [UpdateDate]                  DATETIME         NOT NULL
ALTER TABLE [dbo].[Event] ALTER COLUMN [DepartmentId] UNIQUEIDENTIFIER NOT NULL;

GO 
CREATE UNIQUE NONCLUSTERED INDEX [UX_Tenant_ClientURL]
    ON [dbo].[Department]([ClientURL] ASC, [IsActive] ASC, [IsDeleted] ASC);
GO

--/////////////////////////
GO

Alter table [dbo].[AspNetUsers] Add [DepartmentId] UNIQUEIDENTIFIER NULL
Alter table [dbo].[Blog] Add [DepartmentId] UNIQUEIDENTIFIER NULL
Alter table [dbo].[Employee] Add [DepartmentId] UNIQUEIDENTIFIER NULL
Alter table [dbo].[GalleryAlbum] Add [DepartmentId] UNIQUEIDENTIFIER NULL
Alter table [dbo].[GalleryPhoto] Add [DepartmentId] UNIQUEIDENTIFIER NULL
Alter table [dbo].[HomepageSection] Add [DepartmentId] UNIQUEIDENTIFIER NULL

Alter table [dbo].[SlideshowItem] Add [DepartmentId] UNIQUEIDENTIFIER NULL
Alter table [dbo].[Template] Add [DepartmentId] UNIQUEIDENTIFIER NULL
Alter table [dbo].[TemplateSelection] Add [DepartmentId] UNIQUEIDENTIFIER NULL
Alter table [dbo].[Volunteer] Add [DepartmentId] UNIQUEIDENTIFIER NULL
Alter table [dbo].[VolunteerRegistration] Add [DepartmentId] UNIQUEIDENTIFIER NULL

GO
ALTER TABLE [dbo].[VolunteerRegistration]
    ADD CONSTRAINT [FK_VolunteerRegistration_Department] FOREIGN KEY ([DepartmentId], [TenantId]) REFERENCES [dbo].[Department] ([Id], [TenantId]);
GO

ALTER TABLE [dbo].[Volunteer]
    ADD CONSTRAINT [FK_Volunteer_Department] FOREIGN KEY ([DepartmentId], [TenantId]) REFERENCES [dbo].[Department] ([Id], [TenantId]);
GO

DROP INDEX [UX_TemplateSelection_TypeId]  ON [dbo].[TemplateSelection]

CREATE UNIQUE NONCLUSTERED INDEX [UX_TemplateSelection_TypeId]
    ON [dbo].[TemplateSelection]([TypeId] ASC, [TenantId] ASC);
GO

ALTER TABLE [dbo].[TemplateSelection]
    ADD CONSTRAINT [FK_TemplateSelection_Department] FOREIGN KEY ([DepartmentId], [TenantId]) REFERENCES [dbo].[Department] ([Id], [TenantId]);
GO

ALTER TABLE [dbo].[SlideshowItem]
    ADD CONSTRAINT [FK_SlideshowItem_Department] FOREIGN KEY ([DepartmentId], [TenantId]) REFERENCES [dbo].[Department] ([Id], [TenantId]);
GO

ALTER TABLE [dbo].[Template]
    ADD CONSTRAINT [FK_Template_Department] FOREIGN KEY ([DepartmentId], [TenantId]) REFERENCES [dbo].[Department] ([Id], [TenantId]);
GO

ALTER TABLE [dbo].[HomepageSection]
    ADD CONSTRAINT [FK_HomepageSection_Department] FOREIGN KEY ([DepartmentId], [TenantId]) REFERENCES [dbo].[Department] ([Id], [TenantId]);
GO
ALTER TABLE [dbo].[GalleryPhoto]
    ADD CONSTRAINT [FK_GalleryPhoto_Department] FOREIGN KEY ([DepartmentId], [TenantId]) REFERENCES [dbo].[Department] ([Id], [TenantId]);
GO
ALTER TABLE [dbo].[GalleryAlbum]
    ADD CONSTRAINT [FK_GalleryAlbum_Department] FOREIGN KEY ([DepartmentId], [TenantId]) REFERENCES [dbo].[Department] ([Id], [TenantId]);
GO

ALTER TABLE [dbo].[Event]
    ADD CONSTRAINT [FK_Event_Department] FOREIGN KEY ([DepartmentId], [TenantId]) REFERENCES [dbo].[Department] ([Id], [TenantId]);
GO

ALTER TABLE [dbo].[Employee]
    ADD CONSTRAINT [FK_Employee_Department] FOREIGN KEY ([DepartmentId], [TenantId]) REFERENCES [dbo].[Department] ([Id], [TenantId]);
GO
ALTER TABLE [dbo].[Department]
    ADD CONSTRAINT [FK_Department_Employee] FOREIGN KEY ([AdminId], [TenantId]) REFERENCES [dbo].[Employee] ([Id], [TenantId]);
GO

ALTER TABLE [dbo].[Blog]
    ADD CONSTRAINT [FK_Blog_Department] FOREIGN KEY ([DepartmentId], [TenantId]) REFERENCES [dbo].[Department] ([Id], [TenantId]);
GO

ALTER TABLE [dbo].[AspNetUsers]
    ADD CONSTRAINT [FK_AspNetUsers_Department] FOREIGN KEY ([DepartmentId], [TenantId]) REFERENCES [dbo].[Department] ([Id], [TenantId]);
GO


update [dbo].[AspNetUsers] set [DepartmentId] = (select [Id] from [Department] t where t.TenantId = [AspNetUsers].TenantId and IsDefault = 1) 
update [dbo].[Blog] set [DepartmentId] = (select [Id] from [Department] t where t.TenantId = [Blog].TenantId and IsDefault = 1)
update [dbo].[Employee] set [DepartmentId] = (select [Id] from [Department] t where t.TenantId = [Employee].TenantId and IsDefault = 1)
update [dbo].[GalleryAlbum] set [DepartmentId] = (select [Id] from [Department] t where t.TenantId = [GalleryAlbum].TenantId and IsDefault = 1)
update [dbo].[GalleryPhoto] set [DepartmentId] = (select [Id] from [Department] t where t.TenantId = [GalleryPhoto].TenantId and IsDefault = 1)

update [dbo].[HomepageSection] set [DepartmentId] = (select [Id] from [Department] t where t.TenantId = [HomepageSection].TenantId and IsDefault = 1)
update [dbo].[SlideshowItem] set [DepartmentId] = (select [Id] from [Department] t where t.TenantId = [SlideshowItem].TenantId and IsDefault = 1)
update [dbo].[Template] set [DepartmentId] = (select [Id] from [Department] t where t.TenantId = [Template].TenantId and IsDefault = 1)
update [dbo].[TemplateSelection] set [DepartmentId] = (select [Id] from [Department] t where t.TenantId = [TemplateSelection].TenantId and IsDefault = 1)
update [dbo].[Volunteer] set [DepartmentId] = (select [Id] from [Department] t where t.TenantId = [Volunteer].TenantId and IsDefault = 1)
update [dbo].[VolunteerRegistration] set [DepartmentId] = (select [Id] from [Department] t where t.TenantId = [VolunteerRegistration].TenantId and IsDefault = 1)


GO

Alter table [dbo].[Blog] ALTER COLUMN [DepartmentId] UNIQUEIDENTIFIER NOT NULL
Alter table [dbo].[GalleryAlbum] ALTER COLUMN [DepartmentId] UNIQUEIDENTIFIER NOT NULL
Alter table [dbo].[GalleryPhoto] ALTER COLUMN [DepartmentId] UNIQUEIDENTIFIER NOT NULL
Alter table [dbo].[HomepageSection] ALTER COLUMN [DepartmentId] UNIQUEIDENTIFIER NOT NULL

Alter table [dbo].[SlideshowItem] ALTER COLUMN [DepartmentId] UNIQUEIDENTIFIER NOT NULL
Alter table [dbo].[Template] ALTER COLUMN [DepartmentId] UNIQUEIDENTIFIER NOT NULL
Alter table [dbo].[TemplateSelection] ALTER COLUMN [DepartmentId] UNIQUEIDENTIFIER NOT NULL
Alter table [dbo].[Volunteer] ALTER COLUMN [DepartmentId] UNIQUEIDENTIFIER NOT NULL
Alter table [dbo].[VolunteerRegistration] ALTER COLUMN [DepartmentId] UNIQUEIDENTIFIER NOT NULL

