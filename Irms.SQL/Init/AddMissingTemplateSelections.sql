/*
SELECT TOP 1000 [Id]
      ,[TenantId]
      ,[DepartmentId]
      ,[TypeId]
      ,[EmailTemplateId]
      ,[SmsTemplateId]
      ,[IsEnabled]
FROM [mvms].[dbo].[TemplateSelection]
order by TenantId, DepartmentId, TypeId
*/

select NEWID()

DECLARE @T TABLE (Id INT)
SET NOCOUNT ON
INSERT INTO @T VALUES (0)
INSERT INTO @T VALUES (1)
INSERT INTO @T VALUES (2)
INSERT INTO @T VALUES (3)
INSERT INTO @T VALUES (4)
INSERT INTO @T VALUES (5)
INSERT INTO @T VALUES (6)
INSERT INTO @T VALUES (7)
INSERT INTO @T VALUES (8)
INSERT INTO @T VALUES (9)
INSERT INTO @T VALUES (10)
INSERT INTO @T VALUES (11)
INSERT INTO @T VALUES (12)
INSERT INTO @T VALUES (13)
INSERT INTO @T VALUES (14)
INSERT INTO @T VALUES (15)
INSERT INTO @T VALUES (16)
INSERT INTO @T VALUES (17)
SET NOCOUNT OFF

MERGE INTO [TemplateSelection] as target USING(
	SELECT NEWID(), D.TenantId, D.Id as DepartmentId, T.Id as TypeId,NULL as [EmailTemplateId],NULL as [SmsTemplateId], 0 as IsEnabled
	FROM Department D, @T T

) as source([Id],[TenantId],[DepartmentId],[TypeId],[EmailTemplateId],[SmsTemplateId],[IsEnabled])
on target.[TenantId] = source.[TenantId] and target.[DepartmentId] = source.[DepartmentId] and target.[TypeId] = source.[TypeId]
WHEN NOT MATCHED BY TARGET THEN
	INSERT([Id],[TenantId],[DepartmentId],[TypeId],[EmailTemplateId],[SmsTemplateId],[IsEnabled])
	VALUES([Id],[TenantId],[DepartmentId],[TypeId],[EmailTemplateId],[SmsTemplateId],[IsEnabled]);



select * from TemplateSelection