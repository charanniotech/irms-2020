USE mvms;
GO

BEGIN TRANSACTION

DECLARE @i INT;
DECLARE @aspUserId UNIQUEIDENTIFIER
DECLARE @volunteerId UNIQUEIDENTIFIER
DECLARE @tenantId UNIQUEIDENTIFIER
DECLARE @departmentId UNIQUEIDENTIFIER
DECLARE @email VARCHAR(50)
DECLARE @name VARCHAR(50)

DECLARE @phoneNumber VARCHAR(MAX)
DECLARE @passportNo VARCHAR(MAX)

DECLARE @professionId UNIQUEIDENTIFIER
DECLARE @cityId UNIQUEIDENTIFIER
DECLARE @joiningReasonId UNIQUEIDENTIFIER

SET @i = 1;
SET @tenantId = (SELECT TOP 1 id
                 FROM Tenant
                 WHERE Name = 'Default');
SET @departmentId = (SELECT TOP 1 id
                 FROM Department
                 WHERE Name = 'Default');

SET @professionId = (SELECT TOP 1 id
                     FROM Profession
                     WHERE TenantId = @tenantId);
SET @cityId = (SELECT TOP 1 id
               FROM City
               WHERE TenantId = @tenantId);
SET @joiningReasonId = (SELECT TOP 1 id
                        FROM JoiningReason
                        WHERE TenantId = @tenantId);


WHILE @i < $10000
  BEGIN
    SET @aspUserId = NEWID()
    SET @volunteerId = NEWID()
    SET @email = 'j.doe' + CAST(@i AS VARCHAR(10)) + '@gmail.com'
    SET @name = 'John' + CAST(@i AS VARCHAR(10)) + ' William' + CAST(@i AS VARCHAR(10)) + ' Doe' +
                CAST(@i AS VARCHAR(10))
    SET @phoneNumber = '+' + CAST(ABS(Checksum(NewID()) % 999999999999) AS VARCHAR(13));
    SET @passportNo = 'AA' + CAST(@i AS VARCHAR(10)) + 'BE';

    INSERT INTO AspNetUsers (
      Id,
      AccessFailedCount,
      ConcurrencyStamp,
      Email,
      EmailConfirmed,
      IsSuperAdmin,
      LockoutEnabled,
      LockoutEnd,
      NormalizedEmail,
      NormalizedUserName,
      PasswordHash,
      PhoneNumber,
      PhoneNumberConfirmed,
      SecurityStamp,
      TwoFactorEnabled,
      UserName,
      TenantId,
	  DepartmentId,
      IsEnabled)
    VALUES (
      @aspUserId,
      0,
      NEWID(),
      @email,
      1,
      0,
      1,
      NULL,
      UPPER(@email),
      UPPER(@email),
      'AQAAAAEAACcQAAAAEBNqrOdwg0Qy3KcbJY92UmYRGLBU4OzsX1kRvh4fzRzuRQoiOckQWASN0xPoHiZf0Q==',
      @phoneNumber,
      1,
      NEWID(),
      0,
      @email,
      @tenantId,
	  @departmentId,
      0);

    INSERT INTO mvms.dbo.Volunteer (
      ID,
      TenantId,
      FirstName,
      MiddleName,
      LastName,
      GenderId,
      BirthDate,
      NationalityId,
      NationalId,
      PassportNo,
      ProfessionId,
      AccountTypeId,
      BadgeNo,
      MartialStatusId,
      CityId,
      PhoneNo,
      Email,
      EmergencyContact,
      HasExperience,
      ExperienceLocation,
      ExperienceType,
      ValidationLink,
      JoiningReasonId,
      ParticipationTimeId,
      MaxHoursPerDay,
      HasDiseases,
      Diseases,
      AvatarId,
      UserId,
	  DepartmentId,
      IsDeleted,
      Address,
      SearchContents,
      IsRegistrationFinished)
    VALUES (
      @volunteerId,
      @tenantId,
      'John' + CAST(@i AS VARCHAR(10)),
      'William' + CAST(@i AS VARCHAR(10)),
      'Doe' + CAST(@i AS VARCHAR(10)),
      0,
      '2001-02-15 19:08:49.103',
      1,
      '',
      @passportNo,
      @professionId,
      0,
      '',
      0,
      @cityId,
      @phoneNumber,
      @email,
      '',
      0,
      '',
      '',
      'http://some-fancy-restourant.com',
      @joiningReasonId,
      1,
      3,
      0,
      '',
      NULL,
      @aspUserId,
	  @departmentId,
      0,
      '17th st. 1852 ap 142',
      '',
      0);

    INSERT INTO mvms.dbo.VolunteerRegistration (
      Id,
      TenantId,
      DepartmentId,
      RegistrationDate,
      StatusId,
      VolunteerName,
      VolunteerEmail,
      VolunteerPhone,
      VolunteerNationalId,
      IsDeleted)
    VALUES (
      @volunteerId,
      @tenantId,
      @departmentId,
      '2018-01-17 17:08:49.503',
      0,
      @name,
      @email,
      @phoneNumber,
      @passportNo,
      0);
    SET @i = @i + 1

    INSERT INTO mvms.dbo.VolunteerToSkill
    (
      Id,
      TenantId,
      VolunteerId,
      SkillId)
    VALUES
      (
        newid(),
        @tenantId,
        @volunteerId,
        '7B95FF37-A1E1-420B-8C6D-E6370960C766'
      )

    INSERT INTO mvms.dbo.VolunteerToSkill
    (
      Id,
      TenantId,
      VolunteerId,
      SkillId)
    VALUES
      (
        newid(),
        @tenantId,
        @volunteerId,
        '5E235542-56AA-4C29-8BB2-C7BDD37BFC5B'
      )

    INSERT INTO mvms.dbo.VolunteerParticipationDays
    (
      Id,
      TenantId,
      VolunteerId,
      DayOfWeekId)
    VALUES
      (
        newid(),
        @tenantId,
        @volunteerId,
        1
      )

    INSERT INTO mvms.dbo.VolunteerParticipationDays
    (
      Id,
      TenantId,
      VolunteerId,
      DayOfWeekId)
    VALUES
      (
        newid(),
        @tenantId,
        @volunteerId,
        3
      )


  END

COMMIT