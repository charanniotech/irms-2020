﻿//using Hangfire;
//using Irms.Application;
//using Irms.Application.CampaignInvitations;
//using Irms.Background.Abstract;
//using Irms.Data;
//using Irms.Data.Read.BackgroundService.Queries;
//using Irms.Domain.Entities;
//using MediatR;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.DependencyInjection;
//using System;
//using System.Linq;
//using System.Threading;
//using System.Threading.Tasks;

//namespace Irms.Background.Jobs
//{
//    [AutomaticRetry(Attempts = 0)]
//    public class AcceptedRejectedJob
//    {
//        //private readonly IrmsDataBackgroundJobContext _context;
//        private readonly IMediator _mediator;
//        private readonly IServiceProvider _serviceProvider;
//        private readonly IBackgroundJobsHelperService _backgroundJobsHelper;
//        public AcceptedRejectedJob(//IrmsDataBackgroundJobContext context,
//            IMediator mediator,
//            IBackgroundJobsHelperService backgroundJobsHelper,
//            IServiceProvider serviceProvider)
//        {
//            //_context = context;
//            _mediator = mediator;
//            _backgroundJobsHelper = backgroundJobsHelper;
//            _serviceProvider = serviceProvider;
//        }

//        public async Task DoActions(InvitationType type, CancellationToken token)
//        {
//            using (IServiceScope scope = _serviceProvider.CreateScope())
//            using (IrmsDataBackgroundJobContext _context = scope.ServiceProvider.GetRequiredService<IrmsDataBackgroundJobContext>())
//            {
//                var transType = (int)type;
//                var messages = await _context.CampaignInvitationNextRun
//                    .Include(x => x.CampaignInvitation)
//                    .Include(x => x.Contact)
//                    .Where(x => x.Active && x.Status == 0 && x.CampaignInvitation.InvitationTypeId == transType)
//                    .ToListAsync();

//                if (messages.Any()) // need to send messages
//                {
//                    var templates = await _backgroundJobsHelper.GetCampaignTemplates(messages.Select(x => x.CampaignInvitationId), token);

//                    // Would not update those entites in next job
//                    messages.ForEach(x => x.Status = (int)InvitationMessageStatus.InProgress);
//                    _context.CampaignInvitationNextRun.UpdateRange(messages);

//                    await _context.SaveChangesAsync(token);

//                    var gp = messages.GroupBy(x => x.CampaignInvitationId).ToList();

//                    foreach (var group in gp)
//                    {
//                        var template = templates.FirstOrDefault(y => y.InvitationId == group.Key);

//                        var guests = await _mediator.Send(new GetAcceptedRejectedContactInvitations(group.Key), token);

//                        //var ege = _mapper.Map<List<EventGuest>>(guests.Where(x => x.EmailPreferred && template.EmailBody.IsNotNullOrEmpty()));
//                        var ege = guests.Where(x => x.EmailPreferred && template.EmailBody.IsNotNullOrEmpty())
//                            .Select(x =>
//                            {
//                                return new EventGuest
//                                {
//                                    EmailAcceptedLink = x.EmailAcceptedLink,
//                                    EmailRejectedLink = x.EmailRejectedLink,
//                                    Email = x.Email,
//                                    FullName = x.FullName,
//                                    Id = x.Id,
//                                    Organization = x.Organization,
//                                    Phone = x.Phone,
//                                    Position = x.Position,
//                                    PreferredName = x.PreferredName,
//                                    SenderName = template.EmailSender
//                                };
//                            }).ToList();

//                        //atleast one recipent
//                        if (ege.Any())
//                        {
//                            await _backgroundJobsHelper.SendEmail(ege, template, group.Key, type, token);
//                        }

//                        //sms
//                        //var egs = _mapper.Map<List<EventGuest>>(guests.Where(x => x.SmsPreferred && template.SmsBody.IsNotNullOrEmpty()));
//                        var egs = guests.Where(x => x.SmsPreferred && template.SmsBody.IsNotNullOrEmpty())
//                            .Select(x =>
//                            {
//                                return new EventGuest
//                                {
//                                    SmsAcceptedLink = x.SmsAcceptedLink,
//                                    SmsRejectedLink = x.SmsRejectedLink,
//                                    Email = x.Email,
//                                    FullName = x.FullName,
//                                    Id = x.Id,
//                                    Organization = x.Organization,
//                                    Phone = x.Phone,
//                                    Position = x.Position,
//                                    PreferredName = x.PreferredName,
//                                    SenderName = x.SenderName
//                                };
//                            }).ToList();

//                        //atleast one recipent
//                        if (egs.Any())
//                        {
//                            await _backgroundJobsHelper.SendSms(egs, template, group.Key, type, token);
//                        }

//                        var selectedMessages = messages.Where(x => x.CampaignInvitationId == group.Key)
//                            .ToList();

//                        selectedMessages
//                            .ForEach(x =>
//                            {
//                                x.Status = (int)InvitationMessageStatus.Delivered;
//                                _context.CampaignInvitationNextRun.Update(x);
//                            });
//                        await _context.SaveChangesAsync(token);
//                    }

//                    _context.CampaignInvitationNextRun.RemoveRange(messages);
//                    await _context.SaveChangesAsync(token);
//                }
//            }
//        }

//        //public async Task DoAction(Guid invitationId, InvitationType type, CancellationToken token)
//        //{
//        //    var guests = await _mediator.Send(new GetAcceptedRejectedContactInvitations(invitationId), token);

//        //    //if campaign has guests
//        //    if (guests.Any())
//        //    {
//        //        var templates = await _backgroundJobsHelper.GetCampaignTemplates(invitationId, token);

//        //        //change selected guests status to inprogress
//        //        await _backgroundJobsHelper.ChangeAcceptedRejectedGuestsStatus(_backgroundJobsHelper.AcceptedRejectedGuestsToUpdate(guests, templates.TenantId, invitationId), token);

//        //        //email
//        //        var ege = _mapper.Map<List<EventGuest>>(guests.Where(x => x.EmailPreferred && templates.EmailBody.IsNotNullOrEmpty()));

//        //        //atleast one recipent
//        //        if (ege.Any())
//        //        {
//        //            await _backgroundJobsHelper.SendEmail(ege, templates, invitationId, type, token);
//        //        }

//        //        //sms
//        //        var egs = _mapper.Map<List<EventGuest>>(guests.Where(x => x.SmsPreferred && templates.SmsBody.IsNotNullOrEmpty()));

//        //        //atleast one recipent
//        //        if (egs.Any())
//        //        {
//        //            await _backgroundJobsHelper.SendSms(egs, templates, invitationId, type, token);
//        //        }
//        //    }
//        //}
//    }
//}
