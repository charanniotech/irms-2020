﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Background.ReadModels
{
    public class CampaignTemplates
    {
        public CampaignTemplates(Guid tenantId,
            string eventName,
            DateTime eventDateTime,
            string eventLocation,
            string emailSender,
            string emailSubject,
            string emailBody, string smsBody, string whatsappBody,
            Guid invitationId)
        {
            InvitationId = invitationId;
            TenantId = tenantId;
            EventName = eventName;
            EventDateTime = eventDateTime;
            EventLocation = eventLocation;
            EmailSender = emailSender;
            EmailSubject = emailSubject;
            EmailBody = emailBody;
            SmsBody = smsBody;
            WhatsappBody = whatsappBody;
        }

        public Guid InvitationId { get; set; }
        public Guid TenantId { get; set; }
        public string EventName { get; set; }
        public DateTime EventDateTime { get; set; }
        public string EventLocation { get; set; }
        public string EmailSender { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public string WhatsappBody { get; set; }
        public string SmsBody { get; set; }
    }
}
