﻿using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.CampaignInvitations;
using Irms.Background.Abstract;
using Irms.Background.ReadModels;
using Irms.Data;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Twilio.TwiML.Messaging;
using static Irms.Domain.Entities.ContactListRFI;

namespace Irms.Background
{
    public class BackgroundJobsHelperService : IBackgroundJobsHelperService
    {
        private readonly IrmsDataBackgroundJobContext _context;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly ICampaignInvitationNotifier _notifier;
        private readonly IBaseMediumNotifier _mediumNotifier;
        public BackgroundJobsHelperService(IrmsDataBackgroundJobContext context,
            IMediator mediator,
            IMapper mapper,
            ICampaignInvitationNotifier notifier,
            IBaseMediumNotifier mediumNotifier)
        {
            _context = context;
            _mediator = mediator;
            _mapper = mapper;
            _notifier = notifier;
            _mediumNotifier = mediumNotifier;
        }

        public async Task<CampaignTemplates> GetCampaignTemplates(Guid invitationId, CancellationToken token)
        {
            var temp = await _context.CampaignInvitation
                .AsNoTracking()
                .Where(x => x.Id == invitationId)
                .Select(x => new CampaignTemplates(x.TenantId,
                        x.EventCampaign.Event.Name,
                        x.EventCampaign.Event.StartDateTime,
                        x.EventCampaign.Event.EventLocation.First().Name,
                        x.CampaignEmailTemplate.FirstOrDefault() == null ? null : x.CampaignEmailTemplate.FirstOrDefault().SenderEmail,
                        x.CampaignEmailTemplate.FirstOrDefault() == null ? null : x.CampaignEmailTemplate.FirstOrDefault().Subject,
                        x.CampaignEmailTemplate.FirstOrDefault().Body == null ? null : x.CampaignEmailTemplate.FirstOrDefault().Body,
                        x.CampaignSmsTemplate.FirstOrDefault() == null ? null : x.CampaignSmsTemplate.FirstOrDefault().Body,
                        x.CampaignWhatsappTemplate.FirstOrDefault() == null ? null : x.CampaignWhatsappTemplate.FirstOrDefault().Body,
                        invitationId
                    )
                )
                .FirstOrDefaultAsync(token);

            return temp;
        }

        public async Task<IEnumerable<CampaignTemplates>> GetCampaignTemplates(IEnumerable<Guid> invitationIds, CancellationToken token)
        {
            var temp = await _context.CampaignInvitation
                .AsNoTracking()
                .Where(x => invitationIds.Contains(x.Id))
                .Select(x => new CampaignTemplates(x.TenantId,
                        x.EventCampaign.Event.Name,
                        x.EventCampaign.Event.StartDateTime,
                        x.EventCampaign.Event.EventLocation.First().Name,
                        x.CampaignEmailTemplate.FirstOrDefault() == null ? null : x.CampaignEmailTemplate.FirstOrDefault().SenderEmail,
                        x.CampaignEmailTemplate.FirstOrDefault() == null ? null : x.CampaignEmailTemplate.FirstOrDefault().Subject,
                        x.CampaignEmailTemplate.FirstOrDefault().Body == null ? null : x.CampaignEmailTemplate.FirstOrDefault().Body,
                        x.CampaignSmsTemplate.FirstOrDefault() == null ? null : x.CampaignSmsTemplate.FirstOrDefault().Body,
                        x.CampaignWhatsappTemplate.FirstOrDefault() == null ? null : x.CampaignWhatsappTemplate.FirstOrDefault().Body,
                        x.Id
                    )
                )
                .ToListAsync(token);

            return temp;
        }

        public async Task ChangeInvitationStatus(Guid campaignInvitationId, InvitationStatus status, CancellationToken token)
        {
            var inv = await _context.CampaignInvitation
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == campaignInvitationId, token);

            inv.Status = (int)status;
            inv.ModifiedOn = DateTime.UtcNow;

            await _context.SaveChangesAsync(token);
        }

        public IEnumerable<CampaignInvitationMessageLog> RsvpPendingGuestsToUpdate(IEnumerable<Application.BackgroundService.ReadModels.GuestsListItem> guests, Guid tenantId, Guid campaignInvitationId)
        {
            return guests
                .Select(x => new CampaignInvitationMessageLog
                {
                    Id = Guid.NewGuid(),
                    TenantId = tenantId,
                    CampaignInvitationId = campaignInvitationId,
                    ContactId = x.Id,
                    MessageStatus = (int)InvitationMessageStatus.InProgress,
                    TimeStamp = DateTime.UtcNow
                });
        }

        public async Task ChangeRsvpPendingGuestsStatus(IEnumerable<CampaignInvitationMessageLog> guests, CancellationToken token)
        {
            var inv = await _context.CampaignInvitationMessageLog
                .AsNoTracking()
                .Where(x => guests.Select(y => y.ContactId).Contains(x.ContactId))
                .ToListAsync(token);

            var (toAdd, _, toUpd) = inv.Compare(guests, (o, n) => o.ContactId == n.ContactId
            && o.CampaignInvitationId == n.CampaignInvitationId
            && o.MessageStatus == n.MessageStatus
            && o.InvitationTemplateTypeId == n.InvitationTemplateTypeId);

            //add
            //var logs = _mapper.Map<List<EntityClasses.CampaignInvitationMessageLog>>(guests);
            await _context.CampaignInvitationMessageLog.AddRangeAsync(
                toAdd.Select(x => _mapper.Map<Data.EntityClasses.CampaignInvitationMessageLog>(x)),
                token);

            //update
            foreach (var (o, n) in toUpd)
            {
                o.TimeStamp = DateTime.UtcNow;
            }

            await _context.SaveChangesAsync(token);
        }
        public async Task<bool> SendSms(List<EventGuest> egs, CampaignTemplates templates, Guid invitationId, InvitationType type, CancellationToken token)
        {
            var smsPlaceholders = await _context.CampaignPlaceholder
                 .AsNoTracking()
                 .Where(x => x.InvitationTypeId == (int)type && x.TemplateTypeId == (int)InvitationTemplateType.Sms)
                 .Select(x => x.Placeholder)
                 .ToListAsync(token);

            bool msgsSent = true;
            foreach (var gst in egs)
            {
                //sms message
                var msg = new InvitationMessage
                {
                    TenantId = templates.TenantId,
                    CampaignInvitationId = invitationId,
                    Body = templates.SmsBody,
                    EventName = templates.EventName,
                    EventDateTime = templates.EventDateTime,
                    EventLocation = templates.EventLocation,
                    Recipients = gst.Enumerate().ToList(),
                    Placeholders = smsPlaceholders.ToList()
                };

                //var response = await _mediator.Send(new SendSmsCmd(msg), token);
                var response = await _notifier.NotifyBySms(msg, token);
                var recipient = response.msg.Recipients.First();
                if (response.sent)
                {
                    string body = response.msg.MessageTemplate;
                    foreach (var variable in recipient.TemplateVariables)
                    {
                        body = body.Replace(variable.Name, variable.Value);
                    }

                    var log = new CampaignInvitationMessageLog
                    {
                        Id = Guid.NewGuid(),
                        TenantId = templates.TenantId,
                        CampaignInvitationId = invitationId,
                        ContactId = recipient.Id,
                        InvitationTemplateTypeId = (int)InvitationTemplateType.Sms,
                        ProviderTypeId = (int)response.msg.ProviderType,
                        MessageId = response.msg.MessageId,
                        MessageStatus = (int)InvitationMessageStatus.Triggered,
                        MessageText = body,
                        TimeStamp = DateTime.UtcNow
                    };

                    await LogMessages(log.Enumerate().ToList(), token);

                    //update status of campaigninvitationnextrun
                    if (type == InvitationType.Accepted || type == InvitationType.Rejected)
                    {
                        var guests = log.Enumerate()
                            .Select(x => new CampaignInvitationNextRun
                            {
                                Id = gst.Id,
                                Status = InvitationMessageStatus.Triggered
                            });
                        await ChangeAcceptedRejectedGuestsStatus(guests, token);
                    }
                }
                else
                {
                    string body = response.msg.MessageTemplate;
                    foreach (var variable in recipient.TemplateVariables)
                    {
                        body = body.Replace(variable.Name, variable.Value);
                    }

                    var log = new CampaignInvitationMessageLog
                    {
                        Id = Guid.NewGuid(),
                        TenantId = templates.TenantId,
                        CampaignInvitationId = invitationId,
                        ContactId = recipient.Id,
                        InvitationTemplateTypeId = (int)InvitationTemplateType.Sms,
                        ProviderTypeId = (int)response.msg.ProviderType,
                        MessageStatus = (int)InvitationMessageStatus.Draft,
                        MessageText = body,
                        TimeStamp = DateTime.UtcNow
                    };
                    await ChangeRsvpPendingGuestsStatus(log.Enumerate().ToList(), token);

                    //update status of campaigninvitationnextrun
                    if (type == InvitationType.Accepted || type == InvitationType.Rejected)
                    {
                        var guests = log.Enumerate()
                            .Select(x => new CampaignInvitationNextRun
                            {
                                Id = gst.Id,
                                Status = InvitationMessageStatus.Draft
                            });
                        await ChangeAcceptedRejectedGuestsStatus(guests, token);
                    }

                    msgsSent = false;
                }
            }
            return msgsSent;
        }

        public async Task<bool> SendWhatsappMessage(List<EventGuest> egw, CampaignTemplates templates, Guid invitationId, InvitationType type, CancellationToken token)
        {
            var whatsappPlaceholders = await _context.CampaignPlaceholder
                 .AsNoTracking()
                 .Where(x => x.InvitationTypeId == (int)type && x.TemplateTypeId == (int)InvitationTemplateType.WhatsApp)
                 .Select(x => x.Placeholder)
                 .ToListAsync(token);

            bool whatsappMsgSent = true;
            foreach (var gst in egw)
            {
                //sms message
                var msg = new InvitationMessage
                {
                    TenantId = templates.TenantId,
                    CampaignInvitationId = invitationId,
                    Body = templates.WhatsappBody,
                    EventName = templates.EventName,
                    EventDateTime = templates.EventDateTime,
                    EventLocation = templates.EventLocation,
                    Recipients = gst.Enumerate().ToList(),
                    Placeholders = whatsappPlaceholders.ToList()
                };

                //var response = await _mediator.Send(new SendSmsCmd(msg), token);
                var response = await _notifier.NotifyByWhatsapp(msg, token);
                var recipient = response.msg.Recipients.First();
                if (response.sent)
                {
                    string body = response.msg.MessageTemplate;
                    foreach (var variable in recipient.TemplateVariables)
                    {
                        body = body.Replace(variable.Name, variable.Value);
                    }

                    var log = new CampaignInvitationMessageLog
                    {
                        Id = Guid.NewGuid(),
                        TenantId = templates.TenantId,
                        CampaignInvitationId = invitationId,
                        ContactId = recipient.Id,
                        InvitationTemplateTypeId = (int)InvitationTemplateType.WhatsApp,
                        ProviderTypeId = (int)response.msg.ProviderType,
                        MessageId = response.msg.MessageId,
                        MessageStatus = (int)InvitationMessageStatus.Triggered,
                        MessageText = body,
                        TimeStamp = DateTime.UtcNow
                    };

                    await LogMessages(log.Enumerate().ToList(), token);

                    //update status of campaigninvitationnextrun
                    if (type == InvitationType.Accepted || type == InvitationType.Rejected)
                    {
                        var guests = log.Enumerate()
                            .Select(x => new CampaignInvitationNextRun
                            {
                                Id = gst.Id,
                                Status = InvitationMessageStatus.Triggered
                            });
                        await ChangeAcceptedRejectedGuestsStatus(guests, token);
                    }
                }
                else
                {
                    string body = response.msg.MessageTemplate;
                    foreach (var variable in recipient.TemplateVariables)
                    {
                        body = body.Replace(variable.Name, variable.Value);
                    }

                    var log = new CampaignInvitationMessageLog
                    {
                        Id = Guid.NewGuid(),
                        TenantId = templates.TenantId,
                        CampaignInvitationId = invitationId,
                        ContactId = recipient.Id,
                        InvitationTemplateTypeId = (int)InvitationTemplateType.WhatsApp,
                        ProviderTypeId = (int)response.msg.ProviderType,
                        MessageStatus = (int)InvitationMessageStatus.Draft,
                        MessageText = body,
                        TimeStamp = DateTime.UtcNow
                    };
                    await ChangeRsvpPendingGuestsStatus(log.Enumerate().ToList(), token);

                    //update status of campaigninvitationnextrun
                    if (type == InvitationType.Accepted || type == InvitationType.Rejected)
                    {
                        var guests = log.Enumerate()
                            .Select(x => new CampaignInvitationNextRun
                            {
                                Id = gst.Id,
                                Status = InvitationMessageStatus.Draft
                            });
                        await ChangeAcceptedRejectedGuestsStatus(guests, token);
                    }

                    whatsappMsgSent = false;
                }
            }
            return whatsappMsgSent;
        }


        private async Task LogMessages(List<CampaignInvitationMessageLog> logs, CancellationToken token)
        {
            var data = _mapper.Map<List<Data.EntityClasses.CampaignInvitationMessageLog>>(logs);
            await _context.CampaignInvitationMessageLog.AddRangeAsync(data, token);
            await AppendToEventLog(logs.First(), token);
            await _context.SaveChangesAsync(token);
        }

        private async Task AppendToEventLog(CampaignInvitationMessageLog log, CancellationToken token)
        {
            var record = new Data.EntityClasses.EventLog
            {
                Id = Guid.NewGuid(),
                OccurredOn = DateTime.UtcNow,
                EventType = log.GetType().AssemblyQualifiedName,
                Action = "Campaign invitation sent",
                Description = $"Campaign {log.CampaignInvitationId} {(InvitationTemplateType)log.InvitationTemplateTypeId} message is sent to contact {log.ContactId}",
                TenantId = log.TenantId,
                Data = Newtonsoft.Json.JsonConvert.SerializeObject(log),
                ObjectId = log.CampaignInvitationId,
                ObjectId2 = log.ContactId,
                ObjectId3 = null,
            };

            await _context.EventLog.AddAsync(record, token);
        }

        public async Task<bool> SendContactListRfiEmail(PrefferedMediaReadModel prefference, string subject, string body, Guid tenantId, CancellationToken token)
        {
            return await _mediumNotifier.NotifyByEmail(subject, body, prefference.Guest.Email, tenantId, token);
        }

        public async Task<bool> SendContactListRfiSms(PrefferedMediaReadModel prefference, string body, Guid tenantId, CancellationToken token)
        {
            return await _mediumNotifier.NotifyBySms(prefference.Guest.Id, body, prefference.Guest.Phone, tenantId, token);
        }

        /// <summary>
        /// send emails to guests
        /// </summary>
        /// <param name="ege"></param>
        /// <param name="templates"></param>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> SendEmail(List<EventGuest> ege, CampaignTemplates templates, Guid invitationId, InvitationType type, CancellationToken token)
        {
            var emailPlaceholders = await _context.CampaignPlaceholder
                .AsNoTracking()
                .Where(x => x.InvitationTypeId == (int)type && x.TemplateTypeId == (int)InvitationTemplateType.Email)
                .Select(x => x.Placeholder)
                .ToListAsync(token);

            bool emailsSent = true;
            foreach (var gst in ege)
            {
                //email message
                var msg = new InvitationMessage
                {
                    TenantId = templates.TenantId,
                    Sender = templates.EmailSender,
                    Subject = templates.EmailSubject,
                    Body = templates.EmailBody,
                    EventName = templates.EventName,
                    EventDateTime = templates.EventDateTime,
                    EventLocation = templates.EventLocation,
                    Recipients = gst.Enumerate().ToList(),
                    Placeholders = emailPlaceholders.ToList(),
                    CampaignInvitationId = invitationId
                };

                var emsg = await _notifier.NotifyByEmail(msg, token);
                var recipient = emsg.msg.Recipients.First();

                if (emsg.sent)
                {
                    string body = emsg.msg.HtmlTemplate;
                    foreach (var variable in recipient.TemplateVariables)
                    {
                        body = body.Replace(variable.Name, variable.Value);
                    }

                    var log = new CampaignInvitationMessageLog
                    {
                        Id = Guid.NewGuid(),
                        TenantId = templates.TenantId,
                        CampaignInvitationId = invitationId,
                        ContactId = recipient.Id,
                        MessageId = emsg.msg.MessageId,
                        InvitationTemplateTypeId = (int)InvitationTemplateType.Email,
                        ProviderTypeId = (int)emsg.msg.ProviderType,
                        MessageStatus = (int)InvitationMessageStatus.Triggered,
                        MessageText = body,
                        TimeStamp = DateTime.UtcNow
                    };
                    await LogMessages(log.Enumerate().ToList(), token);
                }
                else
                {
                    string body = emsg.msg.HtmlTemplate;
                    foreach (var variable in recipient.TemplateVariables)
                    {
                        body = body.Replace(variable.Name, variable.Value);
                    }

                    var log = new CampaignInvitationMessageLog
                    {
                        Id = Guid.NewGuid(),
                        TenantId = templates.TenantId,
                        CampaignInvitationId = invitationId,
                        ContactId = recipient.Id,
                        InvitationTemplateTypeId = (int)InvitationTemplateType.Email,
                        ProviderTypeId = (int)emsg.msg.ProviderType,
                        MessageStatus = (int)InvitationMessageStatus.Draft,
                        MessageText = body,
                        TimeStamp = DateTime.UtcNow
                    };

                    await ChangeRsvpPendingGuestsStatus(log.Enumerate().ToList(), token);
                    emailsSent = false;
                }
            }

            return emailsSent;
        }
        public IEnumerable<CampaignInvitationNextRun> AcceptedRejectedGuestsToUpdate(IEnumerable<Data.Read.BackgroundService.ReadModels.AcceptedRejectedContactInvitationListItem> guests, Guid tenantId, Guid campaignInvitationId)
        {
            return guests
                .Select(x => new CampaignInvitationNextRun
                {
                    Id = x.Id,
                    Status = InvitationMessageStatus.InProgress
                });
        }

        public async Task ChangeAcceptedRejectedGuestsStatus(IEnumerable<CampaignInvitationNextRun> guests, CancellationToken token)
        {
            var inv = await _context.CampaignInvitationNextRun
                .AsNoTracking()
                .Where(x => guests.Select(y => y.ContactId).Contains(x.ContactId))
                .ToListAsync(token);

            var (toAdd, _, toUpd) = inv.Compare(guests, (o, n) => o.Id == n.Id);

            //update
            foreach (var (o, n) in toUpd)
            {
                o.Status = (int)n.Status;
            }

            await _context.SaveChangesAsync(token);
        }

        public async Task<ContactListRFI> FindContactListRfi(Guid id, CancellationToken token)
        {
            var data = await _context.ContactListRFI
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id, token);

            return _mapper.Map<ContactListRFI>(data);
        }

        public async Task UpdateContactRfiFormStatus(Guid id, ContactListRfiStatus status, CancellationToken token)
        {
            var data = await _context.ContactListRFI
                .FirstOrDefaultAsync(x => x.Id == id, token);

            data.Status = (int)status;

            await _context.SaveChangesAsync(token);
        }

        public async Task<IEnumerable<PrefferedMediaReadModel>> FetchPrefferedMediasFromContactListRfiForm(Guid contactListId, CancellationToken token)
        {
            var contactIds = await _context.ContactListToContacts
                .Where(x => x.ContactListId == contactListId)
                .Select(x => x.ContactId)
                .ToListAsync(token);

            var prefferedMedias = await _context.CampaignPrefferedMedia
                .Where(x => contactIds.Contains(x.ContactId))
                .Select(x => new PrefferedMediaReadModel(new EventGuest
                {
                    Id = x.Id,
                    FullName = x.Contact.FullName,
                    PreferredName = x.Contact.PreferredName,
                    Email = x.Contact.FullName,
                    Organization = x.Contact.Organization,
                    Phone = x.Contact.MobileNumber,
                    Position = x.Contact.Position
                }, x.Email, x.Sms))
                .ToListAsync(token);

            return prefferedMedias;
        }

        public async Task<IEnumerable<ContactListRFIAnswer>> CreateContactListRfiAnswers(IEnumerable<PrefferedMediaReadModel> guests, ContactListRFI rfi, CancellationToken token)
        {
            var answers = new List<Data.EntityClasses.ContactListRFIAnswer>();

            foreach (var guest in guests)
            {
                if (guest.HasEmail)
                {
                    answers.Add(new Data.EntityClasses.ContactListRFIAnswer
                    {
                        ContactId = guest.Guest.Id,
                        ContactListRfiId = rfi.Id,
                        Id = Guid.NewGuid(),
                        MediaType = (int)MediaType.Email,
                        TenantId = rfi.TenantId
                    });
                }
                if (guest.HasSms)
                {
                    answers.Add(new Data.EntityClasses.ContactListRFIAnswer
                    {
                        ContactId = guest.Guest.Id,
                        ContactListRfiId = rfi.Id,
                        Id = Guid.NewGuid(),
                        MediaType = (int)MediaType.Sms,
                        TenantId = rfi.TenantId
                    });
                }
            }

            _context.ContactListRFIAnswer.AddRange(answers);
            await _context.SaveChangesAsync(token);
            return _mapper.Map<IEnumerable<ContactListRFIAnswer>>(answers);
        }
    }
}
