﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Text;

namespace Irms.Infrastructure.Helpers
{
    public static class ColorTranslator
    {
        public static Color FromHtml(string colorCode)
        {
            colorCode = colorCode.TrimStart('#');

            Color color; // without alpha
            if (colorCode.Length == 6)
                color = Color.FromArgb(255, 
                            int.Parse(colorCode.Substring(0, 2), NumberStyles.HexNumber),
                            int.Parse(colorCode.Substring(2, 2), NumberStyles.HexNumber),
                            int.Parse(colorCode.Substring(4, 2), NumberStyles.HexNumber));
            else // with alpha
                color = Color.FromArgb(
                            int.Parse(colorCode.Substring(0, 2), NumberStyles.HexNumber),
                            int.Parse(colorCode.Substring(2, 2), NumberStyles.HexNumber),
                            int.Parse(colorCode.Substring(4, 2), NumberStyles.HexNumber),
                            int.Parse(colorCode.Substring(6, 2), NumberStyles.HexNumber));

            return color;
        }
    }
}
