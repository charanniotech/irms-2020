﻿using Irms.Application.Abstract;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PhoneNumbers;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services.EmailReachability
{
    public class WhatsappReachabilityManager : IWhatsappReachabilityManager
    {
        private readonly IConfiguration _config;
        public WhatsappReachabilityManager(IConfiguration config)
        {
            _config = config;
        }

        public async Task<bool> CheckWhatsappReachabilityAsync(string unvalidatedPhone)
        {
            //Thread.Sleep(TimeSpan.FromSeconds(4));
            //return new Random().Next(100) <= 50;
            if (string.IsNullOrWhiteSpace(unvalidatedPhone))
            {
                return false;
            }

            var util = PhoneNumberUtil.GetInstance();
            var phone = util.Parse(unvalidatedPhone, null);
            bool isValid = util.IsValidNumber(phone);
            if (!isValid)
            {
                return true;
            }

            var requestContent = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("user", _config["Checkwa:User"]),
                new KeyValuePair<string, string>("apikey", _config["Checkwa:ApiKey"]),
                new KeyValuePair<string, string>("action", "check"),
                new KeyValuePair<string, string>("num", phone.NationalNumber.ToString()),
                new KeyValuePair<string, string>("cod", phone.CountryCode.ToString()),
            });

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Access-Control-Allow-Origin", "*");

            HttpResponseMessage response = await client.PostAsync(
                "https://webservice.checkwa.com/",
                requestContent);
            var resp = await response.Content.ReadAsStreamAsync();
            using (var reader = new StreamReader(resp))
            {
                var result = await reader.ReadToEndAsync();
                dynamic res = JsonConvert.DeserializeObject(result);
                string code = res.code;
                if (code == "001")
                {
                    return true;
                }
                return false;
            }
        }
    }
}
