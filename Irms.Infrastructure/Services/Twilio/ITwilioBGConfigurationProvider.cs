﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services.Twilio
{
    public interface ITwilioBGConfigurationProvider
    {
        Task<TwilioConfiguration> GetConfiguration(Guid tenantId, CancellationToken token);
    }
}