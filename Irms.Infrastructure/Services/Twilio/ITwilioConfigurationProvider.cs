﻿using System.Threading;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services.Twilio
{
    public interface ITwilioConfigurationProvider
    {
        Task<TwilioConfiguration> GetConfiguration(CancellationToken token);
    }
}
