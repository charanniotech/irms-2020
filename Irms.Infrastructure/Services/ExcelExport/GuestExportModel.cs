﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Infrastructure.Services.ExcelExport
{
    public class GuestExportModel : IExportModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string FullName { get; set; }
        public string PreferredName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string EventName { get; set; }
        public string AlternativeEmail { get; set; }
        public string MobileNumber { get; set; }
        public string WorkNumber { get; set; }
        public string SecondaryMobileNumber { get; set; }
        public string Nationality { get; set; }
        public string DocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public string IssuingCountry { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string IsGuest { get; set; }
        public string CreatedBy { get; set; }
        public string CreatorEmail { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifierEmail { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
