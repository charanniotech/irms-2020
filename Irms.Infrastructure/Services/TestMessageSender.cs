﻿using Irms.Application.Abstract.Services.Notifications;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services
{
    public class TestMessageSender : ISmsSender, IEmailSender
    {
        private static readonly string SectionTitleSeparator = new string('-', 20) + Environment.NewLine;

        #region email
        public Task<bool> SendEmail(EmailMessage message, CancellationToken token)
        {
            var dir = Path.Combine(Directory.GetCurrentDirectory(), "Emails");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            foreach (var recipient in message.Recipients)
            {
                var body = message.HtmlTemplate ?? string.Empty;
                var subj = recipient.Subject ?? string.Empty;

                foreach (var variable in recipient.TemplateVariables)
                {
                    body = body.Replace(variable.Name, variable.Value);
                    subj = subj.Replace(variable.Name, variable.Value);
                }

                var msgContent = $"Subject{SectionTitleSeparator}{subj}{Environment.NewLine}{Environment.NewLine}Body{SectionTitleSeparator}{body}";

                File.WriteAllText(Path.Combine(dir, $"{recipient.EmailAddress}-{DateTime.Now.Ticks}.html"), msgContent);
            }

            return Task.FromResult(true);
        }
        #endregion

        #region sms
        public Task<bool> SendSms(SmsMessage message, CancellationToken token)
        {
            var dir = Path.Combine(Directory.GetCurrentDirectory(), "SMS");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            foreach (var recipient in message.Recipients)
            {
                var body = message.MessageTemplate ?? string.Empty;

                foreach (var variable in recipient.TemplateVariables)
                {
                    body = body.Replace(variable.Name, variable.Value);
                }

                File.WriteAllText(Path.Combine(dir, $"{recipient.Phone}-{DateTime.Now.Ticks}.html"), body);
            }

            return Task.FromResult(true);
        }

        public Task<bool> SendWebhookSms(SmsMessage message, CancellationToken token)
        {
            var dir = Path.Combine(Directory.GetCurrentDirectory(), "SMS");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            foreach (var recipient in message.Recipients)
            {
                var body = message.MessageTemplate ?? string.Empty;

                foreach (var variable in recipient.TemplateVariables)
                {
                    body = body.Replace(variable.Name, variable.Value);
                }

                File.WriteAllText(Path.Combine(dir, $"{recipient.Phone}-{DateTime.Now.Ticks}.html"), body);
            }

            return Task.FromResult(true);
        }

        #endregion
    }
}
