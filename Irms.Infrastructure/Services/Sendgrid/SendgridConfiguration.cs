﻿namespace Irms.Infrastructure.Services.Sendgrid
{
    public sealed class SendgridConfiguration
    {
        public SendgridConfiguration(string apiKey, string emailFrom)
        {
            ApiKey = apiKey;
            EmailFrom = emailFrom;
        }

        public string ApiKey { get; }
        public string EmailFrom { get; }

        public bool IsValid =>
            !string.IsNullOrWhiteSpace(ApiKey)
            && !string.IsNullOrWhiteSpace(EmailFrom);
    }
}
