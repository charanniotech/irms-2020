﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Irms.Application.Abstract.Services.Notifications;
using SendGrid;
using SendGrid.Helpers.Mail;
using MediatR;
using Irms.Application.Abstract;
using Irms.Application.Events.Commands;
using Irms.Domain.Entities;
using Newtonsoft.Json;
using Irms.Application;

namespace Irms.Infrastructure.Services.Sendgrid
{
    /// <summary>
    /// Implementation of Email sender by SendGrid API
    /// </summary>
    public class SendgridEmailSender : ISendgridEmailSender
    {
        private readonly ISendgridConfigurationProvider _configurationProvider;
        private readonly ILogger<SendgridEmailSender> _logger;
        private readonly IMediator _mediator;
        private readonly ICurrentUser _user;
        private readonly TenantBasicInfo _tenant;

        public SendgridEmailSender(ISendgridConfigurationProvider configurationProvider,
            ILogger<SendgridEmailSender> logger,
            IMediator mediator,
            ICurrentUser user,
            TenantBasicInfo tenant)
        {
            _configurationProvider = configurationProvider;
            _logger = logger;
            _mediator = mediator;
            _user = user;
            _tenant = tenant;
        }

        public async Task<bool> SendEmail(EmailMessage message, CancellationToken token)
        {
            var cfg = await _configurationProvider.GetConfiguration(token);
            if (!cfg.IsValid)
            {
                throw new Exception("Sendgrid email is not configured");
            }

            return await SendEmailInternal(message, cfg, token);
        }

        public async Task<bool> SendEmailInternal(EmailMessage message, SendgridConfiguration cfg, CancellationToken token)
        {
            var client = new SendGridClient(cfg.ApiKey);
            var msg = ConvertMessage(message, cfg.EmailFrom);

            var response = await client.SendEmailAsync(msg, token);
            if (response.StatusCode == HttpStatusCode.Accepted || response.StatusCode == HttpStatusCode.OK)
            {
                var cmd = new ProviderLogsCmd
                {
                    ProviderType = ProviderType.Sendgrid,
                    TenantId = _tenant.Id,
                    CampaignInvitationId = message.CampaignInvitationId,
                    Request = JsonConvert.SerializeObject(msg),
                    UserId = _user.Id,
                    RequestDate = DateTime.UtcNow,
                    Response = JsonConvert.SerializeObject(response),
                    ResponseDate = DateTime.UtcNow
                };

                await _mediator.Send(cmd, token);
                foreach (var recipient in message.Recipients)
                {
                    _logger.LogInformation("Email \"{0}\" to address \"{1}\" has been sent", recipient.Subject, recipient.EmailAddress);
                }
                return true;
            }
            else
            {
                _logger.LogError("Can not send email: " + await response.Body.ReadAsStringAsync() + " " + response.StatusCode);
                return false;
            }
        }

        internal SendGridMessage ConvertMessage(EmailMessage src, string emailFrom)
        {
            return new SendGridMessage
            {
                From = new EmailAddress(emailFrom),
                HtmlContent = src.HtmlTemplate,
                Personalizations = src.Recipients.Select(ConvertRecipient).ToList()
            };
        }

        internal Personalization ConvertRecipient(EmailMessage.Recipient src)
        {
            return new Personalization
            {
                Subject = src.Subject,
                Tos = new List<EmailAddress> { new EmailAddress(src.EmailAddress) },
                Substitutions = src.TemplateVariables.ToDictionary(k => k.Name, v => v.Value)
            };
        }
    }
}
