﻿using Irms.Application.Abstract.Services;
using PhoneNumbers;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Irms.Infrastructure.Services
{
    public class PhoneNumberValidator : IPhoneNumberValidator
    {
        public IValidPhoneNumber Validate(string unvalidatedPhone, out bool isValid, string defaultCountry = default)
        {
            var (valid, validatedPhone) = Validate(unvalidatedPhone, defaultCountry);
            isValid = valid;
            return validatedPhone;
        }

        public (bool isValid, IValidPhoneNumber number) Validate(string unvalidatedPhone, string defaultCountry = default)
        {
            if (string.IsNullOrWhiteSpace(unvalidatedPhone))
            {
                return (false, null);
            }

            var util = PhoneNumberUtil.GetInstance();
            try
            {

                var phone = util.Parse(unvalidatedPhone, null);
                bool isValid = util.IsValidNumber(phone);
                if (!isValid)
                {
                    return (false, null);
                }

                var validatedPhone = util.Format(phone, PhoneNumberFormat.E164);
                return (true, new ValidPhoneNumber(validatedPhone));
            }
            catch (NumberParseException)
            {
                return (false, null);
            }
        }

        /// <summary>
        /// validate phone
        /// </summary>
        /// <param name="unvalidatedPhone"></param>
        /// <returns></returns>
        public bool CheckPhoneValidation(string unvalidatedPhone)
        {
            //Thread.Sleep(TimeSpan.FromSeconds(4));
            //return new Random().Next(100) <= 50;
            var util = PhoneNumberUtil.GetInstance();

            if (string.IsNullOrWhiteSpace(unvalidatedPhone))
            {
                return false;
            }

            try
            {

                var phone = util.Parse(unvalidatedPhone, null);
                bool isValid = util.IsValidNumber(phone);
                if (isValid)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (NumberParseException)
            {
                return false;
            }
        }

        /// <summary>
        /// validate phone bulk
        /// </summary>
        /// <param name="unvalidatedPhones"></param>
        /// <returns></returns>
        public List<(bool isValid, IValidPhoneNumber number)> ValidateBulk(List<string> unvalidatedPhones)
        {
            var util = PhoneNumberUtil.GetInstance();

            var list = new List<(bool isValid, IValidPhoneNumber number)>();
            foreach (string unvalidatedPhone in unvalidatedPhones)
            {

                if (string.IsNullOrWhiteSpace(unvalidatedPhone))
                {
                    list.Add((false, null));
                }

                try
                {

                    var phone = util.Parse(unvalidatedPhone, null);
                    bool isValid = util.IsValidNumber(phone);
                    if (!isValid)
                    {
                        list.Add((false, null));
                    }
                    else
                    {
                        var validatedPhone = util.Format(phone, PhoneNumberFormat.E164);
                        list.Add((true, new ValidPhoneNumber(validatedPhone)));
                    }
                }
                catch (NumberParseException)
                {
                    list.Add((false, null));
                }
            }
            return list;
        }
    }
}
