﻿using System.Threading;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services
{
    public interface IMalathConfigurationProvider
    {
        Task<MalathConfiguration> GetConfiguration(CancellationToken token);
    }
}