﻿using Irms.Application;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain;
using Irms.Domain.Entities.Tenant;
using Irms.Infrastructure.Services;
using Irms.Infrastructure.Services.Sendgrid;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data
{
    /// <summary>
    /// Configuration class for Sendgrid API
    /// </summary>
    public class SendgridConfigurationProvider : ISendgridConfigurationProvider
    {
        private readonly IRepository<Tenant, Guid> _tenantRepository;
        private readonly TenantBasicInfo _tenant;

        public SendgridConfigurationProvider(IRepository<Tenant, Guid> tenantRepository, TenantBasicInfo tenant)
        {
            _tenantRepository = tenantRepository;
            _tenant = tenant;
        }

        public async Task<SendgridConfiguration> GetConfiguration(CancellationToken token)
        {
            var department = await _tenantRepository.GetById(_tenant.Id, token);
            var result = new SendgridConfiguration(department.SendGridApiKey, department.EmailFrom);

            if (!result.IsValid)
            {
                throw new IncorrectRequestException("Sendgrid configuration is invalid!");
            }

            return result;
        }
    }
}
