﻿using System;
using Irms.Data.EntityClasses;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Irms.Data
{
    internal class IrmsModelBuilder
    {
        internal void BuildModel(ModelBuilder modelBuilder)
        {
            MapCountry(modelBuilder.Entity<Country>());
            MapCountryTranslation(modelBuilder.Entity<CountryTranslation>());
            MapCurrency(modelBuilder.Entity<Currency>()); ;
            MapEventLog(modelBuilder.Entity<EventLog>());
            MapLanguage(modelBuilder.Entity<Language>());
            MapLicensePeriod(modelBuilder.Entity<LicensePeriod>());
            MapPersistedGrants(modelBuilder.Entity<PersistedGrants>());
            MapProduct(modelBuilder.Entity<Product>());
            MapProductServiceCatalogFeature(modelBuilder.Entity<ProductServiceCatalogFeature>());
            MapServiceCatalog(modelBuilder.Entity<ServiceCatalog>());
            MapServiceCatalogFeature(modelBuilder.Entity<ServiceCatalogFeature>());
            MapTemplate(modelBuilder.Entity<Template>());
            MapTemplateSelection(modelBuilder.Entity<TemplateSelection>());
            MapTemplateTranslation(modelBuilder.Entity<TemplateTranslation>());
            MapTenant(modelBuilder.Entity<Tenant>());
            MapTenantContactInfo(modelBuilder.Entity<TenantContactInfo>());
            MapTenantSubscription(modelBuilder.Entity<TenantSubscription>());
            MapUserInfo(modelBuilder.Entity<UserInfo>());
            MapEvent(modelBuilder.Entity<Event>());
            MapEventFeature(modelBuilder.Entity<EventFeature>());
            MapEventLocation(modelBuilder.Entity<EventLocation>());
            MapEventType(modelBuilder.Entity<EventType>());
            MapCampaignPrefferedMedia(modelBuilder.Entity<CampaignPrefferedMedia>());
            MapCampaignEntryCriteriaType(modelBuilder.Entity<CampaignEntryCriteriaType>());
            MapContactReachability(modelBuilder.Entity<ContactReachability>());
            MapEventCampaign(modelBuilder.Entity<EventCampaign>());
            MapContact(modelBuilder.Entity<Contact>());
            MapContactList(modelBuilder.Entity<ContactList>());
            MapContactListToContacts(modelBuilder.Entity<ContactListToContacts>());
            MapDocumentType(modelBuilder.Entity<DocumentType>());
            MapCampaignEmailTemplate(modelBuilder.Entity<CampaignEmailTemplate>());
            MapCampaignInvitation(modelBuilder.Entity<CampaignInvitation>());
            MapCampaignSmstemplate(modelBuilder.Entity<CampaignSmstemplate>());
            MapRfiForm(modelBuilder.Entity<RfiForm>());
            MapRfiFormQuestion(modelBuilder.Entity<RfiFormQuestion>());
            MapRfiFormResponse(modelBuilder.Entity<RfiFormResponse>());
            MapCampaignInvitationResponse(modelBuilder.Entity<CampaignInvitationResponse>());
            MapCampaignInvitationResponseMediaType(modelBuilder.Entity<CampaignInvitationResponseMediaType>());
            MapCampaignInvitationMessageLog(modelBuilder.Entity<CampaignInvitationMessageLog>());
            MapProviderLogs(modelBuilder.Entity<ProviderLogs>());
            MapProviderResponse(modelBuilder.Entity<ProviderResponse>());
            MapCampaignInvitationNextRun(modelBuilder.Entity<CampaignInvitationNextRun>());
            MapCustomField(modelBuilder.Entity<CustomField>());
            MapContactCustomField(modelBuilder.Entity<ContactCustomField>());
            MapContactInvitationTelemetry(modelBuilder.Entity<ContactInvitationTelemetry>());
            MapWhatsappApprovedTemplate(modelBuilder.Entity<WhatsappApprovedTemplate>());
            MapCampaignWhatsappTemplate(modelBuilder.Entity<CampaignWhatsappTemplate>());
            MapContactListImportantField(modelBuilder.Entity<ContactListImportantField>());
            MapContactListRFI(modelBuilder.Entity<ContactListRFI>());
            MapContactListRfiAnswer(modelBuilder.Entity<ContactListRFIAnswer>());
            MapListAnalysisImportantFields(modelBuilder.Entity<ListAnalysisImportantFields>());
        }

        private void MapListAnalysisImportantFields(EntityTypeBuilder<ListAnalysisImportantFields> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.GuestListId).IsRequired();
            entity.Property(e => e.FieldType).IsRequired();
            entity.Property(e => e.Field).IsRequired().HasMaxLength(50);
            entity.Property(e => e.IsRequired).IsRequired();
            entity.Property(e => e.CreatedOn).IsRequired();
            entity.Property(e => e.CreatedById).IsRequired();

            entity.HasOne(d => d.GuestList)
                .WithMany()
                .HasForeignKey(d => new { d.GuestListId, d.TenantId });
        }

        private void MapProviderResponse(EntityTypeBuilder<ProviderResponse> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.ResponseDate).HasColumnType("datetime");

            entity.HasOne(d => d.CampaignInvitationMessageLog)
                .WithMany(d => d.ProviderResponses)
                .HasForeignKey(d => new { d.CampaignInvitationMessageLogId, d.TenantId });

            entity.HasOne(d => d.Contact)
                .WithMany()
                .HasForeignKey(d => new { d.ContactId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ProviderResponse_Contact");
        }

        private void MapProviderLogs(EntityTypeBuilder<ProviderLogs> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.Request).IsRequired();

            entity.Property(e => e.RequestDate).HasColumnType("datetime");

            entity.Property(e => e.ResponseDate).HasColumnType("datetime");

            entity.HasOne(d => d.Contact)
                .WithMany()
                .HasForeignKey(d => new { d.ContactId, d.TenantId })
                .HasConstraintName("FK_ProviderLogs_Contact");

            entity.HasOne(d => d.UserInfo)
                .WithMany()
                .HasForeignKey(d => new { d.UserId, d.TenantId })
                .HasConstraintName("FK_ThirdPartyLogs_UserInfo");
        }

        private void MapCampaignInvitationMessageLog(EntityTypeBuilder<CampaignInvitationMessageLog> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });
            entity.Property(e => e.ContactId).IsRequired();
            entity.Property(e => e.CampaignInvitationId).IsRequired();
            entity.Property(e => e.MessageStatus).IsRequired();
            entity.Property(e => e.MessageText);
            entity.Property(e => e.TimeStamp).HasColumnType("datetime");

            entity.HasOne(d => d.Contact)
                .WithMany()
                .HasForeignKey(d => new { d.ContactId, d.TenantId });

            entity.HasOne(d => d.CampaignInvitation)
                .WithMany()
                .HasForeignKey(d => new { d.CampaignInvitationId, d.TenantId });
        }

        /// <summary>
        /// map campaign form response
        /// </summary>
        /// <param name="entity"></param>
        private void MapRfiFormResponse(EntityTypeBuilder<RfiFormResponse> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.Answer).IsRequired();

            entity.Property(e => e.ResponseDate).HasColumnType("datetime");

            entity.HasOne(d => d.RfiForm)
                .WithMany(p => p.RfiFormResponses)
                .HasForeignKey(d => new { d.RfiFormId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CampaignFormResponse_CampaignForm");

            entity.HasOne(d => d.Contact)
                .WithMany()
                .HasForeignKey(d => new { d.ContactId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CampaignFormResponse_Contact");

            entity.HasOne(d => d.RfiFormQuestion)
                .WithMany(p => p.RfiFormResponse)
                .HasForeignKey(d => new { d.RfiFormQuestionId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CampaignFormResponse_CampaignFormQuestion");
        }

        /// <summary>
        /// map campaign form question
        /// </summary>
        /// <param name="entity"></param>
        private void MapRfiFormQuestion(EntityTypeBuilder<RfiFormQuestion> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.MappedField)
                .IsRequired()
                .HasMaxLength(100);

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.Property(e => e.Question).IsRequired();


            entity.HasOne(d => d.RfiForm)
                .WithMany(p => p.RfiFormQuestions)
                .HasForeignKey(d => new { d.RfiFormId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CampaignFormQuestion_CampaignForm");
        }


        /// <summary>
        /// mapping campaign form
        /// </summary>
        /// <param name="entityTypeBuilder"></param>
        private void MapRfiForm(EntityTypeBuilder<RfiForm> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.FormSettings).IsRequired();

            entity.Property(e => e.FormTheme).IsRequired();

            entity.Property(e => e.ThemeBackgroundImagePath).HasMaxLength(500);

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.Property(e => e.SubmitHtml).IsRequired();

            entity.Property(e => e.ThanksHtml).IsRequired();

            entity.Property(e => e.WelcomeHtml).IsRequired();

            entity.HasOne(d => d.CampaignInvitation)
                .WithMany(x => x.RfiForm)
                .HasForeignKey(d => new { d.CampaignInvitationId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CampaignForm_CampaignInvitation");
        }

        /// <summary>
        /// mapping campaign sms templates
        /// </summary>
        /// <param name="entityTypeBuilder"></param>
        private void MapCampaignSmstemplate(EntityTypeBuilder<CampaignSmstemplate> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.ToTable("CampaignSMSTemplate");

            entity.Property(e => e.AcceptButtonText)
                .IsRequired()
                .HasMaxLength(100);

            entity.Property(e => e.AcceptHtml).IsRequired();

            entity.Property(e => e.BackgroundImagePath).HasMaxLength(300);

            entity.Property(e => e.Body)
                .IsRequired()
                .HasMaxLength(1000);

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.Property(e => e.ProceedButtonText)
                .IsRequired()
                .HasMaxLength(100);

            entity.Property(e => e.RejectButtonText)
                .IsRequired()
                .HasMaxLength(100);

            entity.Property(e => e.RejectHtml).IsRequired();

            entity.Property(e => e.Rsvphtml)
                .IsRequired()
                .HasColumnName("RSVPHtml");

            entity.Property(e => e.SenderName)
                .IsRequired()
                .HasMaxLength(50);

            entity.Property(e => e.WelcomeHtml).IsRequired();

            entity.HasOne(d => d.CampaignInvitation)
                .WithMany(p => p.CampaignSmsTemplate)
                .HasForeignKey(d => new { d.CampaignInvitationId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CampaignSMSTemplate_CampaignInvitation");
        }

        /// <summary>
        /// mapping campaign invitation
        /// </summary>
        /// <param name="entityTypeBuilder"></param>
        private void MapCampaignInvitation(EntityTypeBuilder<CampaignInvitation> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.Title).HasMaxLength(200);

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.Property(e => e.Status).IsRequired();

            entity.HasOne(d => d.EventCampaign)
                .WithMany(x => x.CampaignInvitations)
                .HasForeignKey(d => new { d.EventCampaignId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CampaignInvitation_EventCampaign");
        }

        /// <summary>
        /// map campaign email template
        /// </summary>
        /// <param name="entityTypeBuilder"></param>
        private void MapCampaignEmailTemplate(EntityTypeBuilder<CampaignEmailTemplate> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.BackgroundImagePath).HasMaxLength(300);

            entity.Property(e => e.Body).IsRequired();

            entity.Property(e => e.PlainBody).IsRequired();

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.Property(e => e.Preheader).HasMaxLength(1000);

            entity.Property(e => e.SenderEmail)
                .IsRequired()
                .HasMaxLength(200);

            entity.Property(e => e.Subject)
                .IsRequired()
                .HasMaxLength(500);

            entity.HasOne(d => d.CampaignInvitation)
                .WithMany(p => p.CampaignEmailTemplate)
                .HasForeignKey(d => new { d.CampaignInvitationId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CampaignEmailTemplate_CampaignInvitation");
        }

        /// <summary>
        /// map contact list
        /// </summary>
        /// <param name="entity"></param>
        private void MapContactList(EntityTypeBuilder<ContactList> entity)
        {
            entity.HasKey(e => new
            {
                e.Id,
                e.TenantId
            });

            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(100);
        }

        /// <summary>
        /// map contact
        /// </summary>
        /// <param name="entity"></param>
        private void MapContact(EntityTypeBuilder<Contact> entity)
        {
            entity.HasKey(e => new
            {
                e.Id,
                e.TenantId
            });

            entity.Property(e => e.FullName)
                .IsRequired()
                .HasMaxLength(100);
        }

        /// <summary>
        /// map contact
        /// </summary>
        /// <param name="entity"></param>
        private void MapContactListToContacts(EntityTypeBuilder<ContactListToContacts> entity)
        {
            entity.HasKey(e => new
            {
                e.Id,
                e.TenantId
            });

            entity.HasOne(d => d.ContactList)
                .WithMany(p => p.ContactListToContacts)
                .HasForeignKey(d => new { d.ContactListId, d.TenantId })
                .HasConstraintName("FK_ContactListToContacts_ContactList");

            entity.HasOne(d => d.Contact)
                .WithMany(p => p.ContactListToContacts)
                .HasForeignKey(d => new { d.ContactId, d.TenantId })
                .HasConstraintName("FK_ContactListToContacts_Contacts");
        }

        /// <summary>
        /// map document type
        /// </summary>
        /// <param name="entity"></param>
        private void MapDocumentType(EntityTypeBuilder<DocumentType> entity)
        {
            entity.HasKey(e => e.Id);

            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(100);
        }

        /// <summary>
        /// event campaign mapping
        /// </summary>
        /// <param name="entity"></param>
        private void MapEventCampaign(EntityTypeBuilder<EventCampaign> entity)
        {
            entity.HasKey(e => new
            {
                e.Id,
                e.TenantId
            });

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.Property(e => e.Name)
                            .IsRequired()
                            .HasMaxLength(200);

            entity.HasOne(d => d.GuestList)
                            .WithMany()
                            .HasForeignKey(d => new
                            {
                                d.GuestListId,
                                d.TenantId
                            });

            entity.HasOne(d => d.EntryCriteriaType)
                            .WithMany(p => p.EventCampaign)
                            .HasForeignKey(d => d.EntryCriteriaTypeId)
                            .HasConstraintName("FK_EventCampaign_CampaignEntryCriteriaType");

            entity.HasOne(d => d.Event)
                            .WithMany(p => p.EventCampaign)
                            .HasForeignKey(d => new
                            {
                                d.EventId,
                                d.TenantId
                            })
                            .OnDelete(DeleteBehavior.ClientSetNull)
                            .HasConstraintName("FK_EventCampaign_Event");
        }

        /// <summary>
        /// contact reachibility mapping
        /// </summary>
        /// <param name="entity"></param>
        private void MapContactReachability(EntityTypeBuilder<ContactReachability> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.EmailLastReachableOn).HasColumnType("datetime");

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.Property(e => e.SmsLastReachableOn).HasColumnType("datetime");

            entity.Property(e => e.WhatsAppLastReachableOn).HasColumnType("datetime");

            entity.HasOne(d => d.Contact)
                .WithMany()
                .HasForeignKey(d => new { d.ContactId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ContactReachability_Contacts");
        }
        /// <summary>
        /// campaign entry criteria type id
        /// </summary>
        /// <param name="entity"></param>
        private void MapCampaignEntryCriteriaType(EntityTypeBuilder<CampaignEntryCriteriaType> entity)
        {
            entity.HasKey(e => e.Id);

            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(200);
        }

        /// <summary>
        /// campaign preffered media mapping
        /// </summary>
        /// <param name="entity"></param>
        private void MapCampaignPrefferedMedia(EntityTypeBuilder<CampaignPrefferedMedia> entity)
        {

            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.HasOne(d => d.Contact)
                .WithMany()
                .HasForeignKey(d => new { d.ContactId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CampaignPrefferedMedia_Contacts");

            entity.HasOne(d => d.EventCampaign)
                .WithMany(p => p.CampaignPrefferedMedia)
                .HasForeignKey(d => new { d.EventCampaignId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CampaignPrefferedMedia_EventCampaign");
        }

        /// <summary>
        /// mapping event type
        /// </summary>
        /// <param name="entity"></param>
        private void MapEventType(EntityTypeBuilder<EventType> entity)
        {
            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(100);
        }

        /// <summary>
        /// event location mapping
        /// </summary>
        /// <param name="entity"></param>
        private void MapEventLocation(EntityTypeBuilder<EventLocation> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.City)
                .HasMaxLength(200);

            entity.Property(e => e.CreatedOn)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.ModifiedOn)
                .HasColumnType("datetime");

            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(500);

            entity.Property(e => e.Region).HasMaxLength(200);

            entity.Property(e => e.ZipCode)
                .HasMaxLength(20);

            entity.Property(e => e.Latitude)
                .IsRequired();

            entity.Property(e => e.Longitude)
                .IsRequired();

            entity.HasOne(d => d.Country)
                .WithMany(p => p.EventLocation)
                .HasForeignKey(d => d.CountryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_EventLocation_Country");

            entity.HasOne(d => d.Event)
                .WithMany(p => p.EventLocation)
                .HasForeignKey(d => new { d.EventId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_EventLocation_Event");
        }

        /// <summary>
        /// map event features...
        /// </summary>
        /// <param name="entityTypeBuilder"></param>
        private void MapEventFeature(EntityTypeBuilder<EventFeature> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.HasOne(d => d.Event)
                .WithMany(p => p.EventFeature)
                .HasForeignKey(d => new { d.EventId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_EventFeature_Event");
        }

        private void MapEvent(EntityTypeBuilder<Event> entity)
        {

            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.CreatedOn)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.EndDateTime).HasColumnType("datetime");

            entity.Property(e => e.ImagePath).HasMaxLength(200);

            entity.Property(e => e.ModifiedOn)
                .HasColumnType("datetime");

            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(200);

            entity.Property(e => e.StartDateTime).HasColumnType("datetime");

            entity.Property(e => e.TimeZoneUtcOffset)
                .HasMaxLength(10)
                .IsFixedLength();

            entity.Property(e => e.TimeZoneName)
                .HasMaxLength(100)
                .IsFixedLength();

            entity.HasOne(d => d.EventType)
                .WithMany(p => p.Event)
                .HasForeignKey(d => d.EventTypeId)
                .HasConstraintName("FK_Event_EventType");
        }

        /// <summary>
        /// mapping country
        /// </summary>
        /// <param name="entity"></param>
        private void MapCountry(EntityTypeBuilder<Country> entity)
        {
            entity.Property(e => e.Id)
                .HasColumnName("ID")
                .ValueGeneratedNever();

            entity.Property(e => e.Latitude).HasMaxLength(20);

            entity.Property(e => e.LongIso).HasMaxLength(3);

            entity.Property(e => e.Longigude).HasMaxLength(20);

            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(100);

            entity.Property(e => e.ShortIso).HasMaxLength(2);
        }

        /// <summary>
        /// mapping country translations
        /// </summary>
        /// <param name="entity"></param>
        private void MapCountryTranslation(EntityTypeBuilder<CountryTranslation> entity)
        {
            entity.Property(e => e.Id)
                .HasColumnName("ID")
                .ValueGeneratedNever();

            entity.Property(e => e.Translation)
                        .IsRequired()
                        .HasMaxLength(200);

            entity.HasOne(d => d.Country)
                        .WithMany(p => p.CountryTranslation)
                        .HasForeignKey(d => d.CountryId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_NationalityTranslation_Nationality");
        }

        /// <summary>
        /// mapping currency
        /// </summary>
        /// <param name="entity"></param>
        private void MapCurrency(EntityTypeBuilder<Currency> entity)
        {
            entity.Property(e => e.Id).ValueGeneratedOnAdd();

            entity.Property(e => e.Title).HasMaxLength(20);
        }

        /// <summary>
        /// maping event log for audit log
        /// </summary>
        /// <param name="entity"></param>
        private void MapEventLog(EntityTypeBuilder<EventLog> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.Data).IsRequired();

            entity.Property(e => e.EventType)
                                .IsRequired()
                                .HasMaxLength(256);

            entity.Property(e => e.OccurredOn).HasColumnType("datetime");

            entity.HasOne(d => d.Tenant)
                                .WithMany(p => p.EventLog)
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_EventLog_Tenant");
        }

        /// <summary>
        /// mapping language entity
        /// </summary>
        /// <param name="entity"></param>
        private void MapLanguage(EntityTypeBuilder<Language> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.HasIndex(e => new { e.Culture, e.TenantId })
                .HasName("UX_Language")
                .IsUnique();

            entity.Property(e => e.Culture)
                                .IsRequired()
                                .HasMaxLength(10);

            entity.Property(e => e.Name)
                                .IsRequired()
                                .HasMaxLength(50);

            entity.HasOne(d => d.Tenant)
                                .WithMany(p => p.Language)
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_Language_Tenant");
        }

        /// <summary>
        /// mapping liscense period
        /// </summary>
        /// <param name="entity"></param>
        private void MapLicensePeriod(EntityTypeBuilder<LicensePeriod> entity)
        {
            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(50);
        }

        /// <summary>
        /// mapping presist grants
        /// </summary>
        /// <param name="entity"></param>
        private void MapPersistedGrants(EntityTypeBuilder<PersistedGrants> entity)
        {
            entity.HasKey(e => e.Key);

            entity.Property(e => e.Key).HasMaxLength(200);

            entity.Property(e => e.ClientId)
                                .IsRequired()
                                .HasMaxLength(200);

            entity.Property(e => e.Data).IsRequired();

            entity.Property(e => e.SubjectId).HasMaxLength(200);

            entity.Property(e => e.Type)
                                .IsRequired()
                                .HasMaxLength(50);
        }


        /// <summary>
        /// mapping products
        /// </summary>
        /// <param name="entity"></param>
        private void MapProduct(EntityTypeBuilder<Product> entity)
        {
            entity.Property(e => e.Id).ValueGeneratedNever();

            entity.Property(e => e.CreatedOn)
                                .HasColumnType("datetime")
                                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.DiscountAmount)
                                .HasColumnType("decimal(28, 8)")
                                .HasComputedColumnSql("(([Price]*[DiscountPercentage])/(100))");

            entity.Property(e => e.DiscountPercentage)
                                .HasColumnType("decimal(5, 2)")
                                .HasDefaultValueSql("((0))");

            entity.Property(e => e.FinalPrice)
                                .HasColumnType("decimal(29, 8)")
                                .HasComputedColumnSql("([Price]-([Price]*[DiscountPercentage])/(100))");

            entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

            entity.Property(e => e.ModifiedOn)
                                .HasColumnType("datetime")
                                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

            entity.Property(e => e.Title)
                                .IsRequired()
                                .HasMaxLength(100);

            entity.HasOne(d => d.Currency)
                                .WithMany(p => p.Product)
                                .HasForeignKey(d => d.CurrencyId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_Product_Currency");

            entity.HasOne(d => d.LicensePeriod)
                                .WithMany(p => p.Product)
                                .HasForeignKey(d => d.LicensePeriodId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_Product_LicensePeriod");
        }

        /// <summary>
        /// mapping service catalog feature
        /// </summary>
        /// <param name="entity"></param>
        private void MapProductServiceCatalogFeature(EntityTypeBuilder<ProductServiceCatalogFeature> entity)
        {
            entity.Property(e => e.Id).ValueGeneratedNever();

            entity.HasOne(d => d.Product)
                                .WithMany(p => p.ProductServiceCatalogFeature)
                                .HasForeignKey(d => d.ProductId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_ProductServiceCatalogFeature_Product");

            entity.HasOne(d => d.ServiceCatalogFeature)
                                .WithMany(p => p.ProductServiceCatalogFeature)
                                .HasForeignKey(d => d.ServiceCatalogFeatureId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_ProductServiceCatalogFeature_ServiceCatalogFeature");

            entity.HasOne(d => d.ServiceCatalog)
                                .WithMany(p => p.ProductServiceCatalogFeature)
                                .HasForeignKey(d => d.ServiceCatalogId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_ProductServiceCatalogFeature_ServiceCatalog");
        }

        /// <summary>
        /// mapping service catelog
        /// </summary>
        /// <param name="entity"></param>
        private void MapServiceCatalog(EntityTypeBuilder<ServiceCatalog> entity)
        {
            entity.Property(e => e.Id).ValueGeneratedNever();

            entity.Property(e => e.CreatedOn)
                                .HasColumnType("datetime")
                                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

            entity.Property(e => e.ModifiedOn)
                                .HasColumnType("datetime")
                                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.Title)
                                .IsRequired()
                                .HasMaxLength(100);
        }

        /// <summary>
        /// mapping service catelog
        /// </summary>
        /// <param name="entity"></param>
        private void MapServiceCatalogFeature(EntityTypeBuilder<ServiceCatalogFeature> entity)
        {
            entity.Property(e => e.Id).ValueGeneratedNever();

            entity.Property(e => e.CreatedOn)
                                .HasColumnType("datetime")
                                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.CustomText).HasMaxLength(200);

            entity.Property(e => e.ModifiedOn)
                                .HasColumnType("datetime")
                                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.Title)
                                .IsRequired()
                                .HasMaxLength(200);

            entity.HasOne(d => d.ServiceCatalog)
                                .WithMany(p => p.ServiceCatalogFeature)
                                .HasForeignKey(d => d.ServiceCatalogId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_ServiceCatalogFeature_ServiceCatalog");
        }

        /// <summary>
        /// mapping template
        /// </summary>
        /// <param name="entity"></param>
        private void MapTemplate(EntityTypeBuilder<Template> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.Property(e => e.EmailSubject).HasMaxLength(500);

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.Property(e => e.Name)
                                .IsRequired()
                                .HasMaxLength(250);

            entity.Property(e => e.SmsText).HasMaxLength(1000);

            entity.HasOne(d => d.Tenant)
                                .WithMany(p => p.Template)
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_Template_Tenant");

            entity.HasOne(d => d.UserInfo)
                                .WithMany(p => p.Template)
                                .HasForeignKey(d => new { d.ModifiedById, d.TenantId })
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_Template_Employee");
        }

        /// <summary>
        /// mapping template selections
        /// </summary>
        /// <param name="entity"></param>
        private void MapTemplateSelection(EntityTypeBuilder<TemplateSelection> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.HasOne(d => d.Tenant)
                .WithMany(p => p.TemplateSelection)
                .HasForeignKey(d => d.TenantId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TemplateSelection_Tenant");

            entity.HasOne(d => d.Template)
                                .WithMany(p => p.TemplateSelectionTemplate)
                                .HasForeignKey(d => new { d.EmailTemplateId, d.TenantId })
                                .HasConstraintName("FK_TemplateSelection_Template_Email");

            entity.HasOne(d => d.TemplateNavigation)
                                .WithMany(p => p.TemplateSelectionTemplateNavigation)
                                .HasForeignKey(d => new { d.SmsTemplateId, d.TenantId })
                                .HasConstraintName("FK_TemplateSelection_Template_Sms");
        }

        /// <summary>
        /// mapping template translations
        /// </summary>
        /// <param name="entity"></param>
        private void MapTemplateTranslation(EntityTypeBuilder<TemplateTranslation> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.HasIndex(e => new { e.TemplateId, e.LanguageId })
                .HasName("UX_Template_Translation")
                .IsUnique();

            entity.Property(e => e.EmailSubject).HasMaxLength(500);

            entity.Property(e => e.SmsText).HasMaxLength(1000);

            entity.HasOne(d => d.Tenant)
                                .WithMany(p => p.TemplateTranslation)
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_TemplateTranslation_Tenant");

            entity.HasOne(d => d.Language)
                                .WithMany(p => p.TemplateTranslation)
                                .HasForeignKey(d => new { d.LanguageId, d.TenantId })
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_TemplateTranslation_Language");

            entity.HasOne(d => d.Te)
                                .WithMany(p => p.TemplateTranslation)
                                .HasForeignKey(d => new { d.TemplateId, d.TenantId })
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_TemplateTranslation_Template");
        }

        /// <summary>
        /// mapping tenant
        /// </summary>
        /// <param name="entity"></param>
        private void MapTenant(EntityTypeBuilder<Tenant> entity)
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

            entity.Property(e => e.Address)
                                .IsRequired()
                                .HasMaxLength(200)
                                .HasDefaultValueSql("(N'Alkhobar')");

            entity.Property(e => e.City).HasMaxLength(200);

            entity.Property(e => e.ClientUrl)
                                .HasColumnName("ClientURL")
                                .HasMaxLength(255)
                                .IsUnicode(false);

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.Description)
                                .IsRequired()
                                .HasMaxLength(500)
                                .IsUnicode(false);

            entity.Property(e => e.EmailFrom)
                                .HasMaxLength(255)
                                .IsUnicode(false);

            entity.Property(e => e.FooterPath).HasMaxLength(200);

            entity.Property(e => e.HeaderPath).HasMaxLength(200);

            entity.Property(e => e.LogoPath).HasMaxLength(200);

            entity.Property(e => e.Name)
                                .IsRequired()
                                .HasMaxLength(100)
                                .IsUnicode(false);

            entity.Property(e => e.SendGridApiKey)
                                .HasMaxLength(255)
                                .IsUnicode(false);

            entity.Property(e => e.MalathUsername)
                                .HasMaxLength(255)
                                .IsUnicode(false);

            entity.Property(e => e.MalathPassword)
                                .HasMaxLength(255)
                                .IsUnicode(false);

            entity.Property(e => e.MalathSender)
                                .HasMaxLength(255)
                                .IsUnicode(false);

            entity.Property(e => e.UnifonicSid)
                                .HasMaxLength(255)
                                .IsUnicode(false);

            entity.Property(e => e.UnifonicSender)
                                .HasMaxLength(255)
                                .IsUnicode(false);

            entity.Property(e => e.TwilioAccountSid)
                                .HasMaxLength(255)
                                .IsUnicode(false);

            entity.Property(e => e.TwilioAuthToken)
                                .HasMaxLength(255)
                                .IsUnicode(false);

            entity.Property(e => e.TwilioNotificationServiceId)
                                .HasMaxLength(255)
                                .IsUnicode(false);

            entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

            entity.HasOne(d => d.Country)
                                .WithMany(p => p.Tenant)
                                .HasForeignKey(d => d.CountryId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK__Tenant__CountryI__08D548FA");

            entity.HasOne(d => d.UserInfo)
                                .WithMany(p => p.Tenant)
                                .HasForeignKey(d => new { d.AdminId, d.Id })
                                .HasConstraintName("FK_Tenant_Admin");
        }

        /// <summary>
        /// mapping tenant contact info
        /// </summary>
        /// <param name="entity"></param>
        private void MapTenantContactInfo(EntityTypeBuilder<TenantContactInfo> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId })
                .HasName("PK_TenantContactInfo_1");

            entity.Property(e => e.Email)
                                .IsRequired()
                                .HasMaxLength(50);

            entity.Property(e => e.FirstName)
                                .IsRequired()
                                .HasMaxLength(100);

            entity.Property(e => e.LastName)
                                .IsRequired()
                                .HasMaxLength(100);

            entity.Property(e => e.MobileNo)
                                .IsRequired()
                                .HasMaxLength(20);

            entity.Property(e => e.PhoneNo).HasMaxLength(20);

            entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

            entity.HasOne(d => d.Tenant)
                                .WithMany(p => p.TenantContactInfo)
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_TenantContactInfo_Tenant");
        }

        /// <summary>
        /// mapping tenant subscription
        /// </summary>
        /// <param name="entity"></param>
        private void MapTenantSubscription(EntityTypeBuilder<TenantSubscription> entity)
        {
            entity.Property(e => e.Id).ValueGeneratedNever();

            entity.Property(e => e.CreatedOn)
                                .HasColumnType("datetime")
                                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.ExpiryDateTime).HasColumnType("datetime");

            entity.Property(e => e.ModifiedOn)
                                .HasColumnType("datetime")
                                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.StartDateTime).HasColumnType("datetime");

            entity.HasOne(d => d.Product)
                                .WithMany(p => p.TenantSubscription)
                                .HasForeignKey(d => d.ProductId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_TenantSubscription_Product");

            entity.HasOne(d => d.Tenant)
                                .WithMany(p => p.TenantSubscription)
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_TenantSubscription_Tenant");
        }

        /// <summary>
        /// mapping user info
        /// </summary>
        /// <param name="entity"></param>
        private void MapUserInfo(EntityTypeBuilder<UserInfo> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId })
                .HasName("PK_Employee");

            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

            entity.Property(e => e.BirthDate).HasColumnType("datetime");

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.Email)
                                .IsRequired()
                                .HasMaxLength(50);

            entity.Property(e => e.FirstName)
                                .IsRequired()
                                .HasMaxLength(50);

            entity.Property(e => e.FullName)
                                .IsRequired()
                                .HasMaxLength(101)
                                .HasComputedColumnSql("((isnull([FirstName],'')+' ')+isnull([LastName],''))");

            entity.Property(e => e.LastName)
                                .IsRequired()
                                .HasMaxLength(50);

            entity.Property(e => e.MobileNo)
                                .IsRequired()
                                .HasMaxLength(50);

            entity.Property(e => e.NationalId).HasMaxLength(50);

            entity.Property(e => e.PassportNo).HasMaxLength(50);

            entity.Property(e => e.SearchText).HasMaxLength(1000);

            entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

            entity.HasOne(d => d.TenantNavigation)
                                .WithMany(p => p.UserInfoNavigation)
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_Employee_Tenant");

            //entity.HasOne(d => d.User)
            //    .WithMany(p => p.UserInfo)
            //    .HasForeignKey(d => d.UserId)
            //    .HasConstraintName("FK_Employee_AspNetUsers");
        }

        private void MapCampaignInvitationResponse(EntityTypeBuilder<CampaignInvitationResponse> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId })
                .HasName("PK_CampaignInvitationResponse");

            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

            entity.Property(e => e.ResponseDate).HasColumnType("datetime");

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.HasOne(x => x.CampaignInvitation)
                                .WithMany()
                                .HasForeignKey(x => new { x.CampaignInvitationId, x.TenantId })
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_CampaignInvitationResponse_CampaignInvitation");

            entity.HasOne(x => x.Contact)
                                .WithMany()
                                .HasForeignKey(x => new { x.ContactId, x.TenantId })
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_CampaignInvitationResponse_Contact");

            entity.HasOne(d => d.Tenant)
                                .WithMany(p => p.CampaignInvitationResponses)
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_CampaignInvitationResponse_Tenant");
        }

        private void MapCampaignInvitationResponseMediaType(EntityTypeBuilder<CampaignInvitationResponseMediaType> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId })
                .HasName("PK_CampaignInvitationResponseMediaType");

            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

            entity.Property(e => e.MediaType).IsRequired();

            entity.Property(e => e.AcceptOrReject).IsRequired();

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.HasOne(d => d.Tenant)
                                .WithMany(p => p.CampaignInvitationResponseMediaTypes)
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_CampaignInvitationResponseMediaType_Tenant");

            entity.HasOne(d => d.CampaignInvitationResponse)
                .WithMany(p => p.CampaignInvitationResponseMediaTypes)
                .HasForeignKey(d => new { d.CampaignInvitationResponseId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CampaignInvitationResponseMediaType_CampaignInvitationResponse");
        }

        private void MapCampaignInvitationNextRun(EntityTypeBuilder<CampaignInvitationNextRun> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId })
                .HasName("PK_CampaignInvitationNextRun");

            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

            entity.Property(e => e.Active).IsRequired();

            entity.Property(e => e.StartDate).HasColumnType("datetime").IsRequired();

            entity.Property(e => e.Status).IsRequired();

            entity.HasOne(e => e.Contact)
                                .WithMany()
                                .HasForeignKey(d => new { d.ContactId, d.TenantId })
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_CampaignInvitationNextRun_Contact");

            entity.HasOne(e => e.CampaignInvitation)
                                .WithMany()
                                .HasForeignKey(d => new { d.CampaignInvitationId, d.TenantId })
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_CampaignInvitationNextRun_CampaignInvitation");

            entity.HasOne(d => d.Tenant)
                                .WithMany()
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_CampaignInvitationNextRun_Tenant");
        }

        private void MapCustomField(EntityTypeBuilder<CustomField> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId })
                .HasName("PK_CustomField");

            entity.Property(e => e.Id).HasDefaultValueSql("newid())");

            entity.Property(e => e.FieldName).IsRequired();

            entity.Property(e => e.CreatedOn)
                .HasColumnType("datetime")
                .IsRequired();

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.Property(e => e.CustomFieldType).IsRequired();

            entity.HasOne(d => d.Tenant)
                                .WithMany()
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_CustomField_Tenant");
        }

        private void MapContactCustomField(EntityTypeBuilder<ContactCustomField> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId })
                .HasName("PK_ContactCustomField");

            entity.Property(e => e.Id).HasDefaultValueSql("newid())");

            entity.Property(e => e.CreatedOn)
                .HasColumnType("datetime")
                .IsRequired();

            entity.Property(e => e.ModifiedOn)
                .HasColumnType("datetime");

            entity.HasOne(d => d.Contact)
                                .WithMany(x => x.CustomFields)
                                .HasForeignKey(d => new { d.ContactId, d.TenantId })
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_ContactCustomField_Contact");

            entity.HasOne(d => d.CustomField)
                                .WithMany(z => z.ContactCustomField)
                                .HasForeignKey(d => new { d.CustomFieldId, d.TenantId })
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_ContactCustomField_CustomField");

            entity.HasOne(d => d.Tenant)
                                .WithMany()
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_ContactCustomField_Tenant");
        }

        private void MapContactInvitationTelemetry(EntityTypeBuilder<ContactInvitationTelemetry> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId })
                .HasName("PK_ContactInvitationTelemetry");

            entity.Property(x => x.OpenedOn).IsRequired();

            entity.Property(x => x.MediaType).IsRequired();

            entity.HasOne(d => d.CampaignInvitation)
                .WithMany()
                .HasForeignKey(d => new { d.CampaignInvitationId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.Contact)
                .WithMany()
                .HasForeignKey(d => new { d.ContactId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.Tenant)
                                .WithMany()
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull);
        }

        private void MapWhatsappApprovedTemplate(EntityTypeBuilder<WhatsappApprovedTemplate> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId })
                .HasName("PK_WhatsappApprovedTemplate");

            entity.Property(x => x.Name)
                .IsRequired();

            entity.Property(x => x.InvitationTypeId)
                .IsRequired();

            entity.Property(e => e.CreatedOn)
                .HasColumnType("datetime")
                .IsRequired();

            entity.Property(e => e.ModifiedOn)
                .HasColumnType("datetime");

            entity.Property(e => e.CreatedById)
                .IsRequired();

            entity.HasOne(d => d.Tenant)
                                .WithMany()
                                .HasForeignKey(d => d.TenantId)
                                .OnDelete(DeleteBehavior.ClientSetNull);
        }

        private void MapCampaignWhatsappTemplate(EntityTypeBuilder<CampaignWhatsappTemplate> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.ToTable("CampaignWhatsappTemplate");

            entity.Property(e => e.AcceptButtonText)
                .IsRequired()
                .HasMaxLength(100);

            entity.Property(e => e.AcceptHtml).IsRequired();

            entity.Property(e => e.BackgroundImagePath).HasMaxLength(300);

            entity.Property(e => e.Body)
                .IsRequired()
                .HasMaxLength(1000);

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.Property(e => e.ProceedButtonText)
                .IsRequired()
                .HasMaxLength(100);

            entity.Property(e => e.RejectButtonText)
                .IsRequired()
                .HasMaxLength(100);

            entity.Property(e => e.RejectHtml).IsRequired();

            entity.Property(e => e.Rsvphtml)
                .IsRequired()
                .HasColumnName("RSVPHtml");

            entity.Property(e => e.SenderName)
                .IsRequired()
                .HasMaxLength(50);

            entity.Property(e => e.WelcomeHtml).IsRequired();

            entity.HasOne(d => d.CampaignInvitation)
                .WithMany(d => d.CampaignWhatsappTemplate)
                .HasForeignKey(d => new { d.CampaignInvitationId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.Template)
                .WithMany()
                .HasForeignKey(d => new { d.TemplateId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.Tenant)
                .WithMany()
                .HasForeignKey(d => d.TenantId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }

        private void MapContactListRFI(EntityTypeBuilder<ContactListRFI> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.ToTable("ContactListRFI");

            entity.Property(x => x.Status).IsRequired();

            entity.Property(e => e.CreatedOn).HasColumnType("datetime");

            entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

            entity.HasOne(d => d.RfiForm)
                .WithMany()
                .HasForeignKey(d => new { d.RfiFormId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.GuestList)
                .WithMany()
                .HasForeignKey(d => new { d.GuestListId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.Tenant)
                .WithMany()
                .HasForeignKey(d => d.TenantId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }

        private void MapContactListRfiAnswer(EntityTypeBuilder<ContactListRFIAnswer> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.ToTable("ContactListRFIAnswer");

            entity.HasOne(d => d.Contact)
                .WithMany()
                .HasForeignKey(d => new { d.ContactId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.ContactListRFI)
                .WithMany(f => f.ContactListRfiAnswer)
                .HasForeignKey(d => new { d.ContactListRfiId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.Tenant)
                .WithMany()
                .HasForeignKey(d => d.TenantId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }

        private void MapContactListImportantField(EntityTypeBuilder<ContactListImportantField> entity)
        {
            entity.HasKey(e => new { e.Id, e.TenantId });

            entity.ToTable("ContactListImportantField");

            entity.HasOne(d => d.ContactList)
                .WithMany()
                .HasForeignKey(d => new { d.ContactListId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.CustomField)
                .WithMany()
                .HasForeignKey(d => new { d.CustomFieldId, d.TenantId })
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.Tenant)
                .WithMany()
                .HasForeignKey(d => d.TenantId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}