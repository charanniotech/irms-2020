﻿namespace Irms.Data.Abstract
{
    public interface IConnectionString
    {
        string Value { get; }
    }
}
