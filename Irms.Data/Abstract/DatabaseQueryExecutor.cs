﻿using Irms.Application.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Abstract
{
    public class DatabaseQueryExecutor : IDatabaseQueryExecutor
    {
        private readonly IrmsDataContext _db;
        public DatabaseQueryExecutor(IrmsDataContext db)
        {
            _db = db;
        }

        /// <summary>
        /// Executes database query with interpolated parameters
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<int> ExecuteSqlInterpolatedAsync(FormattableString sql, CancellationToken token)
        {
            return await _db.Database.ExecuteSqlInterpolatedAsync(sql, token);
        }

        /// <summary>
        /// Executes database query with default query sql string
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<int> ExecuteSqlRawAsync(string sql, CancellationToken token)
        {   
            return await _db.Database.ExecuteSqlRawAsync(sql, token);
        }
    }
}
