﻿using Irms.Application.Contact.Commands;
using Irms.Application.Events.Commands;

namespace Irms.Data.Mappers
{
    public class ContactMapper : AutoMapper.Profile
    {
        public ContactMapper()
        {
            CreateMap<Domain.Entities.ContactList, EntityClasses.ContactList>();
            CreateMap<EntityClasses.ContactList, Domain.Entities.ContactList>();

            //create tenant map
            CreateMap<CreateGlobalContactListCmd, Domain.Entities.ContactList>();
            CreateMap<CreateGuestListCmd, Domain.Entities.ContactList>();
            CreateMap<CreateContactCmd, Domain.Entities.ContactList>();


            CreateMap<Domain.Entities.Contact, EntityClasses.Contact>();
            CreateMap<EntityClasses.Contact, Domain.Entities.Contact>()
                .ForMember(x => x.CustomFields, y => y.MapFrom(c => c.CustomFields));

            CreateMap<CreateContactCmd, Domain.Entities.Contact>();

            CreateMap<Domain.Entities.ContactListToContact, EntityClasses.ContactListToContacts>();
            CreateMap<EntityClasses.ContactListToContacts, Domain.Entities.ContactListToContact>();


            //update contacts
            CreateMap<UpdateContactCmd, Domain.Entities.Contact>();

            ////update config, create/update users
            //CreateMap<UpdateConfigCmd, Domain.Entities.Event.Event>();
            //CreateMap<UpdateConfigCmd, Domain.Entities.UserInfo>();
        }
    }
}
