﻿using Irms.Application.UsersInfo.Commands;
using Irms.Data.EntityClasses;
using Irms.Domain.Entities;

namespace Irms.Data.Mappers
{
    public class UserInfoMapper : AutoMapper.Profile
    {
        public UserInfoMapper()
        {
            CreateMap<Domain.Entities.UserInfo, EntityClasses.UserInfo>()
                .ForMember(x => x.RoleId, x => x.MapFrom(m => (int)m.Role))
                .ForMember(x => x.GenderId, x => x.MapFrom(m => m.Gender))
                .ForMember(x => x.Id, x => x.MapFrom(m => m.Id))
                .ForMember(x => x.UserId, x => x.MapFrom(m => m.Id))
                .ForMember(x => x.GenderId, x => x.MapFrom(m => (int?)m.Gender))
                .ForMember(x => x.FullName, x => x.MapFrom(m => m.FirstName + " " + m.LastName))

                .ForMember(x => x.CreatedBy, x => x.Ignore())
                .ForMember(x => x.IsActive, x => x.Ignore())
                .ForMember(x => x.IsDeleted, x => x.Ignore())
                .ForMember(x => x.CreatedOn, x => x.Ignore())
                .ForMember(x => x.TenantId, x => x.Ignore());

            CreateMap<EntityClasses.UserInfo, Domain.Entities.UserInfo>()
                .ForMember(x => x.Role, x => x.MapFrom(m => m.RoleId))
                .ForMember(x => x.Gender, x => x.MapFrom(m => m.GenderId));

            CreateMap<UpdateUserMainInfoCmd, Domain.Entities.UserInfo>()
                .ForMember(x => x.MobileNo, x=> x.MapFrom(m => m.PhoneNo))
                .ForMember(x => x.Gender, x=> x.MapFrom(m => m.Gender));
        }
    }


}
