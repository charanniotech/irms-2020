﻿using Irms.Application.Tenants.Commands;

namespace Irms.Data.Mappers
{
    public class TenantMapper : AutoMapper.Profile
    {
        public TenantMapper()
        {
            CreateMap<Domain.Entities.Tenant.Tenant, EntityClasses.Tenant>();
            CreateMap<EntityClasses.Tenant, Domain.Entities.Tenant.Tenant>();

            //create tenant map
            CreateMap<CreateTenantCmd, Domain.Entities.Tenant.Tenant>();
            CreateMap<CreateTenantCmd, Domain.Entities.Tenant.TenantContactInfo>();
            CreateMap<Domain.Entities.Tenant.TenantContactInfo, EntityClasses.TenantContactInfo>();

            //update tenant
            CreateMap<UpdateTenantCmd, Domain.Entities.Tenant.Tenant>();
            CreateMap<UpdateContactInfoCmd, Domain.Entities.Tenant.TenantContactInfo>();

            //update config, create/update users
            CreateMap<UpdateConfigCmd, Domain.Entities.Tenant.Tenant>();
            CreateMap<UpdateConfigCmd, Domain.Entities.UserInfo>();
        }
    }
}
