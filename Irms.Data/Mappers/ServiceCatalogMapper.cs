﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Mappers
{
    public class ServiceCatalogMapper : AutoMapper.Profile
    {
        public ServiceCatalogMapper()
        {
            CreateMap<Domain.Entities.ServiceCatalogFeature, EntityClasses.ServiceCatalogFeature>()
                .ForMember(x => x.Title, x => x.MapFrom(m => m.FeatureTitle))
                .ForMember(x => x.IsActive, x => x.MapFrom(m => m.IsFeatureEnabled));

            CreateMap<Domain.Entities.ServiceCatalog, EntityClasses.ServiceCatalog>()
                .ForMember(x => x.Id, x => x.MapFrom(m => m.Id))
                .ForMember(x => x.Title, x => x.MapFrom(m => m.Title))
                .ForMember(x => x.IsActive, x => x.MapFrom(m => m.IsActive))
                .ForMember(x => x.CreatedById, x => x.MapFrom(m => m.CreatedById))
                .ForMember(x => x.CreatedOn, x => x.MapFrom(m => m.CreatedOn))
                .ForMember(x => x.ServiceCatalogFeature, x => x.MapFrom(m => m.ServiceCatalogFeatures))
                .IgnoreAllPropertiesWithAnInaccessibleSetter();

            CreateMap<EntityClasses.ServiceCatalog, Domain.Entities.ServiceCatalog>();

        }
    }
}
