﻿using Irms.Application.BackgroundService.ReadModels;
using Irms.Application.CampaignInvitations;
using Irms.Application.CampaignInvitations.CommandHandlers;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.Campaigns.Commands;
using Irms.Data.EntityClasses;

namespace Irms.Data.Mappers
{
    public class CampaignInvitationMapper : AutoMapper.Profile
    {
        public CampaignInvitationMapper()
        {
            CreateMap<Domain.Entities.CampaignInvitation, EntityClasses.CampaignInvitation>()
                .ForMember(x => x.InvitationTypeId, x => x.MapFrom(m => (int)m.InvitationType))
                .ForMember(x => x.Status, x => x.MapFrom(m => (int)m.Status))
                .ForMember(x => x.IntervalType, x => x.MapFrom(m => (int?)m.IntervalType))
                .ForMember(x => x.TenantId, x => x.Ignore())
                .ForMember(x => x.CreatedById, x => x.Ignore())
                .ForMember(x => x.CreatedOn, x => x.Ignore())
                .ForMember(x => x.ModifiedById, x => x.Ignore())
                .ForMember(x => x.ModifiedOn, x => x.Ignore());

            CreateMap<EntityClasses.CampaignInvitation, Domain.Entities.CampaignInvitation>()
                .ForMember(x => x.InvitationType, x => x.MapFrom(m => m.InvitationTypeId))
                .ForMember(x => x.Status, x => x.MapFrom(m => m.Status))
                .ForMember(x => x.IntervalType, x => x.MapFrom(m => (Domain.Entities.IntervalType?)m.IntervalType));

            //create
            CreateMap<CreateOrUpdateCampaignInvitationCmd, Domain.Entities.CampaignInvitation>()
                .ForMember(x => x.EventCampaignId, x => x.MapFrom(m => m.CampaignId));

            CreateMap<Domain.Entities.CampaignEmailTemplate, EntityClasses.CampaignEmailTemplate>()
                .ForMember(x => x.TenantId, x => x.Ignore())
                .ForMember(x => x.CreatedById, x => x.Ignore())
                .ForMember(x => x.CreatedOn, x => x.Ignore())
                .ForMember(x => x.ModifiedById, x => x.Ignore())
                .ForMember(x => x.ModifiedOn, x => x.Ignore());

            CreateMap<Domain.Entities.CampaignSmsTemplate, EntityClasses.CampaignSmstemplate>()
                .ForMember(x => x.TenantId, x => x.Ignore())
                .ForMember(x => x.CreatedById, x => x.Ignore())
                .ForMember(x => x.CreatedOn, x => x.Ignore())
                .ForMember(x => x.ModifiedById, x => x.Ignore())
                .ForMember(x => x.ModifiedOn, x => x.Ignore());


            CreateMap<EntityClasses.CampaignEmailTemplate, Domain.Entities.CampaignEmailTemplate>();
            CreateMap<EntityClasses.CampaignSmstemplate, Domain.Entities.CampaignSmsTemplate>();

            //create
            CreateMap<CreateOrUpdateCampaignEmailTemplateCmd, Domain.Entities.CampaignEmailTemplate>();
            CreateMap<CreateOrUpdateCampaignSmsTemplateCmd, Domain.Entities.CampaignSmsTemplate>()
                .ForMember(x => x.SenderName, x => x.MapFrom(y => y.SenderName ?? ""))
                .ForMember(x => x.Body, x => x.MapFrom(y => y.Body ?? ""));

            //udpate 
            CreateMap<UpdateCampaignEmailResponseFormCmd, EntityClasses.CampaignEmailTemplate>()
                .ForMember(x => x.BackgroundImagePath, x => x.MapFrom(y => y.BackgroundImage));

            //upsert pending inv
            CreateMap<UpsertCampaignInvitationPendingCmd, Domain.Entities.CampaignInvitation>();
            CreateMap<UpsertCampaignInvitationAcceptedCmd, Domain.Entities.CampaignInvitation>();
            CreateMap<UpsertCampaignInvitationRejectedCmd, Domain.Entities.CampaignInvitation>();

            //background service mapping
            CreateMap<GuestsListItem, EventGuest>();
            CreateMap<Domain.Entities.CampaignInvitationMessageLog, EntityClasses.CampaignInvitationMessageLog>();
            CreateMap<EntityClasses.CampaignInvitationMessageLog, Domain.Entities.CampaignInvitationMessageLog>();

            //webhooks response
            CreateMap<Domain.Entities.ProviderResponse, EntityClasses.ProviderResponse>();

            //campaign invitation next run
            CreateMap<EntityClasses.CampaignInvitationNextRun, Domain.Entities.CampaignInvitationNextRun>();
            CreateMap<Domain.Entities.CampaignInvitationNextRun, EntityClasses.CampaignInvitationNextRun>();

            CreateMap<EntityClasses.WhatsappApprovedTemplate, Domain.Entities.WhatsappApprovedTemplate>();
            CreateMap<Domain.Entities.WhatsappApprovedTemplate, EntityClasses.WhatsappApprovedTemplate>();

            CreateMap<EntityClasses.CampaignWhatsappTemplate, Domain.Entities.CampaignWhatsappTemplate>();
            CreateMap<Domain.Entities.CampaignWhatsappTemplate, EntityClasses.CampaignWhatsappTemplate>();

            CreateMap<CreateOrUpdateWhatsappTemplateCmd, Domain.Entities.CampaignWhatsappTemplate>();
            CreateMap<UpdateCampaignWhatsappResponseFormCmd, Domain.Entities.CampaignWhatsappTemplate>();
            CreateMap<CreateOrUpdateWhatsappBOTTemplateCmd, Domain.Entities.CampaignWhatsappTemplate>();
        }
    }
}
