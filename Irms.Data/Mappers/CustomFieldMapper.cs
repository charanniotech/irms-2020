﻿using static Irms.Application.Contact.Commands.UpdateContactCmd;

namespace Irms.Data.Mappers
{
    public class CustomFieldMapper : AutoMapper.Profile
    {
        public CustomFieldMapper()
        {
            CreateMap<Domain.Entities.CustomField, EntityClasses.CustomField>()
                .ForMember(x => x.MinValue, y => y.MapFrom(z => z.MinValue))
                .ForMember(x => x.MaxValue, y => y.MapFrom(z => z.MaxValue))
                .ForMember(x => x.CustomFieldType, y => y.MapFrom(z => (int)z.CustomFieldType))
                .ReverseMap();

            CreateMap<Domain.Entities.ContactCustomField, EntityClasses.ContactCustomField>()
                .ReverseMap();

            CreateMap<CustomField, Domain.Entities.ContactCustomField>()
                .ForMember(x => x.CustomFieldId, y => y.MapFrom(z => z.Id));
        }
    }
}
