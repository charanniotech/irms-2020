﻿
namespace Irms.Data.Mappers
{
    public class ContactReachabilityMapper : AutoMapper.Profile
    {
        public ContactReachabilityMapper()
        {
            CreateMap<Domain.Entities.ContactReachability, EntityClasses.ContactReachability>()
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.TenantId, x => x.Ignore())
                .ForMember(x => x.CreatedById, x => x.Ignore())
                .ForMember(x => x.CreatedOn, x => x.Ignore())
                .ForMember(x => x.ModifiedById, x => x.Ignore())
                .ForMember(x => x.ModifiedOn, x => x.Ignore());

            CreateMap<EntityClasses.ContactReachability, Domain.Entities.ContactReachability>();

        }
    }
}
