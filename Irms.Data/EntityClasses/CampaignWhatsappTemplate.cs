﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.EntityClasses
{
    public class CampaignWhatsappTemplate
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string SenderName { get; set; }
        public string Body { get; set; }
        public string WelcomeHtml { get; set; }
        public string ProceedButtonText { get; set; }
        public string Rsvphtml { get; set; }
        public string AcceptButtonText { get; set; }
        public string RejectButtonText { get; set; }
        public string AcceptHtml { get; set; }
        public string RejectHtml { get; set; }
        public string ThemeJson { get; set; }
        public string BackgroundImagePath { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public Guid? TemplateId { get; set; }
        public bool? IsBot { get; set; }


        public virtual CampaignInvitation CampaignInvitation { get; set; }
        public virtual WhatsappApprovedTemplate Template { get; set; }
        public virtual Tenant Tenant { get; set; }
    }
}
