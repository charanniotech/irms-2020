﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class TenantContactInfo
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string PhoneNo { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public Guid? UpdateBy { get; set; }

        public virtual Tenant Tenant { get; set; }
    }
}
