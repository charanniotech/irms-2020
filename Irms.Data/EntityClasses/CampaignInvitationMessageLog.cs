﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class CampaignInvitationMessageLog
    {
        public CampaignInvitationMessageLog()
        {
            ProviderResponses = new HashSet<ProviderResponse>();
        }
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public Guid ContactId { get; set; }
        public int? InvitationTemplateTypeId { get; set; }
        public int? ProviderTypeId { get; set; }
        public string MessageId { get; set; }
        public int MessageStatus { get; set; }
        public string MessageText { get; set; }
        public DateTime TimeStamp { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual CampaignInvitation CampaignInvitation { get; set; }
        public virtual ICollection<ProviderResponse> ProviderResponses { get; set; }
    }
}
