﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class TemplateSelection
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public int TypeId { get; set; }
        public Guid? EmailTemplateId { get; set; }
        public Guid? SmsTemplateId { get; set; }
        public bool IsEnabled { get; set; }

        public virtual Template Template { get; set; }
        public virtual Template TemplateNavigation { get; set; }
        public virtual Tenant Tenant { get; set; }
    }
}
