﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.EntityClasses
{
    public class ContactListRFIAnswer
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactId { get; set; }
        public Guid ContactListRfiId { get; set; }
        public DateTime? DateAnswered { get; set; }
        public int? MediaType { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual ContactListRFI ContactListRFI { get; set; } 
    }
}
