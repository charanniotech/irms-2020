﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class EventCampaign
    {
        public EventCampaign()
        {
            CampaignPrefferedMedia = new HashSet<CampaignPrefferedMedia>();
            CampaignInvitations = new HashSet<CampaignInvitation>();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid EventId { get; set; }
        public string Name { get; set; }
        public Guid? GuestListId { get; set; }
        public int CampaignType { get; set; }
        public byte? EntryCriteriaTypeId { get; set; }
        public byte? ExitCriteriaType { get; set; }
        public byte CampaignStatus { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public string AcceptCode { get; set; }
        public string RejectCode { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public virtual ContactList GuestList { get; set; }
        public virtual CampaignEntryCriteriaType EntryCriteriaType { get; set; }
        public virtual Event Event { get; set; }
        public virtual ICollection<CampaignPrefferedMedia> CampaignPrefferedMedia { get; set; }
        public virtual ICollection<CampaignInvitation> CampaignInvitations { get; set; }
    }
}
