﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.EntityClasses
{
    public class CampaignInvitationResponseMediaType
    {
        public CampaignInvitationResponseMediaType()
        {

        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationResponseId { get; set; }
        public MediaType MediaType { get; set; }
        public ResponseMediaType AcceptOrReject { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual CampaignInvitationResponse CampaignInvitationResponse { get; set; }
    }
}
