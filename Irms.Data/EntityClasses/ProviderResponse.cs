﻿using System;

namespace Irms.Data.EntityClasses
{
    public partial class ProviderResponse
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactId { get; set; }
        public Guid CampaignInvitationMessageLogId { get; set; }
        public int MessageStatus { get; set; }
        public int ProviderType { get; set; }
        public DateTime ResponseDate { get; set; }
        public string ResponseJson { get; set; }

        public virtual CampaignInvitationMessageLog CampaignInvitationMessageLog { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
