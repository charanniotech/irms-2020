﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class ContactReachability
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactId { get; set; }
        public bool WhatsApp { get; set; }
        public bool Sms { get; set; }
        public bool Email { get; set; }
        public DateTime? WhatsAppLastReachableOn { get; set; }
        public bool? OptStatus { get; set; }
        public DateTime? OptDate { get; set; }
        public DateTime? SmsLastReachableOn { get; set; }
        public DateTime? EmailLastReachableOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public virtual Contact Contact { get; set; }
    }
}
