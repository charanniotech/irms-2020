﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class CampaignInvitation
    {
        public CampaignInvitation()
        {
            CampaignEmailTemplate = new HashSet<CampaignEmailTemplate>();
            CampaignSmsTemplate = new HashSet<CampaignSmstemplate>();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid EventCampaignId { get; set; }
        public string Title { get; set; }
        public bool IsInstant { get; set; }
        public DateTime StartDate { get; set; }
        public int InvitationTypeId { get; set; }
        public int Status { get; set; }
        public int SortOrder { get; set; }
        public int? Interval { get; set; }
        public int? IntervalType { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public virtual EventCampaign EventCampaign { get; set; }
        public virtual ICollection<CampaignEmailTemplate> CampaignEmailTemplate { get; set; }
        public virtual ICollection<CampaignSmstemplate> CampaignSmsTemplate { get; set; }
        public virtual ICollection<CampaignWhatsappTemplate> CampaignWhatsappTemplate{ get; set; }
        public virtual ICollection<RfiForm> RfiForm { get; set; }
    }
}
