﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class RfiForm
    {
        public RfiForm()
        {
            RfiFormQuestions = new HashSet<RfiFormQuestion>();
            RfiFormResponses = new HashSet<RfiFormResponse>();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid? CampaignInvitationId { get; set; }
        public string WelcomeHtml { get; set; }
        public string SubmitHtml { get; set; }
        public string ThanksHtml { get; set; }
        public string FormTheme { get; set; }
        public string ThemeBackgroundImagePath { get; set; }
        public string FormSettings { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public virtual CampaignInvitation CampaignInvitation { get; set; }
        public virtual ICollection<RfiFormQuestion> RfiFormQuestions { get; set; }
        public virtual ICollection<RfiFormResponse> RfiFormResponses { get; set; }
    }
}
