﻿using System;

namespace Irms.Data.EntityClasses
{
    public class CampaignInvitationNextRun
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public DateTime StartDate { get; set; }
        public bool Active { get; set; }
        public int Status { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual CampaignInvitation CampaignInvitation { get; set; }
    }
}
