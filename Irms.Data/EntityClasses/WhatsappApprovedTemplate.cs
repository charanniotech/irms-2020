﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.EntityClasses
{
    public class WhatsappApprovedTemplate
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public int InvitationTypeId { get; set; }
        public string Name { get; set; }
        public string TemplateBody { get; set; }
        public bool HasRfi { get; set; }
        public bool? IsBot { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public virtual Tenant Tenant { get; set; }
    }
}
