﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.EntityClasses
{
    public class CampaignPlaceholder
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Placeholder { get; set; }
        public int TemplateTypeId { get; set; }
        public int InvitationTypeId { get; set; }
    }
}
