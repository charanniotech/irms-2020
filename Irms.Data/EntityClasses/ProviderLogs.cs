﻿using System;

namespace Irms.Data.EntityClasses
{
    public partial class ProviderLogs
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid? CampaignInvitationId { get; set; }
        public Guid? ContactId { get; set; }
        public Guid? UserId { get; set; }
        public int ProviderType { get; set; }
        public string Request { get; set; }
        public DateTime RequestDate { get; set; }
        public string Response { get; set; }
        public DateTime? ResponseDate { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual UserInfo UserInfo { get; set; }
    }
}
