﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.EntityClasses
{
    public class ContactListImportantField
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactListId { get; set; }
        public string FieldName { get; set; }
        public Guid? CustomFieldId { get; set; }

        public Tenant Tenant { get; set; }
        public ContactList ContactList { get; set; }
        public CustomField CustomField { get; set; }
    }
}
