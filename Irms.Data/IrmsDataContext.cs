﻿using System;
using Irms.Data.EntityClasses;
using Irms.Data.IdentityClasses;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Irms.Data
{
    public partial class IrmsDataContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        public IrmsDataContext()
        {
        }

        public IrmsDataContext(DbContextOptions<IrmsDataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CampaignEmailTemplate> CampaignEmailTemplate { get; set; }
        public virtual DbSet<CampaignEntryCriteriaType> CampaignEntryCriteriaType { get; set; }
        public virtual DbSet<RfiForm> RfiForm { get; set; }
        public virtual DbSet<RfiFormQuestion> RfiFormQuestion { get; set; }
        public virtual DbSet<RfiFormResponse> RfiFormResponse { get; set; }
        public virtual DbSet<CampaignInvitation> CampaignInvitation { get; set; }
        public virtual DbSet<CampaignInvitationMessageLog> CampaignInvitationMessageLog { get; set; }
        public virtual DbSet<CampaignPrefferedMedia> CampaignPrefferedMedia { get; set; }
        public virtual DbSet<CampaignSmstemplate> CampaignSmstemplate { get; set; }
        public virtual DbSet<ContactList> ContactList { get; set; }
        public virtual DbSet<ContactListToContacts> ContactListToContacts { get; set; }
        public virtual DbSet<ContactReachability> ContactReachability { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<DocumentType> DocumentType { get; set; }
        public virtual DbSet<CountryTranslation> CountryTranslation { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<EventCampaign> EventCampaign { get; set; }
        public virtual DbSet<EventFeature> EventFeature { get; set; }
        public virtual DbSet<EventLocation> EventLocation { get; set; }
        public virtual DbSet<EventLog> EventLog { get; set; }
        public virtual DbSet<EventType> EventType { get; set; }
        public virtual DbSet<Language> Language { get; set; }
        public virtual DbSet<LicensePeriod> LicensePeriod { get; set; }
        public virtual DbSet<PersistedGrants> PersistedGrants { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductServiceCatalogFeature> ProductServiceCatalogFeature { get; set; }
        public virtual DbSet<ServiceCatalog> ServiceCatalog { get; set; }
        public virtual DbSet<ServiceCatalogFeature> ServiceCatalogFeature { get; set; }
        public virtual DbSet<Template> Template { get; set; }
        public virtual DbSet<TemplateSelection> TemplateSelection { get; set; }
        public virtual DbSet<TemplateTranslation> TemplateTranslation { get; set; }
        public virtual DbSet<Tenant> Tenant { get; set; }
        public virtual DbSet<TenantContactInfo> TenantContactInfo { get; set; }
        public virtual DbSet<TenantSubscription> TenantSubscription { get; set; }
        public virtual DbSet<UserInfo> UserInfo { get; set; }
        public virtual DbSet<ProviderLogs> ProviderLogs { get; set; }
        public virtual DbSet<ProviderResponse> ProviderResponse { get; set; }
        public virtual DbSet<CampaignInvitationResponse> CampaignInvitationResponse { get; set; }
        public virtual DbSet<CampaignInvitationResponseMediaType> CampaignInvitationResponseMediaType { get; set; }
        public virtual DbSet<CampaignInvitationNextRun> CampaignInvitationNextRun { get; set; }
        public virtual DbSet<CampaignPlaceholder> CampaignPlaceholder { get; set; }
        public virtual DbSet<Contact> Contact { get; set; }
        public virtual DbSet<CustomField> CustomField { get; set; }
        public virtual DbSet<ContactCustomField> ContactCustomField { get; set; }
        public virtual DbSet<ContactInvitationTelemetry> ContactInvitationTelemetry { get; set; }
        public virtual DbSet<WhatsappApprovedTemplate> WhatsappApprovedTemplate { get; set; }
        public virtual DbSet<CampaignWhatsappTemplate> CampaignWhatsappTemplate { get; set; }
        public virtual DbSet<ContactListRFI> ContactListRFI { get; set; }
        public virtual DbSet<ContactListRFIAnswer> ContactListRFIAnswer { get; set; }
        public virtual DbSet<ContactListImportantField> ContactListImportantField { get; set; }
        public virtual DbSet<ListAnalysisImportantFields> ListAnalysisImportantFields { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            new IrmsModelBuilder().BuildModel(modelBuilder);
        }
    }
}
