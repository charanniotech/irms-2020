﻿using Autofac;
using Irms.Application.Abstract;
using Irms.Data.Abstract;
using Irms.Data.IdentityClasses;
using Irms.Data.Repositories;
using Irms.Domain.Abstract;
using Irms.Infrastructure.Services;
using Irms.Infrastructure.Services.Sendgrid;
using Irms.Infrastructure.Services.Twilio;

namespace Irms.Data
{
    class IocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder
                .RegisterAssemblyTypes(ThisAssembly)
                .AssignableTo<IBaseRepository>()
                .AsImplementedInterfaces();

            //builder.RegisterType<GuestConverter>().AsImplementedInterfaces();
            builder.RegisterType<EventLogRecorder>().AsImplementedInterfaces();
            builder.RegisterType<TransactionManager>().AsImplementedInterfaces();
            builder.RegisterType<BasicTransaction>().As<ITransaction>();
            builder.RegisterType<UserFactory>().AsImplementedInterfaces();
            //builder.RegisterType<FileCleaner>().AsImplementedInterfaces();

            builder.RegisterType<TwilioConfigurationProvider>().As<ITwilioConfigurationProvider>();
            builder.RegisterType<TwilioBGConfigurationProvider>().As<ITwilioBGConfigurationProvider>();
            //builder.RegisterType<UnifonicConfigurationProvider>().As<IUnifonicConfigurationProvider>();
            builder.RegisterType<SendgridConfigurationProvider>().As<ISendgridConfigurationProvider>();
            builder.RegisterType<DatabaseQueryExecutor>().As<IDatabaseQueryExecutor>();

            //background service
            //builder.RegisterType<UnifonicBGConfigurationProvider>().As<IUnifonicBGConfigurationProvider>();
            builder.RegisterType<MalathBGConfigurationProvider>().As<IMalathBGConfigurationProvider>();
            builder.RegisterType<MalathConfigurationProvider>().As<IMalathConfigurationProvider>();
            builder.RegisterType<SendgridBGConfigurationProvider>().As<ISendgridBGConfigurationProvider>();
        }
    }
}
