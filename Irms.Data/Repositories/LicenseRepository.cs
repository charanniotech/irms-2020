﻿using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.ProductServices.Commands;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Repositories
{
    public class LicenseRepository : IRepository<LicensePeriod, byte>
    {
        private readonly TenantBasicInfo _tenant;
        private readonly IrmsTenantDataContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        public LicenseRepository(TenantBasicInfo tenant, IrmsTenantDataContext context, ICurrentUser user, IMapper mapper)
        {
            _tenant = tenant;
            _context = context;
            _mapper = mapper;
            _user = user;
        }

        public Task<byte> Create(LicensePeriod entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task Delete(byte id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public async Task<LicensePeriod> GetById(byte id, CancellationToken token)
        {
           var licenseEntity = await _context.LicensePeriod.FirstOrDefaultAsync(l => l.Id == id);
            // TODO FIX MAPPER
            return new LicensePeriod(licenseEntity.Id, licenseEntity.Title, licenseEntity.Days);
            //return _mapper.Map<LicensePeriod>(licenseEntity);
        }

        public Task Update(LicensePeriod entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }
    }
}
