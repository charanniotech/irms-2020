﻿using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain;
using Irms.Domain.Entities.Templates;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Irms.Data.Repositories.Templates
{
    /// <summary>
    /// Implementation of Repository for <see cref="Template"/>
    /// </summary>
    public class TemplateRepository : ITemplateRepository<Template, Guid>
    {
        private readonly IrmsTenantDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;
        private readonly ICurrentUser _currentUser;

        public TemplateRepository(IrmsTenantDataContext dataContext, IMapper mapper, TenantBasicInfo tenant, ICurrentUser currentUser)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _tenant = tenant;
            _currentUser = currentUser;
        }
        /// <summary>
        /// Method for getting only SMS template <see cref="Template"/> by Id
        /// </summary>
        /// <param name="id">Template Id</param>
        /// <returns><see cref="Template"/></returns>
        public async Task<Template> GetSmsTemplate(Guid id, CancellationToken token)
        {
            var data = await _dataContext.Template
                .Include(x => x.TemplateTranslation)
                .Select(x => new Irms.Data.EntityClasses.Template
                {
                    Id = x.Id,
                    SmsCompatible = x.SmsCompatible,
                    SmsText = x.SmsText,
                    TypeId = x.TypeId,
                    TemplateTranslation = x.TemplateTranslation
                })
                .FirstOrDefaultAsync(x => x.Id == id && x.TenantId == _tenant.Id, token);

            if (data == null)
            {
                throw new IncorrectRequestException("Template id is incorrect");
            }

            return new Template(
                data.Id,
                data.Name,
                data.SmsCompatible,
                data.SmsText,
                (TemplateType)data.TypeId,
                data.TemplateTranslation
                    .Select(x => new TemplateTranslation(x.LanguageId, x.SmsText)));
        }

        /// <summary>
        /// Method for getting only Email template <see cref="Template"/> by Id
        /// </summary>
        /// <param name="id">Template Id</param>
        /// <returns><see cref="Template"/></returns>
        public async Task<Template> GetEmailTemplate(Guid id, CancellationToken token)
        {
            var data = await _dataContext.Template
                .Include(x => x.TemplateTranslation)
                .Select(x => new Irms.Data.EntityClasses.Template
                {
                    Id = x.Id,
                    Name = x.Name,
                    EmailBody = x.EmailBody,
                    EmailCompatible = x.EmailCompatible,
                    TypeId = x.TypeId,
                    TemplateTranslation = x.TemplateTranslation
                })
                .FirstOrDefaultAsync(x => x.Id == id && x.TenantId == _tenant.Id, token);

            if (data == null)
            {
                throw new IncorrectRequestException("Template id is incorrect");
            }

            return new Template(
                data.Id,
                data.Name,
                data.EmailCompatible,
                data.EmailBody,
                data.EmailSubject,
                (TemplateType)data.TypeId,
                data.TemplateTranslation
                    .Select(x => new TemplateTranslation(x.LanguageId, x.EmailSubject, x.EmailBody)));
        }

        /// <summary>
        /// Method for getting object <see cref="Template"/> which include SMS and Email templates by Id
        /// </summary>
        /// <param name="id">Template Id</param>
        /// <returns><see cref="Template"/></returns>
        public async Task<Template> GetById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.Template
                .Include(x => x.TemplateTranslation)
                .FirstOrDefaultAsync(x => x.Id == id && x.TenantId == _tenant.Id, token);

            if (data == null)
            {
                throw new IncorrectRequestException("Template id is incorrect");
            }

            return new Template(
                data.Id,
                data.Name,
                data.EmailCompatible,
                data.SmsCompatible,
                data.EmailBody,
                data.EmailSubject,
                data.SmsText,
                (TemplateType)data.TypeId,
                data.TemplateTranslation
                    .Select(x => new TemplateTranslation(x.LanguageId, x.EmailSubject, x.EmailBody, x.SmsText)));
        }

        /// <summary>
        /// Method for generating new object <see cref="Template"/> which include SMS and Email template
        /// </summary>
        /// <param name="entity"><see cref="Template"/></param>
        /// <returns>Id of new template</returns>
        public async Task<Guid> Create(Template entity, CancellationToken token)
        {
            var data = _mapper.Map<EntityClasses.Template>(entity);
            data.TenantId = _tenant.Id;
            data.ModifiedById = await GetCurrentUser(token);

            await _dataContext.Template.AddAsync(data, token);

            await _dataContext.TemplateTranslation.AddRangeAsync(
                entity.Translations.Select(x => new Data.EntityClasses.TemplateTranslation
                {
                    Id = Guid.NewGuid(),
                    LanguageId = x.LanguageId,
                    TenantId = _tenant.Id,
                    TemplateId = entity.Id,
                    EmailSubject = x.EmailSubject,
                    EmailBody = x.EmailBody,
                    SmsText = x.SmsText
                }),
                token);

            await _dataContext.SaveChangesAsync(token);
            return entity.Id;
        }

        /// <summary>
        /// Method for update object <see cref="Template"/> which include SMS and Email template
        /// </summary>
        /// <param name="entity"><see cref="Template"/></param>
        public async Task Update(Template entity, CancellationToken token)
        {
            var data = await _dataContext.Template
                .Include(x => x.TemplateTranslation)
                .FirstOrDefaultAsync(x => x.Id == entity.Id && x.TenantId == _tenant.Id, token);

            if (data == null)
            {
                throw new IncorrectRequestException("Template Id is incorrect");
            }

            _mapper.Map(entity, data);

            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = await GetCurrentUser(token);


            var (toAdd, toDel, toUpd) = data.TemplateTranslation.Compare(entity.Translations, (o, n) => o.LanguageId == n.LanguageId);

            //add
            await _dataContext.TemplateTranslation.AddRangeAsync(
                toAdd.Select(x => new Data.EntityClasses.TemplateTranslation
                {
                    Id = Guid.NewGuid(),
                    LanguageId = x.LanguageId,
                    TenantId = _tenant.Id,
                    TemplateId = entity.Id,
                    EmailSubject = x.EmailSubject,
                    EmailBody = x.EmailBody,
                    SmsText = x.SmsText
                }),
                token);

            //del
            _dataContext.TemplateTranslation.RemoveRange(toDel);

            //update
            foreach (var (o, n) in toUpd)
            {
                o.EmailSubject = n.EmailSubject;
                o.EmailBody = n.EmailBody;
                o.SmsText = n.SmsText;
            }

            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// Method for removing object <see cref="Template"/> which templates from databse
        /// </summary>
        /// <param name="id">Id of template<see cref="Template"/> for removing</param>
        public async Task Delete(Guid id, CancellationToken token)
        {
            var template = await _dataContext.Template
                .FirstOrDefaultAsync(x => x.Id == id && x.TenantId == _tenant.Id, token);

            if (template == null)
            {
                throw new IncorrectRequestException("Template Id is incorrect");
            }

            await _dataContext.TemplateTranslation
                .Where(x => x.TemplateId == id)
                .DeleteAsync(token);

            _dataContext.Template.Remove(template);
            await _dataContext.SaveChangesAsync(token);
        }

        private async Task<Guid> GetCurrentUser(CancellationToken token)
        {
            var employee = await _dataContext.UserInfo.FirstOrDefaultAsync(x => x.UserId == _currentUser.Id, token);
            if (employee == null)
            {
                throw new IncorrectRequestException("Current user Id is incorrect");
            }

            return employee.Id;
        }
    }
}
