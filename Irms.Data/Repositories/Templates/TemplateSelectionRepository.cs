﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract.Repositories;
using Irms.Domain.Entities.Templates;
using Microsoft.EntityFrameworkCore;


namespace Irms.Data.Repositories.Templates
{
    /// <summary>
    /// Implementation of Repository for <see cref="TemplateSelection"/>
    /// </summary>
    public class TemplateSelectionRepository : ITemplateSelectionRepository<TemplateSelection, Guid>
    {
        private readonly IrmsTenantDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;

        public TemplateSelectionRepository(IrmsTenantDataContext dataContext, IMapper mapper, TenantBasicInfo tenant)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _tenant = tenant;
        }

        /// <summary>
        /// Method for getting <see cref="TemplateSelection"/> with SMS and Enamil temaplate Ids
        /// </summary>
        /// <param name="id">TemplateSelection Id for searcing</param>
        /// <returns><see cref="TemplateSelection"/></returns>
        public async Task<TemplateSelection> GetById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.TemplateSelection
                .FirstOrDefaultAsync(x => x.Id == id && x.TenantId == _tenant.Id, token);

            if (data == null)
            {
                return null;
            }

            return new TemplateSelection(data.Id, data.IsEnabled, (TemplateType)data.TypeId, data.SmsTemplateId, data.EmailTemplateId);
        }

        /// <summary>
        /// Method can update <see cref="TemplateSelection"/>
        /// </summary>
        /// <param name="entity"></param>
        public async Task Update(TemplateSelection entity, CancellationToken token)
        {
            var data = await _dataContext.TemplateSelection
                .FirstOrDefaultAsync(x => x.Id == entity.Id && x.TenantId == _tenant.Id, token);

            _mapper.Map(entity, data);
            await _dataContext.SaveChangesAsync(token);
        }

        public Task<Guid> Create(TemplateSelection entity, CancellationToken token) => throw new InvalidOperationException();

        public Task Delete(Guid id, CancellationToken token) => throw new InvalidOperationException();

    }
}
