﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Irms.Domain;
using Tenant = Irms.Domain.Entities.Tenant.Tenant;
using Irms.Application.Abstract.Repositories;
using Irms.Domain.Entities.Tenant;
using Irms.Application.Abstract;

namespace Irms.Data.Repositories
{
    public class TenantRepository : ITenantRepository<Tenant, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;

        public TenantRepository(IrmsDataContext dataContext, IMapper mapper, ICurrentUser user)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
        }

        /// <summary>
        /// get tenant only
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Tenant> GetById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.Tenant.FirstOrDefaultAsync(x => x.Id == id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Tenant Id is incorrect");
            }
            var result = _mapper.Map<EntityClasses.Tenant, Tenant>(data);
            return result;
        }

        public async Task<Guid> Create(Tenant entity, CancellationToken token)
        {
            var data = _mapper.Map<Tenant, EntityClasses.Tenant>(entity);
            data.CreatedOn = DateTime.UtcNow;

            //add contact info
            var contactInfo = _mapper.Map<TenantContactInfo, EntityClasses.TenantContactInfo>(entity.ContactInfo);
            data.TenantContactInfo.Add(contactInfo);

            await _dataContext.Tenant.AddAsync(data, token);
            await _dataContext.SaveChangesAsync(token);

            return data.Id;
        }

        /// <summary>
        /// update tenant basic info
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Update(Tenant entity, CancellationToken token)
        {
            var data = await _dataContext.Tenant.FirstOrDefaultAsync(x => x.Id == entity.Id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Tenant Id is incorrect");
            }

            _mapper.Map(entity, data);
            data.UpdatedOn = DateTime.UtcNow;
            data.UpdatedBy = _user.Id;
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// update tenant contact info
        /// </summary>
        /// <param name="contactInfo"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task UpdateContactInfo(TenantContactInfo contactInfo, CancellationToken token)
        {
            var data = await _dataContext.TenantContactInfo
                .FirstOrDefaultAsync(x => x.Id == contactInfo.Id, token);

            if (data == null)
            {
                throw new IncorrectRequestException("Contact info Id is incorrect");
            }

            contactInfo.TenantId = data.TenantId;
            _mapper.Map(contactInfo, data);

            data.UpdatedOn = DateTime.UtcNow;
            data.UpdateBy = _user.Id;
            await _dataContext.SaveChangesAsync(token);
        }

        /// <summary>
        /// disable tenant by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Disable(Guid id, CancellationToken token)
        {
            var data = await _dataContext.Tenant.FirstOrDefaultAsync(x => x.Id == id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Tenant Id is incorrect");
            }

            data.IsDeleted = true;
            data.IsActive = false;
            data.UpdatedOn = DateTime.UtcNow;

            await _dataContext.SaveChangesAsync(token);
        }

        public Task Delete(Guid id, CancellationToken token) => throw new NotImplementedException();
    }
}
