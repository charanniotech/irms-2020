﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Irms.Domain;
using Irms.Application.Abstract.Repositories;
using Irms.Data.EntityClasses;
using Irms.Application.Abstract;
using Irms.Application;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Irms.Data.Repositories
{
    public class RfiFormRepository : IRfiFormRepository<Domain.Entities.RfiForm, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        private readonly TenantBasicInfo _tenant;

        public RfiFormRepository(IrmsDataContext dataContext, IMapper mapper,
            ICurrentUser user,
            TenantBasicInfo tenant)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
            _tenant = tenant;
        }


        public async Task<Domain.Entities.RfiForm> GetByContactListId(Guid id, CancellationToken token)
        {
            var data = await _dataContext.ContactListRFI
                .Include(x => x.RfiForm)
                .FirstOrDefaultAsync(x => x.GuestListId == id, token);
                
            var result = _mapper.Map<RfiForm, Domain.Entities.RfiForm>(data.RfiForm);
            return result;
        }

        /// <summary>
        /// get RFI Form only
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Domain.Entities.RfiForm> GetById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.RfiForm.FirstOrDefaultAsync(x => x.Id == id, token);

            var result = _mapper.Map<RfiForm, Domain.Entities.RfiForm>(data);
            return result;
        }

        /// <summary>
        /// create RFI form
        /// </summary>
        /// <param name="entity">event domain entity</param>
        /// <param name="token"> cancellation token</param>
        /// <returns></returns>
        public async Task<Guid> Create(Domain.Entities.RfiForm entity, CancellationToken token)
        {
            var data = _mapper.Map<Domain.Entities.RfiForm, RfiForm>(entity);
            data.TenantId = _tenant.Id;
            data.CreatedById = _user.Id;
            data.CreatedOn = DateTime.UtcNow;

            await _dataContext.RfiForm.AddAsync(data, token);


            //add questions
            await _dataContext.RfiFormQuestion.AddRangeAsync(
                entity.RfiFormQuestions.Select(x => new RfiFormQuestion
                {
                    Id = x.Id,
                    RfiFormId = entity.Id,
                    Mapped = x.Mapped,
                    TenantId = _tenant.Id,
                    MappedField = x.MappedField,
                    Question = x.Question,
                    SortOrder = x.SortOrder,
                    CreatedById = _user.Id,
                    CreatedOn = DateTime.UtcNow
                }),
                token);

            await _dataContext.SaveChangesAsync(token);
            return data.Id;
        }

        /// <summary>
        /// update RFI form
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task Update(Domain.Entities.RfiForm entity, CancellationToken token)
        {
            var data = await _dataContext.RfiForm
                .Include(x => x.RfiFormQuestions)
                .FirstOrDefaultAsync(x => x.Id == entity.Id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("RFI form Id is incorrect");
            }

            _mapper.Map(entity, data);
            data.ModifiedOn = DateTime.UtcNow;
            data.ModifiedById = _user.Id;

            //Rfi form questions
            var (toAdd, toDel, toUpd) = data.RfiFormQuestions.Compare(entity.RfiFormQuestions, (o, n) => o.Id == n.Id);

            //add
            await _dataContext.RfiFormQuestion.AddRangeAsync(
                toAdd.Select(x => new RfiFormQuestion
                {
                    Id = x.Id,
                    RfiFormId = entity.Id,
                    Mapped = x.Mapped,
                    TenantId = _tenant.Id,
                    MappedField = x.MappedField,
                    Question = x.Question,
                    SortOrder = x.SortOrder,
                    CreatedById = _user.Id,
                    CreatedOn = DateTime.UtcNow
                }),
                token);

            //del
            _dataContext.RfiFormQuestion.RemoveRange(toDel);

            //update
            foreach (var (o, n) in toUpd)
            {
                o.Mapped = n.Mapped;
                o.MappedField = n.MappedField;
                o.Question = n.Question;
                o.SortOrder = n.SortOrder;
                o.ModifiedById = _user.Id;
                o.ModifiedOn = DateTime.UtcNow;
            }

            await _dataContext.SaveChangesAsync(token);
        }

        public async Task<IEnumerable<Domain.Entities.RfiFormQuestion>> GetQuestions(Guid rfiFormId, CancellationToken token)
        {
            var questions = await _dataContext.RfiFormQuestion
                .Where(x => x.RfiFormId == rfiFormId)
                .ToListAsync(token);

            return _mapper.Map<IEnumerable<Domain.Entities.RfiFormQuestion>>(questions);
        }

        public async Task<Guid> CreateContactListRfi(Guid contactListId, Guid rfiFormId, CancellationToken token)
        {
            var entity = new ContactListRFI
            {
                Id = Guid.NewGuid(),
                CreatedById = _tenant.Id,
                CreatedOn = DateTime.UtcNow,
                RfiFormId = rfiFormId,
                GuestListId = contactListId,
                TenantId = _tenant.Id,
                Status = 0
            };

            _dataContext.ContactListRFI.Add(entity);
            await _dataContext.SaveChangesAsync(token);

            return entity.Id;
        }

        public Task Delete(Guid id, CancellationToken token) => throw new NotImplementedException();
    }
}
