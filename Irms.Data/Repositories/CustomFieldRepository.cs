﻿using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.CustomFields.ReadModels;
using Irms.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Repositories
{
    public class CustomFieldRepository : ICustomFieldRepository<CustomField, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        private readonly TenantBasicInfo _tenant;

        public CustomFieldRepository(IrmsDataContext dataContext, 
            IMapper mapper,
            ICurrentUser user,
            TenantBasicInfo tenant)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
            _tenant = tenant;
        }

        public async Task<Guid> Create(CustomField entity, CancellationToken token)
        {
            var data = _mapper.Map<CustomField, EntityClasses.CustomField>(entity);
            data.CreatedOn = DateTime.UtcNow;
            data.CreatedById = _user.Id;
            data.TenantId = _tenant.Id;
            data.Id = Guid.NewGuid();
            data.CustomFieldType = (int)entity.CustomFieldType;
            _dataContext.CustomField.Add(data);
            await _dataContext.SaveChangesAsync();
            return data.Id;
        }

        public async Task<IEnumerable<CustomField>> Load(CancellationToken token)
        {
            var data = await _dataContext.CustomField
                .Where(x => x.TenantId == _tenant.Id)
                .ToListAsync(token);

            return _mapper.Map<IEnumerable<CustomField>>(data);
        }

        public Task Delete(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Domain.Entities.ContactCustomField>> LoadCustomFieldValues(IEnumerable<string> emails, IEnumerable<string> phones, CancellationToken token)
        {
            var data = await _dataContext.ContactCustomField
                .AsNoTracking()
                .Include(x => x.Contact)
                .Include(x => x.CustomField)
                .Where(x => emails.Contains(x.Contact.Email) || phones.Contains(x.Contact.MobileNumber))
                .ToListAsync(token);

            return _mapper.Map<IEnumerable<Domain.Entities.ContactCustomField>>(data);
        }
        public async Task<IEnumerable<Domain.Entities.ContactCustomField>> LoadCustomFieldValues(Guid contactId, CancellationToken token)
        {
            var data = await _dataContext.ContactCustomField
                .AsNoTracking()
                .Include(x => x.Contact)
                .Include(x => x.CustomField)
                .Where(x => x.ContactId == contactId)
                .ToListAsync(token);

            return _mapper.Map<IEnumerable<Domain.Entities.ContactCustomField>>(data);
        }

        public async Task DeleteContactCustomField(Guid id, CancellationToken token)
        {
            var entity = await _dataContext.ContactCustomField
                .FirstOrDefaultAsync(x => x.Id == id, token);

            _dataContext.ContactCustomField.Remove(entity);
            await _dataContext.SaveChangesAsync(token);
        }

        public async Task<IEnumerable<Contact>> LoadContacts(CancellationToken token)
        {
            var data = await _dataContext.Contact
                .AsNoTracking()
                .Where(x => x.TenantId == _tenant.Id)
                .ToListAsync(token);

            return _mapper.Map<IEnumerable<Contact>>(data);
        }

        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CustomField> GetById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CustomField
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id, token);

            var result = _mapper.Map<EntityClasses.CustomField, CustomField>(data);

            return result;
        }

        public async Task Update(CustomField entity, CancellationToken token)
        {
            var data = await _dataContext.CustomField
                .FirstOrDefaultAsync(x => x.Id == entity.Id, token);

            _mapper.Map(entity, data);
            data.ModifiedById = _user.Id;
            data.ModifiedOn = DateTime.UtcNow;
            _dataContext.CustomField.Update(data);
            await _dataContext.SaveChangesAsync();

        }

        public async Task<bool> HasCustomFieldWithThisName(string name, CancellationToken token)
        {
            var data = await _dataContext.CustomField
                .Where(x => x.TenantId == _tenant.Id && x.FieldName == name)
                .ToListAsync(token);

            return data.Count > 0;
        }

        public async Task<IEnumerable<KeyValuePair<string, Guid>>> LoadNationalities(CancellationToken token)
        {
            var data = await _dataContext.Country
                .Where(x => !string.IsNullOrEmpty(x.Nationality))
                .Select(x => new KeyValuePair<string, Guid>(x.Nationality.ToLower(), x.Id))
                .ToListAsync(token);

            return data;
        }

        public async Task<IEnumerable<KeyValuePair<string, Guid>>> LoadCountries(CancellationToken token)
        {
            var data = await _dataContext.Country
                .Where(x => !string.IsNullOrEmpty(x.Name))
                .Select(x => new KeyValuePair<string, Guid>(x.Name.ToLower(), x.Id))
                .ToListAsync(token);

            return data;
        }

        public async Task<IEnumerable<KeyValuePair<string, int>>> LoadDocumentTypes(CancellationToken token)
        {
            var data = await _dataContext.DocumentType
                .Where(x => !string.IsNullOrEmpty(x.Name))
                .Select(x => new KeyValuePair<string, int>(x.Name.ToLower(), x.Id))
                .ToListAsync();

            return data;
        }

        public async Task UpdateContactCustomField(Guid contactId, string customFieldName, string value, CancellationToken token)
        {
            var customField = await _dataContext.ContactCustomField
                .Include(x => x.CustomField)
                .FirstOrDefaultAsync(x => x.ContactId == contactId && x.CustomField.FieldName == customFieldName);

            if(customField == null)
            {
                var customFieldId = await _dataContext.CustomField
                    .FirstOrDefaultAsync(x => x.FieldName == customFieldName);

                customField = new EntityClasses.ContactCustomField
                {
                    Id = Guid.NewGuid(),
                    CreatedOn = DateTime.UtcNow,
                    CreatedById = _tenant.Id,
                    ContactId = contactId,
                    CustomFieldId = customFieldId.Id,
                    TenantId = _tenant.Id,
                    Value = value
                };
                _dataContext.ContactCustomField.Add(customField);
            }
            else
            {
                customField.Value = value;
                customField.ModifiedById = _tenant.Id;
                customField.ModifiedOn = DateTime.UtcNow;
                _dataContext.ContactCustomField.Update(customField);
            }
            await _dataContext.SaveChangesAsync(token);
        }

        public async Task CreateCustomFieldValueRange(IEnumerable<ContactCustomField> entities, CancellationToken token)
        {
            var data = _mapper.Map<IEnumerable<EntityClasses.ContactCustomField>>(entities).ToList();

            data.ForEach(entity =>
            {
                entity.CreatedOn = DateTime.UtcNow;
                entity.CreatedById = _user.Id;
                entity.TenantId = _tenant.Id;
                entity.Id = Guid.NewGuid();
            });

            _dataContext.ContactCustomField.AddRange(data);
            await _dataContext.SaveChangesAsync(token);
        }

        public async Task UpdateCustomFieldValueRange(IEnumerable<ContactCustomField> entities, CancellationToken token)
        {
            var data = _mapper.Map<IEnumerable<EntityClasses.ContactCustomField>>(entities).ToList();

            data.ForEach(entity =>
            {
                entity.ModifiedOn = DateTime.UtcNow;
                entity.ModifiedById = _user.Id;
            });

            _dataContext.ContactCustomField.UpdateRange(data);
            await _dataContext.SaveChangesAsync(token);
        }
    }
}
