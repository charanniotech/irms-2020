﻿using Irms.Application;
using Irms.Application.Abstract.Repositories.UserManagement;
using System;

namespace Irms.Data.IdentityClasses
{
    public class UserFactory : IUserFactory
    {
        private readonly TenantBasicInfo _tenant;

        public UserFactory(TenantBasicInfo tenant)
        {
            _tenant = tenant;
        }

        public IUser CreateUser(
            Guid id,
            string userName,
            string email,
            string phoneNumber,
            bool isEnabled = true,
            bool emailConfirmed = false,
            bool phoneNumberConfirmed = false)
        {
            return new ApplicationUser
            {
                Id = id,
                TenantId = _tenant.Id,
                IsEnabled = isEnabled,
                UserName = userName,
                Email = email,
                EmailConfirmed = emailConfirmed,
                PhoneNumber = phoneNumber,
                PhoneNumberConfirmed = phoneNumberConfirmed
            };
        }

        public IUser CreateUserForTenant(
            Guid id,
            Guid tenantId,
            string userName,
            string email,
            string phoneNumber,
            bool isEnabled = true,
            bool emailConfirmed = false,
            bool phoneNumberConfirmed = false)
        {
            return new ApplicationUser
            {
                Id = id,
                TenantId = tenantId,
                IsEnabled = isEnabled,
                UserName = userName,
                Email = email,
                EmailConfirmed = emailConfirmed,
                PhoneNumber = phoneNumber,
                PhoneNumberConfirmed = phoneNumberConfirmed
            };
        }
    }
}
