﻿using Irms.Application;
using Irms.Application.Abstract.Repositories;
using Irms.Domain;
using Irms.Domain.Entities.Tenant;
using Irms.Infrastructure.Services.Twilio;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data
{
    /// <summary>
    /// Configuration class for Twilio API
    /// </summary>
    public class TwilioConfigurationProvider : ITwilioConfigurationProvider
    {
        private readonly ITenantRepository<Tenant, Guid> _tenantRepository;
        private readonly TenantBasicInfo _tenant;

        public TwilioConfigurationProvider(ITenantRepository<Tenant, Guid> tenantRepository, TenantBasicInfo tenant)
        {
            _tenantRepository = tenantRepository;
            _tenant = tenant;
        }

        public async Task<TwilioConfiguration> GetConfiguration(CancellationToken token)
        {
            var tenant = await _tenantRepository.GetById(_tenant.Id, token);
            var result = new TwilioConfiguration(tenant.TwilioAccountSid, tenant.TwilioAuthToken, tenant.TwilioNotificationServiceId);

            if (!result.IsValid)
            {
                throw new IncorrectRequestException("Twilio configuration is invalid!");
            }

            return result;
        }
    }
}
