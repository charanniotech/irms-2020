﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class ContactCustomField
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CustomFieldId { get; set; }
        public Guid ContactId { get; set; }
        public string Value { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public void Create()
        {
            Id = Guid.NewGuid();
        }

        public void Update()
        {
            ModifiedOn = DateTime.UtcNow;
        }
    }
}
