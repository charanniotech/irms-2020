﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class CampaignWhatsappTemplate
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string SenderName { get; set; }
        public string Body { get; set; }
        public string WelcomeHtml { get; set; }
        public string ProceedButtonText { get; set; }
        public string Rsvphtml { get; set; }
        public string AcceptButtonText { get; set; }
        public string RejectButtonText { get; set; }
        public string AcceptHtml { get; set; }
        public string RejectHtml { get; set; }
        public string ThemeJson { get; set; }
        public string BackgroundImagePath { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public Guid? TemplateId { get; set; }
        public bool? IsBot { get; set; }

        public void Create()
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.UtcNow;
            //AcceptButtonText = string.Empty;
            //RejectButtonText = string.Empty;
            //ProceedButtonText = string.Empty;
            AcceptHtml = string.Empty;
            RejectHtml = string.Empty;
            WelcomeHtml = string.Empty;
            Rsvphtml = string.Empty;
        }

        public void CreateBOT(string acceptedResponse, string rejectedResponse, string defaultResponse, string body)
        {
            Id = Guid.NewGuid();
            IsBot = true;
            CreatedOn = DateTime.UtcNow;
            AcceptHtml = acceptedResponse;
            RejectHtml = rejectedResponse;
            Body = body;
            WelcomeHtml = defaultResponse;
        }
    }
}
