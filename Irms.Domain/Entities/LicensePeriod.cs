﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public partial class LicensePeriod : IEntity<byte>
    {
        public LicensePeriod()
        {
        }

        public LicensePeriod(byte id, string title, int? days)
        {
            Id = id;
            Title = title;
            Days = days;
        }

        public byte Id { get; set; }
        public string Title { get; set; }
        public int? Days { get; set; }
    }
}
