﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities.Templates
{
    public class TemplateSelection : IEntity<Guid>
    {
        internal TemplateSelection(
            Guid id,
            bool isEnabled,
            TemplateType type,
            Guid? smsTemplate,
            Guid? emailTemplate)
        {
            Id = id;
            IsEnabled = isEnabled;
            Type = type;
            SmsTemplate = smsTemplate;
            EmailTemplate = emailTemplate;
        }

        public Guid Id { get; }
        public bool IsEnabled { get; private set; }
        public TemplateType Type { get; }
        public Guid? SmsTemplate { get; private set; }
        public Guid? EmailTemplate { get; private set; }

        public void Update(bool isEnabled, Guid? smsTemplate, Guid? emailTemplate)
        {
            IsEnabled = isEnabled;
            SmsTemplate = smsTemplate;
            EmailTemplate = emailTemplate;
        }
    }
}
