﻿using System;
using Irms.Domain.Abstract;

namespace Irms.Domain.Entities
{
    public class ListAnalysis : IEntity<Guid>
    {
        public Guid GuestListId { get; set; }
        public Guid TenantId { get; set; }

        public Guid Id { get; set; }
    }
}
