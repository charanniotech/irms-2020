﻿using Irms.Domain.Abstract;
using System;

namespace Irms.Domain.Entities
{
    public class UserInfo : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public RoleType Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public Gender? Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string NationalId { get; set; }
        public string PassportNo { get; set; }
        public string SearchText { get; set; }
        public bool IsActive { get; set; }
        public Guid? UserId { get; set; }
        public Guid? AvatarId { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }

        public void Enable()
        {
            IsActive = true;
        }

        public void Disable()
        {
            IsActive = false;
        }

        public void SetAvatar(Guid fileId)
        {
            AvatarId = fileId;
        }



        public void ChangeRole(RoleType role)
        {
            Role = role;
        }

        public void UpdateProfile(string firstName,
            string lastName,
            string mobileNo,
            string email)
        {
            FirstName = firstName;
            LastName = lastName;
            MobileNo = mobileNo;
            Email = email;
        }
    }
}
