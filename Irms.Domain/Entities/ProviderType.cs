﻿namespace Irms.Domain.Entities
{
    public enum ProviderType
    {
        Twilio = 0,
        Unifonic = 1,
        Sendgrid = 2,
        Malath = 3
    }
}
