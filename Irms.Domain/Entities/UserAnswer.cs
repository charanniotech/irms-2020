﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public enum UserAnswer : int
    {
        NOT_ANSWERED = 0,
        ACCEPTED = 1,
        REJECTED = 2,
        EXPIRED = 3
    }
}
