﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class CampaignInvitationNextRun : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public DateTime StartDate { get; set; }
        public bool Active { get; set; }
        public InvitationMessageStatus Status { get; set; }

        public void Create()
        {
            Id = Guid.NewGuid();
            Active = true;
            Status = 0;
        }

        public void Create(Guid contactId, Guid campaignInvitationId)
        {
            ContactId = contactId;
            CampaignInvitationId = campaignInvitationId;
            Create();
        }
    }
}
