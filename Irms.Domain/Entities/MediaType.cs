﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public enum MediaType : int
    {
        Email = 0,
        Sms = 1,
        WhatsApp = 2
    }
}
