﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class ContactListImportantField
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactListId { get; set; }
        public string FieldName { get; set; }
        public Guid? CustomFieldId { get; set; }
    }
}
