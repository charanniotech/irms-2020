﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace Irms.Domain.Entities.Tenant
{
    public class TenantAndDepartment : IEntity<Guid>
    {
        public TenantAndDepartment(
            string name,
            string description,
            string clientUrl,
            string emailFrom,
            DateTime expiryDate)
        {
            Id = Guid.NewGuid();
            Name = name;
            Description = description;
            ClientUrl = clientUrl;
            EmailFrom = emailFrom;
            CreateDate = DateTime.UtcNow;
            UpdateDate = DateTime.UtcNow;
            IsActive = true;
            IsDefault = false;
            ExpiryDate = expiryDate;
            IsDeleted = false;
            EnabledModules = ((SystemModule[])Enum.GetValues(typeof(SystemModule))).ToImmutableList();
        }

        internal TenantAndDepartment(
            Guid tenantId,
            string name,
            string description,
            string clientUrl,
            string emailFrom,
            DateTime createDate,
            DateTime updateDate,
            bool isActive,
            bool isDefault,
            DateTime expiryDate,
            bool isDeleted,
            IEnumerable<SystemModule> enabledModules,
            Guid? adminId)
        {
            Id = tenantId;
            Name = name;
            Description = description;
            ClientUrl = clientUrl;
            EmailFrom = emailFrom;
            CreateDate = createDate;
            UpdateDate = updateDate;
            IsActive = isActive;
            IsDefault = isDefault;
            ExpiryDate = expiryDate;
            IsDeleted = isDeleted;
            EnabledModules = enabledModules.ToImmutableList();
            AdminId = adminId;
        }

        public Guid Id { get; }
        public string Name { get; }
        public string Description { get; }
        public string ClientUrl { get; }
        public string EmailFrom { get; }
        public DateTime CreateDate { get; }
        public DateTime UpdateDate { get; }
        public bool IsActive { get; }
        public bool IsDefault { get; }
        public DateTime ExpiryDate { get; }
        public bool IsDeleted { get; }
        public Guid? AdminId { get; }

        public ImmutableList<SystemModule> EnabledModules { get; }
    }
}
