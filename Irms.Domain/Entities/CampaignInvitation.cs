﻿using System;
using Irms.Domain.Abstract;

namespace Irms.Domain.Entities
{
    public class CampaignInvitation : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Title { get; set; }
        public Guid EventCampaignId { get; set; }
        public bool IsInstant { get; set; }
        public DateTime StartDate { get; set; }
        public InvitationType InvitationType { get; set; }
        public InvitationStatus Status { get; set; }
        public int SortOrder { get; set; }
        public int? Interval { get; set; }
        public IntervalType? IntervalType { get; set; }
        public bool Active { get; set; }
        public CampaignEmailTemplate EmailTemplate { get; set; }
        public CampaignSmsTemplate SmsTemplate { get; set; }
        public CampaignWhatsappTemplate WhatsappTemplate { get; set; }

        /// <summary>
        /// intializing data
        /// </summary>
        public void Create()
        {
            Id = Guid.NewGuid();
            InvitationType = InvitationType.Rsvp;
            Interval = 0;
            Status = InvitationStatus.Draft;
            SortOrder = 1;

            if (IsInstant)
            {
                StartDate = DateTime.UtcNow;
            }
        }

        public void CreateListAnalysisInvitation(Guid campaignId)
        {
            Id = Guid.NewGuid();
            EventCampaignId = campaignId;
            InvitationType = InvitationType.ListAnalysis;
            Interval = 0;
            Status = InvitationStatus.Draft;
            SortOrder = 1;
            IsInstant = true;
            StartDate = DateTime.UtcNow;
        }

        //create invitation pending
        public void CreateInvitationPending()
        {
            Id = Guid.NewGuid();
            InvitationType = InvitationType.Pending;
            Status = InvitationStatus.Draft;
            Interval = 0;
            StartDate = DateTime.UtcNow;
        }

        public void UpdateInvitationPending(CampaignInvitation inv)
        {
            Id = inv.Id;
            InvitationType = InvitationType.Pending;
            Status = InvitationStatus.Draft;
            StartDate = inv.StartDate;
            EventCampaignId = inv.EventCampaignId;
            SortOrder = inv.SortOrder;
        }

        //create invitation accepted
        public void CreateInvitationAccepted()
        {
            Id = Guid.NewGuid();
            InvitationType = InvitationType.Accepted;
            Interval = 0;
            Status = InvitationStatus.Draft;
            StartDate = DateTime.UtcNow;
            IsInstant = true;
        }

        //update invitation accepted
        public void UpdateInvitationAccepted(CampaignInvitation inv)
        {
            Id = inv.Id;
            InvitationType = InvitationType.Accepted;
            Status = InvitationStatus.Draft;
            StartDate = inv.StartDate;
            EventCampaignId = inv.EventCampaignId;
            SortOrder = inv.SortOrder;
        }

        /// <summary>
        /// update invitation
        /// </summary>
        public void Update()
        {
            if (IsInstant)
            {
                StartDate = DateTime.UtcNow;
            }
        }

        //create invitation rejected
        public void CreateInvitationRejected()
        {
            Id = Guid.NewGuid();
            InvitationType = InvitationType.Rejected;
            Interval = 0;
            Status = InvitationStatus.Draft;
            StartDate = DateTime.UtcNow;
            IsInstant = true;
        }

        //update invitation rejected
        public void UpdateInvitationRejected(CampaignInvitation inv)
        {
            Id = inv.Id;
            InvitationType = InvitationType.Rejected;
            Status = InvitationStatus.Draft;
            StartDate = inv.StartDate;
            EventCampaignId = inv.EventCampaignId;
            SortOrder = inv.SortOrder;
        }
    }
}
