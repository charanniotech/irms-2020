﻿using System;
using Irms.Domain.Abstract;

namespace Irms.Domain.Entities
{
    public class Campaign : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid EventId { get; set; }
        public string Name { get; set; }
        public Guid? GuestListId { get; set; }
        public CampaignType CampaignType { get; set; }
        public byte? EntryCriteriaTypeId { get; set; }
        public byte? ExitCriteriaType { get; set; }
        public CampaignStatus CampaignStatus { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public string AcceptCode { get; set; }
        public string RejectCode { get; set; }

        /// <summary>
        /// intializing id
        /// </summary>
        public void Create()
        {
            Id = Guid.NewGuid();
            Name = "Untitled campaign";
            CampaignType = CampaignType.Campaign;
            CampaignStatus = CampaignStatus.Draft;
            Active = false;
            Deleted = false;
        }

        public void CreateListAnalysisCampaign()
        {
            Id = Guid.NewGuid();
            Name = "Untitled List analysis campaign";
            CampaignType = CampaignType.ListAnalysis;
            CampaignStatus = CampaignStatus.Draft;
            Active = false;
            Deleted = false;
        }

        public void Update(Guid guestListId, byte entryCriteriaTypeId, byte exitCriteriaType)
        {
            GuestListId = guestListId;
            EntryCriteriaTypeId = entryCriteriaTypeId;
            ExitCriteriaType = exitCriteriaType;
        }
    }
}
