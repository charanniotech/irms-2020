﻿using Irms.Domain.Abstract;
using System;

namespace Irms.Domain.Entities
{
    public class ProviderLogs : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid? CampaignInvitationId { get; set; }
        public Guid TenantId { get; set; }
        public Guid? ContactId { get; set; }
        public Guid? UserId { get; set; }
        public ProviderType ProviderType { get; set; }
        public string Request { get; set; }
        public DateTime RequestDate { get; set; }
        public string Response { get; set; }
        public DateTime? ResponseDate { get; set; }

        public void Create()
        {
            Id = Guid.NewGuid();
        }
    }
}
