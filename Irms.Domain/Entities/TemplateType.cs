﻿namespace Irms.Domain.Entities
{
    public enum InvitationTemplateType
    {
        Email = 0,
        Sms = 1,
        WhatsApp = 2,
    }
}
