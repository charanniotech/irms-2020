# IRMS-2020 

IRMS-2020 is a Web API project built in Dotnet Core version 3.1.

This is a containerized application and is deployed on Azure Kubernetes Service (AKS) as a service.

## What is application containerization?

Application containerization is an OS-level virtualization method used to deploy and run distributed applications without launching an entire virtual machine (VM) for each app. Multiple isolated applications or services run on a single host and access the same OS kernel. Containers work on bare-metal systems, cloud instances and virtual machines, across Linux and Windows and Mac OSes.

![alt text](https://raw.githubusercontent.com/username/projectname/branch/path/to/img.png)

Application containerization works with microservices and distributed applications, as each container operates independently of others and uses minimal resources from the host.

The most common app containerization technology is Docker

## What is Docker?

Docker is a containerization platform that packages your application and all its dependencies together in the form of a docker container to ensure that your application works seamlessly in any environment

## Azure Kubernetes Service (AKS)

![alt text](https://raw.githubusercontent.com/username/projectname/branch/path/to/img.png)

## How to deploy?

Following are instructions to deploy this application in AKS.

**Step 1**	- Create Azure Kubernetes Service (AKS) cluster.

**Step 2**	- Create Azure Container Registry (ACR) for Docker images.

**Step 3**	- Deploy micro-services to AKS.

**Step 4**	- Create CI/CD pipeline. Configuring bitbucket CI/CD pipeline with AKS or R&D for better CI/CD pipeline with bitbucket and AKS.
