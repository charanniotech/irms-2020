﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Data.Repositories;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Irms.Tests.Unit.ValidData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.Repositories.Product
{

    [TestClass]
    public class ProductRepositoryTest
    {
        [TestMethod]
        public async Task CanCreate()
        {

            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var userId = Guid.NewGuid();
            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);
            
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var productEntity = new EntitiesValidData().DomainProductEntityValidData();
            var currencyEntity = new EntitiesValidData().CurrencyEntityValidData();

            var expectedResult = new EntitiesValidData().ProductEntityValidData(productEntity);

            mapper
                .Setup(x => x.Map<Data.EntityClasses.Product>(It.IsAny<Domain.Entities.Product>()))
                .Returns(expectedResult);

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.Currency.AddAsync(currencyEntity);
                await db.LicensePeriod.AddAsync(expectedResult.LicensePeriod);

                await db.SaveChangesAsync();
            }

            Guid resultId;
            Data.EntityClasses.Product result;
            
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new ProductRepository(tenant, db, currentUser.Object, mapper.Object);
                resultId = await sut.Create(productEntity, CancellationToken.None);

                result = await db.Product.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.Id, productEntity.Id);
            Assert.AreEqual(result.Title, "Test Product");
            Assert.AreEqual(result.Price, 10);
            Assert.AreEqual(result.DiscountPercentage, 5);
            Assert.AreEqual(result.CurrencyId, 1);
        }

        [TestMethod]
        public async Task CanDelete()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var currency = new EntitiesValidData().CurrencyEntityValidData();

            var productEntity = new EntitiesValidData().ProductEntityValidData(currency: currency);

            productEntity.LicensePeriod = new EntitiesValidData().LicesePeriodValidData(productEntity);
            productEntity.CreatedById = Guid.NewGuid();
            productEntity.CreatedOn = DateTime.Now;

            //TODO Fix mapper
            mapper
                .Setup(x => x.Map<Data.EntityClasses.Product>(It.IsAny<Domain.Entities.Product>()));
            //.Returns(expectedResult);

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.Product.AddRangeAsync(
                    productEntity);

                await db.SaveChangesAsync();
                var test = await db.Product.FirstOrDefaultAsync(t => t.IsDeleted == false && t.Id == productEntity.Id, CancellationToken.None);
                
            }

            Data.EntityClasses.Product result;
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new ProductRepository(tenant, db, currentUser.Object, mapper.Object);
                await sut.Delete(productEntity.Id, CancellationToken.None);
                result = await db.Product
                .Include(p => p.LicensePeriod)
                .Include(p => p.ProductServiceCatalogFeature)
                .FirstOrDefaultAsync(p => p.IsDeleted == true && p.Id == productEntity.Id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, productEntity.Id);
            Assert.AreEqual(result.Title, "Test Product");
            Assert.AreEqual(result.Price, 10);
            Assert.AreEqual(result.DiscountPercentage, 5);
            Assert.IsNotNull(result.CurrencyId);
            Assert.AreEqual(result.CurrencyId, 1);
            Assert.IsTrue(result.IsDeleted == true);
        }

        [TestMethod]
        public async Task CanDeleteListOfProducts()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var currency = new EntitiesValidData().CurrencyEntityValidData();

            List<Data.EntityClasses.Product> productEntitys = new List<Data.EntityClasses.Product>(10);
            var license = new EntitiesValidData().LicesePeriodValidData();
            for (int i = 0; i < 10; i++)
            {
                var productEntity = new EntitiesValidData().ProductEntityValidData(currency: currency);

                productEntity.LicensePeriodId = license.Id;
                productEntity.LicensePeriodDays = (int)license.Days;
                productEntity.CreatedById = Guid.NewGuid();
                productEntity.CreatedOn = DateTime.Now;
                productEntitys.Add(productEntity);
                license.Product.Add(productEntity);
            }
            

            //TODO Fix mapper
            mapper
                .Setup(x => x.Map<Data.EntityClasses.Product>(It.IsAny<Domain.Entities.Product>()));
            //.Returns(expectedResult);

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.LicensePeriod.AddAsync(license);
                await db.Product.AddRangeAsync(
                    productEntitys);

                await db.SaveChangesAsync();
                productEntitys = await db.Product.Where(p => p.IsDeleted == false).ToListAsync();
            }

            List<Data.EntityClasses.Product> result;
            var ids = productEntitys.Select(p => p.Id).ToList();

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new ProductRepository(tenant, db, currentUser.Object, mapper.Object);
                await sut.DeleteList(ids, CancellationToken.None);
                result = await db.Product
                .Include(p => p.LicensePeriod)
                .Include(p => p.ProductServiceCatalogFeature)
                .Where(p => ids.Contains(p.Id)).ToListAsync(CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, ids.Count());
            Assert.IsTrue(result.All(r => r.IsDeleted == true));
        }
    }
}
