﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Irms.Tests.Unit.ValidData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Data.Repositories;
using Tenant = Irms.Domain.Entities.Tenant.Tenant;
using Irms.Domain.Entities.Tenant;

namespace Irms.Tests.Unit.Repositories
{
    [TestClass]
    public class TenantRepositoryTest
    {
        /// <summary>
        /// can get tenant by id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.Tenant, Tenant>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var tenant = new EntitiesValidData().TenantValidData();
            var tenantId = tenant.Id;
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.Tenant.AddRangeAsync(tenant);
                await db.SaveChangesAsync();
            }

            //act
            Tenant result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new TenantRepository(db, mapper, currentUser.Object);
                result = await sut.GetById(tenantId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, tenantId);
            Assert.AreEqual(result.Name, "Tenant");
            Assert.AreEqual(result.Description, "IT solutions");
        }


        //create tenant/ customer test
        [TestMethod]
        public async Task CanCreate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Tenant, Data.EntityClasses.Tenant>();
                cfg.CreateMap<TenantContactInfo, Data.EntityClasses.TenantContactInfo>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var tenantId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            //var tenantEntity = new EntitiesValidData().TenantValidData();
            //tenantEntity.Id = tenantId;

            //var tenantContactInfoEntity = new EntitiesValidData().TenantContactInfoValidData();
            //tenantContactInfoEntity.TenantId = tenantId;

            var tenantBuilder = new TenantBuilder();
            var tenantToCreate = tenantBuilder.Build();
            tenantToCreate.ContactInfo = new TenantContactInfoBuilder().Build();
            tenantToCreate.Id = tenantId;
            tenantToCreate.ContactInfo.TenantId = tenantId;

            //act
            Guid resultId;
            Data.EntityClasses.Tenant result;
            Data.EntityClasses.TenantContactInfo contactInfo;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new TenantRepository(db, mapper, currentUser.Object);
                resultId = await sut.Create(tenantToCreate, CancellationToken.None);
                result = await db.Tenant.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
                contactInfo = await db.TenantContactInfo.FirstOrDefaultAsync(x => x.TenantId == resultId, CancellationToken.None);
            }

            //assert
            //tenant
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.Name, "tenant to create");
            Assert.AreEqual(result.Address, "address, Al khobar, saudi arabia");

            //contact info
            Assert.AreEqual(contactInfo.FirstName, "Saif");
            Assert.AreEqual(contactInfo.LastName, "ur Rehman");
            Assert.AreEqual(contactInfo.Email, "test@email.com");
            Assert.AreEqual(contactInfo.MobileNo, "+966554154875");
        }

        //create tenant/ customer test
        [TestMethod]
        public async Task CanUpdate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid tenantId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenantEntity = new EntitiesValidData().TenantValidData();
            tenantEntity.Id = tenantId;



            var tenantBuilder = new TenantBuilder();
            var tenantToUpdate = tenantBuilder.Build();
            tenantToUpdate.Id = tenantId;

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            mapper
                .Setup(x => x.Map<Data.EntityClasses.Tenant>(It.IsAny<Domain.Entities.Tenant.Tenant>()));

            using (var db = new IrmsDataContext(options))
            {
                await db.Tenant.AddRangeAsync(
                    tenantEntity);
                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.Tenant result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new TenantRepository(db, mapper.Object, currentUser.Object);
                await sut.Update(tenantToUpdate, CancellationToken.None);
                result = await db.Tenant.FirstOrDefaultAsync(x => x.Id == tenantId, CancellationToken.None);
            }


            //assert
            //tenant
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, tenantId);
            Assert.AreEqual(result.Name, "Tenant");
            Assert.AreEqual(result.Description, "IT solutions");
            Assert.AreEqual(result.City, "Al khobar, saudi arabic");
        }

        /// <summary>
        /// update tenant contact info
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateContactInfo()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var contactEntity = new EntitiesValidData().TenantContactInfoValidData();
            contactEntity.Id = id;

            var contactToUpdate = new TenantContactInfoBuilder().Build();
            contactToUpdate.Id = id;

            mapper
                .Setup(x => x.Map<Data.EntityClasses.TenantContactInfo>(It.IsAny<TenantContactInfo>()));

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.TenantContactInfo.AddRangeAsync(
                    contactEntity);
                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.TenantContactInfo result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new TenantRepository(db, mapper.Object, currentUser.Object);
                await sut.UpdateContactInfo(contactToUpdate, CancellationToken.None);
                result = await db.TenantContactInfo.FirstOrDefaultAsync(x => x.Id == id, CancellationToken.None);
            }


            //assert
            //tenant
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.FirstName, "Saif");
            Assert.AreEqual(result.LastName, "ur Rehman");
            Assert.AreEqual(result.Email, "test@tenant.com");
        }
    }
}
