﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Data.Repositories;
using Irms.Domain.Entities;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Irms.Tests.Unit.Repositories
{
    [TestClass]
    public class ContactListRepositoryTest
    {
        [TestMethod]
        public async Task CanCreateContactList()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactList, Data.EntityClasses.ContactList>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactListToCreate = new ContactListBuilder().Build();
            contactListToCreate.Id = id;
            contactListToCreate.TenantId = tenant.Id;
            

           //act
           Guid resultId;
            Data.EntityClasses.ContactList result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                resultId = await sut.Create(contactListToCreate, CancellationToken.None);
                result = await db.ContactList.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.Name, contactListToCreate.Name);
        }

        /// <summary>
        /// check GetById method, check result is not null and has same id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetByIdContact()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactList, Data.EntityClasses.ContactList>();
                cfg.CreateMap<Data.EntityClasses.ContactList, ContactList>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactListToCreate = new ContactListBuilder().Build();
            contactListToCreate.Id = id;
            contactListToCreate.TenantId = tenant.Id;

            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                await sut.Create(contactListToCreate, CancellationToken.None);
            }
            // Act

            ContactList result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                result = await sut.GetById(id, CancellationToken.None);
            }

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
        }

        /// <summary>
        /// check Update method, check result is not null and has same id and name
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateContactList()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactList, Data.EntityClasses.ContactList>();
                cfg.CreateMap<Data.EntityClasses.ContactList, ContactList>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactListToCreate = new ContactListBuilder().Build();
            contactListToCreate.Id = id;
            contactListToCreate.TenantId = tenant.Id;

            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                await sut.Create(contactListToCreate, CancellationToken.None);
            }
            // Act

            Data.EntityClasses.ContactList result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                result = await db.ContactList.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id, CancellationToken.None);
                var data = mapper.Map<ContactList>(result);
                result.Name = "new Name";
                await sut.Update(data, CancellationToken.None);
            }

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Name, "new Name");
        }

        /// <summary>
        /// is contact list assigned to a campaign: check variant when it is not assigned,
        /// checks that result is equaled to false
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task IsContactListAssignToACampaign_NotAssigned()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactList, Data.EntityClasses.ContactList>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactListToCreate = new ContactListBuilder().Build();
            contactListToCreate.Id = id;
            contactListToCreate.TenantId = tenant.Id;

            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                db.ContactList.Add(mapper.Map<Data.EntityClasses.ContactList>(contactListToCreate));
                db.EventCampaign.Add(new Data.EntityClasses.EventCampaign
                {
                    Id = Guid.NewGuid()
                });
                await db.SaveChangesAsync();
            }

            // Act
            bool result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                result = await sut.IsContactListAssignToACampaign(id, CancellationToken.None);
            }

            // Assert
            Assert.AreEqual(result, false);
        }

        /// <summary>
        /// is contact list assigned to a campaign: check variant when it is assigned,
        /// checks that result is equaled to true
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task IsContactListAssignToACampaign_Assigned()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactList, Data.EntityClasses.ContactList>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactListToCreate = new ContactListBuilder().Build();
            contactListToCreate.Id = id;
            contactListToCreate.TenantId = tenant.Id;

            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                db.ContactList.Add(mapper.Map<Data.EntityClasses.ContactList>(contactListToCreate));
                db.EventCampaign.Add(new Data.EntityClasses.EventCampaign
                {
                    Id = Guid.NewGuid(),
                    GuestListId = id
                });
                await db.SaveChangesAsync();
            }

            // Act
            bool result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                result = await sut.IsContactListAssignToACampaign(id, CancellationToken.None);                
            }

            // Assert
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// checks if contact list with this name already exists (exist)
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CheckIsNameAlreadyExists_Exists()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactList, Data.EntityClasses.ContactList>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var name = "123";

            var contactListToCreate = new ContactListBuilder().Build();
            contactListToCreate.Id = id;
            contactListToCreate.TenantId = tenant.Id;
            contactListToCreate.Name = name;

            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                db.ContactList.Add(mapper.Map<Data.EntityClasses.ContactList>(contactListToCreate));
                await db.SaveChangesAsync();
            }

            // Act
            bool result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                result = await sut.IsNameAlreadyExists(name, CancellationToken.None);
            }

            // Assert
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// checks if contact list with this name already exists (exist)
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CheckIsNameAlreadyExists_NotExists()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactList, Data.EntityClasses.ContactList>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var name = "123";

            var contactListToCreate = new ContactListBuilder().Build();
            contactListToCreate.Id = id;
            contactListToCreate.TenantId = tenant.Id;
            contactListToCreate.Name = name;

            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                db.ContactList.Add(mapper.Map<Data.EntityClasses.ContactList>(contactListToCreate));
                await db.SaveChangesAsync();
            }

            // Act
            bool result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                result = await sut.IsNameAlreadyExists("123123", CancellationToken.None);
            }

            // Assert
            Assert.AreEqual(result, false);
        }

        /// <summary>
        /// checks if can delete contact list, ensures that contact list no more exists
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteContactList()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactList, Data.EntityClasses.ContactList>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactListToCreate = new ContactListBuilder().Build();
            contactListToCreate.Id = id;
            contactListToCreate.TenantId = tenant.Id;

            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                db.ContactList.Add(mapper.Map<Data.EntityClasses.ContactList>(contactListToCreate));
                await db.SaveChangesAsync();
            }

            // Act
            Data.EntityClasses.ContactList result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, mapper, currentUser.Object);
                await sut.Delete(id, CancellationToken.None);
                result = await db.ContactList.FirstOrDefaultAsync(x => x.Id == id, CancellationToken.None);
            }

            // Assert
            Assert.IsNull(result);
        }

        /// <summary>
        /// Test for checking if contact list is empty
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCheckIfContactListIsEmpty()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var contactListGuid = Guid.NewGuid();

            using (var db = new IrmsDataContext(options))
            {
                db.ContactListToContacts.Add(new Data.EntityClasses.ContactListToContacts
                {
                 ContactId = Guid.NewGuid(),
                 ContactListId = contactListGuid
                });
                await db.SaveChangesAsync();
            }
            // Act
            bool result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListRepository(db, null, null);
                result = await sut.IsContactListEmpty(contactListGuid, CancellationToken.None);
            }

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(false, result);
        }
    }
}
