﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class RfiFormBuilder
    {
        public Guid Id { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string WelcomeHtml { get; set; }
        public string SubmitHtml { get; set; }
        public string ThanksHtml { get; set; }
        public string FormTheme { get; set; }
        public string FormSettings { get; set; }
        public string ThemeBackgroundImagePath { get; set; }
        public ImmutableList<RfiFormQuestion> RfiFormQuestions { get; private set; }


        public static implicit operator RfiForm(RfiFormBuilder instance)
        {
            return instance.Build();
        }

        public RfiForm Build()
        {
            var questions = new[]
            {
                new RfiFormQuestion
                {
                    Id = Guid.NewGuid(),
                    Question = "emailquestion",
                    Mapped = true,
                    MappedField = "email",
                    SortOrder = 1
                },

                new RfiFormQuestion
                {
                    Id = Guid.NewGuid(),
                    Question = "dateofbirthquestion",
                    Mapped = false,
                    MappedField = null,
                    SortOrder = 2
                },
            }.ToImmutableList();

            return new RfiForm
            {
                Id = Guid.NewGuid(),
                CampaignInvitationId = Guid.NewGuid(),
                WelcomeHtml = "{\"content\":\"english content\",\"contentArabic\":\"arabic content\",\"button\":\"Proceed\",\"buttonArabic\":\"تقدم\"}",
                SubmitHtml = "<button>save</button>",
                ThanksHtml = "{\"content\":\"english content\",\"contentArabic\":\"arabic content\",\"button\":\"Proceed\",\"buttonArabic\":\"تقدم\"}",
                FormTheme = "<p>form theme...</button>",
                FormSettings = "<p>form settings</p>",
                ThemeBackgroundImagePath = "abc.png",
                RfiFormQuestions = questions
            };
        }

        public Data.EntityClasses.RfiForm BuildEntity()
        {
            return new Data.EntityClasses.RfiForm
            {
                Id = Guid.NewGuid(),
                CampaignInvitationId = Guid.NewGuid(),
                WelcomeHtml = "<p>welcome...</p>",
                SubmitHtml = "<button>save</button>",
                ThanksHtml = "<p>thanks...</p>",
                FormTheme = "<p>form theme...</button>",
                FormSettings = "<p>form settings</p>",
                ThemeBackgroundImagePath = "abc.png"
            };
        }
    }
}
