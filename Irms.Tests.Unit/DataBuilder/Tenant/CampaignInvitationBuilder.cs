﻿using Irms.Domain.Entities;
using System;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class CampaignInvitationBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid EventCampaignId { get; set; }
        public bool IsInstant { get; set; }
        public DateTime StartDate { get; set; }
        public int InvitationTypeId { get; set; }
        public int SortOrder { get; set; }
        public bool Active { get; set; }

        public static implicit operator CampaignInvitation(CampaignInvitationBuilder instance)
        {
            return instance.Build();
        }

        public CampaignInvitation Build()
        {
            return new CampaignInvitation
            {
                Id = Guid.NewGuid(),
                IsInstant = false,
                EventCampaignId = Guid.NewGuid(),
                Active = true,
                InvitationType = InvitationType.Rsvp
            };
        }

        public Data.EntityClasses.CampaignInvitation BuildEntity()
        {
            return new Data.EntityClasses.CampaignInvitation
            {
                Id = Guid.NewGuid(),
                IsInstant = false,
                EventCampaignId = Guid.NewGuid(),
                Active = true,
                InvitationTypeId = 0
            };
        }
    }
}
