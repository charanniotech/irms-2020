﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class CampaignSmsTemplateBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string SenderName { get; set; }
        public string Body { get; set; }
        public string WelcomeHtml { get; set; }
        public string ProceedButtonText { get; set; }
        public string Rsvphtml { get; set; }
        public string AcceptButtonText { get; set; }
        public string RejectButtonText { get; set; }
        public string AcceptHtml { get; set; }
        public string RejectHtml { get; set; }
        public string ThemeJson { get; set; }
        public string BackgroundImagePath { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public static implicit operator CampaignSmsTemplate(CampaignSmsTemplateBuilder instance)
        {
            return instance.Build();
        }

        public CampaignSmsTemplate Build()
        {
            return new CampaignSmsTemplate
            {
                Id = Guid.NewGuid(),
                TenantId = Guid.NewGuid(),
                CampaignInvitationId = Guid.NewGuid(),
                AcceptButtonText = "accept_text",
                ProceedButtonText = "proceed_text",
                RejectButtonText = "reject_text",
                RSVPHtml = "rsvp_html",
                SenderName = "sender_name",
                WelcomeHtml = "welcome_html",
                Body = "template_body",
                AcceptHtml = "accept_html",
                RejectHtml = "reject_html",
                ThemeJson = "theme_json",
                BackgroundImagePath = "abc.png"
            };
        }

        public Data.EntityClasses.CampaignSmstemplate BuildEntity()
        {
            return new Data.EntityClasses.CampaignSmstemplate
            {
                Id = Guid.NewGuid(),
                CampaignInvitationId = Guid.NewGuid(),
                AcceptButtonText = "accept_text",
                ProceedButtonText = "proceed_text",
                RejectButtonText = "reject_text",
                Rsvphtml = "rsvp_html",
                SenderName = "sender_name",
                WelcomeHtml = "welcome_html",
                Body = "template_body",
                AcceptHtml = "accept_html",
                RejectHtml = "reject_html",
                ThemeJson = "theme_json",
                BackgroundImagePath = "abc.png"
            };
        }
    }
}
