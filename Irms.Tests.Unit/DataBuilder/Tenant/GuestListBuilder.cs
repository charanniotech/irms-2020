﻿using System;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class GuestListBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Name { get; set; }
        public bool IsGlobal { get; set; }
        public bool IsGuest { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }


        public static implicit operator Domain.Entities.ContactList(GuestListBuilder instance)
        {
            return instance.Build();
        }

        public Domain.Entities.ContactList Build()
        {
            return new Domain.Entities.ContactList()
            {
                Name = "GuestList1",
                CreatedOn = DateTime.UtcNow,
                IsGlobal = true,
                IsGuest = true
            };
        }
    }
}
