﻿using Irms.Application;
using Irms.Data;
using Irms.Data.Read.Tenant.ReadModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class TenantBasicInfoBuilder
    {
        public string Name { get; set; }
        public Guid TenantId { get; set; }
        public bool IsDefaultTenant { get; set; }

        public Guid DepartmentId { get; set; }
        public string ClientUrl { get; set; }
        public string EmailFrom { get; set; }
        public bool IsBtoC { get; set; }

        public static TenantBasicInfo BuildDefault(string title = "foo")
        {
            return new TenantBasicInfo(
                Guid.NewGuid(), title, $"{title}.com", $"admin@{title}.com", false);
        }

        public static TenantListItem BuildDefaultList(string name = "foo")
        {
            return new TenantListItem()
            {
                Id = Guid.NewGuid(),
                Name = name,
                Address = "Al khobar KSA",
                Email = "takamul@ksa.com"
            };
        }

        public TenantBasicInfo Build()
        {
            return new TenantBasicInfo(
                TenantId, Name, ClientUrl, EmailFrom, IsBtoC);
        }
    }
}
