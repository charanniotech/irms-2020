﻿using Irms.Domain.Entities.Templates;
using System;
using System.Collections.Generic;

namespace Irms.Tests.Unit.DataBuilder.Template
{
    public class TemplateBuilder
    {
        public TemplateBuilder()
        {
            Id = Guid.NewGuid();
            Name = "Template";
            EmailCompatible = true;
            SmsCompatible = true;
            EmailBody = "email body";
            EmailSubject = "email subject";
            SmsText = "sms text";
            Type = TemplateType.EventAssignment;
            Translation = new List<TemplateTranslation>
            {
                new TemplateTranslation(Guid.NewGuid(), "email body", "email subject", "sms text")
            };
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool EmailCompatible { get; set; }
        public bool SmsCompatible { get; set; }
        public string EmailBody { get; set; }
        public string EmailSubject { get; set; }
        public string SmsText { get; set; }
        public TemplateType Type { get; set; }
        public List<TemplateTranslation> Translation { get; set; }

        public static implicit operator Domain.Entities.Templates.Template(TemplateBuilder instance)
        {
            return instance.Build();
        }

        public Domain.Entities.Templates.Template Build()
        {
            return new Domain.Entities.Templates.Template(
                Id,
                Name,
                EmailCompatible,
                SmsCompatible,
                EmailBody,
                EmailSubject,
                SmsText,
                Type,
                Translation);
        }
    }
}
