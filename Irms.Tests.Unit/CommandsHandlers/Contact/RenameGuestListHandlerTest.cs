﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.CommandHandlers;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using Irms.Domain.Entities;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.Contact
{
    [TestClass]
    public class RenameGuestListHandlerTest
    {
        /// <summary>
        /// check RenameGuestListHandler, check result is not null, verify mediator's publish method 
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ValidateRenameGuestListHandler()
        {
            var mediator = new Mock<IMediator>();
            var contactListRepository = new Mock<IContactListRepository<Domain.Entities.ContactList, Guid>>();
            var mapper = new Mock<IMapper>();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var expectedResult = new ContactBuilder().Build();

            Guid guid = Guid.NewGuid();

            contactListRepository
                .Setup(x => x.GetById(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid g, CancellationToken t) => Task.FromResult(new ContactList
                {
                    Id = guid
                }));

            contactListRepository
                .Setup(x => x.Update(It.IsAny<ContactList>(), CancellationToken.None))
                .Returns((ContactList g, CancellationToken t) => Task.FromResult(new ContactList
                {
                    Id = guid
                }));

            var prodHandler = new RenameGuestListHandler(tenant, contactListRepository.Object, mediator.Object, mapper.Object);
            var cmd = new RenameGuestListCmd();


            //Act
            var id = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(id);
            mediator.Verify(
                x => x.Publish(
                    It.IsAny<GuestListRenamed>(),
                    It.IsAny<CancellationToken>()
                    ));
        }
    }
}
