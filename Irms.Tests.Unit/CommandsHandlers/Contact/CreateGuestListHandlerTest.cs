﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.CommandHandlers;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using Irms.Domain.Entities;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.Contact
{
    [TestClass]
    public class CreateGuestListHandlerTest
    {
        /// <summary>
        /// check CreateGuestListHandler, check result is not null and equal, verify mediator's publish method 
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ValidateCreateGuestListHandler()
        {
            var mediator = new Mock<IMediator>();
            var contactListRepository = new Mock<IContactListRepository<Domain.Entities.ContactList, Guid>>();
            var mapper = new Mock<IMapper>();

            mediator
                .Setup(x => x.Publish(It.IsAny<GuestListCreated>(), CancellationToken.None))
                .Returns((GuestListCreated q, CancellationToken t) => MediatR.Unit.Task);

            var expectedResult = new GuestListBuilder().Build();
            mapper
                .Setup(x => x.Map<Domain.Entities.ContactList>(It.IsAny<CreateGuestListCmd>()))
                .Returns(expectedResult);

            var tenant = TenantBasicInfoBuilder.BuildDefault();
            var eventId = Guid.NewGuid();
            var prodHandler = new CreateGuestListHandler(tenant, contactListRepository.Object, mediator.Object, mapper.Object);
            var cmd = new CreateGuestListCmd("New Event guest list", eventId);

            //Act
            var id = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(id);
            contactListRepository.Verify(
               x => x.Create(
                   It.Is<ContactList>(l => l.TenantId == tenant.Id),
                   It.IsAny<CancellationToken>()
               )
            );
            mediator.Verify(
                 x => x.Publish(
                   It.Is<GuestListCreated>(l => l.ContactListId == id),
                   It.IsAny<CancellationToken>()
                )
        );
        }
    }
}
