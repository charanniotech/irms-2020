﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.CommandHandlers;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using Irms.Domain.Entities;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.CampaignInvitations
{
    [TestClass]
    public class DuplicateInvitationHandlerTest
    {
        /// <summary>
        /// unit test for duplicate invitation, checks that result is not null and equal to id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanHandleDuplicateInvitation()
        {
            //Arrange
            var id = Guid.NewGuid();
            var newId = Guid.NewGuid();

            var repo = new Mock<ICampaignInvitationRepository<CampaignInvitation, Guid>>();
            var fileService = new Mock<IFileUploadService>();

            fileService
                .Setup(x => x.LoadFile(It.IsAny<string>()))
                .Returns((string s) => Task.FromResult(new byte[0]));

            fileService
                .Setup(x => x.UploadFile(It.IsAny<string>(), It.IsAny<Stream>()))
                .Returns((string s, Stream st) => MediatR.Unit.Task);

            var campaignInvitation = new CampaignInvitation
            {
                Id = id,
                Active = true,
                EventCampaignId = id,
                Interval = 0,
                IntervalType = IntervalType.Days,
                InvitationType = InvitationType.Accepted,
                IsInstant = true,
                SortOrder = 0,
                StartDate = DateTime.UtcNow,
                TenantId = id,
                Title = "123"
            };

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignEmailTemplate, CampaignEmailTemplate>();
                cfg.CreateMap<Data.EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>();
            });

            var mapper = config.CreateMapper();

            var smsTemplate = new CampaignSmsTemplateBuilder().BuildEntity();
            var emailTemplate = new CampaignEmailTemplateBuilder().BuildEntity();

            var mediator = new Mock<IMediator>();

            mediator
                .Setup(x => x.Publish(It.IsAny<InvitationDuplicated>(), CancellationToken.None))
                .Returns((InvitationDuplicated id, CancellationToken t) => MediatR.Unit.Task);

            repo
                .Setup(x => x.GetById(It.IsAny<Guid>(),CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(campaignInvitation));

            IEnumerable<CampaignInvitation> listData = new List<CampaignInvitation>()
            {
                campaignInvitation
            };

            repo
                .Setup(x => x.GetById(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(campaignInvitation));

            repo
                .Setup(x => x.LoadInvitationGroup(It.IsAny<Guid>(), It.IsAny<InvitationType>(), CancellationToken.None))
                .Returns((Guid q, InvitationType it, CancellationToken t) => Task.FromResult(listData));

            repo
                .Setup(x => x.Create(It.IsAny<CampaignInvitation>(), CancellationToken.None))
                .Returns((CampaignInvitation q, CancellationToken t) => Task.FromResult(newId));

            repo
                .Setup(x => x.GetCampaignSmsTemplateByInvitationId(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(mapper.Map<CampaignSmsTemplate>(smsTemplate)));

            repo
                .Setup(x => x.GetCampaignEmailTemplateByInvitationId(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(mapper.Map<CampaignEmailTemplate>(emailTemplate)));
           
            var prodHandler = new DuplicateInvitationHandler(repo.Object, mediator.Object,fileService.Object);

            var cmd = new DuplicateInvitationCmd(id);

            //Act
            var apiResult = await prodHandler.Handle(cmd, CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
        }
    }
}
