﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.CommandHandlers;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.CampaignInvitations
{
    /// <summary>
    /// tests if it can create or update sms template, check for not null result
    /// </summary>
    [TestClass]
    public class CreateOrUpdateCampaignSmsTemplateHandlerTest
    {
        [TestMethod]
        public async Task CanCreateOrUpdateCampaignSmsTemplateHandlerTest_Create()
        {
            //Arrange
            var repo = new Mock<ICampaignInvitationRepository<CampaignInvitation, Guid>>();
            var mediator = new Mock<IMediator>();
            var mapper = new Mock<IMapper>();
            var configuration = new Mock<IConfiguration>();

            var id = Guid.NewGuid();

            var emailTemplate = new CampaignSmsTemplate
            {
                CampaignInvitationId = id,
                AcceptButtonText = "accept_text",
                ProceedButtonText = "proceed_text",
                RejectButtonText = "reject_text",
                RSVPHtml = "rsvp_html",
                SenderName = "sender_name",
                WelcomeHtml = "welcome_html",
                Body = "template_body",
                AcceptHtml = "accept_html",
                RejectHtml = "reject_html",
                ThemeJson = "theme_json",
                BackgroundImagePath = "abc.png"
            };

            repo
                .Setup(x => x.CreateCampaignSmsTemplate(It.IsAny<CampaignSmsTemplate>(), CancellationToken.None))
                .Returns((CampaignSmsTemplate q, CancellationToken t) => Task.FromResult(id));

            repo
                .Setup(x => x.UpdateCampaignSmsTemplate(It.IsAny<CampaignSmsTemplate>(), CancellationToken.None))
                .Returns((CampaignSmsTemplate q, CancellationToken t) => MediatR.Unit.Task);

            repo
                .Setup(x => x.GetCampaignSmsTemplateById(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(emailTemplate));

            mediator
                .Setup(x => x.Publish(It.IsAny<CampaignSmsTemplateCreatedOrUpdated>(), CancellationToken.None))
                .Returns((CampaignSmsTemplateCreatedOrUpdated q, CancellationToken t) => MediatR.Unit.Task);

            mapper
                .Setup(x => x.Map<CampaignSmsTemplate>(It.IsAny<CreateOrUpdateCampaignSmsTemplateCmd>()))
                .Returns(emailTemplate);

            configuration
                .Setup(x => x[It.IsAny<string>()])
                .Returns((string k) => default);

            var cmd = new CreateOrUpdateCampaignSmsTemplateCmd
            {
                CampaignInvitationId = id,
                AcceptButtonText = "accept_text",
                ProceedButtonText = "proceed_text",
                RejectButtonText = "reject_text",
                RSVPHtml = "rsvp_html",
                SenderName = "sender_name",
                WelcomeHtml = "welcome_html",
                Body = "template_body",
                AcceptHtml = "accept_html",
                RejectHtml = "reject_html",
                ThemeJson = "theme_json",
                BackgroundImagePath = "abc.png"
            };
            var procHandler = new CreateOrUpdateCampaignSmsTemplateHandler(repo.Object, mediator.Object, mapper.Object, configuration.Object);

            //Act
            var result = await procHandler.Handle(cmd, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task CanCreateOrUpdateCampaignSmsTemplateHandlerTest_Update()
        {
            //Arrange
            var repo = new Mock<ICampaignInvitationRepository<CampaignInvitation, Guid>>();
            var mediator = new Mock<IMediator>();
            var mapper = new Mock<IMapper>();
            var configuration = new Mock<IConfiguration>();

            var id = Guid.NewGuid();

            var emailTemplate = new CampaignSmsTemplate
            {
                Id = id,
                CampaignInvitationId = id,
                AcceptButtonText = "accept_text",
                ProceedButtonText = "proceed_text",
                RejectButtonText = "reject_text",
                RSVPHtml = "rsvp_html",
                SenderName = "sender_name",
                WelcomeHtml = "welcome_html",
                Body = "template_body",
                AcceptHtml = "accept_html",
                RejectHtml = "reject_html",
                ThemeJson = "theme_json",
                BackgroundImagePath = "abc.png"
            };

            repo
                .Setup(x => x.CreateCampaignSmsTemplate(It.IsAny<CampaignSmsTemplate>(), CancellationToken.None))
                .Returns((CampaignSmsTemplate q, CancellationToken t) => Task.FromResult(id));

            repo
                .Setup(x => x.UpdateCampaignSmsTemplate(It.IsAny<CampaignSmsTemplate>(), CancellationToken.None))
                .Returns((CampaignSmsTemplate q, CancellationToken t) => MediatR.Unit.Task);

            repo
                .Setup(x => x.GetCampaignSmsTemplateById(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(emailTemplate));

            mediator
                .Setup(x => x.Publish(It.IsAny<CampaignSmsTemplateCreatedOrUpdated>(), CancellationToken.None))
                .Returns((CampaignSmsTemplateCreatedOrUpdated q, CancellationToken t) => MediatR.Unit.Task);

            mapper
                .Setup(x => x.Map<CampaignSmsTemplate>(It.IsAny<CreateOrUpdateCampaignSmsTemplateCmd>()))
                .Returns(emailTemplate);

            configuration
                .Setup(x => x[It.IsAny<string>()])
                .Returns((string k) => default);

            var cmd = new CreateOrUpdateCampaignSmsTemplateCmd
            {
                Id = id,
                CampaignInvitationId = id,
                AcceptButtonText = "accept_text",
                ProceedButtonText = "proceed_text",
                RejectButtonText = "reject_text",
                RSVPHtml = "rsvp_html",
                SenderName = "sender_name",
                WelcomeHtml = "welcome_html",
                Body = "template_body",
                AcceptHtml = "accept_html",
                RejectHtml = "reject_html",
                ThemeJson = "theme_json",
                BackgroundImagePath = "abc.png"
            };
            var procHandler = new CreateOrUpdateCampaignSmsTemplateHandler(repo.Object, mediator.Object, mapper.Object, configuration.Object);

            //Act
            var result = await procHandler.Handle(cmd, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(id, result);
        }
    }
}
