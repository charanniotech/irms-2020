﻿using Irms.Application.Product.CommandHandlers;
using Irms.Application.Abstract.Repositories;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.Base;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Product.Commands;
using System.Threading;
using Irms.Application.Product.Events;

namespace Irms.Tests.Unit.CommandsHandlers.Product
{
    [TestClass]
    public class UpdateProductHandlerTest
    {
        [TestMethod]
        public async Task CanHandle()
        {
            var mediator = new Mock<IMediator>();
            var prodactRepository = new Mock<IProductRepository<Domain.Entities.Product, Guid>>();
            var licenseRepository = new Mock<IRepository<Domain.Entities.LicensePeriod, byte>>();
            var mapper = new Mock<IMapper>();
            
            mediator
                .Setup(x => x.Publish(It.IsAny<ProductCreated>(), CancellationToken.None))
                .Returns((ProductCreated q, CancellationToken t) => MediatR.Unit.Task);

            prodactRepository
                .Setup(x => x.Create(It.IsAny<Domain.Entities.Product>(), CancellationToken.None))
                .Returns((Domain.Entities.Product e, CancellationToken t) => Task.FromResult(Guid.NewGuid()));

            licenseRepository
                .Setup(x => x.Create(It.IsAny<Domain.Entities.LicensePeriod>(), CancellationToken.None))
                .Returns((Domain.Entities.LicensePeriod e, CancellationToken t) => Task.FromResult(byte.MinValue));


            var prodHandler = new UpdateProductHandler(mediator.Object, prodactRepository.Object, licenseRepository.Object, mapper.Object);
            var cmd = new UpdateProductCmd("Test product", 100, 1);


            //Act
            var id = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(id);
        }

    }
}
