﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Data.Read.Tenant.ReadModels;
using Irms.Data.Read.Tenant.Queries;
using Irms.Data.Read.Abstract;
using Irms.WebApi.AdminControllers;
using Irms.Data.Read;
using Irms.Application.Tenants.Commands;

namespace Irms.Tests.Unit.Controller.AdminControllers
{
    [TestClass]
    public class TenantControllerTests
    {
        /// <summary>
        /// get tenant list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetTenantList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var items = new[]
            {
                new TenantListItem()
                {
                    Id = id,
                    Name = "test",
                    Address = "Al khobar KSA",
                    Email = "takamul@ksa.com"
                },
                new TenantListItem()
                {
                    Id = Guid.NewGuid(),
                    Name = "tenant 2",
                    Address = "Al khobar KSA",
                    Email = "takamul@ksa.com"
                },
            };

            IPageResult<TenantListItem> tenantListItems = new PageResult<TenantListItem>(items, 2);

            mediator
                .Setup(x => x.Send(It.IsAny<TenantListQuery>(), CancellationToken.None))
                    .Returns((TenantListQuery q, CancellationToken t) => Task.FromResult(tenantListItems));

            var sut = new TenantController(mediator.Object);

            //Act
            var result = await sut.GetTenantList(
                new TenantListQuery(1, 10, TimeFilterRange.AllTime, string.Empty),
                CancellationToken.None);

            //Assert
            //Assert
            Assert.IsNotNull(result);

            var tenant = result.Items.First();

            Assert.AreEqual(result.TotalCount, 2);
            Assert.AreEqual(tenant.Id, id);
            Assert.AreEqual(tenant.Name, "test");
            Assert.AreEqual(tenant.Address, "Al khobar KSA");
            Assert.AreEqual(tenant.Email, "takamul@ksa.com");
            Assert.AreEqual(result.Items.Skip(1).First().Name, "tenant 2");

        }

        /// <summary>
        /// check email uniqueness
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CheckEmailUniqueness()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            bool isExist = true;

            mediator
                .Setup(x => x.Send(It.IsAny<CheckContactInfoEmailUniqueness>(), CancellationToken.None))
                    .Returns((CheckContactInfoEmailUniqueness q, CancellationToken t) => Task.FromResult(isExist));

            var sut = new TenantController(mediator.Object);

            //Act
            var result = await sut.CheckEmailUniqueness(
                new CheckContactInfoEmailUniqueness() { Email = "saif.ciit@gmail.com" },
                CancellationToken.None);

            //Assert
            //Assert
            Assert.IsNotNull(result);

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// get tenant basic details
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetTenantDetails()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var tenantDetails = new TenantDetails
            {
                Id = id,
                Name = "tenant one",
                Description = "tenant desc",
                IsActive = true,
                Address = "Al khobar"
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetTenantDetails>(), CancellationToken.None))
                .Returns((GetTenantDetails q, CancellationToken t) => Task.FromResult(tenantDetails));

            var sut = new TenantController(mediator.Object);

            //Act
            var result = await sut.Get(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Name, "tenant one");
            Assert.AreEqual(result.IsActive, true);
            Assert.AreEqual(result.Address, "Al khobar");
        }

        /// <summary>
        /// get tenant contact info
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetTenantContactInfo()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var contactInfo = new TenantContactInfo
            {
                Id = id,
                FirstName = "saif",
                LastName = "rehman",
                Email = "saif.ciit@gmail.com",
                MobileNo = "+971502630996",
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetTenantContactInfo>(), CancellationToken.None))
                .Returns((GetTenantContactInfo q, CancellationToken t) => Task.FromResult(contactInfo));

            var sut = new TenantController(mediator.Object);

            //Act
            var result = await sut.GetTenantContactInfo(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.FirstName, "saif");
            Assert.AreEqual(result.LastName, "rehman");
            Assert.AreEqual(result.Email, "saif.ciit@gmail.com");
            Assert.AreEqual(result.MobileNo, "+971502630996");
        }

        /// <summary>
        /// get tenant configurations
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetTenantConfig()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var configInfo = new TenantConfigInfo
            {
                Id = id,
                HasOwnDomain = false,
                ClientUrl = "https://ithra.takamulstg.com",
                FirstName = "saif",
                LastName = "rehman",
                Email = "saif.ciit@gmail.com",
                MobileNo = "+971502630996",
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetTenantConfigInfo>(), CancellationToken.None))
                .Returns((GetTenantConfigInfo q, CancellationToken t) => Task.FromResult(configInfo));

            var sut = new TenantController(mediator.Object);

            //Act
            var result = await sut.GetTenantConfigInfo(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.HasOwnDomain, false);
            Assert.AreEqual(result.ClientUrl, "https://ithra.takamulstg.com");
            Assert.AreEqual(result.FirstName, "saif");
            Assert.AreEqual(result.LastName, "rehman");
            Assert.AreEqual(result.Email, "saif.ciit@gmail.com");
            Assert.AreEqual(result.MobileNo, "+971502630996");
        }
        [TestMethod]
        public async Task CanCreateTenant()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateTenantCmd>(), CancellationToken.None))
                .Returns((CreateTenantCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new TenantController(mediator.Object);

            //Act
            var apiResult = await sut.Create(
                new CreateTenantCmd
                {
                    Name = "tenant one",
                    Description = "tenant desc",
                    City = "Al khobar",
                    Address = "Al khobar"
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
        }

        /// <summary>
        /// update tenant basic info
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateTenant()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<UpdateTenantCmd>(), CancellationToken.None))
                .Returns((UpdateTenantCmd q, CancellationToken t) => MediatR.Unit.Task);

            var sut = new TenantController(mediator.Object);

            //Act
            await sut.Update(
                new UpdateTenantCmd
                {
                    Name = "tenant one",
                    Description = "tenant desc",
                    City = "Al khobar",
                    Address = "Al khobar"
                },
                CancellationToken.None);
        }

        /// <summary>
        /// update tenant contactinfo
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateTenantContactInfo()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<UpdateContactInfoCmd>(), CancellationToken.None))
                .Returns((UpdateContactInfoCmd q, CancellationToken t) => MediatR.Unit.Task);

            var sut = new TenantController(mediator.Object);

            //Act
            await sut.UpdateContactInfo(
                new UpdateContactInfoCmd
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Saif",
                    LastName = "ur Rehman",
                    Email = "test@email.com",
                    MobileNo = "+966554154875",
                    PhoneNo = "+966554154875"
                },
                CancellationToken.None);
        }


        /// <summary>
        /// update tenant config
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateTenantConfig()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<UpdateConfigCmd>(), CancellationToken.None))
                .Returns((UpdateConfigCmd q, CancellationToken t) => MediatR.Unit.Task);

            var sut = new TenantController(mediator.Object);

            //Act
            await sut.UpdateConfig(
                new UpdateConfigCmd
                {
                    Id = Guid.NewGuid(),
                    ClientUrl = "https://ithra.takamulstg.com",
                    FirstName = "Saif",
                    LastName = "ur Rehman",
                    Email = "test@email.com",
                    MobileNo = "+966554154875",
                },
                CancellationToken.None);
        }

        /// <summary>
        /// can get tenant logo for tenant/client portal
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetTenantLogo()
        {
            //Arrange
            var mediator = new Mock<IMediator>();

            var logo = new TenantLogo(
                "https://irms-storage-cdn.azureedge.net/files-container/customers/25d71a94-26b6-4659-999e-be9a7a4c146e_logo.png"
            );

            mediator
                .Setup(x => x.Send(It.IsAny<GetTenantLogo>(), CancellationToken.None))
                .Returns((GetTenantLogo q, CancellationToken t) => Task.FromResult(logo));

            var sut = new TenantController(mediator.Object);

            //Act
            var result = await sut.GetTenantLogo(
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.LogoPath);
        }
    }
}
