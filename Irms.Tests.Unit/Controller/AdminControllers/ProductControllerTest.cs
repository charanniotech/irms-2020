﻿using Irms.Application.Product.Commands;
using Irms.Application.ProductServices.Commands;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Product.Queries;
using Irms.Data.Read.Product.ReadModels;
using Irms.Data.Read.Service.Queries;
using Irms.Data.Read.Service.ReadModels;
using Irms.Domain.Entities;
using Irms.Tests.Unit.ValidData;
using Irms.WebApi.AdminControllers;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.Controller.SettingsControllers
{
    [TestClass]
    public class ProductControllerTest
    {
        [TestMethod]
        public async Task CanCreateProduct()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateProductCmd>(), CancellationToken.None))
                .Returns((CreateProductCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new ProductController(mediator.Object);

            //Act
            var apiResult = await sut.Create(
                CreateProductRequestObject(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
        }

        [TestMethod]
        public async Task CanUpdateProduct()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateProductCmd>(), CancellationToken.None))
                .Returns((CreateProductCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new ProductController(mediator.Object);
            var product = UpdateProductRequestObject(id);

            //Act
            var apiResult = await sut.UpdateProduct(
                product,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
        }
        [TestMethod]
        public async Task CanGetProductsByIds()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            List<Product> products = new List<Product>();
            for (int i = 0; i < 2; i++)
            {
                products.Add(new EntitiesValidData().DomainProductEntityValidData());
            }
            IEnumerable<Product> response = products;

            mediator
                .Setup(x => x.Send(It.IsAny<GetProductsByIdsCmd>(), CancellationToken.None))
                .Returns((GetProductsByIdsCmd q, CancellationToken t) => Task.FromResult(response));

            var sut = new ProductController(mediator.Object);
            var ids = products.Select(p => p.Id);
            var request = new GetProductsByIdsCmd(ids);

            //Act
            var apiResult = await sut.GetProductsByIds(
                request,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response, apiResult);
        }
        [TestMethod]
        public async Task CanGetProductById()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var response = new EntitiesValidData().DomainProductEntityValidData();

            mediator
                .Setup(x => x.Send(It.IsAny<GetProductByIdCmd>(), CancellationToken.None))
                .Returns((GetProductByIdCmd q, CancellationToken t) => Task.FromResult(response));

            var sut = new ProductController(mediator.Object);

            //Act
            var apiResult = await sut.GetProductById(
                response.Id,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response, apiResult);
        }

        [TestMethod]
        public async Task CanGetProductList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var request = new GetProductListQuery(1, 10, "", Data.Read.TimeFilterRange.AllTime);

            List<ProductListItem> items = new List<ProductListItem>();
            for (int i = 0; i < 2; i++)
            {
                items.Add(new ProductListItem { Id = Guid.NewGuid() });
            }
            IPageResult<ProductListItem> response = new PageResult<ProductListItem>(items, items.Count);


            mediator
                .Setup(x => x.Send(It.IsAny<GetProductListQuery>(), CancellationToken.None))
                .Returns((GetProductListQuery q, CancellationToken t) => Task.FromResult(response));

            var sut = new ProductController(mediator.Object);
            //Act
            var apiResult = await sut.GetProductList(
                request,
                CancellationToken.None);


            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response, apiResult);

        }
        [TestMethod]
        public async Task CanDeleteProductById()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var request = new DeleteProductByIdCmd(Guid.NewGuid());
            var response = new MediatR.Unit();

            mediator
                .Setup(x => x.Send(It.IsAny<DeleteProductByIdCmd>(), CancellationToken.None))
                .Returns((DeleteProductByIdCmd q, CancellationToken t) => Task.FromResult(response));

            var sut = new ProductController(mediator.Object);
            //Act
            var apiResult = await sut.DeleteProductById(
                request,
                CancellationToken.None);


            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response, apiResult);
        }
        [TestMethod]
        public async Task CanCreateService()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var request = new CreateServiceCmd("Test Service", true);
            var response = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateServiceCmd>(), CancellationToken.None))
                .Returns((CreateServiceCmd q, CancellationToken t) => Task.FromResult(response));

            var sut = new ProductController(mediator.Object);
            //Act
            var apiResult = await sut.CreateService(
                request,
                CancellationToken.None);


            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response, apiResult);
        }
        [TestMethod]
        public async Task CanGetAllAvailableServices()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var request = new GetAvailableServicesCmd();

            List<ServicesList> services = new List<ServicesList>();
            for (int i = 0; i < 2; i++)
            {
                services.Add(new ServicesList
                {
                    Id = Guid.NewGuid(),
                    IsActive = true,
                    Title = Guid.NewGuid().ToString(),
                    ServiceFeatures = GetServiceFeatureList()
                });
            }

            IEnumerable<ServicesList> response = services;

            mediator
                .Setup(x => x.Send(It.IsAny<GetAvailableServicesCmd>(), CancellationToken.None))
                .Returns((GetAvailableServicesCmd q, CancellationToken t) => Task.FromResult(response));

            var sut = new ProductController(mediator.Object);
            //Act
            var apiResult = await sut.GetAvailableServices(
                request,
                CancellationToken.None);


            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response, apiResult);
        }
        [TestMethod]
        public async Task CanGetAvailableProductServices()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var request = Guid.NewGuid();

            List<ServicesList> services = new List<ServicesList>();
            for (int i = 0; i < 2; i++)
            {
                services.Add(new ServicesList
                {
                    Id = Guid.NewGuid(),
                    IsActive = true,
                    Title = Guid.NewGuid().ToString(),
                    ServiceFeatures = GetServiceFeatureList()
                });
            }

            IEnumerable<ServicesList> response = services;

            mediator
                .Setup(x => x.Send(It.IsAny<GetProductServicesCmd>(), CancellationToken.None))
                .Returns((GetProductServicesCmd q, CancellationToken t) => Task.FromResult(response));

            var sut = new ProductController(mediator.Object);
            //Act
            var apiResult = await sut.GetAvailableProductServices(
                request,
                CancellationToken.None);


            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response, apiResult);
        }

        [TestMethod]
        public async Task CanCreateServiceFeature()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var request = new CreateServiceFeatureCmd(Guid.NewGuid());
            request.ServiceFeature = new ServiceFeature(Guid.NewGuid(), "Service Feature Title", true);

            mediator
                .Setup(x => x.Send(It.IsAny<CreateServiceFeatureCmd>(), CancellationToken.None))
                .Returns((CreateServiceFeatureCmd q, CancellationToken t) => Task.FromResult(request.ServiceFeature.FeatureId));

            var sut = new ProductController(mediator.Object);
            //Act
            var apiResult = await sut.CreateServiceFeature(
                request,
                CancellationToken.None);


            Assert.IsNotNull(apiResult);
            Assert.AreEqual(request.ServiceFeature.FeatureId, apiResult);

        }



        private CreateProductCmd CreateProductRequestObject()
        {
            return new CreateProductCmd
            {
                ProductName = "Test product name",
                Price = 100,
                CurrencyId = 1,
                DisountPercentage = 20,
                LicenseDays = 30,
                LicenseName = "20% off",
                Services = GetProductServiceList()
            };
        }
        private UpdateProductCmd UpdateProductRequestObject(Guid productId)
        {
            return new UpdateProductCmd
            {
                Id = productId,
                ProductName = "Test product name",
                Price = 100,
                CurrencyId = 1,
                DisountPercentage = 20,
                LicenseDays = 30,
                LicenseName = "20% off",
                Services = GetProductServiceList()
            };
        }

        private List<ProductService> GetProductServiceList()
        {
            var psList = new List<ProductService>();
            var ps = new ProductService
            {
                IsActive = true,
                Title = "Test product service",
                ServiceFeatures = GetServiceFeatureList()
            };
            psList.Add(ps);
            return psList;
        }

        private List<ServiceFeature> GetServiceFeatureList()
        {
            var sfList = new List<ServiceFeature>();
            var sf = new ServiceFeature
            {
                FeatureTitle = "Test Feature Title",
                IsFeatureEnabled = true
            };
            sfList.Add(sf);
            return sfList;
        }
    }
}
