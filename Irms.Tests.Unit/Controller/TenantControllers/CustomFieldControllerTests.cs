﻿using Irms.Application.CustomFields.Commands;
using Irms.Data.Read.CustomField.Queries;
using Irms.Data.Read.CustomField.ReadModels;
using Irms.Domain;
using Irms.Domain.Entities;
using Irms.WebApi.TenantControllers;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.Controller.TenantControllers
{
    /// <summary>
    /// Contains tests for the controller
    /// </summary>
    [TestClass]
    public class CustomFieldControllerTests
    {
        /// <summary>
        /// Tests create command sending
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateTest()
        {
            // Arrange
            var mediator = new Mock<IMediator>();

            var guid = Guid.NewGuid();

            mediator.Setup(x => x.Send(It.IsAny<AddCustomFieldCmd>(), CancellationToken.None))
                .Returns((AddCustomFieldCmd q, CancellationToken t) => Task.FromResult(guid));

            // Act
            var ctr = new CustomFieldController(mediator.Object);
            var result = await ctr.Create(new AddCustomFieldCmd
            {
                CustomFieldType = Domain.Entities.CustomFieldType.Text,
                Name = "123"
            }, CancellationToken.None);


            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result, guid);
        }

        /// <summary>
        /// Tests update command sending
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateTest()
        {
            // Arrange
            var mediator = new Mock<IMediator>();

            var guid = Guid.NewGuid();

            mediator.Setup(x => x.Send(It.IsAny<UpdateCustomFieldCmd>(), CancellationToken.None))
                .Returns((UpdateCustomFieldCmd q, CancellationToken t) => Task.FromResult(guid));

            // Act
            var ctr = new CustomFieldController(mediator.Object);
            var result = await ctr.Update(new UpdateCustomFieldCmd
            {
                CustomFieldType = Domain.Entities.CustomFieldType.Text,
                Name = "123"
            }, CancellationToken.None);


            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result, guid);
        }


        /// <summary>
        /// Test for getting all custom fields by custom field 
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetAllTest()
        {
            // Arrange
            var guid = Guid.NewGuid();
            IEnumerable<CustomFieldReadModel> data = new List<CustomFieldReadModel> { new CustomFieldReadModel { CustomFieldType = 0, Name = "123", Id = guid } };

            var mediator = new Mock<IMediator>();
            mediator.Setup(x => x.Send(It.IsAny<GetCustomFieldsQuery>(), CancellationToken.None))
                .Returns((GetCustomFieldsQuery q, CancellationToken t) => Task.FromResult(data));
            
            // Act
            var ctr = new CustomFieldController(mediator.Object);
            var result = await ctr.GetAll(
                new GetCustomFieldsQuery
                {
                    Fields = Guid.NewGuid().Enumerate()
                }, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(guid, result.First().Id);
            Assert.AreEqual("123", result.First().Name);
            Assert.AreEqual(0, result.First().CustomFieldType);
        }
    }
}
