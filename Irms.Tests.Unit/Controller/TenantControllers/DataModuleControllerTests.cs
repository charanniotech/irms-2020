﻿using Irms.Application.Abstract.Services;
using Irms.Application.DataModule.Commands;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using Irms.Infrastructure.Services.ExcelExport;
using Irms.WebApi.TenantControllers;
using MediatR;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using OfficeOpenXml.ConditionalFormatting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Twilio.TwiML.Messaging;

namespace Irms.Tests.Unit.Controller.TenantControllers
{
    /// <summary>
    /// Test class for data module controller tests
    /// </summary>
    [TestClass]
    public class DataModuleControllerTests
    {
        /// <summary>
        /// Test method for can get campaign list, receives page model for campaign list on entry, checks result to be campaign list page
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();

            var res = new PageResult<CampaignDataReadModel>(new List<CampaignDataReadModel>
            {
                new CampaignDataReadModel
                {
                 LastUpdate = DateTime.UtcNow,
                 Name = "123",
                 Status = "123"
                }
            }, 10);

            mediator.Setup(x => x.Send(It.IsAny<GetCampaignDataQuery>(), CancellationToken.None))
                .Returns((GetCampaignDataQuery q, CancellationToken t) => Task.FromResult((IPageResult<CampaignDataReadModel>)res));

            //Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.GetCampaignList(Guid.NewGuid(), CancellationToken.None);


            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(res.TotalCount, result.TotalCount);
            Assert.AreEqual(res.Items.First().Name, result.Items.First().Name);
            Assert.AreEqual(res.Items.First().Status, result.Items.First().Status);
            Assert.AreEqual(res.Items.First().LastUpdate, result.Items.First().LastUpdate);

        }

        /// <summary>
        /// Test for getting rfi form summary name, receives command with rfi form info on entry and checks rfi form info page after
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetRfiFormInfo()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var res = new PageResult<RfiFormInfoReadModel>(new List<RfiFormInfoReadModel>
            {
                new RfiFormInfoReadModel
                {
                    Id = Guid.NewGuid(),
                    Responses = 5,
                    Target = "Accepted Guests",
                    Title = "Title",
                    TotalVisits = 1,
                    UniqueVisits = 2
                }
            }, 10);

            mediator.Setup(x => x.Send(It.IsAny<GetRfiFormInfoQuery>(), CancellationToken.None))
                .Returns((GetRfiFormInfoQuery q, CancellationToken t) => Task.FromResult((IPageResult<RfiFormInfoReadModel>)res));

            //Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.GetRfiFormInfo(new GetRfiFormInfoQuery(Guid.NewGuid(), 1, 10), CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(res.TotalCount, result.TotalCount);
            Assert.AreEqual(res.Items.First().Id, result.Items.First().Id);
            Assert.AreEqual(res.Items.First().Responses, result.Items.First().Responses);
            Assert.AreEqual(res.Items.First().Target, result.Items.First().Target);
            Assert.AreEqual(res.Items.First().Title, result.Items.First().Title);
            Assert.AreEqual(res.Items.First().TotalVisits, result.Items.First().TotalVisits);
            Assert.AreEqual(res.Items.First().UniqueVisits, result.Items.First().UniqueVisits);
        }

        /// <summary>
        /// unit test to get list analysis forms
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetListAnalysisRfiFormInfo()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var res = new PageResult<ListAnalysisRfiFormInfoReadModel>(new List<ListAnalysisRfiFormInfoReadModel>
            {
                new ListAnalysisRfiFormInfoReadModel
                {
                    Id = Guid.NewGuid(),
                    Responses = 5,
                    ListName = "Accepted Guests List",
                    Title = "Title",
                    TotalVisits = 1,
                    UniqueVisits = 2
                }
            }, 10);

            mediator.Setup(x => x.Send(It.IsAny<GetListAnalysisRfiFormInfoQuery>(), CancellationToken.None))
                .Returns((GetListAnalysisRfiFormInfoQuery q, CancellationToken t) => Task.FromResult((IPageResult<ListAnalysisRfiFormInfoReadModel>)res));

            //Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.GetListAnalysisRfiFormInfo(new GetListAnalysisRfiFormInfoQuery(Guid.NewGuid(), 1, 10), CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(res.TotalCount, result.TotalCount);
            Assert.AreEqual(res.Items.First().Id, result.Items.First().Id);
            Assert.AreEqual(res.Items.First().Responses, result.Items.First().Responses);
            Assert.AreEqual(res.Items.First().ListName, result.Items.First().ListName);
            Assert.AreEqual(res.Items.First().Title, result.Items.First().Title);
            Assert.AreEqual(res.Items.First().TotalVisits, result.Items.First().TotalVisits);
            Assert.AreEqual(res.Items.First().UniqueVisits, result.Items.First().UniqueVisits);
        }

        /// <summary>
        /// Test to get rfi form name, receives rfi form id on entry and checks name for rfi form as a result
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetRfiFormName()
        {
            //Arrange
            var mediator = new Mock<IMediator>();

            var data = new RfiFormNameReadModel("123", 35);

            mediator.Setup(x => x.Send(It.IsAny<GetRfiFormNameQuery>(), CancellationToken.None))
                .Returns((GetRfiFormNameQuery q, CancellationToken t) => Task.FromResult(data));

            //Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.GetRfiFormName(new GetRfiFormNameQuery(Guid.NewGuid()), CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(data, result);
        }

        /// <summary>
        /// Test for getting rfi form summary name, receives command with rfi form id on entry and returns rfi form summary name
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetRfiFormSummaryName()
        {
            //Arrange
            var mediator = new Mock<IMediator>();

            var res = new RfiFormSummaryReadModel
            {
                Id = Guid.NewGuid(),
                Questions = new List<RfiFormSummaryReadModel.RfiFormSummaryQuestionReadModel>
             {
                 new RfiFormSummaryReadModel.RfiFormSummaryQuestionReadModel
                 {
                     Id = Guid.NewGuid(),
                     AnsweredCount = 12,
                     MappedField = "mappedField",
                     Question = "question",
                     QuestionMappingType = 1,
                     Responses = "",
                     SortOrder = 0
                 }
             },
                ResponseCount = 12
            };

            mediator.Setup(x => x.Send(It.IsAny<GetRfiFormSummaryQuery>(), CancellationToken.None))
                .Returns((GetRfiFormSummaryQuery q, CancellationToken t) => Task.FromResult(res));

            //Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.GetRfiFormSummary(Guid.NewGuid(), CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(res.ResponseCount, result.ResponseCount);
            Assert.AreEqual(res.Id, result.Id);
            Assert.AreEqual(res.Questions.First().Responses, result.Questions.First().Responses);
            Assert.AreEqual(res.Questions.First().Id, result.Questions.First().Id);
            Assert.AreEqual(res.Questions.First().MappedField, result.Questions.First().MappedField);
            Assert.AreEqual(res.Questions.First().Question, result.Questions.First().Question);
            Assert.AreEqual(res.Questions.First().QuestionMappingType, result.Questions.First().QuestionMappingType);
            Assert.AreEqual(res.Questions.First().SortOrder, result.Questions.First().SortOrder);
        }

        /// <summary>
        /// Test for getting rfi form additional information, receives command with page info on entry and returns rfi form summary additional info
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetRfiFormAdditionalInfo()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var res = new PageResult<RfiFormSummaryAdditionalInfo>(new List<RfiFormSummaryAdditionalInfo>
            {
                new RfiFormSummaryAdditionalInfo
                {
                    Id = Guid.NewGuid(),
                    Response = "123"
                }
            }, 10);

            mediator.Setup(x => x.Send(It.IsAny<GetRfiFormSummaryAdditionalInfoQuery>(), CancellationToken.None))
                .Returns((GetRfiFormSummaryAdditionalInfoQuery q, CancellationToken t) => Task.FromResult((IPageResult<RfiFormSummaryAdditionalInfo>)res));

            //Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.GetRfiFormAdditionalInfo(new GetRfiFormSummaryAdditionalInfoQuery(Guid.NewGuid(), Guid.NewGuid(), 1, 10, ""), CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(res.TotalCount, result.TotalCount);
            Assert.AreEqual(res.Items.First().Id, result.Items.First().Id);
            Assert.AreEqual(res.Items.First().Response, result.Items.First().Response);
        }

        /// <summary>
        /// Test for getting rfi form response, retrieves command with page ifno for rfi form and returns responses in guest name and submission date etries only
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetRfiFormResponses()
        {
            // Arrange
            var mediator = new Mock<IMediator>();
            var res = new PageResult<RfiFormResponseReadModel>(
                new List<RfiFormResponseReadModel>
                {
                    new RfiFormResponseReadModel
                    {
                     GuestName = "123",
                     SubmissionDate = DateTime.UtcNow
                    }
                }
            , 10);

            mediator.Setup(x => x.Send(It.IsAny<GetRfiFormResponseQuery>(), CancellationToken.None))
                .Returns((GetRfiFormResponseQuery q, CancellationToken t) => Task.FromResult((IPageResult<RfiFormResponseReadModel>)res));

            // Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.GetRfiFormResponse(new GetRfiFormResponseQuery(Guid.NewGuid(), 1, 10, ""), CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(res.TotalCount, result.TotalCount);
            Assert.AreEqual(res.Items.First().GuestName, result.Items.First().GuestName);
            Assert.AreEqual(res.Items.First().SubmissionDate, result.Items.First().SubmissionDate);
        }

        /// <summary>
        /// Test for getting rfi form response, retrieves command with page ifno for rfi form and returns responses in guest name and submission date etries only
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetRfiFormQuestions()
        {
            // Arrange
            var mediator = new Mock<IMediator>();
            IEnumerable<RfiFormQuestionsModel> res = new List<RfiFormQuestionsModel>
            {
                new RfiFormQuestionsModel
                {
                 Id = Guid.NewGuid(),
                 Question = "123",
                 SortOrder = 1
                }
            };

            mediator.Setup(x => x.Send(It.IsAny<GetRfiFormQuestionQuery>(), CancellationToken.None))
                .Returns((GetRfiFormQuestionQuery q, CancellationToken t) => Task.FromResult(res));

            // Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.GetRfiFormQuestions(Guid.NewGuid(), CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, res.Count());
            Assert.AreEqual(res.First().Id, result.First().Id);
            Assert.AreEqual(res.First().Question, result.First().Question);
            Assert.AreEqual(res.First().SortOrder, result.First().SortOrder);
        }

        /// <summary>
        /// Exports rfi form data in an excel format
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanExportRfiFormResponses()
        {
            // Arrange
            var mediator = new Mock<IMediator>();
            var excel = new Mock<IExcelGenerator>();
            Stream memoryStream = new MemoryStream(new byte[2] { 0, 1 });

            IEnumerable<RfiFormQuestionsModel> questions = new List<RfiFormQuestionsModel>
            {
                new RfiFormQuestionsModel
                {
                 Id = Guid.NewGuid(),
                 Question = "23",
                 SortOrder = 1
                }
            };

            IEnumerable<RfiFormAnswersExportModel> answers = new List<RfiFormAnswersExportModel>
            {
                new RfiFormAnswersExportModel
                {
                 SortOrder = 1,
                 Id = Guid.NewGuid(),
                 Answer = "123",
                 ContactId = Guid.NewGuid(),
                 GuestName = "123",
                 QuestionId = Guid.NewGuid(),
                 SubmissionDate = "date"
                }
            };

            mediator.Setup(x => x.Send(It.IsAny<GetRfiFormQuestionQuery>(), CancellationToken.None))
                .Returns((GetRfiFormQuestionQuery q, CancellationToken t) => Task.FromResult(questions));

            mediator.Setup(x => x.Send(It.IsAny<GetRfiFormExportDataQuery>(), CancellationToken.None))
                .Returns((GetRfiFormExportDataQuery q, CancellationToken t) => Task.FromResult(answers));

            excel.Setup(x => x.GenerateRfiExportReport<RfiFormQuestionsModel, RfiFormAnswersExportModel>(It.IsAny<List<RfiFormQuestionsModel>>(), It.IsAny<List<RfiFormAnswersExportModel>>(), It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()));

            // Act
            var ctr = new DataModuleController(mediator.Object, excel.Object);
            var result = await ctr.ExportRfiFormQuestions(new GetRfiFormExportDataQuery(), CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test for getting rfi form response, retrieves command with page ifno for rfi form and returns responses in guest name and submission date etries only
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetRfiFormGuestResponse()
        {
            // Arrange
            var mediator = new Mock<IMediator>();
            var res = new GuestResponseReadModel
            {
                GuestName = "guest name",
                SubmittedDate = DateTime.UtcNow,
                ResponseData = new List<GuestResponseReadModel.GuestResponseData>
                {
                    new GuestResponseReadModel.GuestResponseData
                    {
                        Id = Guid.NewGuid(),
                        MappedField = "field",
                        Question = "question",
                        QuestionMappedType = 0,
                        QuestionResponse = "response",
                        SortOrder = 0
                    }
                }
            };

            mediator.Setup(x => x.Send(It.IsAny<GetRfiFormResponseDetailPageQuery>(), CancellationToken.None))
                .Returns((GetRfiFormResponseDetailPageQuery q, CancellationToken t) => Task.FromResult(res));

            // Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.GetGuestResponse(new GetRfiFormResponseDetailPageQuery(Guid.NewGuid(), Guid.NewGuid()), CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.ResponseData.Count());
            Assert.AreEqual(res.ResponseData.First().Id, result.ResponseData.First().Id);
            Assert.AreEqual(res.ResponseData.First().MappedField, result.ResponseData.First().MappedField);
            Assert.AreEqual(res.ResponseData.First().Question, result.ResponseData.First().Question);
            Assert.AreEqual(res.ResponseData.First().QuestionMappedType, result.ResponseData.First().QuestionMappedType);
            Assert.AreEqual(res.ResponseData.First().QuestionResponse, result.ResponseData.First().QuestionResponse);
            Assert.AreEqual(res.ResponseData.First().SortOrder, result.ResponseData.First().SortOrder);
        }

        /// <summary>
        /// Test for removing empty guest list from global contact
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteEmptyContactList()
        {
            // Arrange
            var mediator = new Mock<IMediator>();

            mediator.Setup(x => x.Send(It.IsAny<DeleteContactListCmd>(), CancellationToken.None))
                .Returns((DeleteContactListCmd q, CancellationToken t) => Task.FromResult(MediatR.Unit.Value));

            // Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.RemoveContactList(Guid.NewGuid(), CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(MediatR.Unit.Value, result);
        }

        /// <summary>
        /// Test for getting total global guests count
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetGlobalGuestsCount()
        {
            // Arrange
            var mediator = new Mock<IMediator>();

            var res = 1;

            mediator.Setup(x => x.Send(It.IsAny<GetGlobalGuestsCountQuery>(), CancellationToken.None))
                .Returns((GetGlobalGuestsCountQuery q, CancellationToken t) => Task.FromResult(res));

            // Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.GetGlobalGuestsCount(CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(res, result);
        }

        /// <summary>
        /// Test for getting global guests
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetGlobalGoests()
        {
            // Arrange
            var mediator = new Mock<IMediator>();

            var res = new PageResult<GlobalGuestListReadModel>(
                new List<GlobalGuestListReadModel>
                {
                    new GlobalGuestListReadModel
                    {
                        Campaign = "campaign",
                        Count = "1",
                        Event = "event",
                        Id = Guid.NewGuid(),
                        LastUpdatedBy = "person",
                        Name = "list name",
                        UpdatedDate = DateTime.UtcNow
                    }
                },
                1
            );

            mediator.Setup(x => x.Send(It.IsAny<GetGlobalGuestListQuery>(), CancellationToken.None))
                .Returns((GetGlobalGuestListQuery q, CancellationToken t) => Task.FromResult((IPageResult<GlobalGuestListReadModel>)res));

            // Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.GetGlobalGuests(new GetGlobalGuestListQuery(1, 5, ""), CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(res, result);
            Assert.AreEqual(res.TotalCount, result.TotalCount);
            Assert.AreEqual(res.Items.Count(), result.Items.Count());
            Assert.AreEqual(res.Items.First().Campaign, result.Items.First().Campaign);
            Assert.AreEqual(res.Items.First().Count, result.Items.First().Count);
            Assert.AreEqual(res.Items.First().Event, result.Items.First().Event);
            Assert.AreEqual(res.Items.First().Id, result.Items.First().Id);
            Assert.AreEqual(res.Items.First().LastUpdatedBy, result.Items.First().LastUpdatedBy);
            Assert.AreEqual(res.Items.First().Name, result.Items.First().Name);
            Assert.AreEqual(res.Items.First().UpdatedDate, result.Items.First().UpdatedDate);
        }

        /// <summary>
        /// Test for getting global contacts in guest list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetGlobalGuestList()
        {
            // Arrange
            var mediator = new Mock<IMediator>();

            var res = new PageResult<GlobalGuestContactReadModel>(
                new List<GlobalGuestContactReadModel>
                {
                    new GlobalGuestContactReadModel
                    {
                        Email = "email@email.com",
                        GuestName = "guest_name",
                        MobileNumber = "9023815215039823",
                        Status = "Accepted"
                    }
                },
                1
            );

            mediator.Setup(x => x.Send(It.IsAny<GetGlobalGuestContactQuery>(), CancellationToken.None))
                .Returns((GetGlobalGuestContactQuery q, CancellationToken t) => Task.FromResult((IPageResult<GlobalGuestContactReadModel>)res));

            // Act
            var ctr = new DataModuleController(mediator.Object, null);
            var result = await ctr.GetGlobalGuestList(new GetGlobalGuestContactQuery(1, 5, "", Guid.NewGuid()), CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(res, result);
            Assert.AreEqual(res.TotalCount, result.TotalCount);
            Assert.AreEqual(res.Items.Count(), result.Items.Count());
            Assert.AreEqual(res.Items.First().Email, result.Items.First().Email);
            Assert.AreEqual(res.Items.First().GuestName, result.Items.First().GuestName);
            Assert.AreEqual(res.Items.First().MobileNumber, result.Items.First().MobileNumber);
            Assert.AreEqual(res.Items.First().Status, result.Items.First().Status);
        }
    }
}
