﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.WebApi.TenantControllers;
using System;
using Irms.Application.RfiForms.Commands;
using Irms.Data.Read.RfiForm.Queries;

namespace Irms.Tests.Unit.Controller.AdminControllers
{
    [TestClass]
    public class RfiFormControllerTests
    {

        private Mock<IMediator> _mediator;

        [TestInitialize]
        public void Init()
        {
            _mediator = new Mock<IMediator>();
        }

        /// <summary>
        /// unit test to RFI form
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetRfiForm()
        {
            //Arrange
            var id = Guid.NewGuid();

            var rfi = new Data.Read.RfiForm.ReadModels.RfiForm
            {
                Id = id,
                CampaignInvitationId = Guid.NewGuid(),
                WelcomeHtml = "<p>welcome...</p>",
                SubmitHtml = "<button>save</button>",
                ThanksHtml = "<p>thanks...</p>",
                FormTheme = "<p>form theme...</button>",
                FormSettings = "<p>form settings</p>",
                ThemeBackgroundImagePath = "abc.png"
            };

            _mediator
                .Setup(x => x.Send(It.IsAny<GetRfiFormQry>(), CancellationToken.None))
                    .Returns((GetRfiFormQry q, CancellationToken t) => Task.FromResult(rfi));

            var sut = new RfiFormController(_mediator.Object);

            //Act
            var result = await sut.GetRfiForm(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.IsNotNull(result.CampaignInvitationId);
            Assert.AreEqual(result.WelcomeHtml, "<p>welcome...</p>");
            Assert.AreEqual(result.SubmitHtml, "<button>save</button>");
            Assert.AreEqual(result.ThanksHtml, "<p>thanks...</p>");
            Assert.AreEqual(result.FormTheme, "<p>form theme...</button>");
            Assert.AreEqual(result.FormSettings, "<p>form settings</p>");
            Assert.AreEqual(result.ThemeBackgroundImagePath, "abc.png");
        }

        /// <summary>
        /// unit test for create/update campaign RFI form
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpsertRfiForm()
        {
            //Arrange
            var id = Guid.NewGuid();

            _mediator
                .Setup(x => x.Send(It.IsAny<UpsertRfiFormCmd>(), CancellationToken.None))
                .Returns((UpsertRfiFormCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new RfiFormController(_mediator.Object);

            //Act
            var apiResult = await sut.UpsertRfiForm(
                new UpsertRfiFormCmd
                {
                    Id = id,
                    CampaignInvitationId = Guid.NewGuid(),
                    WelcomeHtml = "<p>welcome...</p>",
                    SubmitHtml = "<button>save</button>",
                    ThanksHtml = "<p>thanks...</p>",
                    FormTheme = "<p>form theme...</button>",
                    FormSettings = "<p>form settings</p>",
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }
    }
}