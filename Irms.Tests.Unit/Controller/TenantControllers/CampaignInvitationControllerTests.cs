﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.WebApi.TenantControllers;
using Irms.Data.Read.Abstract;
using System.Linq;
using Irms.Data.Read.CampaignInvitation.ReadModels;
using System.Collections.Generic;
using Irms.Data.Read.CampaignInvitation.Queries;
using Irms.Application.Campaigns.Commands;
using System;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Domain.Entities;
using Irms.Data.Read.CampaignInvitation.QueryHandlers;
using Irms.Data.Read.Campaign.Queries;
using Irms.Data.Read.Campaign.ReadModels;
using Irms.Application.Campaigns.ReadModels;
using Irms.Application.Campaigns.Queries;
using Irms.Application.CampaignInvitations.CommandHandlers;

namespace Irms.Tests.Unit.Controller.AdminControllers
{
    [TestClass]
    public class CampaignInvitationControllerTests
    {
        /// <summary>
        /// can get campaign place holders list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignPlaceHolderList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = 1;
            var items = new List<CampaignPlaceholderListItem>
            {
                new CampaignPlaceholderListItem
                (
                    id,
                    "Sender name",
                    "{{sender_name}}"
                ),
                new CampaignPlaceholderListItem
                (
                    2,
                    "{{Rsvp link}}",
                    "{{rsvp_from_link}}"
                )
            };

            IEnumerable<CampaignPlaceholderListItem> list = new List<CampaignPlaceholderListItem>(items);

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignPlaceholderList>(), CancellationToken.None))
                .Returns((GetCampaignPlaceholderList q, CancellationToken t) => Task.FromResult(list));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.GetCampaignPlaceholders(
                new GetCampaignPlaceholderList(1, 10, string.Empty, Guid.NewGuid(), 0, false),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var campaign = result.First();

            Assert.AreEqual(campaign.Id, id);
            Assert.AreEqual(campaign.Title, "Sender name");
            Assert.AreEqual(campaign.Placeholder, "{{sender_name}}");
            Assert.AreEqual(result.Skip(1).First().Placeholder, "{{rsvp_from_link}}");
        }

        /// <summary>
        /// get invitation rsvp
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignInvitationRsvp()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var campaignInfo = new Data.Read.CampaignInvitation.ReadModels.CampaignInvitation
            {
                Id = id,
                IsInstant = true,
                StartDate = DateTime.UtcNow
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignInvitation>(), CancellationToken.None))
                    .Returns((GetCampaignInvitation q, CancellationToken t) => Task.FromResult(campaignInfo));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.Get(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.IsInstant, true);
        }
        /// <summary>
        /// can create invitation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateCampaignInvitation()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateOrUpdateCampaignInvitationCmd>(), CancellationToken.None))
                .Returns((CreateOrUpdateCampaignInvitationCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var apiResult = await sut.CreateOrUpdate(
                new CreateOrUpdateCampaignInvitationCmd
                {
                    CampaignId = id,
                    IsInstant = false,
                    StartDate = DateTime.UtcNow
                },
                CancellationToken.None); ;

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// can create or update email template specific to an invitation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateOrUpdateCampaignEmailTemplate()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateOrUpdateCampaignEmailTemplateCmd>(), CancellationToken.None))
                .Returns((CreateOrUpdateCampaignEmailTemplateCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var apiResult = await sut.CreateOrUpdateCampaignEmailTemplate(
                new CreateOrUpdateCampaignEmailTemplateCmd
                {
                    CampaignInvitationId = id,
                    Subject = "TestSubject",
                    Preheader = "PreHeader",
                    SenderEmail = "sender@email.com",
                    Body = "template_body",
                    AcceptHtml = "accept_html",
                    RejectHtml = "reject_html",
                    ThemeJson = "theme_json",
                    BackgroundImagePath = "abc.png"
                },
                CancellationToken.None); ;

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// can update campain email response
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateCampaignEmailResponseForm()
        {
            //Arrange
            var mediator = new Mock<IMediator>();

            mediator
                .Setup(x => x.Send(It.IsAny<UpdateCampaignEmailResponseFormCmd>(), CancellationToken.None))
                .Returns((UpdateCampaignEmailResponseFormCmd q, CancellationToken t) => Task.FromResult(MediatR.Unit.Value));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var apiResult = await sut.UpdateCampaignEmailResponseForm(
                new UpdateCampaignEmailResponseFormCmd
                {
                    Id = Guid.NewGuid(),
                    AcceptHtml = "accept_html",
                    ThemeJson = "theme_json",
                    RejectHtml = "reject_html",
                    BackgroundImage = "reject_html",
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, MediatR.Unit.Value);
        }

        /// <summary>
        /// can send test email
        /// </summary>
        /// <returns></returns>

        [TestMethod]
        public async Task CanSendTestEmail()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<SendTestEmailCmd>(), CancellationToken.None))
                .Returns((SendTestEmailCmd q, CancellationToken t) => Task.FromResult(true));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var apiResult = await sut.SendTestEmail(
                new SendTestEmailCmd
                {
                    Id = id
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, true);
        }

        /// <summary>
        /// can get email template
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignEmailTemplate()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var emailTemplate = new Data.Read.CampaignInvitation.ReadModels.CampaignEmailTemplate
            {
                Id = id,
                Subject = "TestSubject",
                Preheader = "PreHeader",
                SenderEmail = "sender@email.com",
                Body = "template_body",
                AcceptHtml = "accept_html",
                RejectHtml = "reject_html",
                ThemeJson = "theme_json",
                BackgroundImagePath = "abc.png"
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignEmailTemplate>(), CancellationToken.None))
                    .Returns((GetCampaignEmailTemplate q, CancellationToken t) => Task.FromResult(emailTemplate));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.GetCampaignEmailTemplate(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Subject, "TestSubject");
            Assert.AreEqual(result.Preheader, "PreHeader");
            Assert.AreEqual(result.SenderEmail, "sender@email.com");
            Assert.AreEqual(result.Body, "template_body");
            Assert.AreEqual(result.AcceptHtml, "accept_html");
            Assert.AreEqual(result.RejectHtml, "reject_html");
            Assert.AreEqual(result.ThemeJson, "theme_json");
            Assert.AreEqual(result.BackgroundImagePath, "abc.png");
        }

        /// <summary>
        /// checks if it can load sms senders name. Would be changed in future. Currently checks if the result is not null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanLoadAvailableSmsTemplateNames()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            IReadOnlyCollection<string> names = new string[] { "123" };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignSmsTemplateSenders>(), CancellationToken.None))
                    .Returns((GetCampaignSmsTemplateSenders q, CancellationToken t) => Task.FromResult(names));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.LoadAvailableSmsTemplateNames(CancellationToken.None);

            //Asser
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), names.Count());
        }

        /// <summary>
        /// checks if it can send test sms, checks the result to be not null and has the same amount of numbers as on input
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanSendTestSms()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            IEnumerable<string> phones = new string[] { "+966556565656" };

            mediator
                .Setup(x => x.Send(It.IsAny<SendTestSmsCmd>(), CancellationToken.None))
                    .Returns((SendTestSmsCmd q, CancellationToken t) => Task.FromResult(phones));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.SendTestSms(new SendTestSmsCmd
            {
                InvitationId = id,
                SmsList = phones
            }, CancellationToken.None);

            //Asser
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), phones.Count());
        }

        /// <summary>
        /// can create or update sms template specific to an invitation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateOrUpdateCampaignSmsTemplate()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateOrUpdateCampaignSmsTemplateCmd>(), CancellationToken.None))
                .Returns((CreateOrUpdateCampaignSmsTemplateCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var apiResult = await sut.CreateOrUpdateCampaignSmsTemplate(
                new CreateOrUpdateCampaignSmsTemplateCmd
                {
                    CampaignInvitationId = id,
                    ProceedButtonText = "proceed_text",
                    RejectButtonText = "reject_text",
                    RSVPHtml = "rsvp_html",
                    SenderName = "sender_name",
                    WelcomeHtml = "welcome_html",
                    Body = "template_body",
                    AcceptHtml = "accept_html",
                    RejectHtml = "reject_html",
                    ThemeJson = "theme_json",
                    BackgroundImagePath = "abc.png"
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }


        /// <summary>
        /// can get sms template
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignSmsTemplate()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var emailTemplate = new Data.Read.CampaignInvitation.ReadModels.CampaignSmsTemplate
            {
                Id = id,
                CampaignInvitationId = id,
                AcceptButtonText = "accept_text",
                ProceedButtonText = "proceed_text",
                RejectButtonText = "reject_text",
                RSVPHtml = "rsvp_html",
                SenderName = "sender_name",
                WelcomeHtml = "welcome_html",
                Body = "template_body",
                AcceptHtml = "accept_html",
                RejectHtml = "reject_html",
                ThemeJson = "theme_json",
                BackgroundImagePath = "abc.png"
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignSmsTemplate>(), CancellationToken.None))
                    .Returns((GetCampaignSmsTemplate q, CancellationToken t) => Task.FromResult(emailTemplate));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.GetCampaignSmsTemplate(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.CampaignInvitationId, id);
            Assert.AreEqual(result.AcceptButtonText, "accept_text");
            Assert.AreEqual(result.ProceedButtonText, "proceed_text");
            Assert.AreEqual(result.RejectButtonText, "reject_text");
            Assert.AreEqual(result.RSVPHtml, "rsvp_html");
            Assert.AreEqual(result.SenderName, "sender_name");
            Assert.AreEqual(result.WelcomeHtml, "welcome_html");
            Assert.AreEqual(result.Body, "template_body");
            Assert.AreEqual(result.AcceptHtml, "accept_html");
            Assert.AreEqual(result.RejectHtml, "reject_html");
            Assert.AreEqual(result.ThemeJson, "theme_json");
            Assert.AreEqual(result.BackgroundImagePath, "abc.png");
        }


        //invitation pending...

        /// <summary>
        /// unit test for get invitation list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignInvitationPendingList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();
            var items = new[]
            {
                new CampaignInvitationListItem
                {
                   Id = id,
                   Title = "Reminder_one",
                   SortOrder = 1

                },
                new CampaignInvitationListItem
                {
                   Id = id,
                   Title = "Reminder_two",
                   SortOrder = 2
                }
            };

            IEnumerable<CampaignInvitationListItem> list = new List<CampaignInvitationListItem>(items);

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignInvitationList>(), CancellationToken.None))
                .Returns((GetCampaignInvitationList q, CancellationToken t) => Task.FromResult(list));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.GetCampaignInvitationPendingList(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var ip = result.First();

            Assert.IsNotNull(result);
            Assert.AreEqual(ip.Id, id);
            Assert.AreEqual(ip.Title, "Reminder_one");
            Assert.AreEqual(ip.SortOrder, 1);
            Assert.AreEqual(result.Skip(1).First().Title, "Reminder_two");
        }

        /// <summary>
        /// can get campaign invitation pending
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignInvitationPending()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var invitationPending = new Data.Read.CampaignInvitation.ReadModels.CampaignInvitation
            {
                Id = id,
                Interval = 6,
                IntervalType = IntervalType.Hours,
                EmailSubject = "TestSubject",
                EmailSender = "test@email.com",
                Title = "123"
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignInvitation>(), CancellationToken.None))
                    .Returns((GetCampaignInvitation q, CancellationToken t) => Task.FromResult(invitationPending));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.GetCampaignInvitationPending(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Interval, 6);
            Assert.AreEqual(result.IntervalType, IntervalType.Hours);
            Assert.AreEqual(result.EmailSubject, "TestSubject");
            Assert.AreEqual(result.EmailSender, "test@email.com");
            Assert.AreEqual(result.Title, "123");
        }

        /// <summary>
        /// unit test for create/update campaign pending invitation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpsertCampaignInvitationPending()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<UpsertCampaignInvitationPendingCmd>(), CancellationToken.None))
                .Returns((UpsertCampaignInvitationPendingCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var apiResult = await sut.UpsertCampaignInvitationPending(
                new UpsertCampaignInvitationPendingCmd
                {
                    Id = id,
                    EventCampaignId = Guid.NewGuid(),
                    Interval = 1,
                    IntervalType = IntervalType.Days,
                    Title = "123"
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// unit test to get accepted list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignInvitationAcceptedList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();
            var items = new[]
            {
                new CampaignInvitationListItem
                {
                   Id = id,
                   Title = "Reminder_one",
                   SortOrder = 1

                },
                new CampaignInvitationListItem
                {
                   Id = id,
                   Title = "Reminder_two",
                   SortOrder = 2
                }
            };

            IEnumerable<CampaignInvitationListItem> list = new List<CampaignInvitationListItem>(items);

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignInvitationList>(), CancellationToken.None))
                .Returns((GetCampaignInvitationList q, CancellationToken t) => Task.FromResult(list));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.GetCampaignInvitationAcceptedList(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var ip = result.First();

            Assert.IsNotNull(result);
            Assert.AreEqual(ip.Id, id);
            Assert.AreEqual(ip.Title, "Reminder_one");
            Assert.AreEqual(ip.SortOrder, 1);
            Assert.AreEqual(result.Skip(1).First().Title, "Reminder_two");
        }

        /// <summary>
        /// unit test to get campaign invitation accepted detail
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignInvitationAccepted()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var invitation = new Data.Read.CampaignInvitation.ReadModels.CampaignInvitation
            {
                Id = id,
                Interval = 6,
                IntervalType = IntervalType.Hours,
                EmailSubject = "TestSubject",
                EmailSender = "test@email.com",
                Title = "123"
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignInvitation>(), CancellationToken.None))
                    .Returns((GetCampaignInvitation q, CancellationToken t) => Task.FromResult(invitation));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.GetCampaignInvitationAccepted(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Interval, 6);
            Assert.AreEqual(result.IntervalType, IntervalType.Hours);
            Assert.AreEqual(result.EmailSubject, "TestSubject");
            Assert.AreEqual(result.EmailSender, "test@email.com");
            Assert.AreEqual(result.Title, "123");
        }

        /// <summary>
        /// unit test for create/update campaign accepted invitation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpsertCampaignInvitationAccepted()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<UpsertCampaignInvitationAcceptedCmd>(), CancellationToken.None))
                .Returns((UpsertCampaignInvitationAcceptedCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var apiResult = await sut.UpsertCampaignInvitationAccepted(
                new UpsertCampaignInvitationAcceptedCmd
                {
                    Id = id,
                    EventCampaignId = Guid.NewGuid(),
                    Interval = 1,
                    IntervalType = IntervalType.Days,
                    Title = "123"
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// unit test to get rejected list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignInvitationRejectedList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();
            var items = new[]
            {
                new CampaignInvitationListItem
                {
                   Id = id,
                   Title = "Reminder_one",
                   SortOrder = 1

                },
                new CampaignInvitationListItem
                {
                   Id = id,
                   Title = "Reminder_two",
                   SortOrder = 2
                }
            };

            IEnumerable<CampaignInvitationListItem> list = new List<CampaignInvitationListItem>(items);

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignInvitationList>(), CancellationToken.None))
                .Returns((GetCampaignInvitationList q, CancellationToken t) => Task.FromResult(list));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.GetCampaignInvitationRejectedList(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var ip = result.First();

            Assert.IsNotNull(result);
            Assert.AreEqual(ip.Id, id);
            Assert.AreEqual(ip.Title, "Reminder_one");
            Assert.AreEqual(ip.SortOrder, 1);
            Assert.AreEqual(result.Skip(1).First().Title, "Reminder_two");
        }

        /// <summary>
        /// unit test to get campaign invitation rejected detail
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignInvitationRejected()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var invitation = new Data.Read.CampaignInvitation.ReadModels.CampaignInvitation
            {
                Id = id,
                Interval = 6,
                IntervalType = IntervalType.Hours,
                EmailSubject = "TestSubject",
                EmailSender = "test@email.com",
                Title = "123"
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignInvitation>(), CancellationToken.None))
                    .Returns((GetCampaignInvitation q, CancellationToken t) => Task.FromResult(invitation));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.GetCampaignInvitationRejected(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Interval, 6);
            Assert.AreEqual(result.IntervalType, IntervalType.Hours);
            Assert.AreEqual(result.EmailSubject, "TestSubject");
            Assert.AreEqual(result.EmailSender, "test@email.com");
            Assert.AreEqual(result.Title, "123");
        }

        /// <summary>
        /// get list analysis template
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignInvitationListAnalysis()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var invitation = new Data.Read.CampaignInvitation.ReadModels.CampaignInvitation
            {
                Id = id,
                Interval = 6,
                IntervalType = IntervalType.Hours,
                EmailSubject = "TestSubject",
                EmailSender = "test@email.com",
                Title = "123"
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignInvitation>(), CancellationToken.None))
                    .Returns((GetCampaignInvitation q, CancellationToken t) => Task.FromResult(invitation));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.GetCampaignInvitationListAnalysis(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Interval, 6);
            Assert.AreEqual(result.IntervalType, IntervalType.Hours);
            Assert.AreEqual(result.EmailSubject, "TestSubject");
            Assert.AreEqual(result.EmailSender, "test@email.com");
            Assert.AreEqual(result.Title, "123");
        }

        /// <summary>
        /// unit test for create/update campaign accepted invitation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpsertCampaignInvitationRejected()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<UpsertCampaignInvitationRejectedCmd>(), CancellationToken.None))
                .Returns((UpsertCampaignInvitationRejectedCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var apiResult = await sut.UpsertCampaignInvitationRejected(
                new UpsertCampaignInvitationRejectedCmd
                {
                    Id = id,
                    EventCampaignId = Guid.NewGuid(),
                    Interval = 1,
                    IntervalType = IntervalType.Days,
                    Title = "123"
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// unit test for create/update campaign accepted invitation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDuplicateInvitation()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<DuplicateInvitationCmd>(), CancellationToken.None))
                .Returns((DuplicateInvitationCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var apiResult = await sut.DuplicateInvitation(
                new DuplicateInvitationCmd(id),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// unit test for removing invitation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanRemoveInvitation()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<DeleteInvitationCmd>(), CancellationToken.None))
                .Returns((DeleteInvitationCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var apiResult = await sut.DeleteInvitation(
                new DeleteInvitationCmd { Id = id },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// unit test for removing whatsapp template
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanRemoveWhatsappInvitation()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<DeleteWhatsappTemplateCmd>(), CancellationToken.None))
                .Returns((DeleteWhatsappTemplateCmd q, CancellationToken t) => Task.FromResult(MediatR.Unit.Value));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var apiResult = await sut.DeleteWhatsappTemplate(
                new DeleteWhatsappTemplateCmd { InvitationId = id },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, MediatR.Unit.Value);
        }

        /// <summary>
        /// unit test to get basic compaign invitation ifno
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetBasicCompaignInvitationInfo()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var ci = new CampaignInvitationBasicInfo
            {
                Id = id,
                InvitationType = InvitationType.Accepted
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignInvitationBasicInfo>(), CancellationToken.None))
                .Returns((GetCampaignInvitationBasicInfo q, CancellationToken t) => Task.FromResult(ci));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.GetBasicInvitationInfo(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.InvitationType, InvitationType.Accepted);
        }

        [TestMethod]
        public async Task CanGetCampaignSummary()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var campaignSummary = new CampaignSummary
            {
                Stats = new CampaignSummary.CampaignSummaryStats
                {
                    EmailCount = 1,
                    GuestCount = 2,
                    SendTime = 3,
                    SmsCount = 4,
                    WhatsAppCount = 5
                }
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignSummaryQuery>(), CancellationToken.None))
                .Returns((GetCampaignSummaryQuery q, CancellationToken t) => Task.FromResult(campaignSummary));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.GetCampaignSummary(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(campaignSummary.Stats.EmailCount, result.Stats.EmailCount);
            Assert.AreEqual(campaignSummary.Stats.GuestCount, result.Stats.GuestCount);
            Assert.AreEqual(campaignSummary.Stats.SendTime, result.Stats.SendTime);
            Assert.AreEqual(campaignSummary.Stats.SmsCount, result.Stats.SmsCount);
            Assert.AreEqual(campaignSummary.Stats.WhatsAppCount, result.Stats.WhatsAppCount);
        }

        [TestMethod]
        public async Task CanCreateOrUpdateWhatsappTemplate()
        {
            // Arrange
            var mediator = new Mock<IMediator>();
            var cmd = new CreateOrUpdateWhatsappTemplateCmd();

            var res = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateOrUpdateWhatsappTemplateCmd>(), CancellationToken.None))
                .Returns((CreateOrUpdateWhatsappTemplateCmd q, CancellationToken t) => Task.FromResult(res));

            var sut = new CampaignInvitationController(mediator.Object);
            // Act
            try
            {
                await sut.CreateOrUpdateCampaignWhatsappTemplate(
                    cmd,
                    CancellationToken.None);
            }
            catch (Exception ex)
            {
                //Assert
                Assert.Fail("Expected no exception, got: ", ex.Message);
            }
        }

        /// <summary>
        /// unit test to create or update whatsapp BOT template
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateOrUpdateWhatsappBOTTemplate()
        {
            // Arrange
            var mediator = new Mock<IMediator>();
            var cmd = new CreateOrUpdateWhatsappBOTTemplateCmd();

            var res = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateOrUpdateWhatsappBOTTemplateCmd>(), CancellationToken.None))
                .Returns((CreateOrUpdateWhatsappBOTTemplateCmd q, CancellationToken t) => Task.FromResult(res));

            var sut = new CampaignInvitationController(mediator.Object);
            // Act
            try
            {
                await sut.CreateOrUpdateWhatsappBOTTemplate(
                    cmd,
                    CancellationToken.None);
            }
            catch (Exception ex)
            {
                //Assert
                Assert.Fail("Expected no exception, got: ", ex.Message);
            }
        }

        /// <summary>
        /// unit test to create or update whatsapp response form
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateOrUpdateWhatsappResponseForm()
        {
            // Arrange
            var mediator = new Mock<IMediator>();
            var cmd = new UpdateCampaignWhatsappResponseFormCmd();

            var res = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<UpdateCampaignWhatsappResponseFormCmd>(), CancellationToken.None));

            var sut = new CampaignInvitationController(mediator.Object);
            // Act
            try
            {
                await sut.CreateOrUpdateWhatsappResponseForm(
                    cmd,
                    CancellationToken.None);
            }
            catch (Exception ex)
            {
                //Assert
                Assert.Fail("Expected no exception, got: ", ex.Message);
            }
        }

        [TestMethod]
        public async Task CanLoadWhatsappTemplate()
        {
            // Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var res = new Data.Read.CampaignInvitation.ReadModels.CampaignWhatsappTemplate
            {
                AcceptButtonText = "accept",
                AcceptHtml = "accepthtml",
                BackgroundImagePath = "path",
                Body = "body",
                CampaignInvitationId = Guid.NewGuid(),
                Id = id,
                ProceedButtonText = "proceed",
                RejectButtonText = "reject",
                RejectHtml = "reject",
                Rsvphtml = "rsvp",
                SenderName = "sender_name",
                TemplateId = Guid.NewGuid(),
                TenantId = Guid.NewGuid(),
                ThemeJson = "{}",
                WelcomeHtml = "welcome"
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignWhatsappTemplateQuery>(), CancellationToken.None))
                .Returns((GetCampaignWhatsappTemplateQuery q, CancellationToken t) => Task.FromResult(res));

            var sut = new CampaignInvitationController(mediator.Object);
            // Act
            var result = await sut.LoadWhatsappTemplate(
                     id,
                     CancellationToken.None);


            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(res.AcceptButtonText, result.AcceptButtonText);
            Assert.AreEqual(res.AcceptHtml, result.AcceptHtml);
            Assert.AreEqual(res.BackgroundImagePath, result.BackgroundImagePath);
            Assert.AreEqual(res.Body, result.Body);
            Assert.AreEqual(res.CampaignInvitationId, result.CampaignInvitationId);
            Assert.AreEqual(res.Id, result.Id);
            Assert.AreEqual(res.ProceedButtonText, result.ProceedButtonText);
            Assert.AreEqual(res.RejectButtonText, result.RejectButtonText);
            Assert.AreEqual(res.RejectHtml, result.RejectHtml);
            Assert.AreEqual(res.Rsvphtml, result.Rsvphtml);
            Assert.AreEqual(res.SenderName, result.SenderName);
            Assert.AreEqual(res.TemplateId, result.TemplateId);
            Assert.AreEqual(res.TenantId, result.TenantId);
            Assert.AreEqual(res.ThemeJson, result.ThemeJson);
            Assert.AreEqual(res.WelcomeHtml, result.WelcomeHtml);
        }

        /// <summary>
        /// can load whatspp bot template
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanLoadWhatsappBOTTemplate()
        {
            // Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var res = new Data.Read.CampaignInvitation.ReadModels.CampaignWhatsappBOTTemplate
            {
                DefaultResponse = string.Empty,
                AcceptCode = "2I8",
                RejectCode = "987"
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignWhatsappBOTTemplateQuery>(), CancellationToken.None))
                .Returns((GetCampaignWhatsappBOTTemplateQuery q, CancellationToken t) => Task.FromResult(res));

            var sut = new CampaignInvitationController(mediator.Object);
            // Act
            var result = await sut.LoadWhatsappBOTTemplate(
                     id,
                     CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(res.DefaultResponse, result.DefaultResponse);
            Assert.AreEqual(res.Id, result.Id);
            Assert.AreEqual(res.AcceptCode, result.AcceptCode);
            Assert.AreEqual(res.RejectCode, result.RejectCode);
        }

        /// <summary>
        /// checks if it can send test whatsapp, checks the result to be not null and has the same amount of numbers as on input
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanSendTestWhatsapp()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            IEnumerable<string> phones = new string[] { "+966556565656" };

            mediator
                .Setup(x => x.Send(It.IsAny<SendTestWhatsappCmd>(), CancellationToken.None))
                    .Returns((SendTestWhatsappCmd q, CancellationToken t) => Task.FromResult(phones));

            var sut = new CampaignInvitationController(mediator.Object);

            //Act
            var result = await sut.SendTestWhatsapp(new SendTestWhatsappCmd
            {
                InvitationId = id,
                WhatsappList = phones
            }, CancellationToken.None);

            //Asser
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), phones.Count());
        }
    }
}
