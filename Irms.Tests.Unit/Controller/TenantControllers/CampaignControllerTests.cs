﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Application.Campaigns.Commands;
using Irms.WebApi.TenantControllers;
using Irms.Data.Read.Campaign.Queries;
using Irms.Data.Read.Campaign.ReadModels;
using Irms.Application.Campaigns.ReadModels;
using Irms.Data.Read.Abstract;
using System.Collections.Generic;
using System.Linq;
using Irms.Application.CampaignInvitations.ReadModels;

namespace Irms.Tests.Unit.Controller.AdminControllers
{
    [TestClass]
    public class CampaignControllerTests
    {
        /// <summary>
        /// can get campaign list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();
            var items = new[]
            {
                new CampaignListItem
                (
                    id,
                    "campaignone",
                    0,
                    100,
                    DateTime.UtcNow
                ),
                new CampaignListItem
                (
                    Guid.NewGuid(),
                    "campaigntwo",
                    1,
                    25,
                    DateTime.UtcNow
                )
            };

            IPageResult<CampaignListItem> list = new PageResult<CampaignListItem>(items, 10);

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignList>(), CancellationToken.None))
                .Returns((GetCampaignList q, CancellationToken t) => Task.FromResult(list));

            var sut = new CampaignController(mediator.Object);

            //Act
            var result = await sut.GetCampaignList(
                new GetCampaignList(1, 10, string.Empty, Guid.NewGuid()),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var campaign = result.Items.First();

            Assert.AreEqual(result.TotalCount, 10);
            Assert.AreEqual(campaign.Id, id);
            Assert.AreEqual(campaign.Name, "campaignone");
            Assert.AreEqual(campaign.Status, Domain.Entities.CampaignStatus.Draft);
            Assert.AreEqual(result.Items.Skip(1).First().Name, "campaigntwo");
        }

        /// <summary>
        /// can get campaign info
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignInfo()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var campaignInfo = new CampaignInfo
            {
                Id = id,
                Name = "Event one",
                CampaignStatus = Domain.Entities.CampaignStatus.Draft,
                GuestListId = Guid.NewGuid()
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetCampaignInfo>(), CancellationToken.None))
                    .Returns((GetCampaignInfo q, CancellationToken t) => Task.FromResult(campaignInfo));

            var sut = new CampaignController(mediator.Object);

            //Act
            var result = await sut.GetCampaignInfo(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Name, "Event one");
            Assert.AreEqual(result.CampaignStatus, Domain.Entities.CampaignStatus.Draft);
        }

        /// <summary>
        /// check reachability
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCheckReachability()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var stats = new ReachabilityStats
            {
                EmailCount = 100,
                NotReachableCount = 10,
                SmsCount = 232,
                WhatsAppCount = 222
            };
            bool reachabilitySubscribed = true;

            mediator
                .Setup(x => x.Send(It.IsAny<CheckReachability>(), CancellationToken.None))
                    .Returns((CheckReachability q, CancellationToken t) => Task.FromResult((reachabilitySubscribed, stats)));

            var sut = new CampaignController(mediator.Object);

            //Act
            var result = await sut.CheckReachability(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.reachabilitySubscribed, true);
            Assert.AreEqual(result.stats.EmailCount, 100);
            Assert.AreEqual(result.stats.NotReachableCount, 10);
            Assert.AreEqual(result.stats.WhatsAppCount, 222);
            Assert.AreEqual(result.stats.SmsCount, 232);
        }

        /// <summary>
        /// can get preffered media statistics
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetPrefferedMediaStats()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var stats = new PrefferedMediaStats
            {
                EmailCount = 100,
                SmsCount = 232,
                WhatsAppCount = 222
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetPrefferedMediaStats>(), CancellationToken.None))
                    .Returns((GetPrefferedMediaStats q, CancellationToken t) => Task.FromResult(stats));

            var sut = new CampaignController(mediator.Object);

            //Act
            var result = await sut.GetPrefferedMediaStats(
                Guid.NewGuid(),
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.EmailCount, 100);
            Assert.AreEqual(result.WhatsAppCount, 222);
            Assert.AreEqual(result.SmsCount, 232);
        }

        /// <summary>
        /// user can create campaigns
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateCampaign()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateCampaignCmd>(), CancellationToken.None))
                .Returns((CreateCampaignCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new CampaignController(mediator.Object);

            //Act
            var apiResult = await sut.Create(
                new CreateCampaignCmd
                {
                    EventId = id
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// create list analysis campaign
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateListAnalysisCampaign()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var result = new ListAnalysisCampaignResponse
            {
                CampaignId = Guid.NewGuid(),
                CampaignInvitationId = Guid.NewGuid()
            };

            mediator
                .Setup(x => x.Send(It.IsAny<CreateListAnalysisCampaignCmd>(), CancellationToken.None))
                .Returns((CreateListAnalysisCampaignCmd q, CancellationToken t) => Task.FromResult(result));

            var sut = new CampaignController(mediator.Object);

            //Act
            var apiResult = await sut.CreateListAnalysisCampaign(
                new CreateListAnalysisCampaignCmd
                {
                    EventId = Guid.NewGuid(),
                    GuestListId = Guid.NewGuid()
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult.CampaignId, result.CampaignId);
            Assert.AreEqual(apiResult.CampaignInvitationId, result.CampaignInvitationId);
        }

        /// <summary>
        /// can update campaign
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateCampaign()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var response = new MediatR.Unit();

            mediator
                .Setup(x => x.Send(It.IsAny<UpdateCampaignCmd>(), CancellationToken.None))
                .Returns((UpdateCampaignCmd q, CancellationToken t) => MediatR.Unit.Task);

            var sut = new CampaignController(mediator.Object);

            //Act
            var apiResult = await sut.Update(
                new UpdateCampaignCmd
                {
                    Name = "Campaign one",
                    Id = id,
                    GuestListId = Guid.NewGuid(),
                    ExitCriteriaType = 1
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response, apiResult);
        }

        [TestMethod]
        public async Task CanUpdatePrefferedMedia()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var response = new MediatR.Unit();

            mediator
                .Setup(x => x.Send(It.IsAny<UpdateCampaignCmd>(), CancellationToken.None))
                .Returns((UpdateCampaignCmd q, CancellationToken t) => MediatR.Unit.Task);

            var sut = new CampaignController(mediator.Object);

            //Act
            var apiResult = await sut.UpdatePrefferedMedia(
                new UpdatePreferredMediaCmd
                {
                    CampaignId = id,
                    GuestPreferredMediaList = new List<GuestPreferredMedia>()
                    {
                        new GuestPreferredMedia
                        {
                            Id = Guid.NewGuid(),
                            Email = true,
                            Sms = false,
                            WhatsApp = false
                        }
                    }
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response, apiResult);
        }

        /// <summary>
        /// can delete event by id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteCampaignById()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var request = new DeleteCampaignCmd();
            request.Id = Guid.NewGuid();

            var response = new MediatR.Unit();

            mediator
                .Setup(x => x.Send(It.IsAny<DeleteCampaignCmd>(), CancellationToken.None))
                .Returns((DeleteCampaignCmd q, CancellationToken t) => Task.FromResult(response));

            var sut = new CampaignController(mediator.Object);
            //Act
            var apiResult = await sut.Delete(
                request,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response, apiResult);
        }

        /// <summary>
        /// can set test campaign
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanSendTestCampaign()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var request = new TestCampaignCmd
            {
                Id = Guid.NewGuid()
            };

            var response = new TestCampaignSendInfoModel
            {
                EmailsSend = 0,
                SmsSend = 0,
                Error = default
            };

            mediator
                .Setup(x => x.Send(It.IsAny<TestCampaignCmd>(), CancellationToken.None))
                .Returns((TestCampaignCmd q, CancellationToken t) => Task.FromResult(response));

            var sut = new CampaignController(mediator.Object);

            //Act
            var apiResult = await sut.TestCampaign(
                request);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response, apiResult);
        }

        //[TestMethod]
        //public async Task CanLoadMediaWithoutCheckingReachability()
        //{
        //    //Arrange
        //    var mediator = new Mock<IMediator>();

        //    IPageResult<GuestReachabilityListItem> response = new PageResult<GuestReachabilityListItem>(new List<GuestReachabilityListItem>
        //    {
        //        new GuestReachabilityListItem
        //        {
        //            Email =true,
        //            EmailChecked = true,
        //            FullName = "full Name",
        //            Id = Guid.NewGuid(),
        //            Sms = true,
        //            WhatsApp = true,
        //            SmsChecked = true,
        //            WhatsAppChecked = true
        //        },
        //    }, 1);

        //    mediator
        //        .Setup(x => x.Send(It.IsAny<GetGuestsReachabilityList>(), CancellationToken.None))
        //        .Returns((GetGuestsReachabilityList q, CancellationToken t) => Task.FromResult(response));

        //    var sut = new CampaignController(mediator.Object);

        //    //Act
        //    var request = new GetGuestsReachabilityList();
        //    var apiResult = await sut.GetContactsWithoutReachabilityList(
        //        request,e);

        //    //Assert
        //    Assert.IsNotNull(apiResult);
        //    Assert.AreEqual(response, apiResult);
        //}

        [TestMethod]
        public async Task CanLoadMediaCheckingReachability()
        {
            //Arrange
            var mediator = new Mock<IMediator>();

            IPageResult<GuestReachabilityListItem> response = new PageResult<GuestReachabilityListItem>(new List<GuestReachabilityListItem>
            {
                new GuestReachabilityListItem
                {
                    Email =true,
                    EmailChecked = true,
                    FullName = "full Name",
                    Id = Guid.NewGuid(),
                    Sms = true,
                    WhatsApp = true,
                    SmsChecked = true,
                    WhatsAppChecked = true,
                    WhatsAppOptedIn = false,
                },
            }, 1);

            mediator
                .Setup(x => x.Send(It.IsAny<GetGuestsReachabilityList>(), CancellationToken.None))
                .Returns((GetGuestsReachabilityList q, CancellationToken t) => Task.FromResult(response));

            var sut = new CampaignController(mediator.Object);

            //Act
            var request = new GetGuestsReachabilityList();
            var apiResult = await sut.GetContactsReachabilityList(
                request);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(response.Items.First().FullName, apiResult.page.Items.First().FullName);
            Assert.AreEqual(response.Items.First().WhatsAppOptedIn, apiResult.page.Items.First().WhatsAppOptedIn);
        }
    }
}
