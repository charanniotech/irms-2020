﻿//using Irms.Application.Abstract;
//using Irms.Application.Abstract.Repositories.Base;
//using Irms.Application.Abstract.Services;
//using Irms.Application.Abstract.Services.Notifications;
//using Irms.Application.Messages.Services;
//using Irms.Application.Templates.Queries;
//using Irms.Domain;
//using Irms.Domain.Entities;
//using Irms.Domain.Entities.Guest;
//using Irms.Infrastructure.Services;
//using Irms.Tests.Unit.DataBuilder.Event;
//using Irms.Tests.Unit.DataBuilder.Guest;
//using MediatR;
//using Microsoft.EntityFrameworkCore.Metadata.Builders;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace Irms.Tests.Unit.Notifications
//{
//    [TestClass]
//    public class BaseNotifierTests
//    {
//        private Mock<IMediator> _mediator;
//        private Mock<ISmsSender> _smsSender;
//        private Mock<IEmailSender> _emailSender;
//        private Mock<IPhoneNumberValidator> _phoneNumberValidator;
//        private Mock<IRepository<UserInfo, Guid>> _employeeRepository;
//        private Mock<IMessageTranslator> _translator;
//        private Mock<ICurrentLanguage> _language;

//        [TestInitialize]
//        public void Setup()
//        {
//            _mediator = new Mock<IMediator>();
//            _smsSender = new Mock<ISmsSender>();
//            _emailSender = new Mock<IEmailSender>();
//            _phoneNumberValidator = new Mock<IPhoneNumberValidator>();
//            _employeeRepository = new Mock<IRepository<UserInfo, Guid>>();
//            _translator = new Mock<IMessageTranslator>();
//            _translator.Setup(x => x.Translate(It.IsAny<string>(), It.IsAny<CultureInfo>()))
//                .Returns<string, CultureInfo>((s, c) => new string(s.Reverse().ToArray()));

//            _language = new Mock<ICurrentLanguage>();
//        }

//        [TestMethod]
//        public async Task SendEmail()
//        {
//            var template = new DataBuilder.Template.TemplateBuilder().Build();

//            _mediator.Setup(x => x.Send(It.IsAny<GetActiveEmailTemplate>(), It.IsAny<CancellationToken>()))
//                .Returns(Task.FromResult(template));

//            var sut = new ForgotPasswordNotifier(_mediator.Object, _smsSender.Object, _emailSender.Object, _language.Object, _phoneNumberValidator.Object);

//            var guestBuilder = new GuestBuilder();
//            guestBuilder.ContactInfo.Email = "aaa@test.com";

//            var guestList = guestBuilder.BuildList();

//            await sut.NotifyByEmail(guestList,
//                new EventBuilder(guestList).Build(),
//                CancellationToken.None);

//            _emailSender.Verify(x => x.SendEmail(It.Is<EmailMessage>(em => em.HtmlTemplate == template.EmailBody && em.Recipients.All(r => r.EmailAddress == "aaa@test.com")), It.IsAny<CancellationToken>()));
//        }

//        [TestMethod]
//        public async Task SendEmail_NoTemplate()
//        {
//            var sut = new ForgotPasswordNotifier(_mediator.Object, _smsSender.Object, _emailSender.Object, _language.Object, _phoneNumberValidator.Object);

//            await sut.NotifyByEmail(
//                new GuestBuilder().BuildList(),
//                new EventBuilder().Build(),
//                CancellationToken.None);

//            _emailSender.Verify(x => x.SendEmail(It.IsAny<EmailMessage>(), It.IsAny<CancellationToken>()), Times.Never);
//        }

//        [TestMethod]
//        public async Task SendSms()
//        {
//            var template = new DataBuilder.Template.TemplateBuilder().Build();

//            _mediator.Setup(x => x.Send(It.IsAny<GetActiveSmsTemplate>(), It.IsAny<CancellationToken>()))
//                .Returns(Task.FromResult(template));

//            _phoneNumberValidator.Setup(x => x.Validate(It.Is<string>(n => n == "123"), It.IsAny<string>()))
//                .Returns<string, string>((s1, s2) => (true, new ValidPhoneNumber(s1)));

//            var sut = new ForgotPasswordNotifier(_mediator.Object, _smsSender.Object, _emailSender.Object, _language.Object, _phoneNumberValidator.Object);

//            var guestBuilder = new GuestBuilder();
//            guestBuilder.ContactInfo.PhoneNo = "123";
//            var guestList = guestBuilder.BuildList();

//            await sut.NotifyBySms(guestList,
//                new EventBuilder(guestList).Build(),
//                CancellationToken.None);

//            _smsSender.Verify(x => x.SendSms(It.Is<SmsMessage>(em => em.MessageTemplate == template.SmsText && em.Recipients.All(r => r.Phone == "123")), It.IsAny<CancellationToken>()));
//        }

//        [TestMethod]
//        public async Task SendSms_InvalidPhones()
//        {
//            var template = new DataBuilder.Template.TemplateBuilder().Build();

//            _mediator.Setup(x => x.Send(It.IsAny<GetActiveSmsTemplate>(), It.IsAny<CancellationToken>()))
//                .Returns(Task.FromResult(template));

//            _phoneNumberValidator.Setup(x => x.Validate(It.Is<string>(n => n == "123"), It.IsAny<string>()))
//                .Returns<string, string>((s1, s2) => (true, new ValidPhoneNumber(s1)));

//            var sut = new ForgotPasswordNotifier(_mediator.Object, _smsSender.Object, _emailSender.Object, _language.Object, _phoneNumberValidator.Object);

//            var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() =>
//                sut.NotifyBySms(new GuestBuilder().BuildList(),
//                    new EventBuilder().Build(),
//                    CancellationToken.None));
//            Assert.AreEqual("Phone number format is invalid", ex.Message);
//        }
//    }
//}
